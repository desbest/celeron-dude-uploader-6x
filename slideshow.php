<?php
define ( 'ACCOUNT_PHP', 1 );
define ( 'NO_AUTH_CHECK', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/functions_userfiles.inc.php';
require_once 'includes/messages_view.inc.php';

$tpl_slideshow = new Template(TPL_DIR . 'tpl_slideshow.php');

$folder_id = (int)gpc('folder_id', 'G', 0);
$folder_key = gpc ( 'folder_key', 'G', '' );


// get folder info
$result = $mysqlDB->query2 ( "SELECT f.*, u.username FROM uploader_userfolders AS f LEFT JOIN uploader_users AS u USING(userid) WHERE folder_id=$folder_id LIMIT 1" );
if ( $result->rowCount() )
{
	$folder = $result->fetchAssoc();
	processFolder ( $folder, true );
}
else
{
	$tpl_message->set ( 'message', $lang_view['file_unavailable'] );
	$tpl_uploader->set ( 'content', $tpl_message, 1 );
	exit;
}

// check folder key
if ( $folder['folder_key'] != '' && strcasecmp ( $folder['folder_key'], $folder_key ) !== 0 )
{
	go_to ( UPLOADER_URL ); exit;
}

// is folder viewable? public?
if ( $USER['userid'] != $folder['userid'] && $folder['permission']['access'] == 'private' )
{
	if ( !$USER['logged_in'] )
	{
		$msg = parse($lang_commons['not_logged_in'], array('{login_url}'=>UPLOADER_URL.(MOD_REWRITE?'login?':'account.php?action=login&amp;').'return_url='.rawurlencode(current_page()), '{register_url}'=>UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')));
		$tpl_message->set ( 'message', $msg );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
		exit;
	}

	$relationship = compute_contact_relationship ( $folder['userid'], $USER['userid'] );
	$allowed = ( $folder['permission']['family'] && $relationship['is_family'] ) || ( $folder['permission']['friend'] && $relationship['is_friend'] );
	if ( !$allowed )
	{
		$tpl_message->set ( 'message', $lang_view['folder_unavailable'] );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
		exit;
	}
}

$back_url = $folder['userid'] == $USER['userid'] ? ( $_SERVER['HTTP_REFERER'] == $folder['browse_url'] ? $folder['browse_url'] : $folder['url'] ) : $folder['browse_url'];
$images = get_user_files_in_folder ( $folder['userid'], $folder['folder_id'], 0, 0, 'file_id', 'ASC', 'file_isimage=1' );
$count = count($images);

if ( !$count )
{
	$tpl_message->set ( 'message', $lang_view['no_images'] );
	$tpl_message->set ( 'back_url', $back_url );
	$tpl_uploader->set ( 'content', $tpl_message, true );
	exit;
}

for($i=0; $i<$count; ++$i)
{
	processFile($images[$i]);
}
$tpl_uploader->set ( 'page_title', parse ( $lang_view['title_slideshow'], '{folder_name}',  ( $folder['folder_name'] ) ) );
$tpl_slideshow->set ( 'back_url', $back_url );
$tpl_slideshow->setr('images', $images);
$tpl_uploader->set('content', $tpl_slideshow, 1);
?>