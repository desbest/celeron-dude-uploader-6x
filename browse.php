<?php
define ( 'NO_AUTH_CHECK', 1 );
define ( 'BROWSE_PHP', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/functions_userfiles.inc.php';

$uploader_view = $UPL['SETTINGS']['uploader_view'];

switch ( $action )
{
	case 'browse':
	{
		$tpl_browse = new Template ( TPL_DIR . 'tpl_user.php' );
		$tpl_browse->set ( 'action', $action );

		$files_per_page = 24;
		$sort = trim ( gpc ( 'sort', 'G', 'date_asc' ) );
		$current_page = abs ( (int)gpc ( 'page', 'G', 1 ) );

		$userid = abs ( (int)gpc ( 'userid', 'G', 0 ) );
		$username = gpc ( 'username', 'G', '' );
		$folder_id = gpc ( 'folder_id', 'G', 0 );
		$error = 'none';

		// get target user info
		if ( $username != '' )
			$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE username='%s' LIMIT 1", $mysqlDB->escape ( $username ) ) );
		else
			$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE userid=%d LIMIT 1", $userid ) );

		if ( !$mysqlDB->getRowCount() )
		{
			$tpl_message->set ( 'message', $lang_misc['user_not_found'] );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			exit;
		}

		// target user info
		$userinfo = $mysqlDB->getAssoc();
		processUser ( $userinfo );
		$userid = $userinfo['userid'];
		$username = $userinfo['username'];
		$mysqlDB->free();
		$userinfo['info_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'info/' . $userinfo['username'] : 'browse.php?action=info&amp;userid=' . $userinfo['userid'] );
		$userinfo['message_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'pm/' . $username : 'usercp.php?action=sendpm&userid=' . $userinfo['userid'] );

		// relationship between viewing user and owner
		$is_owner = ( $USER['logged_in'] && $userid == $USER['userid'] );
		$relationship = $USER['logged_in'] ? compute_contact_relationship ( $userid, $USER['userid'] ) : array ( 'is_contact' => 0, 'is_friend' => 0, 'is_family' => 0 );

		$tpl_browse->set ( 'user', $userinfo );

		if ( $folder_id <= 0 )
		{
			// no folder selected, list all viewable folders
			$user_folders = get_user_folders ( $userid, 0, true );
			$public_folders = array();
			$current_folder = array();
			$user_files = array();
			$count = count ( $user_folders );
			for ( $i = 0; $i < $count; ++$i )
			{
				$folder = $user_folders[$i];
				// check if folder is public or private and current viewing user has permission to browse
				$perm = get_folder_access_permission ( $folder );
				if ( !$is_owner && $perm['access'] == 'hidden' ) continue;
				$allowed = ( $perm['friend'] && $relationship['is_friend'] ) || ( $perm['family'] && $relationship['is_family'] );
				if ( $perm['access'] == 'public' || $allowed || $is_owner ) $public_folders[] = $folder;
			}
			$count = count ( $public_folders );
			for ( $i = 0; $i < $count; ++$i )
			{
				processFolder ( $public_folders[$i], true );
				if ( $public_folders[$i]['folder_id'] == $folder_id )
					$current_folder = $public_folders[$i];
			}

			// Any public folders?
			if ( !count ( $public_folders ) )
			{
				$tpl_message->set ( 'message', parse ( $lang_browse['no_public_folders'], '{user}', $userinfo['username'] ) );
				$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'members' : 'browse.php' ) );
				$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			}
			else
			{
				$tpl_browse->set ( 'action', 'list_folders' );
				$tpl_browse->set ( 'public_folders', $public_folders );
				$tpl_uploader->set ( 'content', $tpl_browse, true );
			}
		}
		else
		{
			// folder selected
			$folder_key = gpc ( 'folder_key', 'G', '' );
			$current_folder = get_user_folders ( $userid, $folder_id, true );
			processFolder ( $current_folder, true );
			// check folder key
			if ( $current_folder['folder_key'] != '' && strcasecmp ( $current_folder['folder_key'], $folder_key ) !== 0 )
			{
				go_to ( UPLOADER_URL ); exit;
			}

			// check folder permission
			$perm = get_folder_access_permission ( $current_folder );
			$allowed = ( $perm['friend'] && $relationship['is_friend'] ) || ( $perm['family'] && $relationship['is_family'] );
			if ( !$is_owner && $current_folder['permission']['access'] == 'private' && !$allowed )
			{
				$tpl_message->set ( 'message', $lang_browse['bad_folder'] );
				$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'members' : 'browse.php' ) );
				$tpl_uploader->setr ( 'content', $tpl_message, 1 );
				exit;
			}

			$total_pages = ceil ( $current_folder['files_count'] / $files_per_page );
			if ( $current_page < 1 ) $current_page = 1;
			if ( $current_page > $total_pages ) $current_page = $total_pages;
			$start_offset = ( $current_page - 1 ) * $files_per_page;

			$user_files = get_user_files_in_folder ( $userid, $folder_id, $start_offset, $files_per_page, 'file_id', 'asc' );
			$count = count ( $user_files );
			for ( $i = 0; $i < $count; ++$i )
				processFile ( $user_files[$i] );
			$base_url = UPLOADER_URL . ( MOD_REWRITE ? sprintf ( 'browse/%s/%u%s', encodeurlraw ( $current_folder['username'] ), $folder_id, ( $current_folder['folder_key'] == '' ? '' : '_' . $current_folder['folder_key'] ) ) : 'browse.php?action=browse&amp;userid=' . $userid . '&amp;folder_id=' . $folder_id . '&amp;folder_key=' . $current_folder['folder_key'] );

			$tpl_vars = array
			(
				'current_folder'	=> $current_folder,
				'user_files'		=> $user_files,
				'user'				=> $userinfo,
				'current_page'		=> $current_page,
				'next_page_url'		=> $base_url . ( MOD_REWRITE ? '/page' : '&amp;page=' ) . ( $current_page + 1 ),
				'prev_page_url' 	=> $base_url . ( MOD_REWRITE ? '/page' : '&amp;page=' ) . ( $current_page - 1 ),
				'per_page'			=> $files_per_page,
				'file_start'		=> $start_offset + 1,
				'file_end'			=> $start_offset + count ( $user_files ),
				'current_page'		=> $current_page,
				'total_pages'		=> $total_pages,
			);
			$tpl_browse->set ( 'action', 'browse' );
			$tpl_browse->set ( $tpl_vars );
			$tpl_uploader->set ( 'content', $tpl_browse, true );
		}
	}
	break;




	case 'info':
	{
		$tpl_userinfo = new Template ( TPL_DIR . 'tpl_user.php' );
		$tpl_userinfo->set ( 'action', $action );

		$userid = abs((int)gpc ( 'userid', 'G', 0 ));
		$username = gpc ( 'username', 'G', '' );

		if ( $userid ) $mysqlDB->query ( "SELECT * FROM uploader_users WHERE userid=$userid LIMIT 1;" );
		else $mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE username='%s' LIMIT 1;", $mysqlDB->escape ( $username ) ) );

		if ( $mysqlDB->getRowCount() )
		{
			$userinfo = $mysqlDB->getAssoc();
			processUser ( $userinfo, true );
			$username = $userinfo['username'];
			$userid = $userinfo['userid'];

			// contact?
			$userinfo['relationship'] = compute_contact_relationship ( $USER['userid'], $userid );

			// reverse relationship?
			$userinfo['reverse_relationship'] = compute_contact_relationship ( $userid, $USER['userid'] );

			// format user info
			$userinfo['reg_date_ago'] = number_format ( ( time() - $userinfo['reg_date'] ) / 86400, 1 );
			$userinfo['reg_date'] = date ( $UPL['CONFIGS']['TIME_FORMAT'], $userinfo['reg_date'] );
			$userinfo['last_login_ago'] = number_format ( ( time() - $userinfo['last_login_time'] ) / 86400, 1 );
			$userinfo['last_login'] = date ( $UPL['CONFIGS']['TIME_FORMAT'], $userinfo['last_login_time'] );
			$userinfo['browse_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'browse/' . $username : 'browse.php?action=browse&amp;userid=' . $userid );
			$userinfo['message_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'pm/' . $username : 'usercp.php?action=sendpm&userid=' . $userid );
			$tpl_userinfo->setr ( 'user', $userinfo );
			$tpl_userinfo->set ( 'back_url', previous_page(UPLOADER_URL.(MOD_REWRITE?'members':'browse.php')) );
			$tpl_uploader->set ( 'page_title', $lang_browse['title2'] );
			$tpl_uploader->setr ( 'content', $tpl_userinfo, 1 );
		}
		else
		{
			$tpl_message->set ( 'message', $lang_misc['user_invalid'] );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			exit;
		}
	}
	break;

	default:
	{
		if ( $UPL['SETTINGS']['browsing'] == 'none' )
		{
			$tpl_message->set ( 'message', $lang_browse['disabled'] );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			exit;
		}
		elseif ( $UPL['SETTINGS']['browsing'] == 'reg' && !$UPL['USER']['logged_in'] )
		{
			$tpl_message->set ( 'message', $lang_browse['login_required'] );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			exit;
		}

		$tpl_browse_users = new Template ( TPL_DIR .  'tpl_browse.php' );
		$public_only = gpc ( 'public', 'G', 0 );
		$current_page = abs ( intval ( gpc ( 'page', 'G', 1 ) ) );

		$where = $public_only ? 'WHERE fl_has_public > 0' : '';

		// get total users
		$total_users = 0;
		$all_users = array ( );
		if ( !$mysqlDB->query ( "SELECT COUNT(userid) AS total_users FROM uploader_users $where" ) ) exit ( $mysqlDB->error ( __FILE__, __LINE__ ) );
		if ( $mysqlDB->getRowCount() )
		{
			$result = $mysqlDB->getAssoc();
			$total_users = $result['total_users'];
			$mysqlDB->free();
		}
		$number_of_pages = ceil ( $total_users / $UPL['CONFIGS']['USERLIST_PERPAGE'] );

		if ( $current_page > $number_of_pages || $current_page < 1 )
		{
			$current_page = 1;
		}
		$start = ($current_page-1)*$UPL['CONFIGS']['USERLIST_PERPAGE'];
		$end = $UPL['CONFIGS']['USERLIST_PERPAGE'];

		if ( !$mysqlDB->query ( "SELECT userid,username,reg_date,email,pref_show_email, fl_has_public AS has_public, pref_accepts_pm FROM uploader_users $where ORDER BY username LIMIT $start, $end;" ) ) exit ( $mysqlDB->error ( __FILE__, __LINE__ ) );

		if ( $mysqlDB->getRowCount() )
		{
			while ( false !== ( $user = $mysqlDB->getAssoc() ) )
			{
				processUser($user, false);
				$user['reg_date'] = date ( $UPL['CONFIGS']['TIME_FORMAT3'], $user['reg_date'] );
				$user['email'] = $user['pref_show_email'] ? $user['email'] :  '';
				$user_list [] = $user;
			}
			$mysqlDB->free();
		}

		// pages links
		$page_links = array ( );
		if ( $total_users )
		{
			for ( $i = 1; $i <= $number_of_pages; ++$i ) $page_links [] = array ( 'url' => MOD_REWRITE ? UPLOADER_URL . 'members/' . ( $public_only ? 'public/' : '' ) . 'page' . $i : 'browse.php?page=' . $i . ( $public_only ? '&amp;public=1' : '' ), 'number' => $i );
			if ( $current_page != $number_of_pages ) $page_links [] = array ( 'url' => MOD_REWRITE ? UPLOADER_URL . 'members/' . ( $public_only ? 'public/' : '' ) . 'page' . ($current_page+1) : 'browse.php?page=' . ( $current_page + 1 ) . ( $public_only ? '&amp;public=1' : '' ), 'number' => 'NEXT' );
			if ( $current_page != 1 ) array_unshift ( $page_links, array ( 'url' => MOD_REWRITE ? UPLOADER_URL . 'members/' . ( $public_only ? 'public/' : '' ) . 'page' . ($current_page-1) : 'browse.php?page=' . ( $current_page - 1 ) . ( $public_only ? '&amp;public=1' : '' ), 'number' => 'PREV' ) );
		}

		$tpl_browse_users->set ( 'public_only', $public_only );
		$tpl_browse_users->set ( 'pages', $page_links );
		$tpl_browse_users->set ( 'current_page', $current_page );
		$tpl_browse_users->set ( 'total_users', $total_users );
		$tpl_browse_users->setr ( 'users', $user_list );
		$tpl_uploader->set ( 'page_title', $lang_browse['title1'] );
		$tpl_uploader->setr ( 'content', $tpl_browse_users, 1 );
	}
	break;
}
?>