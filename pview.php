<?php
define ( 'NO_PERSISTENT', 0 );
define ( 'NO_AUTH_CHECK', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_upload.inc.php';
require_once 'includes/pub_settings.inc.php';


if ( $action == 'full' )
{
	$file_id = (int)gpc ( 'file_id', 'G', 0 );

	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_pfiles WHERE file_id=$file_id LIMIT 1" );
	if ( !$result->isGood() ) exit ( $mysqlDB->error() );
	if ( $result->rowCount() )
	{
		$file = array();
		$upload_set = array();
		$previous_file = array();
		$next_file = array();

		// get file
		$file = $result->fetchAssoc();
		processPublicFile ( $file );
		$result->free();

		// get upload set
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id={$file['upload_id']} LIMIT 1" );
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		// is file really an image?
		if(!$file['file_isimage'])exit(go_to($file['url']));

		// output
		$tpl_view = new Template ( TPL_DIR . 'tpl_public_view_image_full.php' );
		$tpl_view->setr ( 'file', $file );
		$tpl_view->setr ( 'upload_set', $upload_set );
		$tpl_view->set ( 'page_title', $file['file_name'] );
		$tpl_view->display();
	}
	else
	{
		$tpl_message->set ( 'message', $lang_public['file_not_found'] );
		$tpl_uploader->set ( 'page_title', $lang_public['title_file_not_found'] );
		$tpl_uploader->set ( 'content', $tpl_message, true );
		exit;
	}
}
else
{
	$file_id = (int)gpc ( 'file_id', 'G', 0 );

	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_pfiles WHERE file_id=$file_id LIMIT 1" );
	if ( !$result->isGood() ) exit ( $mysqlDB->error() );
	if ( $result->rowCount() )
	{
		$file = array();
		$upload_set = array();
		$previous_file = array();
		$next_file = array();

		// get file
		$file = $result->fetchAssoc();
		processPublicFile ( $file );
		$result->free();

		// get upload set
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id={$file['upload_id']} LIMIT 1" );
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		// is file really an image?
		if(!$file['file_isimage'])exit(go_to($file['url']));

		// get total images in this set
		$upload_set['total_images'] = 0;
		$mysqlDB->query("SELECT COUNT(file_id) AS total_images FROM uploader_pfiles WHERE upload_id={$file['upload_id']} AND file_isimage=1");
		if($mysqlDB->getRowCount())
		{
			$result = $mysqlDB->getAssoc();
			$upload_set['total_images'] = $result['total_images'];
		}

		// get position
		$file['position'] = 0;
		$mysqlDB->query("SELECT COUNT(file_id) AS position FROM uploader_pfiles WHERE upload_id={$file['upload_id']} AND file_isimage=1 AND file_id<{$file['file_id']}");
		if($mysqlDB->getRowCount())
		{
			$result = $mysqlDB->getAssoc();
			$file['position'] = $result['position']+1;
		}

		// previous
		$mysqlDB->query("SELECT * FROM uploader_pfiles WHERE file_id < {$file['file_id']} AND upload_id={$upload_set['upload_id']} AND file_isimage=1 ORDER BY file_id DESC LIMIT 1");
		if($mysqlDB->getRowCount())
		{
			$previous_file = $mysqlDB->getAssoc();
			processPublicFile($previous_file);
		}
		// next
		$mysqlDB->query("SELECT * FROM uploader_pfiles WHERE file_id > {$file['file_id']} AND upload_id={$upload_set['upload_id']} AND file_isimage=1 ORDER BY file_id ASC LIMIT 1");
		if($mysqlDB->getRowCount())
		{
			$next_file = $mysqlDB->getAssoc();
			processPublicFile($next_file);
		}

		// output
		$tpl_view = new Template ( TPL_DIR . 'tpl_public_view_image.php' );
		$tpl_view->setr ( 'file', $file );
		$tpl_view->setr ( 'upload_set', $upload_set );
		$tpl_view->setr ( 'previous_file', $previous_file );
		$tpl_view->setr ( 'next_file', $next_file );
		$tpl_view->set ( 'page_title', $file['file_name'] );
		$tpl_view->display();
	}
	else
	{
		$tpl_message->set ( 'message', $lang_public['file_not_found'] );
		$tpl_uploader->set ( 'page_title', $lang_public['title_file_not_found'] );
		$tpl_uploader->set ( 'content', $tpl_message, true );
		exit;
	}
}
?>