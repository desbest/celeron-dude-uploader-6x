<?php
// Configs
$chunk=16480;


// End configs
@set_time_limit(0);
@ignore_user_abort(true);
@set_magic_quotes_runtime(0);
require'includes/functions_mime.inc.php';
require'includes/mysql.class.php';
require'includes/configs.inc.php';
require'includes/constants.inc.php';
function out($f){header('Content-type: image/gif');@readfile($f);exit;}
extract($UPL['MYSQL'],EXTR_OVERWRITE);
$M=new mysqlDB($host,$username,$password,$database,0);
$file_id=isset($_GET['file_id'])?(int)$_GET['file_id']:0;
$file_key=isset($_GET['file_key'])?trim($_GET['file_key']):'';
$action=isset($_GET['action'])?$_GET['action']:'';
$thumb_size=isset($_GET['size'])?$_GET['size']:'';
$update_view=isset($_GET['update_view'])?(bool)$_GET['update_view']:false;
// Get file and user info
$r=$M->query("SELECT t1.*, folder_permission, folder_ispublic, bw_reset_last, bw_reset_period, bw_reset_auto, bw_used, bw_max, bw_xfer_rate "
. "FROM uploader_userfiles AS t1 INNER JOIN uploader_userfolders AS t2 "
. "USING(folder_id) INNER JOIN uploader_users AS t3 ON(t2.userid=t3.userid) WHERE file_id=$file_id");
if(!$r)exit($M->error(__LINE__,'gf'));
if(!$M->getRowCount())out('data/file_not_found.gif');
$file=$M->getAssoc();
$M->free();

// check file key
if(strcasecmp($file_key,$file['file_key'])!==0)out('data/file_not_found.gif');

//check permission
if($file['folder_ispublic']==FOLDER_PRIVATE)
{
	// check current user
	$cpass=isset($_COOKIE['uploader_password'])?$M->escape($_COOKIE['uploader_password']):'';
	$cuid=isset($_COOKIE['uploader_userid'])?(int)$_COOKIE['uploader_userid']:0;
	$M->query("SELECT userid, level FROM uploader_users WHERE userid=$cuid AND password='$cpass';");
	if($M->getRowCount())
	{
		$r=$M->getAssoc();
		$cuid=$r['userid'];

		if($r['level'] != LEVEL_ADMIN && $file['userid']!=$cuid)
		{
			// current file doesn't belong to current user
			$rel = array ( 'is_contact' => 0, 'is_friend' => 0, 'is_family' => 0 );
			$M->query ( "SELECT contact_type FROM uploader_usercontacts WHERE userid={$file['userid']} AND contact_userid=$cuid" );
			if($M->getRowCount())
			{
				$result = $M->getAssoc();
				$M->free();
				$type = $result['contact_type'];
				$rel['is_contact']=1;
				$rel['is_friend']=(bool)($type&CONTACT_FRIEND);
				$rel['is_family']=(bool)($type&CONTACT_FAMILY);
			}
			$perm = $file['folder_permission'];
			$allowed = ($perm&FOLDER_FRIEND_ACCESS && $rel['is_friend']) || ($perm&FOLDER_FAMILY_ACCESS && $rel['is_family']);
			if(!$allowed) out('data/access_denied.gif');
		}
	}
	else out('data/access_denied.gif'); // not logged in
}

// Check bandwidth usage
if($action!='thumb'||($action=='thumb'&&$thumb_size=='large'))
{
	$bw_used=$file['bw_used'];
	$bw_max=$file['bw_max']*1024;
	if($bw_max>0&&$bw_used>$bw_max)
	{
		$lstrst=(time()-$file['bw_reset_last'])/86400;
		if($file['bw_reset_auto']&&$lstrst>=$file['bw_reset_period'])$M->query(sprintf("UPDATE uploader_users SET bw_reset_last='%s', bw_used=0 WHERE userid=%d;",time(),$file['userid']));
		else out('data/bandwidth_exceeded.gif');
	}
}

// get file
$file_location = $file['file_location'];
$file_name = $file['file_name'];
if ( !is_file ( $file_location ) ) out('data/file_not_found.gif');

if($action=='thumb')
{
	$tloc = $file_location . '_' . $thumb_size;
	while(!is_file($tloc))
	{
		switch($thumb_size)
		{
			case 'square': $thumb_size = 'small'; break;
			case 'small': $thumb_size = 'large'; break;
			default: case 'large': $thumb_size = ''; break;
		}
		$tloc = $file_location . ($thumb_size!=''?'_'.$thumb_size:'');
	}
	$file_location = $tloc;
}

if(!is_file($file_location))out($action=='thumb'?'data/no_thumb.gif':'data/file_not_found.gif');

// Send headers
header('Cache-control: max-age=2592000');
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T',time()+2592000));
if($action=='download')header('Content-disposition: attachment;filename="'.$file_name.'"');
else header('Content-disposition: inline;filename="'.$file_name.'"');
header('Content-type: '.mime_type($file['file_extension'],1));
header('Content-length: '.filesize($file_location));

// Send file
$speed=$file['bw_xfer_rate'];
$sleep=$speed?floor(($chunk/($speed*1024))*1000000):0;
$sent=0;
if(false===($fp=fopen($file_location,'rb')))exit;
do{$buf=fread($fp,$chunk);$sent+=strlen($buf);print$buf;flush();usleep($sleep);}while(!feof($fp)&&!connection_aborted());
fclose($fp);

// Update bandwidth usage
if($action!='thumb'||($action=='thumb'&&$thumb_size=='large'))
	$M->query(sprintf("UPDATE uploader_users SET bw_used=bw_used+%f WHERE userid=%d;",$sent/1024,$file['userid']));

// update views and last view
if($action!='thumb')
	$M->query("UPDATE uploader_userfiles SET file_views=file_views+1, file_last_view=" . time() . " WHERE file_id={$file['file_id']}");
?>