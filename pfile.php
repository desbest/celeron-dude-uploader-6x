<?php
header('Cache-control: max-age=2592000');
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T',time()+2592000));
$chunk=20480; // bytes
@set_time_limit(0);
@ignore_user_abort(true);
@set_magic_quotes_runtime(0);
require_once'includes/functions_mime.inc.php';
require_once'includes/configs.inc.php';
require_once'includes/mysql.class.php';
require_once'includes/pub_settings.inc.php';
$PUB=&$UPL['PUBLIC_SETTINGS'];
function out($f){header('Content-type: image/gif');@readfile($f);exit;}
extract($UPL['MYSQL'],EXTR_OVERWRITE);
$M=new mysqlDB($host,$username,$password,$database,0);

// get input
$file_id=isset($_GET['file_id'])?(int)$_GET['file_id']:0;
$action=isset($_GET['action'])?trim($_GET['action']):'';
$thumb_size=isset($_GET['size'])?$_GET['size']:'';

// get file
$r=$M->query2("SELECT * FROM uploader_pfiles WHERE file_id=$file_id LIMIT 1");
if(!$r->isGood()||!$r->rowCount()) out('data/public_file_removed.gif');
$f=$r->fetchAssoc();
$file_location = $f['file_location'];

// check file views and bandwidth limit
if($f['file_views'] >= $PUB['max_views'] || (($f['file_views']*$f['file_size'])/1048576) >= $PUB['max_bandwidth'])
{
	// remove file
	$M->query("DELETE FROM uploader_pfiles WHERE file_id=$file_id LIMIT 1");
	@unlink($file_location);
	@unlink($file_location . '_small' );
	@unlink($file_location . '_large' );
	@unlink($file_location . '_square' );
	// does this set have any remaining files?
	$r=$M->query2("SELECT COUNT(file_id) FROM uploader_pfiles WHERE upload_id={$f['upload_id']}");
	$row=$r->fetchArray();
	if($row[0]==0)
		$M->query("DELETE FROM uploader_puploads WHERE upload_id={$f['upload_id']} LIMIT 1");
	out('data/public_file_removed.gif');
}

if(!is_file($file_location)) out('data/public_file_removed.gif');

// thumbs?
if($action=='thumb')
{
	$tloc = $file_location . '_' . $thumb_size;
	while(!is_file($tloc))
	{
		switch($thumb_size)
		{
			case 'square': $thumb_size = 'small'; break;
			case 'small': $thumb_size = 'large'; break;
			default: case 'large': $thumb_size = ''; break;
		}
		$tloc = $file_location . ($thumb_size!=''?'_'.$thumb_size:'');
	}
	$file_location = $tloc;
}

// send file
header('Content-type: '.mime_type($f['file_name']));
if($action=='download') header('Content-disposition: attachment;filename="'.$f['file_name'].'";');
else header('Content-disposition: inline;filename="'.$f['file_name'].'";');
$speed=$PUB['transfer_rate'];
$sleep=$speed?floor(($chunk/($speed*1024))*1000000):0;
$sent=0;
if(false===($fp=fopen($file_location,'rb')))exit('Could not open file.');
do{$buf=fread($fp,$chunk);$sent+=strlen($buf);print$buf;flush();usleep($sleep);}while(!feof($fp)&&!connection_aborted());
fclose($fp);

// update view count and last view$
if($action!='thumb')
	$M->query("UPDATE uploader_pfiles SET file_views=file_views+1, file_lastview=".time()." WHERE file_id={$file_id};");
?>