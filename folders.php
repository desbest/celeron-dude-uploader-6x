<?php
define ( 'FOLDERS_PHP', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_folders.inc.php';
require_once 'includes/functions_userfiles.inc.php';

$tpl_folders = new Template ( TPL_DIR .  '/tpl_folders.php' );
$tpl_error = new Template ( TPL_DIR .  '/tpl_error.php' );

$userid = $UPL['USER']['userid'];

// to template
$tpl_folders->set ( 'action', $action );

// wut doing?
switch ( $action )
{
	case 'togperm':
	{
		$folder_id = (int)gpc ( 'folder_id', 'GP', 0 );
		$folder = $folder_id ? get_user_folders ( $userid, $folder_id ) : array();
		$ajax = gpc ( 'ajax', 'GP', false );

		if ( count ( $folder ) )
		{
			processFolder ( $folder );
			switch ( $folder['folder_ispublic'] )
			{
				case FOLDER_PUBLIC: $next_perm = 'hidden'; $folder['folder_ispublic'] = FOLDER_HIDDEN; break;
				case FOLDER_HIDDEN: $next_perm = 'private'; $folder['folder_ispublic'] = FOLDER_PRIVATE; break;
				case FOLDER_PRIVATE: default: $next_perm = 'public'; $folder['folder_ispublic'] = FOLDER_PUBLIC; break;
			}
			$permission = get_folder_access_permission ( $folder );
			$mysqlDB->query ( "UPDATE uploader_userfolders SET folder_ispublic={$folder['folder_ispublic']} WHERE userid={$userid} AND folder_id=$folder_id;" );
			if ( $mysqlDB->getAffectRowCount() )
			{
				$response = array ( 'result' => 'success', 'current_perm' => $next_perm, 'permission' => $permission, 'browse_url' => $folder['browse_url'] );
				update_public_folder_status ( $userid );
			}
			else $response = array ( 'result' => 'failed', 'message' => 'Unable to update folder permission' );
		}
		else $response = array ( 'result' => 'failed', 'message' => 'Invalid folder' );

		// return response
		if ( $ajax !== false )
		{
			print json_encode ( $response );
		}
		else go_to ( isset ( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : ( UPLOADER_URL . ( MOD_REWRITE ? 'folders' : 'folders.php' ) ) );
	}
	break;

	case 'update_description':
	{
		if ( !count ( $_POST ) ) exit;
		$description = trim ( gpc ( 'description', 'P', '' ) );
		$folder_id = (int)gpc ( 'folder_id', 'P', 0 );
		$folder = $folder_id ? get_user_folders ( $userid, $folder_id ) : array();

		if ( count ( $folder ) )
		{
			$description = $mysqlDB->escape ( substr ( $description, 0, 255 ) );
			$mysqlDB->query ( "UPDATE uploader_userfolders SET folder_description='$description' WHERE userid={$userid} AND folder_id=$folder_id;" );
			$response = array ( 'result' => 'success' );
		}
		else $response = array ( 'result' => 'failed', 'message' => 'Invalid folder' );

		// return response
		print json_encode ( $response );
	}
	break;

	case 'edit':
	{
		$folder_id = (int)gpc ( 'folder_id', 'GP', 0 );
		$folder = $folder_id ? get_user_folders ( $userid, $folder_id ) : array();
		$return_url = gpc('return_url', 'PG', UPLOADER_URL .(MOD_REWRITE?'folders':'folders.php'));

		if ( count ( $folder ) )
		{
			if ( $task == 'edit' )
			{
				$properties = array ( 'name' => '', 'description' => '', 'access' => 'hidden', 'friend_access' => 0, 'family_access' => 0, 'is_gallery' => 0 );
				$new_properties = gpc ( 'folder', 'P', $properties );
				$error  = 'none';

				// process checkboxes values because if they're not checked they don't exist
				$new_properties['friend_access'] = isset($new_properties['friend_access']) ? (int)$new_properties['friend_access'] : 0;
				$new_properties['family_access'] = isset($new_properties['family_access']) ? (int)$new_properties['family_access'] : 0;
				$new_properties['is_gallery'] = isset($new_properties['is_gallery']) ? (int)$new_properties['is_gallery'] : 0;

				// check folder name
				if ( $folder['folder_renameable'] )
				{
					if ( $new_properties['name'] == '' ) $error = $lang_folders['folder_no_name'];
					elseif ( preg_match ( '#[\t\r\n]#', $new_properties['name'] ) ) $error = $lang_folders['folder_bad_char'];
					elseif ( strlen ( $new_properties['name'] ) < $UPL['CONFIGS']['FOLDER_MIN_LEN'] ) $error = parse ( $lang_folders['folder_short_name'], '{length}', $UPL['CONFIGS']['FOLDER_MIN_LEN'] );
					elseif ( strlen ( $new_properties['name'] ) > $UPL['CONFIGS']['FOLDER_MAX_LEN'] ) $error = parse ( $lang_folders['folder_long_name'], '{length}', $UPL['CONFIGS']['FOLDER_MAX_LEN'] );
					elseif ( strcasecmp ( $new_properties['name'], $folder['folder_name'] ) !== 0 )
					{
						$insert['folder_name'] = $mysqlDB->escape ( trim ( $new_properties['name'] ) );
						// check if folder exists
						$mysqlDB->query ( "SELECT COUNT(folder_id) AS folder_count FROM uploader_userfolders WHERE userid={$userid} AND folder_name='{$new_properties['name']}';" );
						$result = $mysqlDB->getAssoc();
						$mysqlDB->free();
						if ( $result['folder_count'] ) $error = $lang_folders['folder_exists'];
					}
				}
				// folder description
				$insert['folder_description'] = $mysqlDB->escape ( trim ( $new_properties['description'] ) );

				// folder permission
				switch ( $new_properties['access'] )
				{
					case 'public': $insert['folder_ispublic'] = FOLDER_PUBLIC; break;
					case 'hidden': $insert['folder_ispublic'] = FOLDER_HIDDEN; break;
					case 'private': default: $insert['folder_ispublic'] = FOLDER_PRIVATE; break;
				}
				$permission = array
				(
					'friend' => $new_properties['friend_access'],
					'family' => $new_properties['family_access']
				);
				$insert['folder_permission'] = folderperm2int ( $permission );

				// save
				if ( $error == 'none' )
				{
					$result = $mysqlDB->query ( "UPDATE uploader_userfolders SET " . $mysqlDB->buildInsertStatement ( $insert ) . " WHERE userid={$userid} AND folder_id={$folder_id}" );
					if ( !$result ) exit ( $mysqlDB->error() );
					update_public_folder_status ( $userid );
					go_to($return_url);
				}
				else
				{
					$folder = array_merge ( $folder, $new_properties );
					$folder['renameable'] = $folder['folder_renameable'];

					$tpl_error->set ( 'error', $error );
					$tpl_folders->set ( 'folder', $folder );
					$tpl_folders->set ( 'error', $tpl_error );
					$tpl_folders->set ( 'return_url', $return_url );
					$tpl_uploader->setr ( 'content', $tpl_folders, 1 );
				}
			}
			else
			{
				// start edit
				$permission = get_folder_access_permission ( $folder );
				$permission['public'] = $folder['folder_ispublic'] == FOLDER_PUBLIC;
				$folder['access'] = $permission['access'];
				$folder['friend_access'] = $permission['friend'];
				$folder['family_access'] = $permission['family'];
				$folder['name'] = $folder['folder_name'];
				$folder['description'] = $folder['folder_description'];
				$folder['renameable'] = $folder['folder_renameable'];

				$tpl_folders->set ( 'return_url', (isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:(UPLOADER_URL.(MOD_REWRITE?'folders':'folders.php'))) );
				$tpl_folders->set ( 'folder', $folder );
				$tpl_uploader->set ( 'content', $tpl_folders, 1 );
			}
		}
		else
		{
			$tpl_message->set ( 'message', $lang_folders['folder_invalid'] );
			$tpl_uploader->set ( 'content', $tpl_message, 1 );
		}
	}
	break;

	case 'delete':
	{
		$folder_id = (int)gpc ( 'folder_id', 'GP', 0 );
		$folder = $folder_id ? get_user_folders ( $userid, $folder_id ) : array();

		if ( count ( $folder ) )
		{
			if ( !$folder['folder_deleteable'] )
			{
				$tpl_message->set ( 'message', $lang_folders['folder_cant_delete'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}

			if ( $task == 'confirm' )
			{
				$delete_mode = gpc ( 'delete_mode', 'P', 'delete' );

				if ( $delete_mode == 'delete' )
				{
					$file_ids = array();
					// get location of files in the folder
					$mysqlDB->query ( "SELECT file_location, file_name, file_id FROM uploader_userfiles WHERE userid={$userid} AND folder_id=$folder_id;" );
					if ( $mysqlDB->getRowCount() )
					{
						while ( false !== ( $file = $mysqlDB->getAssoc() ) )
						{
							$file_ids[] = $file['file_id'];
							delete_file ( $file['file_location'] );
							// logging
							if ( $UPL['SETTINGS']['log'] == 2 ) upload_log ( sprintf ( 'Deleted: %s', entities($file['file_name']) ) );
						}
						$mysqlDB->free();
					}
					$list = implode ( ',', $file_ids );

					// remove folder
					$mysqlDB->query ( "DELETE FROM uploader_userfolders WHERE userid={$userid} AND folder_id=$folder_id;" );

					// remove files in folder
					$mysqlDB->query ( "DELETE FROM uploader_userfiles WHERE userid={$userid} AND folder_id=$folder_id;" );

					// remove comments
					if ( $list != '' )
						if ( !$mysqlDB->query ( "DELETE FROM uploader_usercomments WHERE object_id IN ($list) AND comment_type=" . COMMENT_FILE ) ) exit ( $mysqlDB->error(__LINE__, __FILE__) );

					update_public_folder_status ( $userid );
				}
				else
				{
					// Find home directory (My Document)
					$mysqlDB->query ( "SELECT folder_id FROM uploader_userfolders WHERE userid={$userid} AND folder_homefolder=1 LIMIT 1;" );
					if ( $mysqlDB->getRowCount() )
					{
						$result = $mysqlDB->getAssoc();
						$mysqlDB->free();
						// change folder_id
						$mysqlDB->query ( "UPDATE uploader_userfiles SET folder_id={$result['folder_id']} WHERE userid={$userid} AND folder_id=$folder_id;" );
						// remove folder
						$mysqlDB->query ( "DELETE FROM uploader_userfolders WHERE userid={$userid} AND folder_id=$folder_id;" );
						update_public_folder_status ( $userid );
					}
					else exit ( 'Fatal error: Could not find home directory. folders.php line '. __LINE__ );
				}
				header ( 'Location: ' . UPLOADER_URL . ( MOD_REWRITE ? 'folders' : 'folders.php' ) );
			}
			else
			{
				$tpl_folders->set ( 'folder', $folder );
				$tpl_uploader->set ( 'content', $tpl_folders, 1 );
			}
		}
		else
		{
			$tpl_message->set ( 'message', $lang_folders['folder_invalid'] );
			$tpl_uploader->set ( 'content', $tpl_message, 1 );
		}
	}
	break;

	case 'create':
	{
		$create_max =  $UPL['USER']['fl_max_folders'];

		// permission to create?
		if ( !$UPL['USER']['fl_allow_folders'] )
		{
			$tpl_message->set ( 'message', $lang_folders['folder_no_perm_create'] );
			$tpl_uploader->set ( 'content', $tpl_message, 1 );
			exit;
		}

		// max folder limit reached?
		if ( $UPL['USER']['fl_max_folders'] > 0 )
		{
			$mysqlDB->query ( "SELECT COUNT(folder_id) AS folder_count FROM uploader_userfolders WHERE userid={$userid};" );
			$result = $mysqlDB->getAssoc();
			if ( $result['folder_count'] >= $UPL['USER']['fl_max_folders'] )
			{
				$tpl_message->set ( 'message', $lang_folders['folder_limit'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}
		}

		// do create
		if ( $task == 'create' )
		{
			$new_folder = array ( 'name' => '', 'description' => '', 'access' => 'private', 'friend_access' => 0, 'family_access' => 0, 'is_gallery' => 0 );
			$folder = gpc ( 'folder', 'P', $new_folder );
			$return_url = gpc('return_url', 'PG', UPLOADER_URL .(MOD_REWRITE?'folders':'folders.php'));
			$error  = 'none';

			// process checkboxes values because if they're not checked they don't exist
			$folder['friend_access'] = isset($folder['friend_access']) ? (int)$folder['friend_access'] : 0;
			$folder['family_access'] = isset($folder['family_access']) ? (int)$folder['family_access'] : 0;
			$folder['is_gallery'] = isset($folder['is_gallery']) ? (int)$folder['is_gallery'] : 0;

			// Check folder name
			if ( $folder['name'] == '' ) $error = $lang_folders['folder_no_name'];
			elseif ( preg_match ( '#[\t\r\n]#', $folder['name'] ) ) $error = $lang_folders['folder_bad_char'];
			elseif ( strlen ( $folder['name'] ) < $UPL['CONFIGS']['FOLDER_MIN_LEN'] ) $error = parse ( $lang_folders['folder_short_name'], '{length}', $UPL['CONFIGS']['FOLDER_MIN_LEN'] );
			elseif ( strlen ( $folder['name'] ) > $UPL['CONFIGS']['FOLDER_MAX_LEN'] ) $error = parse ( $lang_folders['folder_long_name'], '{length}', $UPL['CONFIGS']['FOLDER_MAX_LEN'] );
			else
			{
				$new_folder['name'] = $mysqlDB->escape ( trim ( $folder['name'] ) );
				// check if folder exists
				$mysqlDB->query ( "SELECT COUNT(folder_id) AS folder_count FROM uploader_userfolders WHERE userid={$userid} AND folder_name='{$new_folder['name']}';" );
				$result = $mysqlDB->getAssoc();
				$mysqlDB->free();
				if ( $result['folder_count'] ) $error = $lang_folders['folder_exists'];
			}

			// folder properties
			$new_folder['is_gallery'] = $folder['is_gallery'];

			// folder description
			$new_folder['description'] = $mysqlDB->escape ( trim ( $folder['description'] ) );

			// folder access permission
			$new_folder['permission'] = array
			(
				'friend' => $folder['friend_access'],
				'family' => $folder['family_access']
			);

			switch ( $folder['access'] )
			{
				case 'public': $folder_access = FOLDER_PUBLIC; break;
				case 'hidden': $folder_access = FOLDER_HIDDEN; break;
				case 'private': $folder_access = FOLDER_PRIVATE; break;
			}

			if ( $error == 'none' )
			{
				$insert = array
				(
					'folder_id'				=> null,
					'folder_name'			=> $new_folder['name'],
					'folder_description'	=> $new_folder['description'],
					'folder_isgallery'		=> $new_folder['is_gallery'],
					'folder_ispublic'		=> $folder_access,
					'folder_key'			=> get_rand ( 5 ),
					'folder_permission'		=> folderperm2int($new_folder['permission']),
					'folder_deleteable'		=> 1,
					'folder_renameable'		=> 1,
					'userid'				=> $userid
				);

				$result = $mysqlDB->query ( "INSERT INTO uploader_userfolders SET " . $mysqlDB->buildInsertStatement ( $insert ) );
				if ( !$result ) exit ( $mysqlDB->error() );
				update_public_folder_status ( $userid );
				go_to($return_url);
			}
			else
			{
				$tpl_folders->set ( 'return_url', $return_url );
				$tpl_error->set ( 'error', $error );
				$tpl_folders->set ( 'folder', $folder );
				$tpl_folders->set ( 'error', $tpl_error );
				$tpl_uploader->setr ( 'content', $tpl_folders, 1 );
			}
		}
		else
		{
			$new_folder = array ( 'name' => '', 'description' => '', 'access' => 'hidden', 'friend_access' => 0, 'family_access' => 0, 'is_gallery' => 0 );
			$tpl_folders->set ( 'return_url', (isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:(UPLOADER_URL.(MOD_REWRITE?'folders':'folders.php'))) );
			$tpl_folders->set ( 'folder', $new_folder );
			$tpl_uploader->setr ( 'content', $tpl_folders, 1 );
		}
	}
	break;

	default:
	{
		$user_folders = get_user_folders ( $userid, 0, true );
		processFolders ( $user_folders );
		$tpl_folders->set ( 'user_folders', $user_folders );
		$tpl_uploader->set ( 'page_title', $lang_titles['mf_fld_title2'] );
		$tpl_uploader->set ( 'content', $tpl_folders, 1 );
	}
}
?>