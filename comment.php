<?php
define ( 'COMMENT_PHP', 1 );
require_once 'includes/commons.inc.php';

switch ( $action )
{
	case 'comment_image':
	case 'comment_file':
	{
		$file_id = gpc ( 'file_id', 'P', 0 );
		$comment = gpc ( 'comment', 'P', '' );
		$error = 'none';

		// check comment
		$comment = trim ( $comment );
		if ( $comment == '' ) $error = 'Blank comment';

		// check file
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_userfiles WHERE file_id=$file_id LIMIT 1;" );

		if ( !$result->rowCount() )
			$error = 'Invalid file';
		else $result->free();

		if ( $error == 'none' )
		{
			$insert = array
			(
				'comment_id'	=> NULL,
				'comment_date'	=> time(),
				'comment_ip'	=> $_SERVER['REMOTE_ADDR'],
				'comment_type'	=> COMMENT_FILE,
				'comment_message' => $mysqlDB->escape ( $comment ),
				'object_id'		=> $file_id,
				'userid'		=> $USER['userid']
			);

			if ( !$mysqlDB->query ( "INSERT INTO uploader_usercomments SET " . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
		}
		go_to ( previous_page ( UPLOADER_URL ) );
	}
	break;

	case 'delete':
	case 'delete_file_comment':
	case 'delete_image_comment':
	{
		$comment_id = (int)gpc ( 'comment_id', 'G', 0 );

		// delete comment
		$result = $mysqlDB->query2 ( "DELETE FROM uploader_usercomments WHERE userid={$USER['userid']} AND comment_id=$comment_id" );

		go_to ( previous_page ( UPLOADER_URL ) );
	}
	break;

	case 'comment_announcement':
	{
		$aid = gpc ( 'announcement_id', 'P', 0 );
		$comment = gpc ( 'comment', 'P', '' );
		$error = 'none';

		// check comment
		$comment = trim ( $comment );
		if ( $comment == '' ) $error = 'Blank comment';

		// check announcement
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_announcements WHERE announcement_id=$aid LIMIT 1;" );
		if ( !$result->isGood() ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
		if ( $result->rowCount() )
		{
			$ann = $result->fetchAssoc();
			if ( !$ann['announcement_allowcomment'] ) $error = 'Cannot comment on this announcement.';
		}
		else $error = 'Invalid announcement';

		if ( $error == 'none' )
		{
			$insert = array
			(
				'comment_id'	=> NULL,
				'comment_date'	=> time(),
				'comment_ip'	=> $_SERVER['REMOTE_ADDR'],
				'comment_type'	=> COMMENT_ANNOUNCEMENT,
				'comment_message' => $mysqlDB->escape ( $comment ),
				'object_id'		=> $ann['announcement_id'],
				'userid'		=> $USER['userid']
			);
			if ( !$mysqlDB->query ( "INSERT INTO uploader_usercomments SET " . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
		}
		go_to ( previous_page ( UPLOADER_URL ) );
	}
	break;

	default: go_to ( previous_page ( UPLOADER_URL ) );
}
?>