<?php
define ( 'ACCOUNT_PHP', 1 );
define ( 'NO_AUTH_CHECK', 1 );

require_once 'includes/commons.inc.php';
require_once 'includes/messages_account.inc.php';

$tpl_error = new Template ( TPL_DIR . 'tpl_error.php' );

switch ( $action )
{
	case 'register':
	{

		$ban = isBanned();
		if ( is_array ( $ban ) && $ban['uploader'] )
		{
			$tpl_message->set ( 'message', $lang_misc['ip_banned'] );
			$tpl_uploader->set ( 'content', $tpl_message, true );
			exit;
		}

		// Registration disable or user is already logged in?
		if ( !$UPL['SETTINGS']['reg'] )
		{
			$tpl_message->set ( array ( 'message' => parse ( $lang_account['reg_disabled'], '{reason}', $UPL['SETTINGS']['regmsg'] ), 'back_url' => 'index.php' ) );
			$tpl_uploader->set ( array ( 'content' => &$tpl_message, 'page_title' => $lang_titles['reg_title4'] ), '', 1 );
			exit;
		}
		else if ( $UPL['USER']['logged_in'] ) exit ( header ( 'Location: index.php' ) );

		// OK to register now, start up
		$tpl_register  = new Template ( TPL_DIR . 'tpl_register.php' );
		$userinfo = gpc ( 'userinfo', 'P', array ( 'name' => '', 'email' => '', 'pbrowse' => 1, 'pemail' => 0, 'pmessage' => 1 ) );
		$errors = array ( );

		// user settings
		require_once 'includes/usr_settings.inc.php';
		$user_settings = $UPL['USER_SETTINGS'];

		// check user inputs
		$userinfo['name'] = trim ( $userinfo['name'] );
		$userinfo['email'] = trim ( $userinfo['email'] );
		if ( !isset ( $userinfo['pbrowse'] ) ) $userinfo['pbrowse'] = false;
		if ( !isset ( $userinfo['pemail'] ) ) $userinfo['pemail'] = false;
		if ( !isset ( $userinfo['pmessage'] ) ) $userinfo['pmessage'] = false;

		// to template
		$tpl_register->set ( 'userinfo', $userinfo );
		$tpl_register->set ( 'restrictions', $user_settings['restrictions'] );

		if ( $task == 'register' )
		{
			// Process registration
			if ( $demo ) exit ( 'Demo only!' );

			// user exists with same name or email. Values (name,email,both)
			$username_exists = false;
			$useremail_exists = false;

			if ( $userinfo['name'] != '' || $userinfo['email'] != '' )
			{
				$mysqlDB->query ( sprintf ( "SELECT username,email FROM uploader_users WHERE username='%s' OR email='%s' OR reg_email='%s' LIMIT 1;", $mysqlDB->escape ( $userinfo['name'] ), $mysqlDB->escape ( $userinfo['email'] ), $mysqlDB->escape ( $userinfo['email'] ) ) );

				if ( $mysqlDB->getRowCount() )
				{
					$existing_user = $mysqlDB->getAssoc();
					$mysqlDB->free();
					$username_exists = strcasecmp ( $userinfo['name'], $existing_user['username'] ) === 0;
					$useremail_exists = strcasecmp ( $userinfo['email'], $existing_user['email'] ) === 0;
				}
			}
			// check username
			if ( $userinfo['name'] == '' ) $errors [] = $lang_account['reg_no_name'];
			elseif ( strlen ( $userinfo['name'] ) < MIN_USERNAME_LEN ) $errors [] = parse ( $lang_account['reg_short_name'], '{min_length}', MIN_USERNAME_LEN );
			elseif ( MAX_USERNAME_LEN > 0 && strlen ( $userinfo['name'] ) > MAX_USERNAME_LEN ) $errors [] = parse ( $lang_account['reg_long_name'], '{max_length}', MAX_USERNAME_LEN );
			elseif ( preg_match ( '#[^a-z0-9_]#i', $userinfo['name'] ) ) $errors [] = $lang_account['reg_bad_name'];
			elseif ( $user_settings['restrictions']['disallowed_names'] != '' && in_array ( strtolower ( $userinfo['name'] ), explode ( ',', $user_settings['restrictions']['disallowed_names'] ) ) ) $errors [] = $lang_account['reg_disallowed_name'];
			elseif ( $username_exists ) $errors[] = parse ( $lang_account['reg_name_taken'], '{username}', entities ( $userinfo['name'] ) );

			// check password
			if ( $userinfo['pass1'] == '' ) $errors [] = $lang_account['reg_no_pass'];
			elseif ( $userinfo['pass1'] != $userinfo['pass2'] ) $errors [] = $lang_account['reg_pass_no_match'];

			// check email
			if ( $userinfo['email'] == '' ) $errors [] = $lang_account['reg_no_email'];
			elseif ( strlen ( $userinfo['email'] ) > 100 || !preg_match ( "#(.+?)\@(.+?)#i", $userinfo['email'] ) ) $errors [] = $lang_account['reg_invalid_email'];
			elseif ( $useremail_exists ) $errors [] = $lang_account['reg_email_exists'];

			// Add user if no errors
			if ( count ( $errors ) == 0 )
			{
				$act_code = get_rand ( 32 );

				$new_user_info = array
				(
					'userid'			=> NULL,
					'username'			=> $userinfo['name'],
					'password'			=> md5 ( $userinfo['pass1'] ),
					'email'				=> $userinfo['email'],
					'level'				=> LEVEL_NORMAL,
					'max_messages' 		=> (int)$UPL['CONFIGS']['DEFAULT_MAX_MESSAGE'],
					'is_activated'		=> 0,
					'is_approved'		=> (int)$UPL['SETTINGS']['approval'],
					'is_suspended'		=> 0,
					'last_login_time'	=> 0,
					'last_login_ip'		=> '0.0.0.0',
					'reg_email'			=> $userinfo['email'],
					'reg_date'			=> time(),
					'reg_ip'			=> $_SERVER['REMOTE_ADDR'],
					'pref_accepts_pm'	=> (int)$userinfo['pmessage'],
					'pref_show_email'	=> (int)$userinfo['pemail'],
					'bw_used'			=> 0,
					'bw_max'			=> $user_settings['new_user_settings']['bw_max'],
					'bw_reset_last'		=> 0,
					'bw_reset_period'	=> $user_settings['new_user_settings']['bw_reset_period'],
					'bw_reset_auto'		=> (int)$user_settings['new_user_settings']['bw_auto_reset'],
					'bw_xfer_rate'		=> (int)$user_settings['new_user_settings']['bw_xfer_rate'],
					'fl_max_storage'	=> $user_settings['new_user_settings']['fl_max_storage'],
					'fl_max_filesize'	=> $user_settings['new_user_settings']['fl_max_filesize'],
					'fl_allowed_types'	=> $user_settings['new_user_settings']['fl_allowed_filetypes'],
					'fl_images_only'	=> (int)$user_settings['new_user_settings']['fl_images_only'],
					'fl_rename_permission' => (int)$user_settings['new_user_settings']['fl_allow_rename'],
					'fl_allow_folders'	=> (int)$user_settings['new_user_settings']['fl_create_folder'],
					'fl_max_folders'	=> $user_settings['new_user_settings']['fl_max_folders'],
					'fl_watermark'		=> (int)$user_settings['new_user_settings']['fl_watermark'],
					'xtr_admin_comments'		=> '',
					'xtr_new_email_address'		=> '',
					'xtr_activation_code'		=> md5 ( $act_code ),
					'xtr_password_reset_code'	=> md5 ( get_rand ( 100 ) ),
					'xtr_change_email_code'		=> md5 ( get_rand ( 100 ) ),
				);

				if ( !$mysqlDB->query ( "INSERT INTO uploader_users SET " . $mysqlDB->buildInsertStatement ( $new_user_info ) . ";" ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

				if ( $mysqlDB->getAffectRowCount ( ) )
				{
					$userid = $mysqlDB->getInsertId();

					// create default folder
					$insert = array
					(
						'folder_id'				=> null,
						'folder_name'			=> 'My Documents',
						'folder_description'	=> '',
						'folder_isgallery'		=> 0,
						'folder_ispublic'		=> 0,
						'folder_permission'		=> 0,
						'folder_deleteable'		=> 0,
						'folder_homefolder'		=> 1,
						'folder_renameable'		=> 0,
						'userid'				=> $userid
					);
					if ( !$mysqlDB->query ( "INSERT INTO uploader_userfolders SET " . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error() );

					// Activation required?
					if ( $UPL['SETTINGS']['activation_req'] )
					{
						$tpl_email_header = new Template ( TPL_DIR . 'tpl_email_header.php' );
						$tpl_email_footer = new Template ( TPL_DIR . 'tpl_email_footer.php' );
						$tpl_email_activation = new TEmplate ( TPL_DIR . 'tpl_email_activation.php' );

						$activation_url = UPLOADER_URL . ( MOD_REWRITE ? 'account' : 'account.php' ) . '?action=activate&userid=' . $userid . '&code=' . $act_code;

						$tpl_email_header->set ( 'username', $userinfo['name'] );
						$tpl_email_activation->set ( 'activation_url', $activation_url );

						$message = $tpl_email_header->display ( true );
						$message .= $tpl_email_activation->display ( true );
						$message .= $tpl_email_footer->display ( true );

						send_email ( $userinfo['email'], $lang_account['reg_act_email_subj'], $message, 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );
					}

					// notify admin of new user?
					if ( $UPL['SETTINGS']['notify_reg'] )
					{
						send_email ( $UPL['SETTINGS']['email'], $lang_misc['reg_notify_email_subj'], parse ( $lang_misc['reg_notify'], '{username}', $userinfo['name'] ), 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );
					}

                    // All done, success
					$msg [] = parse ( $lang_account['reg_success1'], '{username}', $userinfo['name'] );
					if ( $UPL['SETTINGS']['activation_req'] ) $msg [] = parse ( $lang_account['reg_success2'], '{email}', $userinfo['email'] );
					if ( !$UPL['SETTINGS']['approval'] ) $msg [] = $lang_account['reg_success3'];

					$tpl_message->set ( 'message', implode ( ' ', $msg ) );
					$tpl_message->set ( 'back_url', 'account.php?action=login' );
					$tpl_uploader->setr ( 'content', $tpl_message );
					$tpl_uploader->set ( 'page_title', $lang_titles['reg_title3'] );
					$tpl_uploader->display ( );
				}
				else
				{
					exit ( 'Internal error: Could not add user to database in ' . __FILE__ . ' near ' . __LINE__ );
				}
			}
			else
			{
				// Display form along with error messages
				$tpl_error->setr ( 'error', $errors );
				$tpl_register->setr ( 'error', $tpl_error );
				$tpl_uploader->set ( array ( 'content' => &$tpl_register, 'page_title' => $lang_titles['reg_title2'] ) );
				$tpl_uploader->display ( );
			}
		}
		else
		{
			// Display form to user
			$tpl_uploader->set ( array ( 'content' => &$tpl_register, 'page_title' => $lang_titles['reg_title1'], 'errors' => $errors ) );
			$tpl_uploader->display ( );
		}
	}
	break;

	case 'checkname':
	{
		require_once 'includes/usr_settings.inc.php';
		$user_settings = $UPL['USER_SETTINGS'];

		$name = trim ( gpc ( 'name', 'GP' ) );
		$error = 'none';

		if ( $name == '' ) $error = $lang_account['reg_no_name'];
		elseif ( strlen ( $name ) < MIN_USERNAME_LEN ) $error = parse ( $lang_account['reg_short_name'], '{min_length}', MIN_USERNAME_LEN );
		elseif ( MAX_USERNAME_LEN && strlen ( $name ) > MAX_USERNAME_LEN ) $error = parse ( $lang_account['reg_long_name'], '{max_length}', MAX_USERNAME_LEN );
		elseif ( preg_match ( '#[^a-z0-9_]#i', $name ) ) $error = $lang_account['reg_bad_name'];
		elseif ( $user_settings['restrictions']['disallowed_names'] != '' && in_array ( strtolower ( $name ), explode ( ',', $user_settings['restrictions']['disallowed_names'] ) ) ) $error = $lang_account['reg_disallowed_name'];
		else
		{
			$mysqlDB->query ( sprintf ( "SELECT userid FROM uploader_users WHERE username='%s' LIMIT 1;", $mysqlDB->escape ( $name ) ) );
			if ( $mysqlDB->getRowCount() )
			{
				$mysqlDB->free();
				$error = parse ( $lang_account['reg_name_taken'], '{username}', entities ( $name ) );
			}
		}
		$response = array ( 'result' => ( $error == 'none' ? 'success' : 'failed' ), 'message' => $error );
		print json_encode ( $response );
	}
	break;

	case 'resend_activation':
	{
		$tpl_act = new Template ( TPL_DIR . 'tpl_activate.php' );
		$email = trim ( gpc ( 'email', 'P', '' ) );
		$err = 'none';

		if ( $task == 'activate' )
		{
			if ( $demo ) exit ( 'Demo only!' );

			if ( $email == '' )
			{
				$err = $lang_account['act_no_email'];
			}
			else
			{
				if ( !$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE email='%s' LIMIT 1;", $mysqlDB->escape ( $email ) ) ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

				if ( $mysqlDB->getRowCount() )
				{
					$userinfo = $mysqlDB->getAssoc();
					$mysqlDB->free();

					if ( $userinfo['is_activated'] )
					{
						$err = $lang_account['act_already_activated'];
					}
					else
					{
						$act_code = get_rand ( 32 );
						$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET xtr_activation_code='%s' WHERE userid=%d;", md5 ( $act_code ), $userinfo['userid'] ) );

						$tpl_email_header = new Template ( TPL_DIR . 'tpl_email_header.php' );
						$tpl_email_footer = new Template ( TPL_DIR . 'tpl_email_footer.php' );
						$tpl_email_activation = new TEmplate ( TPL_DIR . 'tpl_email_activation.php' );

						$activation_url = UPLOADER_URL . ( MOD_REWRITE ? 'account' : 'account.php' ) . '?action=activate&userid=' . $userinfo['userid'] . '&code=' . $act_code;

						$tpl_email_header->set ( 'username', $userinfo['username'] );
						$tpl_email_activation->set ( 'activation_url', $activation_url );

						$message = $tpl_email_header->display ( true );
						$message .= $tpl_email_activation->display ( true );
						$message .= $tpl_email_footer->display ( true );

						send_email ( $userinfo['email'], $lang_account['reg_act_email_subj'], $message, 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );
					}
				}
				else $err = $lang_account['act_email_not_found'];
			}

			// errors?
			if ( $err == 'none' )
			{
				// success
				$tpl_message->set ( 'message', $lang_account['act_email_sent'] );
				$tpl_message->set ( 'back_url', 'account.php?action=login' );
				$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			}
			else
			{
				// display form again with errors
				$tpl_error->set ( 'error', $err );
				$tpl_act->setr( 'error', $tpl_error );
				$tpl_uploader->set ( 'page_title', $lang_misc['error'] );
				$tpl_uploader->setr ( 'content', $tpl_act, 1 );
			}
		}
		else
		{
			// display form
			$tpl_uploader->setr ( 'content', $tpl_act );
			$tpl_uploader->set ( 'page_title', $lang_titles['act_title1'], 1 );
		}
	}
	break;

	case 'activate':
	{
		$userid   = abs ( (int)gpc ( 'userid', 'G', 0 ) );
		$act_code = gpc ( 'code',   'G', '' );
		$result   = 'none';

		$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE userid=%d AND xtr_activation_code='%s' LIMIT 1;", $userid, md5 ( $act_code ) ) );

		if ( $mysqlDB->getRowCount() )
		{
			$userinfo = $mysqlDB->getAssoc();
			$mysqlDB->free();
			$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET is_activated=1, xtr_activation_code='%s' WHERE userid=%d;", md5 ( get_rand ( 1024 ) ), $userinfo['userid'] ) );
			$result = parse ( $lang_account['act_activated'], '{username}', $userinfo['username'] );
		}
		else
		{
			$result = $lang_account['act_invalid_code'];
		}
		$tpl_message->set ( 'message', $result );
		$tpl_uploader->setr ( 'content', $tpl_message );
		$tpl_uploader->display ( );
	}
	break;

	case 'password':
	{
		$tpl_pass = new Template ( TPL_DIR . 'tpl_password.php' );

		if ( $task == 'password' )
		{
			if ( $demo ) exit ( 'Demo only!' );

			// Get user info
			$email = trim ( gpc ( 'email', 'P' ) );
			$err = 'none';

			if ( $email == '' )
			{
				$err = $lang_account['pass_no_email'];
			}
			else
			{
				$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE email='%s' LIMIT 1;", $mysqlDB->escape ( $email ) ) );

				if ( $mysqlDB->getRowCount ( ) )
				{
					$pw_code = get_rand ( 32 );
					$userinfo = $mysqlDB->getAssoc();
					$mysqlDB->free();
					$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET xtr_password_reset_code='%s' WHERE userid=%d;", md5 ( $pw_code ), $userinfo['userid'] ) );

					$tpl_email_header = new Template ( TPL_DIR . 'tpl_email_header.php' );
					$tpl_email_footer = new Template ( TPL_DIR . 'tpl_email_footer.php' );
					$tpl_email_pass = new TEmplate ( TPL_DIR . 'tpl_email_passrequest.php' );

					$reset_url = UPLOADER_URL . ( MOD_REWRITE ? 'account' : 'account.php' ) . '?action=resetpassword&userid=' . $userinfo['userid'] . '&code=' . $pw_code;

					$tpl_email_header->set ( 'username', $userinfo['username'] );
					$tpl_email_pass->set ( 'ip_address', $_SERVER['REMOTE_ADDR'] );
					$tpl_email_pass->set ( 'reset_url', $reset_url );

					$message = $tpl_email_header->display ( true );
					$message .= $tpl_email_pass->display ( true );
					$message .= $tpl_email_footer->display ( true );

					send_email ( $userinfo['email'], $lang_account['pass_email_subj'], $message, 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );
				}
				else
				{
					$err = $lang_account['pass_email_not_found'];
				}
			}

			// errors?
			if ( $err == 'none' )
			{
				$tpl_message->set ( 'message', parse ( $lang_account['pass_sent'], '{username}', $userinfo['username'] ) );
				$tpl_message->set ( 'back_url', 'account.php?action=login' );
				$tpl_uploader->setr ( 'content', $tpl_message, 1 );
			}
			else
			{
				// display form again with error messages
				$tpl_error->set ( 'error', $err );
				$tpl_pass->setr( 'error', $tpl_error );
				$tpl_uploader->set ( array ( 'page_title' => $lang_misc['error'], 'content' => &$tpl_pass ), '', 1 );
			}
		}
		else
		{
			// show form for user to enter email address
			$tpl_uploader->setr ( 'content', $tpl_pass, 1 );
		}
	}
	break;

	case 'resetpassword':
	{
		$userid = abs ( (int)gpc ( 'userid', 'PG', 0 ) );
		$code   = trim ( gpc ( 'code',   'PG', '' ) );
		$result = 'none';

		// find user
		$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE userid=%d AND xtr_password_reset_code='%s' LIMIT 1;", $userid, md5 ( $code ) ) );

		if ( $mysqlDB->getRowCount() )
		{
			// update
			$userinfo = $mysqlDB->getAssoc();
			$pw_new = get_rand ( 6 );
			$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET password='%s', xtr_password_reset_code='%s' WHERE userid=%d;", md5 ( $pw_new ), md5 ( get_rand ( 1024 ) ), $userinfo['userid'] ) );

			$tpl_email_header = new Template ( TPL_DIR . 'tpl_email_header.php' );
			$tpl_email_footer = new Template ( TPL_DIR . 'tpl_email_footer.php' );
			$tpl_email_pass = new TEmplate ( TPL_DIR . 'tpl_email_passreset.php' );

			$tpl_email_header->set ( 'username', $userinfo['username'] );
			$tpl_email_pass->set ( 'new_password', $pw_new );
			$tpl_email_pass->set ( 'login_url', UPLOADER_URL . ( MOD_REWRITE ? 'login' : 'account.php?action=login' ) );

			$message = $tpl_email_header->display ( true );
			$message .= $tpl_email_pass->display ( true );
			$message .= $tpl_email_footer->display ( true );

			send_email ( $userinfo['email'], $lang_account['pass_email_subj'], $message, 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );

            // OK
			$result = $lang_account['pass_reset'];
		}
		else
		{
			$result = $lang_account['pass_invalid'];
		}
		// show result
		$tpl_message->set ( 'message', $result );
		$tpl_message->set ( 'back_url', 'account.php?action=login' );
		$tpl_uploader->setr ( 'content', $tpl_message, 1 );
	}
	break;

	case 'confirm_email_change':
	{
		$userid = abs ( (int)gpc ( 'userid', 'G', 0 ) );
		$code   = gpc ( 'code', 'G', '' );
		$result = 'none';

		$mysqlDB->query ( sprintf ( "SELECT userid,username,email,xtr_new_email_address FROM uploader_users WHERE userid=%d AND xtr_change_email_code='%s' LIMIT 1;", $userid, md5 ( $code ) ) );

		if ( $mysqlDB->getRowCount() )
		{
			$userinfo = $mysqlDB->getAssoc();
			$mysqlDB->free();
			if ( $userinfo['xtr_new_email_address'] == '' ) exit ( 'Unexpected error, xtr_new_email_address is blank in account.php line ' . __LINE__ );
			$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET xtr_new_email_address='', email='%s', xtr_change_email_code='%s' WHERE userid=%d;", $userinfo['xtr_new_email_address'], md5 ( get_rand ( 100 ) ), $userinfo['userid'] ) );
			$result = $lang_account['email_changed'];
		}
		else $result = $lang_account['email_invalid_code'];
		// show result
		$tpl_message->set ( 'message', $result );
		$tpl_uploader->setr ( 'content', $tpl_message, 1 );
	}
	break;

	case 'login':
	{
		// already logged in? quit
		if ( $UPL['USER']['logged_in'] )
			exit ( go_to ( UPLOADER_URL ) );

		// start up
		$tpl_login = new Template ( TPL_DIR . 'tpl_login.php' );
		$err = 'none';

		// get inputs
		$username = gpc ( 'username', 'PG', gpc ( 'uploader_username', 'C' ) );
		$password = gpc ( 'password', 'P' );
		$remember = gpc ( 'remember', 'P', true );
		$return_to= gpc ( 'return', 'GP', gpc ( 'return_url', 'GP', '' ) );

		// to templates
		$tpl_login->set ( 'username', $username );
		$tpl_login->set ( 'password', $password );
		$tpl_login->set ( 'remember', $remember );
		$tpl_login->set ( 'return_url', $return_to );

		if ( $task == 'login' )
		{
			// Do login
			if ( $username == '' || $password == '' )
			{
				$err = $lang_account['log_no_input'];
			}
			else
			{
				// Find user
				$mysqlDB->query ( sprintf ( "SELECT userid,username,password FROM uploader_users WHERE username='%s' LIMIT 1;", $mysqlDB->escape ( $username ) ) );

				if ( $mysqlDB->getRowCount() )
				{
					$userinfo = $mysqlDB->getAssoc();

					if ( $userinfo['password'] != md5 ( $password ) )
					{
						$err = $lang_account['log_bad_password'];
					}
					else
					{
						// Success
						$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET last_login_ip='%s', last_login_time=%d WHERE userid=%d;", $_SERVER['REMOTE_ADDR'], time ( ), $userinfo['userid'] ) );
						// set cookie and send user to myfiles
						$time_out = $remember ? time ( ) + 2592000 : 0;
						$domain   = $UPL['CONFIGS']['COOKIE_DOMAIN'];
						setcookie ( 'uploader_username', $userinfo['username'], $time_out, '/', $domain, 0 );
						setcookie ( 'uploader_userid',   $userinfo['userid'], $time_out, '/', $domain, 0 );
						setcookie ( 'uploader_password', md5 ( $password ), $time_out, '/', $domain, 0 );
					}
				}
				else
				{
					$err = $lang_account['log_bad_user'];
				}
			}
			// login success?
			if ( $err == 'none' )
			{
				if($return_to != '') go_to($return_to);
				else go_to( MOD_REWRITE ? UPLOADER_URL . 'myfiles' : 'myfiles.php' );
			}
			else
			{
				// show login form with errors
				$tpl_error->set ( 'error', $err );
				$tpl_login->setr( 'error', $tpl_error );
				$tpl_uploader->set ( array ( 'page_title' => $lang_titles['log_title1'], 'content' => &$tpl_login ), '', 1 );
			}
		}
		else
		{
			// show login form
			$tpl_uploader->set ( array ( 'page_title' => $lang_titles['log_title2'], 'content' => &$tpl_login ), '', 1 );
		}
	}
	break;

	case 'logout':
	{
		// clear all cookies
		$dm = $UPL['CONFIGS']['COOKIE_DOMAIN'];
		setcookie ( 'uploader_username', '', -1, '/', $dm );
		setcookie ( 'uploader_userid', '', -1, '/', $dm );
		setcookie ( 'uploader_password', '', -1, '/', $dm );
		header ( 'Location: ' .(  MOD_REWRITE ? UPLOADER_URL . 'login' : 'account.php?action=login') );
	}
	break;
}
?>