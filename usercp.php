<?php
define ( 'USERCP_PHP', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_usercp.inc.php';
$tpl_usercp = new Template ( TPL_DIR . 'tpl_usercp.php' );
$tpl_error = new Template ( TPL_DIR . 'tpl_error.php' );


$tpl_usercp->set ( 'action', $action );
$tpl_usercp->set ( 'new_messages_count', $UPL['USER']['unread_messages'] );
$tpl_uploader->set ( 'page_title', $lang_titles['ucp_title1'] );


// what to do?
if ( $action == 'checkname' )
{
	$username = gpc ( 'username', 'G', '' );
	$result = 'none';

	if ( $username == '' ) $result = $lang_usercp['ucp_no_user'];
	else
	{
		$r = $mysqlDB->query ( sprintf ( "SELECT users.*, COUNT(messages.messageid) AS messages_count FROM uploader_users AS users LEFT JOIN uploader_messages AS messages USING(userid) WHERE users.username='%s' GROUP BY messages.userid ;", $mysqlDB->escape($username) ) );
		if ( !$r ) exit ( $mysqlDB->error() );

		if ( $mysqlDB->getRowCount () )
		{
			$userinfo = $mysqlDB->getAssoc();
			$mysqlDB->free();
			// does receipient want to receive messages?
			if ( $userinfo['pref_accepts_pm'] )
			{
				if ( $userinfo['max_messages'] == 0 || $userinfo['messages_count'] < $userinfo['max_messages'] )
				{
					$result = 'OK';
				}
				else $result = $lang_usercp['ucp_inbox_full'];
			}
			else $result = parse ( $lang_usercp['ucp_message_denied'], '{username}', $username );
		}
		else $result = $lang_usercp['ucp_bad_user'];
	}
	print $result;
}
elseif ( $action == 'editprofile' )
{
	if ( $task == 'save' )
	{
		if ( $demo ) exit ( 'Demo only!' );
		$current_password 	= gpc ( 'current_password', 'P' );
		$new_password 		= gpc ( 'new_password', 'P' );
		$new_password_conf  = gpc ( 'new_password_confirm', 'P' );
		$email 				= trim ( gpc ( 'email', 'P' ) );
		$error 				= 'none';

		if ( $current_password == '' )
		{
			$error = $lang_usercp['ucp_no_pass'];
		}
		elseif ( md5 ( $current_password ) != $UPL['USER']['password'] )
		{
			$error = $lang_usercp['ucp_incorrect_pass'];
		}
		else
		{
			// new password?
			if ( ( $new_password != '' ) )
			{
				if ( $new_password != $new_password_conf )
				{
					$error = $lang_usercp['ucp_pass_no_match'];
				}
				else
				{
					$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET password='%s' WHERE userid=%d;", md5 ( $new_password ), $UPL['USER']['userid'] ) );
					setcookie ( 'uploader_password', md5 ( $new_password ), time()+2592000, '/', $UPL['CONFIGS']['COOKIE_DOMAIN'], 0 );
				}
			}

			// new email?
			if ( $email != '' && strcasecmp ( $email, $UPL['USER']['email'] ) != 0 )
			{
				if ( !preg_match ( "#([a-z0-9_]+)\@([a-z0-9\-])#i", $email ) )
				{
					$error = $lang_usercp['ucp_bad_email'];
				}
				else
				{
					// create email change code
					$change_code = get_rand ( 32 );
					$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET xtr_change_email_code='%s', xtr_new_email_address='%s' WHERE userid=%d;", md5 ( $change_code ), $mysqlDB->escape ( $email ), $UPL['USER']['userid'] ) );

					$reset_url = UPLOADER_URL . ( MOD_REWRITE ? 'account' : 'account.php' ) . '?action=confirm_email_change&userid=' . $USER['userid'] . '&code=' . $change_code;

					$tpl_email_header = new Template ( TPL_DIR . 'tpl_email_header.php' );
					$tpl_email_footer = new Template ( TPL_DIR . 'tpl_email_footer.php' );
					$tpl_email_change = new TEmplate ( TPL_DIR . 'tpl_email_changeemail.php' );

					$tpl_email_header->set ( 'username', $USER['username'] );
					$tpl_email_change->set ( 'ip_address', $_SERVER['REMOTE_ADDR'] );
					$tpl_email_change->set ( 'reset_url', $reset_url );

					$message = $tpl_email_header->display ( true );
					$message .= $tpl_email_change->display ( true );
					$message .= $tpl_email_footer->display ( true );

					send_email ( $UPL['USER']['email'], $lang_usercp['ucp_email_subject'], $message, 'From: Uploader Admin <' . $UPL['SETTINGS']['email'] . '>' );
				}
			}
		}

		if ( $error == 'none' )
		{
			// show result
			$tpl_message->set ( 'message', $lang_usercp['ucp_saved'] );
			$tpl_message->set ( 'back_url', 'usercp.php' );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
		}
		else
		{
			// show form with error
			$tpl_error->set ( 'error', $error );
			$tpl_usercp->setr ( 'error', $tpl_error );
			$tpl_uploader->setr ( 'content', $tpl_usercp );
			$tpl_uploader->set ( 'page_title', $lang_misc['error'], 1 );
		}
	}
	else
	{
		// show form
		$tpl_uploader->set ( 'content', $tpl_usercp, 1 );
	}
}
elseif ( $action == 'editoptions' )
{
	if ( $task == 'save' )
	{
		$pemail  = gpc ( 'pemail',  'P', 0 );
		$pmessage= gpc ( 'pmessage', 'P', 0 );
		$mysqlDB->query ( sprintf ( "UPDATE uploader_users SET pref_accepts_pm=%d, pref_show_email=%d WHERE userid=%d;", (int)$pmessage, (int)$pemail, $UPL['USER']['userid'] ) );
		// back to preferences
		header ( 'Location: ' . $_SERVER['HTTP_REFERER'] );
	}
	else
	{
		// show form
		$inf = array
		(
			'pemail'	=> $UPL['USER']['pref_show_email'],
			'pmessage'	=> $UPL['USER']['pref_accepts_pm'],
		);
		$tpl_usercp->set ( 'user', $inf);
		$tpl_uploader->set ( 'content', $tpl_usercp, 1 );
	}
}
elseif ( $action == 'pm' )
{
	// load PM
	$messages = array ( );
	$mysqlDB->query ( sprintf ( "SELECT * FROM uploader_messages WHERE userid=%d ORDER BY date DESC;", $UPL['USER']['userid'] ) );
	if ( $mysqlDB->getRowCount() )
	{
		while ( false !== ( $pm = $mysqlDB->getAssoc() ) )
		{
			$pm['read_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'usercp' : 'usercp.php' ) . '?action=readpm&amp;id=' . $pm['messageid'];
			$pm['subject'] 	= $pm['subject'];
			$messages[] 	= $pm;
		}
	}
	// display them
	$tpl_usercp->setr ( 'messages', $messages );
	$tpl_uploader->setr ( 'content', $tpl_usercp, 1 );
}
elseif ( $action == 'readpm' )
{
	$pmid = abs ( (int)gpc ( 'id', 'G', 0 ) );

	$mysqlDB->query ( "SELECT * FROM uploader_messages WHERE messageid={$pmid};" );

	if ( $mysqlDB->getRowCount() )
	{
		$message = $mysqlDB->getAssoc();
		// mark message as Read
		$mysqlDB->query ( "UPDATE uploader_messages SET is_read=1 WHERE messageid={$pmid};" );
		// remove HTML
		$message['subject'] =  $message['subject'];
		$message['message'] =  $message['message'];
		// Remove long words
		$message['message'] = nl2br ( entities ( $message['message'] ) );
		$message['message'] = preg_replace ( '#([^\s\n\<\>]{80,})#ie', 'wordwrap ( \'$1\', 80, "<br />", 1 )', $message['message']  );
		$message['message'] = str_replace ( "\t", '    ', $message['message'] );
		$message['message'] = str_replace ( '  ', ' &nbsp;', $message['message'] );
		$message['message'] = parse_bb ( $message['message'] );
		$message['date'] = date ( $UPL['CONFIGS']['TIME_FORMAT'], $message['date'] );
		$message['send_profile_url'] = MOD_REWRITE ? UPLOADER_URL . 'info/' . $message['from_username'] : UPLOADER_URL . 'browse.php?action=info&userid=' . $message['from_id'];
		$message['reply_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'usercp' : 'usercp.php' ) . '?action=sendpm&amp;userid=' . $message['from_id'] . '&amp;replyto=' . $pmid;
		$message['delete_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'usercp' : 'usercp.php' ) . '?action=pmactions&amp;task=delete&amp;messages[]=' . $pmid;

		$tpl_usercp->setr ( 'message', $message );
		$tpl_uploader->setr ( 'content', $tpl_usercp, 1 );
	}
	else
	{
		$tpl_message->set ( 'message', $lang_usercp['ucp_bad_id'] );
		$tpl_uploader->set ( array ( 'page_title' => $lang_misc['error'], 'content' => &$tpl_message ), '', 1 );
	}
}
elseif ( $action == 'sendpm' )
{

	// get user inputs
	$userid 	= abs ( (int)gpc ( 'userid', 'G', 0 ) );
	$username   = gpc ( 'username', 'G', '' );
	$replyto 	= abs ( (int)gpc ( 'replyto', 'G', 0 ) );
	$newpm 		= gpc ( 'newpm', 'P', array ( 'to' => '', 'subject' => '', 'message' => '' ) );
	$error		= 'none';

	$newpm['to'] 		= trim ( $newpm['to'] );
	$newpm['subject'] 	= trim ( $newpm['subject'] );
	$newpm['message'] 	= trim ( $newpm['message'] );

	if ( $replyto > 0 )
	{
		$mysqlDB->query ( "SELECT * FROM uploader_messages WHERE messageid={$replyto} LIMIT 1;" );

		if ( $mysqlDB->getRowCount() )
		{
			$message = $mysqlDB->getAssoc();
			$mysqlDB->free();

			$newpm['subject'] 	= 'Re: ' . entities ( $message['subject'] );
			$newpm['to'] 		= $message['from_username'];
			$newpm['message']   = sprintf ( "%s (%s):\n| %s", $message['from_username'], date ( $UPL['CONFIGS']['TIME_FORMAT2'], $message['date'] ), str_replace ( "\n", "\n| ", $message['message'] ) );
		}
	}
	elseif ( $userid || $username != '' )
	{
		if ( $userid ) $mysqlDB->query ( "SELECT * FROM uploader_users WHERE userid={$userid} LIMIT 1;" );
		else $mysqlDB->query ( sprintf ( "SELECT * FROM uploader_users WHERE username='%s' LIMIT 1;", $mysqlDB->escape ( $username ) ) );

		if ( $mysqlDB->getRowCount() )
		{
			$userinfo = $mysqlDB->getAssoc();
			$newpm['to'] = $userinfo['username'];
			$mysqlDB->free();
		}
	}

    // to template
	$tpl_usercp->setr ( 'newpm', $newpm );

	if ( $task == 'sendpm' )
	{
		if ( $newpm['to'] == '' ) $error = $lang_usercp['ucp_no_user'];
		elseif ( $newpm['subject'] == '' ) $error = $lang_usercp['ucp_no_subject'];
		elseif ( strlen ( $newpm['subject'] ) > 255 ) $error = $lang_usercp['ucp_long_subject'];
		elseif ( $newpm['message'] == '' ) $error = $lang_usercp['ucp_no_message'];
		elseif ( strlen ( $newpm['message'] ) > 5000 ) $error = $lang_usercp['ucp_long_message'];
		else
		{
			$r = $mysqlDB->query ( sprintf ( "SELECT users.*, COUNT(messages.messageid) AS messages_count FROM uploader_users AS users LEFT JOIN uploader_messages AS messages USING(userid) WHERE users.username='%s' GROUP BY messages.userid ;", $mysqlDB->escape($newpm['to']) ) );
			if ( !$r ) exit ( $mysqlDB->error() );

			if ( $mysqlDB->getRowCount () )
			{
				$userinfo = $mysqlDB->getAssoc();
				$mysqlDB->free();
				// does receipient want to receive messages?
				if ( $userinfo['pref_accepts_pm'] )
				{
					if ( $userinfo['max_messages'] == 0 || $userinfo['messages_count'] < $userinfo['max_messages'] )
					{
						$msg = array
						(
							'messageid' 	=> NULL,
							'userid' 		=> $userinfo['userid'],
							'from_id' 		=> $UPL['USER']['userid'],
							'from_username' => $UPL['USER']['username'],
							'subject' 		=> $mysqlDB->escape ( $newpm['subject'] ),
							'message'		=> $mysqlDB->escape ( $newpm['message'] ),
							'folder'		=> 0, // not used for now
							'date'			=> time(),
							'is_read'		=> 0,
						);
						if ( !$mysqlDB->query ( "INSERT INTO uploader_messages SET " . $mysqlDB->buildInsertStatement ( $msg ) . ";" ) ) exit ( $mysqlDB->error (__LINE__) );
					}
					else $error = $lang_usercp['ucp_inbox_full'];
				}
				else $error = parse ( $lang_usercp['ucp_message_denied'], '{username}', $newpm['to'] );
			}
			else $error = $lang_usercp['ucp_bad_user'];
		}

		// any errors?
		if ( $error == 'none' )
		{
			// show result
			$tpl_message->set ( array ( 'message' => $lang_usercp['ucp_message_sent'], 'back_url' => ( UPLOADER_URL . ( MOD_REWRITE ? 'usercp' : 'usercp.php' ) ) ) );
			$tpl_uploader->setr ( 'content', $tpl_message, 1 );
		}
		else
		{
			// show form with error
			$tpl_error->set ( 'error', $error );
			$tpl_usercp->setr ( 'error', $tpl_error );
			$tpl_uploader->setr ( 'content', $tpl_usercp );
			$tpl_uploader->set ( 'page_title', $lang_misc['error'], 1 );
		}
	}
	else
	{
		// show sendPM form
		$tpl_usercp->setr ( 'messages', $msg );
		$tpl_uploader->setr ( 'content', $tpl_usercp, 1 );
	}
}
elseif ( $action == 'pmactions' )
{
	$messages = gpc ( 'messages', 'PG', array ( ) );
	$count = is_array ( $messages ) ? count ( $messages ) : 0;
	if ( $count )
	{
		for ( $i = 0; $i < $count; ++$i ) $messages[$i] = 'messageid='.(int)$messages[$i];

		switch ( $task )
		{
			case 'mark_unread':
			case 'mark_read':
			{
				$mysqlDB->query ( "UPDATE uploader_messages SET is_read=" . ( $task == 'mark_unread' ? 0 : 1 ) . " WHERE userid={$UPL['USER']['userid']} AND ( " . implode ( ' OR ', $messages ) . ' );' );
			}
			break;
			case 'delete':
			{
				$mysqlDB->query ( "DELETE FROM uploader_messages WHERE userid={$UPL['USER']['userid']} AND ( " . implode ( ' OR ', $messages ) . ' );' );
			}
			break;
		}
	}
	go_to('usercp.php?action=pm');
}
else
{
	$inf = array
	(
		'name'		 		=> $UPL['USER']['username'],
		'email'		 		=> $UPL['USER']['email'],
		'level'		 		=> $UPL['USER']['level'] == LEVEL_ADMIN ? 'Admin' : ( $UPL['USER']['level'] == LEVEL_MODERATOR ? 'Moderator' : 'Normal' ),
		'reg_date'	 		=> date ( $UPL['CONFIGS']['TIME_FORMAT'], $UPL['USER']['reg_date'] ),
		'bw_last_reset'  	=> round ( ( time ( ) - $UPL['USER']['bw_reset_last'] ) / 86400, 1 ),
		'bw_reset_period'	=> $UPL['USER']['bw_reset_period'],
		'bw_auto_reset'		=> $UPL['USER']['bw_reset_auto'],
		'bw_max'			=> get_size ( $UPL['USER']['bw_max'], 'MB' ),
		'max_storage'		=> get_size ( $UPL['USER']['fl_max_storage'], 'MB' ),
		'pemail'			=> $UPL['USER']['pref_show_email'],
		'pmessage'			=> $UPL['USER']['pref_accepts_pm'],
	);
	$tpl_usercp->set ( 'user', $inf);
	// display usercp main page
	$tpl_uploader->set ( 'content', $tpl_usercp, 1 );
}
?>