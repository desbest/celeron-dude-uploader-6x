<?php
define ( 'MYFILES_PHP', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_myfiles.inc.php';
require_once 'includes/functions_types.inc.php';
require_once 'includes/functions_userfiles.inc.php';

$tpl_myfiles = new Template ( TPL_DIR .  '/tpl_myfiles.php' );
$tpl_error = new Template ( TPL_DIR .  '/tpl_error.php' );

$userid = $UPL['USER']['userid'];
$username = $UPL['USER']['userid'];

// wut doing?
switch ( $action )
{
	case 'quickrename':
	{
		if ( !count ( $_POST ) ) exit;
		$file_id = (int)gpc ( 'file_id', 'P', 0 );
		$newname = trim ( gpc ( 'newname', 'P', '' ) );
		$error = 'none';

		if ( $UPL['USER']['fl_rename_permission'] == 0 )
			$error = 'NO_RENAME';
		else
		{
			// check if file exists
			$file = get_user_file ( $userid, $file_id );
			$old_extension = get_extension ( $file['file_name'] );
			$old_name = $file['file_name'];

			if(!count($file))
				$error = 'INVALID_FILE';
			elseif($UPL['USER']['fl_rename_permission'] == 1 && ($old_extension != get_extension($newname)))
				$error = 'NO_RENAME_EXTENSION';
			else
			{
				// check new name
				if ( $newname == '' ) $error = 'EMPTY_NAME';
				elseif ( strlen ( $newname ) < $UPL['CONFIGS']['FILE_MIN_LEN'] ) $error = 'SHORT_NAME';
				elseif ( strlen ( $newname ) > $UPL['CONFIGS']['FILE_MAX_LEN'] ) $error = 'LONG_NAME';
			}
		}

		if ( $error == 'none' )
		{
			// save
			$mysqlDB->query ( "UPDATE uploader_userfiles SET file_name='" . $mysqlDB->escape ( $newname ) . "' WHERE userid={$userid} AND file_id=$file_id" );

			// process the file again
			$file = get_user_file ( $userid, $file_id );
			processFile ( $file );
			unset($file['file_key'], $file['file_location']);
			$response = array ( 'result' => 'success', 'file' => $file );
			// log
			if ( $UPL['SETTINGS']['log'] == 2 ) upload_log ( sprintf ( 'Renamed: %s to %s', $old_name, $newname ) );
		}
		else $response = array ( 'result' => 'fail', 'message' => $error );

		print json_encode ( $response );
	}
	break;

	case 'move':
	{
		$current_folder_id = (int)gpc ( 'current_folder_id', 'P', 0 );
		$target_folder_id = (int)gpc ( 'move_to_folder_id', 'P', 0 );
		$file_ids = gpc ( 'file_ids', 'P', array() );
		if ( count ( $file_ids ) && $target_folder_id && $target_folder_id != $current_folder_id )
		{
			// verify target folder
			$target_folder = get_user_folders ( $userid, $target_folder_id );
			if ( !count ( $target_folder ) )
			{
				$tpl_message->set ( 'error', $lang_myfiles['folder_invalid'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}
			// make list of files and query
			$file_ids = array_values ( $file_ids );
			for ( $i = 0; $i < count ( $file_ids ); ++$i ) $file_ids[$i] =  (int)$file_ids[$i];
			$list = implode ( ',', $file_ids );
			$images_only = $target_folder['folder_isgallery'] ? 'AND file_isimage=1' : '';

			// move according to target folder
			if ( !$mysqlDB->query ( "UPDATE uploader_userfiles SET folder_id={$target_folder['folder_id']} WHERE userid={$userid} $images_only AND file_id IN ($list)" ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
		}
		go_to(UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php').'?folder_id='.$current_folder_id);
	}
	break;

	case 'delete':
	{
		$file_ids = gpc ( 'file_ids', 'P', array() );
		$current_folder_id = (int)gpc ( 'current_folder_id', 'P', 0 );
		if ( count ( $file_ids ) )
		{
			$file_ids = array_values ( $file_ids );
			for ( $i = 0; $i < count ( $file_ids ); ++$i ) $file_ids[$i] =  (int)$file_ids[$i];
			$list = implode ( ',', $file_ids );

			// get location of files in the folder
			$mysqlDB->query ( "SELECT file_location, file_name FROM uploader_userfiles WHERE userid={$userid} AND file_id IN ($list)" );

			if ( $mysqlDB->getRowCount() )
			{
				while ( false !== ( $file = $mysqlDB->getAssoc() ) )
				{
					delete_file ( $file['file_location'] );

					// log delete?
					if ( $UPL['SETTINGS']['log'] == 2 ) upload_log ( sprintf ( 'Deleted: %s', entities($file['file_name']) ) );
				}
				$mysqlDB->free();
			}
			// remove files from database
			$mysqlDB->query ( "DELETE FROM uploader_userfiles WHERE userid={$userid} AND file_id IN ($list)" );

			// remove comments on files from database
			if ( !$mysqlDB->query ( "DELETE FROM uploader_usercomments WHERE object_id IN ($list) AND comment_type=" . COMMENT_FILE ) ) exit ( $mysqlDB->error(__LINE__, __FILE__) );
		}
		go_to(UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php').'?folder_id='.$current_folder_id);
	}
	break;

	case 'get_code':
	{
		$file_ids = gpc ( 'file_ids', 'P', array() );
		$current_folder_id = (int)gpc ( 'current_folder_id', 'P', 0 );

		if ( count ( $file_ids ) )
		{
			$file_ids = array_values ( $file_ids );
			for ( $i = 0; $i < count ( $file_ids ); ++$i ) $file_ids[$i] =  (int)$file_ids[$i];
			$list = implode ( ',', $file_ids );

			// get files info
			$files = array();
			$mysqlDB->query ( "SELECT * FROM uploader_userfiles WHERE userid={$userid} AND file_id IN($list)" );
			if ( $mysqlDB->getRowCount() )
			{
				while ( false !== ( $file = $mysqlDB->getAssoc() ) )
				{
					processFile ( $file );
					$files[] = array
					(
						'name' 			=> $file['file_name'],
						'is_image' 		=> (int)$file['file_isimage'],
						'url' 			=> $file['url'],
						'url' 			=> $file['file_isimage'] ? $file['fullsize_url'] : $file['url'],
						'direct_url' 		=> $file['direct_url'],
						'small_thumb_url' 	=> $file['small_thumb_url']
					);
				}
				$mysqlDB->free();
			}
			$tpl_img = new Template ( TPL_DIR . 'tpl_img.php' );
			$tpl_img->set ( 'files', $files );
			$tpl_img->set ( 'back_url', UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php').'?folder_id='.$current_folder_id );
			$tpl_uploader->set ( 'content', $tpl_img, 1 );
		}
		else go_to(UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php').'?folder_id='.$current_folder_id);
	}
	break;

	default:
	{
		$files_per_page = 20;

		$folder_id = (int)gpc ( 'folder_id', 'G', 0 );
		$sort = trim ( gpc ( 'sort', 'G', 'date_desc' ) );
		$current_page = abs ( (int)gpc ( 'page', 'G', 1 ) );

		// Get all user folders
		$user_folders = get_user_folders ( $userid, 0, 1 );
		// no folder selected, start at home folder
		if ( !$folder_id ) $folder_id = $user_folders[0]['folder_id'];
		$current_folder = array();
		$count = count ( $user_folders );
		for ( $i = 0; $i < $count; ++$i )
		{
			processFolder ( $user_folders[$i] );
			if ( $user_folders[$i]['folder_id'] == $folder_id ) $current_folder = $user_folders[$i];
		}

		// current folder exists?
		if ( !count ( $current_folder ) )
		{
			$tpl_message->set ( 'message', $lang_myfiles['folder_invalid'] );
			$tpl_uploader->set ( 'page_title', $lang_myfiles['title_invalid_folder'] );
			$tpl_uploader->set ( 'content', $tpl_message, 1 );
			exit;
		}

		// Current folder
		processFolder ( $current_folder );
		$current_folder['upload_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'upload' : 'upload.php' ) . '?folder_id=' . $current_folder['folder_id'];
		$current_folder['tog_perm_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'folders' : 'folders.php' ) . '?action=togperm&amp;folder_id=' . $current_folder['folder_id'];

		// Get user files in the current folder
		$total_pages = ceil ( $current_folder['files_count'] / $files_per_page );
		if ( $current_page < 1 ) $current_page = 1;
		if ( $current_page > $total_pages ) $current_page = $total_pages;
		$start_offset = $current_page > 1 ? ( $current_page - 1 ) * $files_per_page : 0;

		// sortings
		if ( !strstr ( $sort, '_' ) ) $sort = 'date_desc';
		list ( $sort_by, $sort_order ) = explode ( '_', $sort );
		switch ( $sort_by )
		{
			case 'type': $sort_column = 'file_extension'; break;
			case 'name': $sort_column = 'file_name'; break;
			case 'size': $sort_column = 'file_size'; break;
			case 'date': default: $sort_column = 'file_id'; $sort_by = 'date'; break;
		}
		if ( $sort_order != 'asc' && $sort_order != 'desc' ) $sort_order = 'asc';

		// get files and process urls and such
		$user_files = get_user_files_in_folder ( $userid, $folder_id, $start_offset, $files_per_page, $sort_column, $sort_order );
		processFiles ( $user_files );

		// usage statistics
		$storage_used = 0;
		$mysqlDB->query("SELECT SUM(file_size) AS total FROM uploader_userfiles WHERE userid={$userid}");
		if($mysqlDB->getRowCount())
		{
			$result = $mysqlDB->getAssoc();
			$storage_used = $result['total'];
			$mysqlDB->free();
		}
		$stats = array
		(
			'max_storage'			=> $UPL['USER']['fl_max_storage'] * 1024 * 1024,
			'max_bandwidth'			=> $UPL['USER']['bw_max'] * 1024,
			'bandwidth_used'		=> $UPL['USER']['bw_used'],
			'storage_used'			=> $storage_used,
		);
		$stats['bandwidth_percentage'] = ceil ( $stats['max_bandwidth'] ? ( $stats['bandwidth_used'] / $stats['max_bandwidth'] ) * 100 : 0 );
		$stats['storage_percentage'] = ceil ( $stats['max_storage'] ? ( $stats['storage_used'] / $stats['max_storage'] ) * 100 : 0 );

		$next_page_url = UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . '?folder_id=' . $folder_id . '&amp;page=' . ( $current_page + 1 ) . ( '&amp;sort=' . $sort_by . '_' . $sort_order );
		$prev_page_url = UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . '?folder_id=' . $folder_id . '&amp;page=' . ( $current_page - 1 <= 0 ? 1 : $current_page - 1 ) . ( '&amp;sort=' . $sort_by . '_' . $sort_order );


		$base_url = UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . '?folder_id=' . $folder_id;
		$sort_url = array
		(
			'type'	=> $base_url . '&amp;sort=type_' . ( $sort_by == 'type' && $sort_order == 'asc' ? 'desc' : 'asc' ),
			'name'	=> $base_url . '&amp;sort=name_' . ( $sort_by == 'name' && $sort_order == 'asc' ? 'desc' : 'asc' ),
			'size'	=> $base_url . '&amp;sort=size_' . ( $sort_by == 'size' && $sort_order == 'asc' ? 'desc' : 'asc' ),
			'date'	=> $base_url . '&amp;sort=date_' . ( $sort_by == 'date' && $sort_order == 'asc' ? 'desc' : 'asc' ),
		);

		// Display
		$tpl_vars = array
		(
			'user_files'		=> $user_files,
			'user_folders'		=> $user_folders,
			'current_folder'	=> $current_folder,
			'stats'			=> $stats,
			'per_page'		=> $files_per_page,
			'file_start'		=> $start_offset + 1,
			'file_end'		=> $start_offset + count ( $user_files ),
			'next_page_url'		=> $next_page_url,
			'prev_page_url'		=> $prev_page_url,
			'sort_by'		=> $sort_by,
			'sort_order'		=> $sort_order,
			'sort_url'		=> $sort_url,
			'current_page'		=> $current_page,
			'total_pages'		=> $total_pages,
		);
		$tpl_myfiles->set ( $tpl_vars );

		$tpl_uploader->set ( array ( 'content' => &$tpl_myfiles, 'page_title' => $lang_myfiles['title_default'] ), '', 1 );
	}
}
?>