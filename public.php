<?php
define('UPLOAD_PHP',1);
define('NO_AUTH_CHECK',1);
require_once 'includes/commons.inc.php';
require_once 'includes/messages_upload.inc.php';
require_once 'includes/pub_settings.inc.php';

if ( !$PUB['enabled'] )
{
	$tpl_message->set ( 'message', $lang_public['disabled'] );
	$tpl_uploader->set ( 'page_title', $lang_public['ptitle1'] );
	$tpl_uploader->set ( 'content', $tpl_message, 1 );
	exit;
}

$upload_max = get_byte_value ( ini_get ( 'upload_max_filesize' ) );
$post_max = get_byte_value ( ini_get ( 'post_max_size' ) );
$max_php_file_size = min ( $upload_max, $post_max );
$max_filesize = $PUB['max_file_size'] > 0 ? $PUB['max_file_size'] * 1024 : $max_php_file_size;

$restrictions = array
(
	'max_file_size'	=> $max_filesize,
	'allowed_types'	=> $PUB['images_only'] ? 'jpg,jpeg,gif,png' : $PUB['allowed_filetypes'],
	'images_only'	=> $PUB['images_only']
);

if ( $action == 'upload' )
{
	$ban = isBanned();
	if ( is_array ( $ban ) && $ban['public'] )
	{
		$tpl_message->set ( 'message', $lang_misc['ip_banned'] );
		$tpl_uploader->set ( 'content', $tpl_message, true );
		exit;
	}

	require_once 'includes/functions_upload.inc.php';
	require_once 'includes/image.class.php';
	require_once 'includes/upload.class.php';

	// get inputs
	$upload_set = gpc ( 'upload', 'P', array ( 'public' => 1, 'name' => '', 'description' => '' ) );
	$upload_set['public'] = isset ( $upload_set['public'] ) ? (int)$upload_set['public'] : 0;
	$upload_set['description'] = trim ( $upload_set['description'] );

	// detect upload failure due to large files
	if ( !count ( $_POST ) && !count ( $_FILES ) )
	{
		$tpl_message->set ( 'message', parse ( $lang_upload['upl_max_php_size'], '{max}', get_size ( $max_php_file_size, 'B', 0 ) ) );
		$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'upload' : 'upload.php' ) );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
		exit;
	}

	// initialize
	$uploaded = array();
	$errors = array();
	$public_dir = str_replace ( '\\', '/', realpath ( $PUB['files_dir'] ) ) . '/';

	// determine file location
	$dir_limit = 32000; //  max sub directories in a directory
	$paths = array();
	$paths[] = mt_rand ( 0, $dir_limit );
	$paths[] = mt_rand ( 0, $dir_limit );
	$paths[] = mt_rand ( 0, $dir_limit );
	$paths[] = mt_rand ( 0, $dir_limit );
	$paths[] = mt_rand ( 0, $dir_limit );

	$path = implode ( '/', $paths );
	if ( !is_dir ( $public_dir . $path ) )
		make_dir_recursive ( $public_dir, $path );

	// upload validating class
	$upload = new upload();
	$upload->set ( 'allowed_extensions', ($PUB['allowed_filetypes']==''||$PUB['allowed_filetypes']=='any') ? '' : $PUB['allowed_filetypes'] );
	$upload->set ( 'unallowed_extensions', $UPL['SETTINGS']['filetypes'] );
	$upload->set ( 'max_file_size', $max_filesize );

	// Process batch zip files
	while ( list ( $name , $file ) = each ( $_FILES ) )
	{
		if ( ( $file['name'] == 'upload.zip' || $file['name'] == 'batch.zip' ) && is_zip ( $file['tmp_name'] ) && !isset($file['extracted_from_zip']) )
		{
			process_zip_file ( $file['tmp_name'] );
			unset ( $_FILES[$name] );
			@unlink ( $file['tmp_name'] );
		}
	}
	reset ( $_FILES );

	// process ALL files
	while ( list ( $name , $file ) = each ( $_FILES ) )
	{
		if ( get_magic_quotes_gpc ( ) )
			$file['name'] = stripslashes ( $file['name'] );

		$file['name'] = str_replace ( '/', '_', $file['name'] );
		if ( preg_match ( '#\.php\..+#i', $file['name'] ) )
			$file['name'] = str_replace ( '.php', '_php', $file['name'] );

		// validate file first
		$error = 'none';
		if ( !$upload->validate ( $file, $error ) )
		{
			// report error
			switch ( $error )
			{
				case 'extension_not_allowed': $errors[] = parse ( $lang_upload['upl_bad_extension'], '{filename}', $file['name'] ); break;
				case 'http_error': case 'file_not_uploaded': $errors[] = parse ( $lang_upload['upl_partial_upload'], '{filename}', $file['name'] ); break;
				case 'max_file_size': $errors[] = parse ( $lang_upload['upl_max_size'], array ( '{filename}' => $file['name'], '{max_file_size}' => get_size ( $max_filesize, 'B', 1 ) ) ); break;
				case 'file_empty': continue; break; // just in case
				default: $errors[] = 'Unknown error: ' . $error; break;
			}
			// get rid of uploaded file
			if ( @is_file ( $file['tmp_name'] ) ) @unlink ( $file['tmp_name'] );
			// skip this file
			continue;
		}

		$is_image = is_image ( $file['tmp_name'] );

		// image upload only?
		if ( $PUB['images_only'] && !$is_image )
		{
			$errors[] = parse ( $lang_upload['upl_not_image'], '{filename}', $file['name'] );
			continue;
		}

		// check image dimension
		if ( $is_image )
		{
			$image_info = @getimagesize ( $file['tmp_name'] );
			if ( is_array ( $image_info ) )
			{
				list ( $max_width, $max_height ) = explode ( 'x', $UPL['CONFIGS']['MAX_IMAGE_DIMENSION'] );
				if ( $image_info[0] > $max_width || $image_info[1] > $max_height )
				{
					$errors[] = parse ( $lang_upload['upl_large_image'], array ( '{filename}' => $file['name'], '{max_dimension}' => $UPL['CONFIGS']['MAX_IMAGE_DIMENSION'] ) );
					continue;
				}
			}
			else continue;
		}


		// determine unique file name
		do
		{
			clearstatcache();
			$name = uniqid ( '', true );
			$file_location = $public_dir . $path . '/' . $name;
		}
		while ( is_file ( $file_location ) );

		//  move file to that location
		if ( !@rename ( $file['tmp_name'], $file_location ) && !@copy ( $file['tmp_name'], $file_location ) )
		{
			exit ( 'Fatal error (line ' . __LINE__ . ': Could not move or copy uploaded files to the user files directory. Check directory permission!' );
		}

		// file is OK, add it
		$insert = array
		(
			'file_id'		=> NULL,
			'file_name'		=> $mysqlDB->escape ( $file['name'] ),
			'file_location'	=> $file_location,
			'file_size'		=> filesize ( $file_location ),
			'file_views'	=> 0,
			'file_lastview'	=> time(),
			'file_isimage'	=> (int)$is_image,
			'upload_id'		=> 0
		);

		if ( $mysqlDB-> query ( 'INSERT INTO uploader_pfiles SET ' . $mysqlDB->buildInsertStatement ( $insert ) ) )
		{
			$file_id = $mysqlDB->getInsertId();

			//log upload
			if ( $PUB['log_upload'] )
				public_upload_log ( "uploaded {$file['name']}, file_id = $file_id" );

			if ( $is_image )
			{
				$image = new image ( $file_location );
				if ( $image )
				{
					// watermark?
					if ( $PUB['wm'] )
					{
						list ( $hor, $ver ) = explode ( ',', $PUB['wm_pos'] );
						$copy = new image ( $image );
						$copy->watermark ( $PUB['wm_path'], $hor, $ver );
						$copy->export ( $file_location );
						$copy->destroy();
						unset($copy);
					}

					// large version
					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['LARGE_THUMB'] );
					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, false );
						$image->export ( $file_location . '_large' );
					}

					// small version
					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SMALL_THUMB'] );
					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, false );
						if($PUB['image_info'])
						{
							$copy = new image ( $image );
							$copy->addInfo();
							$copy->export ( $file_location . '_small' );
							$copy->destroy();
							unset($copy);
						}
						else $image->export ( $file_location . '_small' );
					}

					// square version
					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SQUARE_THUMB'] );
					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, true );
						$image->export ( $file_location . '_square' );
					}
					$image->destroy();
				}
			}

			// add to list
			$insert['file_id'] = $file_id;
			processPublicFile ( $insert );
			$uploaded[] = $insert;
		}
		else
		{
			@unlink ( $file['tmp_name'] );
			delete_file($file_location);
			exit ( 'Fatal error: Could not insert into database. public.php line ' . __LINE__ );
		}
	}

	// add upload set
	if ( count ( $uploaded ) )
	{
		$file_ids = array();
		reset ( $uploaded );
		for ( $i = 0; $i < count ( $uploaded ); ++$i )
			$file_ids[] = (int)$uploaded[$i]['file_id'];
		$list = implode ( ',', $file_ids );

		// add upload set
		$upload_key = strtolower ( get_rand ( 10 ) );
		$insert = array
		(
			'upload_id'			=> NULL,
			'upload_name'		=> isset ( $upload_set['name'] ) ? $mysqlDB->escape ( substr ( $upload_set['name'], 0, 64 ) ) : '',
			'upload_date'		=> time(),
			'upload_description'=> isset ( $upload_set['description'] ) ? $mysqlDB->escape ( substr ( $upload_set['description'], 0, 255 ) ) : '',
			'upload_key'		=> $upload_key,
			'upload_ispublic'	=> (int)$upload_set['public'],
			'upload_ip'			=> $_SERVER['REMOTE_ADDR']
		);
		if ( !$mysqlDB->query ( 'INSERT INTO uploader_puploads SET ' . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

		$upload_id = $mysqlDB->getInsertId();

		// link files to set id (upload_id)
		if ( !$mysqlDB->query ( "UPDATE uploader_pfiles SET upload_id=$upload_id WHERE file_id IN ($list)" ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	}

	if ( !count ( $uploaded ) && !count ( $errors ) )
	{
		// redirect back to the upload form
		go_to ( UPLOADER_URL . ( MOD_REWRITE ? 'public' : 'public.php' ) );
	}
	else
	{
		if ( count ( $uploaded ) )
		{
			$upload_set = $insert;
			$upload_set['upload_id'] = $upload_id;
			processPublicSet ( $upload_set );
		}

		// show result page
		$tpl_result = new Template ( TPL_DIR . 'tpl_public_uploaded.php' );
		$tpl_result->set ( 'errors', $errors );
		$tpl_result->set ( 'uploaded', $uploaded );
		$tpl_result->set ( 'upload_set', $upload_set );
		$tpl_uploader->set ( 'content', $tpl_result, 1 );
	}
}
elseif ( $action == 'manage' )
{
	$upload_id = (int)gpc ( 'upload_id', 'G', 0 );
	$upload_key = trim ( gpc ( 'upload_key', 'G', '' ) );

	// load upload set
	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id=$upload_id LIMIT 1" );
	if ( $result->rowCount() )
	{
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		if ( $upload_key == $upload_set['upload_key'] )
		{
			// load files in this set
			$files = array();
			$result = $mysqlDB->query2 ( "SELECT * FROM uploader_pfiles WHERE upload_id=$upload_id ORDER BY file_id ASC" );
			if ( $result->rowCount() )
			{
				while ( false !== ( $file = $result->fetchAssoc() ) )
				{
					processPublicFile ( $file );
					$files[] = $file;
				}
				$result->free();
			}

			$tpl_manage = new Template ( TPL_DIR . 'tpl_public_manage.php' );
			$tpl_manage->setr ( 'files', $files );
			$tpl_manage->setr ( 'upload_set', $upload_set );
			$tpl_uploader->set ( 'page_title', $lang_public['title_manage'] );
			$tpl_uploader->set ( 'content', $tpl_manage, true );
		}
		else
		{
			$tpl_message->set ( 'message', $lang_public['invalid_set_key'] );
			$tpl_uploader->set ( 'page_title', $lang_public['title_invalid_key'] );
			$tpl_uploader->set ( 'content', $tpl_message, true );
		}
	}
	else
	{
		$tpl_message->set ( 'message', $lang_public['set_not_found'] );
		$tpl_uploader->set ( 'page_title', $lang_public['title_invalid_set'] );
		$tpl_uploader->set ( 'content', $tpl_message, true );
	}
}
elseif ( $action == 'editsetinfo' )
{
	// get inputs
	$upload_id = (int)gpc ( 'upload_id', 'P', 0 );
	$upload_key = trim ( gpc ( 'upload_key', 'P', '' ) );

	$upload_set_input = gpc ( 'upload', 'P', array ( 'public' => 1, 'name' => '', 'description' => '' ) );
	$upload_set_input['public'] = isset ( $upload_set_input['public'] ) ? (int)$upload_set_input['public'] : 0;
	$upload_set_input['description'] = trim ( $upload_set_input['description'] );

	// load upload set
	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id=$upload_id LIMIT 1" );
	if ( $result->rowCount() )
	{
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		if ( $upload_key == $upload_set['upload_key'] )
		{
			// update set info
			$insert = array
			(
				'upload_name'		=> isset ( $upload_set_input['name'] ) ? $mysqlDB->escape ( substr ( $upload_set_input['name'], 0, 64 ) ) : '',
				'upload_description'=> isset ( $upload_set_input['description'] ) ? $mysqlDB->escape ( substr ( $upload_set_input['description'], 0, 255 ) ) : '',
				'upload_ispublic'	=> (int)$upload_set_input['public'],
			);

			if ( !$mysqlDB->query ( 'UPDATE uploader_puploads SET ' . $mysqlDB->buildInsertStatement ( $insert ) . " WHERE upload_id=$upload_id" ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

			// go back
			go_to ( $upload_set['manage_url'] );
		}
		else go_to(UPLOADER_URL);
	}
	else go_to(UPLOADER_URL);
}
elseif ( $action == 'deletefiles' )
{
	// get inputs
	$upload_id = (int)gpc ( 'upload_id', 'P', 0 );
	$upload_key = trim ( gpc ( 'upload_key', 'P', '' ) );
	$file_ids = gpc ( 'file_ids', 'P', array() );

	// load upload set
	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id=$upload_id LIMIT 1" );
	if ( $result->rowCount() )
	{
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		if ( $upload_key == $upload_set['upload_key'] )
		{
			// remove files
			for ( $i = 0; $i < count ( $file_ids ); ++$i )
			{
				$file_ids[$i] = intval ( $file_ids[$i] );
			}
			$list = implode ( ',', $file_ids );

			// get file locations
			$result = $mysqlDB->query2 ( "SELECT file_location FROM uploader_pfiles WHERE file_id IN ($list)" );
			if ( $result->rowCount() )
			{
				while ( false !== ( $file = $result->fetchAssoc() ) )
					delete_public_file ( $file['file_location'] );
				$result->free();
			}

			// remove from table
			$mysqlDB->query ( "DELETE FROM uploader_pfiles WHERE file_id IN($list)" );

			// any files left in this set?
			$result = $mysqlDB->query2 ( "SELECT COUNT(file_id) AS files_count FROM uploader_pfiles WHERE upload_id=$upload_id" );
			$row = $result->fetchAssoc();
			if ( !$row['files_count'] )
			{
				// remove it
				$mysqlDB->query ( "DELETE FROM uploader_puploads WHERE upload_id=$upload_id" );
				$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'public' : 'public.php' ) );
				$tpl_message->set ( 'message', $lang_public['set_deleted'] );
				$tpl_uploader->set ( 'page_title', $lang_public['title_set_deleted'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}

			// go back
			go_to ( $upload_set['manage_url'] );
		}
		else go_to(UPLOADER_URL);
	}
	else go_to(UPLOADER_URL);
}
elseif ( $action == 'viewset' )
{
	// view uploaded files
	$upload_id = (int)gpc ( 'upload_id', 'G', 0 );
	$tpl_view = new Template ( TPL_DIR . 'tpl_public_view_set.php' );

	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id=$upload_id" );
	if ( $result->rowCount() )
	{
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		// get files
		$files = array();
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_pfiles WHERE upload_id=$upload_id ORDER BY file_id ASC" );
		if ( $result->isGood() && $result->rowCount() )
		{
			while ( false !== ( $file = $result->fetchAssoc() ) )
			{
				processPublicFile ( $file );
				$files[] = $file;
			}
			$result->free();
		}

		$tpl_view->setr ( 'upload_set', $upload_set );
		$tpl_view->setr ( 'files', $files );
		$tpl_uploader->set ( 'content', $tpl_view, 1 );
	}
	else
	{
		$tpl_message->set ( 'message', $lang_public['set_not_found'] );
		$tpl_uploader->set ( 'page_title', $lang_public['title_invalid_set'] );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
	}
}
elseif ( $action == 'slideshow' )
{
	// view uploaded files
	$upload_id = (int)gpc ( 'upload_id', 'G', 0 );
	$tpl_slideshow = new Template ( TPL_DIR . 'tpl_slideshow.php' );

	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_puploads WHERE upload_id=$upload_id" );
	if ( $result->rowCount() )
	{
		$upload_set = $result->fetchAssoc();
		processPublicSet ( $upload_set );
		$result->free();

		// get files
		$files = array();
		$result = $mysqlDB->query2 ( "SELECT * FROM uploader_pfiles WHERE upload_id=$upload_id AND file_isimage=1 ORDER BY file_id ASC" );
		if ( $result->isGood() && $result->rowCount() )
		{
			while ( false !== ( $file = $result->fetchAssoc() ) )
			{
				processPublicFile ( $file );
				$files[] = $file;
			}
			$result->free();
		}

		if ( !count ( $files ) )
		{
			$tpl_message->set ( 'message', $lang_public['slideshow_no_image'] );
			$tpl_message->set ( 'back_url', $upload_set['view_url'] );
			$tpl_uploader->set ( 'page_title', $lang_public['slideshow_no_image'] );
			$tpl_uploader->set ( 'content', $tpl_message, 1 );
		}
		else
		{
			$tpl_slideshow->setr ( 'back_url', $upload_set['view_url'] );
			$tpl_slideshow->setr ( 'images', $files );
			$tpl_uploader->set ( 'content', $tpl_slideshow, 1 );
		}
	}
	else
	{
		$tpl_message->set ( 'message', $lang_public['set_not_found'] );
		$tpl_uploader->set ( 'page_title', $lang_public['title_invalid_set'] );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
	}
}
elseif ( $action == 'browse' )
{
	// browse uploaded sets
	$per_page = $PUB['sets_per_page'];
	$current_page = (int)gpc ( 'page', 'G', 1 );

	// get total number of sets
	$result = $mysqlDB->query2 ( "SELECT COUNT(p.upload_id) AS total_sets FROM uploader_puploads AS p WHERE upload_ispublic=1" );
	if ( $result->error() ) exit ( $mysqlDB->error() );
	$r = $result->fetchAssoc();
	$total_sets = $r['total_sets'];

	// paginate
	$total_pages = ceil ( $total_sets / $per_page );
	if ( $current_page < 1 ) $current_page = 1;
	if ( $current_page > $total_pages ) $current_page = $total_pages;
	$start_offset = $current_page > 1 ? ( $current_page - 1 ) * $per_page : 0;

	$next_page_url = UPLOADER_URL . ( MOD_REWRITE ? 'public/browse?page=' : 'public.php?action=browse&amp;page=' ) . ($current_page + 1);
	$prev_page_url = UPLOADER_URL . ( MOD_REWRITE ? 'public/browse?page=' : 'public.php?action=browse&amp;page=' ) . ($current_page - 1);


	$sets = array();
	$result = $mysqlDB->query2 ( "SELECT *, COUNT(file_id) AS image_count FROM uploader_puploads JOIN uploader_pfiles USE INDEX(upload_id) USING(upload_id) WHERE upload_ispublic=1 GROUP BY uploader_puploads.upload_id ORDER BY file_id DESC LIMIT $start_offset, $per_page" );
	if ( $result->error() ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	while ( false !== ( $file = $result->fetchAssoc() ) )
	{
		// although $file contains both the set info and file info, we have to split them in case processPublicFile and processPublicSet
		// generate the same array keys. Just a precaution.
		$set = $file;
		processPublicFile ( $file );
		processPublicSet ( $set );
		$sets[] = array ( 'set' => $set, 'file' => $file );
	}
	$result->free();

	// display the upload form
	$tpl_pbrowse = new Template ( TPL_DIR . 'tpl_public_browse.php' );
	$tpl_pbrowse->set ( 'total_pages', $total_pages );
	$tpl_pbrowse->set ( 'total_sets', $total_sets );
	$tpl_pbrowse->set ( 'current_page', $current_page );
	$tpl_pbrowse->set ( 'next_page_url', $next_page_url );
	$tpl_pbrowse->set ( 'prev_page_url', $prev_page_url );

	$tpl_pbrowse->setr ( 'upload_sets', $sets );
	$tpl_uploader->set ( 'page_title', $lang_public['ptitle1'] );
	$tpl_uploader->set ( 'content', $tpl_pbrowse, 1 );
}
else
{
	// get latest files
	$latest_limit = $PUB['latest_limit'];

	$latest = array();

	if ( $latest_limit )
	{
		$result = $mysqlDB->query2 ( "SELECT *, COUNT(file_id) AS image_count FROM uploader_puploads JOIN uploader_pfiles USE INDEX(upload_id) USING(upload_id) WHERE file_isimage=1 AND upload_ispublic=1 GROUP BY uploader_puploads.upload_id ORDER BY file_id DESC LIMIT $latest_limit" );
		if ( $result->error() ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
		while ( false !== ( $file = $result->fetchAssoc() ) )
		{
			// although $file contains both the set info and file info, we have to split them in case processPublicFile and processPublicSet
			// generate the same array keys. Just a precaution.
			$set = $file;
			processPublicFile ( $file );
			processPublicSet ( $set );
			$latest[] = array ( 'set' => $set, 'file' => $file );
		}
		$result->free();
	}

	$ban = isBanned();
	$is_banned = is_array ( $ban ) && $ban['public'];

	// display the upload form
	$tpl_public = new Template ( TPL_DIR . 'tpl_public.php' );
	$tpl_public->setr ( 'latest', $latest );
	$tpl_public->set ( 'latest_limi', $latest_limit );
	$tpl_public->set ( 'restrictions', $restrictions );
	$tpl_public->set ( 'user_is_banned', $is_banned );
	$tpl_uploader->set ( 'page_title', $lang_public['ptitle1'] );
	$tpl_uploader->set ( 'content', $tpl_public, 1 );
}
?>