<?php
/*
  This is the password reset email template. The following variables are available:
  $login_url, $new_password
*/
?>
The password to your account has been reset. The new password is: <?=$new_password?>


You can login by going to <?=$login_url?>