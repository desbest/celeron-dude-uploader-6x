<?php
/*
  This is the change email request email template. The following variables are available:
  $ip_address, $reset_url
*/
?>
You or someone with the IP address <?=$ip_address?> have requested to change the email address associated with your account.

If you did not perform this action, discard this email. Your email address will not be changed.

Click on the URL below to confirm the change of your email address.

<?=$reset_url?>