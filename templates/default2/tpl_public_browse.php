<!-- tpl_public_browse.php -->

<?php if ( count ( $upload_sets ) ) : ?>
<div class="rounded green" style="color:#708059;">
    <h1>Uploaded file &amp; image sets</h1>
	<p>
		Found <?=$total_sets?> sets:
		<?php if ( $current_page > 1 ) : ?>
		<a href="<?=$prev_page_url?>" class="special">Previous</a>
		<?php else: ?>
		Previous
		<?php endif; ?>
		(<?=$current_page?>/<?=$total_pages?>)
		<?php if ( $current_page < $total_pages ) : ?>
		<a href="<?=$next_page_url?>" class="special">Next</a>
		<?php else: ?>
		Next
		<?php endif; ?>
	</p>
</div>
<div class="gallery">
    <?php reset ( $upload_sets ); while ( list ( $row_num, $tmp ) = each ( $upload_sets ) ) : $set = $tmp['set']; $file = $tmp['file']; ?>
	<?=$row_num%6==0?'<div class="spacer"></div>':''?>
    <div class="cell" style="width:110px;margin:0 0 15px 7px;">
        <div class="inner_cell">
            <div class="top"><?=$set['image_count']?> file<?=$set['image_count']>1?'s':''?></div>
            <div class="center">
                <a href="<?=$set['view_url']?>"><img src="<?=$file['file_isimage']?$file['square_thumb_url']:UPLOADER_URL . 'templates/default2/images/nothumb.gif'?>" alt="<?=entities($file['file_name'], ENT_QUOTES)?>" class="img1 thumb" /></a>
            </div>
            <div class="bottom">
                <span class="name">
                    By <strong><?=$set['upload_name']==''?'Anonymous':entities($set['upload_name'])?></strong>
                    <br /><?=get_date('M d, Y', $set['upload_date'])?>
                </span>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
</div>
<div class="spacer"></div>
<?php endif; ?>