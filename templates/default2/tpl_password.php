<!-- tpl_password.php -->

<h1>Password recovery</h1>

<p>Enter the email associated with your account below. An email with instructions on how to change your password will be sent to you.</p>

<?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>

<form method="post" action="account.php?action=password">
    <input type="hidden" name="task" value="password" />
    <p>
        <input type="text" name="email" value="" size="30" maxlength="200" />
        <input type="submit" value="Submit" />
        <input type="button" onclick="go('<?=UPLOADER_URL.(MOD_REWRITE?'login':'account.php?action=login')?>');" value="Cancel" />
    </p>
</form>