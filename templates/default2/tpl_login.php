<!-- tpl_login.php -->
<?php global $UPL; ?>

<h1>User login</h1>

<p>
    Please enter your login information below.
	<?php if ( $UPL['SETTINGS']['reg'] ) : ?>
	If you do not have an account, you can <a href="account.php?action=register" class="special">register</a> for one.
	<?php endif; ?>
</p>

<?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>

<form action="<?=MOD_REWRITE?'login':'account.php?action=login'?>" method="post">
    <input type="hidden" name="action" value="login" />
    <input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="<?=@$return_url?>" />

    <table cellspacing="1" cellpadding="3" border="0" style="margin-top: 10px;">
        <tr>
            <td>Username</td>
            <td><input type="text" tabindex="1" name="username" id="username" maxlength="100" size="30" value="<?=$username;?>" />
            &nbsp; <a href="account.php?action=resend_activation" class="special">Didn't receive an activation email?</a></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" tabindex="2" name="password" id="password" maxlength="30" size="30" />
            &nbsp; <a href="account.php?action=password" class="special">Forgot your password?</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="checkbox" tabindex="3" name="remember" id="remember" value="1"  <?=$remember?'checked="checked"':''?> class="chkbox" />
                <label for="remember">Remember my login information.</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Login" /> <input type="button" value="Cancel" onclick="go('index.php');" /></td>
        </tr>
    </table>

</form>

<script type="text/javascript">
<!--
function focusUsernameField ( )
{
	var usernameField = getObj ( 'username' );
	if ( usernameField ) usernameField.focus();
	return true;
}
addLoadEvent ( focusUsernameField );
-->
</script>