<?php
global $UPL, $USER, $PUB;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<base href="<?=$UPL['SETTINGS']['uploader_url']?>" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?=entities($file['file_name']);?> - Uploader v6</title>
<link rel="stylesheet" type="text/css" href="templates/default2/style.css?v=<?=UPLOADER_VERSION?>" media="all">
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="templates/default2/style_ie.css?v=<?=UPLOADER_VERSION?>" media="all">
<![endif]-->
<script type="text/javascript" charset="utf-8">
<!--
var base_url = "<?=$UPL['SETTINGS']['uploader_url']?>";
-->
</script>
<script type="text/javascript" charset="utf-8" src="templates/default2/prototype.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default2/common.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default2/javascript.js?v=<?=UPLOADER_VERSION?>"></script>
</head>
<body>
    <noscript>
        <p style="text-align:center;font-size:1.2em;color:red;font-weight:bold;">Please enable Javascript to properly access this service.</p>
    </noscript>
    <div id="container">
        <div id="container2">
            <div id="top">
                Welcome <span style="color:#FF8000;"><?=$UPL['USER']['username']?>!</span>
            </div>
            <div id="menu">
                <div id="menu_links">
                    <div style="float:left;">
                        <a href="./">Announcements</a>
                        <?php if ( $PUB['enabled'] ) : ?>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'public':'public.php')?>">Public Upload</a>
                        <?php endif; ?>
                        <?php if ( is_browse_enabled() ) : ?>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'members':'browse.php')?>">Members</a>
                        <?php endif; ?>
                        <?php /* If user is logged in, show this menu */ if ( $UPL['USER']['logged_in'] ) : ?>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php')?>">My Files</a>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'upload':'upload.php')?>">Upload</a>
                        <?php /* Otherwise, show this menu instead */ else: ?>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'login':'account.php?action=login')?>">Login</a>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')?>">Register</a>
                        <?php endif; ?>
                    </div>

                    <?php /* If user is logged in, show this menu */ if ( $UPL['USER']['logged_in'] ) : ?>
                    <div style="float:right;">
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp':'usercp.php')?>">UserCP</a>
                        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'account?action=logout':'account.php?action=logout')?>" onclick="return confirm('Confirm logout?');">Logout</a>
                    </div>
                    <?php endif;?>
                </div>
            </div>

            <div id="content">
                <div style="color:#253525;line-height:1.6em;">
                    <span style="font-size:1.2em;font-weight:bold;color:#000"><?=entities($file['file_name'])?></span><br />
                    <p style="color:#808080">
                        Uploaded <?=get_date('M d, Y',$upload_set['upload_date'])?> by
                        <strong><?=entities($upload_set['upload_name']==''?'Anonymous':$upload_set['upload_name'])?></strong>
                        and has been viewed <strong><?=number_format($file['file_views'])?></strong> times.
                    </p>
                    <p style="color:#808080">
                        <a href="<?=$file['fullsize_url']?>" class="special"><strong>View the full version</strong> of this image</a> or <a href="<?=$file['download_url']?>" class="special"><strong>Download</strong> the full version</a>
                    </p>
                </div>

                <div style="float:left;margin-right:15px;">
                    <p style="padding:10px 5px 5px 0">
                        <a href="<?=$file['fullsize_url']?>">
                            <img src="<?=$file['large_thumb_url']?>" alt="<?=entities($file['file_name'])?>" title="<?=entities($file['file_name'])?>" />
                        </a>
                    </p>
                </div>

                <div style="float:left;width:340px;">
                    <div style="padding:10px 0 5px 0">
                        <?php if ( count ( $previous_file ) ) : ?>
                        <div style="float:left;margin-right:10px;">
                            <a href="<?=$previous_file['url']?>"><img src="<?=$previous_file['square_thumb_url']?>" alt="previous image" /></a>
                        </div>
                        <?php endif; ?>

                        <div style="float:left;background-color:#f0f0f0;width:70px;height:60px;padding:5px;text-align:center;line-height:1.5em;font-size:1.0em;font-family:arial;color:#777">
                            Image<br /><strong><?=$file['position']?></strong> of <strong><?=$upload_set['total_images']?></strong>
                            <br />
                            <a href="<?=$upload_set['view_url']?>" class="special grey">Browse</a>
                        </div>

                        <?php if ( count ( $next_file ) ) : ?>
                        <div style="float:left;margin-left:10px;">
                            <a href="<?=$next_file['url']?>"><img src="<?=$next_file['square_thumb_url']?>" alt="next image" /></a>
                        </div>
                        <?php endif; ?>

                        <div class="spacer"></div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</body>
</html>