<!-- tpl_settings.php -->
<?php /* WARNING: DO NOT EDIT ANY FORM FIELD NAMES */ ?>
<h1>Uploader Settings</h1>

<p class="p1">
    <strong class="s2">Settings for:</strong> &nbsp;
    <?=$action=='settings' ? 'Uploader' : '<a href="admin.php?action=settings" class="special">Uploader</a>'?> -
    <?=$action=='user_settings' ? 'New Users' : '<a href="admin.php?action=user_settings" class="special">New Users</a>'?> -
    <?=$action=='emails' ? 'Emails' : '<a href="admin.php?action=emails" class="special">Emails</a>'?> -
	<?=$action=='public' ? 'Public Uploader' : '<a href="admin.php?action=public" class="special">Public Uploader</a>'?> -
	<?=$action=='plans' ? 'Upgrade Plans' : '<a href="admin.php?action=plans" class="special">Upgrade Plans</a>'?>
</p>

<?php /* Uploader settings */ if ( $action == 'settings' ) : ?>
<?php /* If settings have been saved, display this little notification */ if ( isset ( $_GET['saved'] ) ) : ?>
<p class="note1" style="text-align:center;">All settings have been saved!</p>
<?php endif; ?>

<form action="admin.php" method="post">
    <input type="hidden" name="action" value="settings" />
    <input type="hidden" name="task" value="save" />

    <fieldset>
        <legend>Uploader core settings</legend>
        <table id="tbl1" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td colspan="2" style="line-height:1.5em;">
                    The following settings must be correct in order for the Uploader to function properly. Use <a href="http://www.google.com/search?hl=en&lr=&safe=off&q=absolute+path+vs+relative+path&btnG=Search">absolute path</a> if you are specifying a directory or a URL that is <strong>outside</strong> of the Uploader directory.
                    <span style="color:red">For all paths and URLs,add a trailing slash at the end</span>. Instead of <span style="color:#707070">/some/path</span> enter
                    <span style="color:#707070">/some/path/</span>
                </td>
            </tr>
	    <tr>
		<td colspan="2" style="line-height:1.5em;">Absolute path to the uploader directory appears to be: <?=$uploader_absolute_path?></td>
	    </tr>
            <tr>
                <td style="width:120px;">Uploader URL</td>
                <td>
                    <input type="text" name="settings[uploader_url]" value="<?=$settings['uploader_url']?>" size="75" />
                    <span class="help" onclick="help('URL to the uploader directory. This URL will be used to generate urls to different parts of the uploader. Example: http://celerondude.com/uploader/');">Explain</span>
                </td>
            </tr>
            <tr>
                <td>Userfiles Directory</td>
                <td>
                    <input type="text" name="settings[userfiles_dir]" value="<?=$settings['userfiles_dir']?>" size="75" />
                    <span class="help" onclick="help('Path to the directory where user files will be stored. If folder is outside of the uploader directory, enter the absolute path. Example: /home/public_html/uploader/userfiles/.');">Explain</span>
                </td>
            </tr>
            <tr>
                <td style="width:120px;">Admin Emails</td>
                <td><input type="text" name="settings[email]" value="<?=$settings['email']?>" size="75" /> <span class="help" onclick="help('Email address(es) of the admin. All notifications will be sent to this email address. You can enter more than one email address by seperating them with a comma: one@celerondude.com, two@celerondude.com, three@celerondude.com.... Emails will be sent to all addresses entered.');">Explain</span></td>
            </tr>
            <tr>
                <td>Maintenance mode</td>
                <td style="line-height:1.5em;">
                    <input type="checkbox" name="settings[m]" id="settings[m]" value="1" <?=$settings['m']==1?'checked="checked"':''?> class="chkbox" onclick="showIt('m_msg',this.checked);" />
                    <label for="settings[m]">Turn on maintenance mode.</label> <span class="help" onclick="help('This will block user access to the uploader and display a message. This mode should be enabled when you are upgrading the uploader or doing anything that you need the users to not access the uploader.');">Explain</span><br />
                    <span id="m_msg" <?=!$settings['m']?'style="display:none;"':''?>>
                        <textarea name="settings[m_msg]" cols="85" rows="3" style="padding:3px;"><?=entities($settings['m_msg'])?></textarea>
                        <br /><small>Enter the reason for maintenance here, if you wish.</small>
                    </span>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Uploader options</legend>
        <table id="tbl2" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td colspan="2" style="line-height:1.5em;">
                    The following settings are optional and affect the features/functionality of the Uploader.
                </td>
            </tr>
            <tr>
                <td>Illegal Filetypes</td>
                <td>
                    <input type="text" name="settings[filetypes]" value="<?=$settings['filetypes']?>" size="75" />
                    <span class="help" onclick="help('File types that the users cannot upload. This setting overrides the setting for individual users. Warning: You should not leave this field blank. Enter all known file extensions that your server might parse and execute (such as ASP, PHP, Perl, or HTACCESS files). Seperate them with a comma but do not put a space in between. Example: php,pl,cgi,asp');">Explain</span>
                </td>
            </tr>
            <tr>
                <td>Upload record</td>
                <td colspan="2" style="line-height:1.5em;">
                    <select name="settings[log]">
                        <option value="2" <?=$settings['log']==2?'selected="selected"':''?>>Log all relevant actions</option>
                        <option value="1" <?=$settings['log']==1?'selected="selected"':''?>>Log upload actions only</option>
                        <option value="0" <?=$settings['log']==0?'selected="selected"':''?>>None (Disabled)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="width:120px;">Registration</td>
                <td style="line-height:1.5em;">
                    <input type="checkbox"  name="settings[reg]" value="1" <?=$settings['reg']==1?'checked="checked"':''?> id="registration" class="chkbox" onclick="showIt('regDisabled',!this.checked);"/>
                    <label for="registration">Enable new user registration.</label>

                    <span id="regDisabled" <?=$settings['reg']?'style="display:none;"':''?>>
                        <textarea name="settings[regmsg]" cols="85" rows="3" style="padding:3px;"><?=entities($settings['regmsg'])?></textarea>
                        <br /><small>You can enter the reason here if you wish.</small>
                    </span>

                    <br />
                    <input type="checkbox"  name="settings[approval]" value="1" <?=$settings['approval'] ?'checked="checked"':''?> id="approval" class="chkbox" />
                    <label for="approval">Automatically approve new accounts.</label> <span class="help" onclick="help('The uploader cannot be accessed if the user has not been approved. This allows you to manually approve new users or automatically approve all new users.');">Explain</span>
                    <br />
                    <input type="checkbox"  name="settings[activation_req]" value="1" <?=$settings['activation_req'] ?'checked="checked"':''?> id="activation"  class="chkbox"/>
                    <label for="activation">Accounts must be activated.</label> <span class="help" onclick="help('Activation requirement requires that the user account be activated through email. This is to verify that the user has entered a valid email address. Enable this if you wish to do so.');">Explain</span>
                    <br />
                    <input type="checkbox" name="settings[notify_reg]" id="notify_reg" value="1" <?=$settings['notify_reg'] ?'checked="checked"':''?> class="chkbox" />
                    <label for="notify_reg">Notify admin(s) of new users through email.</label>
                </td>
            </tr>
            <tr>
                <td>User/file browsing</td>
                <td colspan="2" style="line-height:1.5em;">
                    <select name="settings[browsing]">
                        <option value="any" <?=$settings['browsing']=='any'?'selected="selected"':''?>>Anyone (registered or unregistered) can browse for users and browse users' files.</option>
                        <option value="reg" <?=$settings['browsing']=='reg'?'selected="selected"':''?>>Only registered users can browser other user files.</option>
                        <option value="none" <?=$settings['browsing']=='none'?'selected="selected"':''?>>No one, this feature is disabled. No user can browser other users' files.</option>
                    </select>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Image options</legend>

        <table id="tbl3" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td colspan="2" style="line-height:1.5em;">
                    The following settings apply to images uploaded by the users. All these features require GD2, which is
                    <?=function_exists('imagecreatetruecolor')?'<span style="color:green;font-weight:bold">installed!</span>':'<span style="color:red;font-weight:bold">NOT INSTALLED!</span>'?></span>
                </td>
            </tr>
            <tr>
                <td>Watermark file</td>
                <td>
                    <input type="text" name="settings[wm_path]" value="<?=$settings['wm_path']?>" size="75" />
                    <span class="help" onclick="help('The path to the PNG watermark image that is used to watermark user uploaded images. Watermarking can be disabled in the setting below.');">Explain</span>
                </td>
            </tr>
            <tr>
                <td>Watermark options</td>
                <td>
                    <select name="settings[wm]">
                        <option value="always" <?=$settings['wm']=='always'?'selected="selected"':''?>>Always watermark</option>
                        <option value="user" <?=$settings['wm']=='user'?'selected="selected"':''?>>Base on individual user setting</option>
                        <option value="never" <?=$settings['wm']=='never'?'selected="selected"':''?>>Never watermark</option>
                    </select>
                    <select name="settings[wm_pos]">
                        <option value="left,top" <?=$settings['wm_pos']=='left,top'?'selected="selected"':''?>>Top Left</option>
                        <option value="center,top" <?=$settings['wm_pos']=='center,top'?'selected="selected"':''?>>Top Center</option>
                        <option value="right,top" <?=$settings['wm_pos']=='right,top'?'selected="selected"':''?>>Top Right</option>
                        <option value="left,center" <?=$settings['wm_pos']=='left,center'?'selected="selected"':''?>>Center Left</option>
                        <option value="center,center" <?=$settings['wm_pos']=='center,center'?'selected="selected"':''?>>Center</option>
                        <option value="right,center" <?=$settings['wm_pos']=='right,center'?'selected="selected"':''?>>Center Right</option>
                        <option value="left,bottom" <?=$settings['wm_pos']=='left,bottom'?'selected="selected"':''?>>Bottom Left</option>
                        <option value="center,bottom" <?=$settings['wm_pos']=='center,bottom'?'selected="selected"':''?>>Bottom Center (default)</option>
                        <option value="right,bottom" <?=$settings['wm_pos']=='right,bottom'?'selected="selected"':''?>>Bottom Right</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Uploader view</td>
                <td>
                    <input type="checkbox" name="settings[uploader_view]" id="uploader_view" value="1" <?=$settings['uploader_view']?'checked="checked"':''?> class="chkbox" />
                    <label for="uploader_view">Use the uploader to view images.</label>
                    <span class="help" onclick="help('This will modify the URL to use images so that the image will be loaded in a page where you can put ads around the image. This feature requires mod_rewrite.');">Explain</span>
                </td>
            </tr>
        </table>
    </fieldset>

    <p style="text-align:center;">
        <input type="submit" value="Save" class="blue_button" /> <input type="reset" class="blue_button"  value="Undo changes" onclick="return confirm('Reset form?');" />
    </p>
</form>
<?php
// ######################################################################################
// THIS IS A SEPERATOR
// ######################################################################################
/* New user settings */ elseif ( $action == 'user_settings' ) : ?>
<p>
    These settings are given to new users when they register. Changing these settings will not affect currently registered users.
    To globally change settings of registered users, visit the <a href="admin.php?action=users" class="special">Users</a> section.
</p>
<br />
<?php /* If settings have been saved, display this little notification */ if ( $saved ) : ?>
<p class="note1" style="text-align:center;">All settings have been saved!</p>
<?php endif; ?>

<form action="admin.php" method="post">
    <input type="hidden" name="action" value="user_settings" />
    <input type="hidden" name="task" value="save" />

    <fieldset>
        <legend>Registration &amp; Restrictions</legend>
        <table id="tbl_rr" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td>Disallowed names</td>
                <td>
                    <input type="text" name="settings[disallowed_names]" size="75" value="<?=$settings['disallowed_names']?>" />
                </td>
                <td class="cr"><span class="help" onclick="help('List all the names that are not allowed. Seperate them with commas but leave no space between the command and the name.\nEx:admin,administrator,celerondude,bad name,uploader v6\nUser name matching is case insensitive, you do NOT have to enter both username and USERNAME.');">Explain</span></td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>File Limits &amp; Restrictions</legend>
        <table id="tbl1" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td style="width:120px;">Max storage</td>
                <td>
                    <input type="text" name="settings[fl_max_storage]" value="<?=$settings['fl_max_storage']?>" size="5" id="mst" style="text-align:center;" onkeyup=";" /> MB (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help" onclick="help('The maximum diskspace (in Megabyte) that the user can occupy.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max upload file size</td>
                <td>
                    <input type="text" name="settings[fl_max_filesize]" value="<?=$settings['fl_max_filesize']?>" size="5" id="mfs" style="text-align:center;" onkeyup=";" /> KB (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help" onclick="help('The maximum file size (in Kilobyte) that the user can upload. Remember that PHP has a default size limit on uploaded files. That limit is usually 2MB. Even if you specify a value greater than the PHP default value, the user will still not be able to upload files greater than the value set by PHP. You will need to edit two PHP settings in php.ini, they are \'upload_max_filesize\' and \'post_max_size\'. You will either need to contact your webhost or edit php.ini if you are hosting yourself. Two php.ini settings have been inserted into the .htaccess file in the uploader directory (same location as index.php, admin.php, etc...) that will attempt to modify the settings mentioned above. If this method works, then you will not need to contact your web host.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max folders</td>
                <td>
                    <input type="text" name="settings[fl_max_folders]" value="<?=$settings['fl_max_folders']?>" size="5" style="text-align:center;" onkeyup=";" /> folders (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help"  onclick="help('Maximum number of folders the user can create. Enter 0 for unlimited.');">Explain</span></td>
            </tr>
            <tr>
                <td>Allowed filetypes</td>
                <td>
                    <input type="text" name="settings[fl_allowed_filetypes]" value="<?=$settings['fl_allowed_filetypes']?>" size="75" />
                </td>
                <td class="cr"><span class="help" onclick="help('Specify the file types that the users are allowed to upload. Entery ANY or leave it blank for no limit (except for those specified in Uploader Settings).\nSeperate the extensions with commas, do NOT use spaces. Extension matching is case insensitive, you do not have to enter upper and lower case extensions.\nExample: gif,png,jpeg,zip,rar');">Explain</span></td>
            </tr>
            <tr>
                <td>Images only</td>
                <td>
                    <input type="radio" name="settings[fl_images_only]" value="1" <?=$settings['fl_images_only']==1?'checked="checked"':''?> class="radio" id="fl_images_only_1" class="chkbox" /><label for="fl_images_only_1">Images only</label><br />
                    <input type="radio" name="settings[fl_images_only]" value="0" <?=$settings['fl_images_only']==0?'checked="checked"':''?> class="radio" id="fl_images_only_2" /><label for="fl_images_only_2">Any file types</label>
                </td>
                <td class="cr"><span class="help" onclick="help('This setting will force the user to upload images only. Detecting whether the uploaded file is an image is done by reading the file, not just looking at the file extension (which can be renamed) so the Allowed Filetypes settings will be overridden by this one. For example, if you allow the user to upload .txt file but it is not an image file, it will not be uploaded.');">Explain</span></td>
            </tr>
            <tr>
                <td>File rename</td>
                <td>
                    <input type="radio" name="settings[fl_allow_rename]" value="2" <?=$settings['fl_allow_rename']==2?'checked="checked"':''?> class="radio"  id="fl_allow_rename_2" /><label for="fl_allow_rename_2">File name and extension</label><br />
                    <input type="radio" name="settings[fl_allow_rename]" value="1" <?=$settings['fl_allow_rename']==1?'checked="checked"':''?> class="radio" id="fl_allow_rename_1" /><label for="fl_allow_rename_1">File name only</label><br />
                    <input type="radio" name="settings[fl_allow_rename]" value="0" <?=$settings['fl_allow_rename']==0?'checked="checked"':''?> class="radio" id="fl_allow_rename_0" /><label for="fl_allow_rename_0">User cannot rename files</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Allow user to rename files or not. If user is allowed to rename files, specify if the user can rename the file extension. Basically it would defeat the purpose of having file type limit when the user can rename a file that he/she is not allowed to one that is allowed (ex:EXE to JPG). Of course even if the user is allowed to rename the extension to anything, he/she cannot rename it to the extensions set in the Uploader Illegal Filetypes setting.');">Explain</span></td>
            </tr>
            <tr>
                <td>Folder creation</td>
                <td>
                    <input type="radio" name="settings[fl_create_folder]" value="1" <?=$settings['fl_create_folder']==1?'checked="checked"':''?> class="radio" id="fl_create_folder_1" /><label for="fl_create_folder_1">User can create folders</label><br />
                    <input type="radio" name="settings[fl_create_folder]" value="0" <?=$settings['fl_create_folder']==0?'checked="checked"':''?> class="radio" id="fl_create_folder_2" /><label for="fl_create_folder_2">User CANNOT create folders</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Allow users to create folders. All user created folders are created inside THAT user\'s folder. For example, if a user named \'cdude\' created a folder named \'images\', the folder created would be \'userfiles/cdude/images\'');">Explain</span></td>
            </tr>
            <tr>
                <td>Image watermark</td>
                <td>
                    <input type="radio" name="settings[fl_watermark]" value="1" <?=$settings['fl_watermark']==1?'checked="checked"':''?> class="radio" id="fl_watermark1" /><label for="fl_watermark1">Watermark images uploaded by this user</label><br />
                    <input type="radio" name="settings[fl_watermark]" value="0" <?=$settings['fl_watermark']==0?'checked="checked"':''?> class="radio" id="fl_watermark2" /><label for="fl_watermark2">Do not watermark</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Specify whether images uploaded by this user should be watermarked or not. This setting CAN be overridden by the global uploader setting.');">Explain</span></td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Bandwidth Limits &amp; Restrictions</legend>
        <table id="tbl2" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td colspan="3">
                    Bandwidth limit will ONLY work if your server has <a href="http://httpd.apache.org/docs/mod/mod_rewrite.html">mod_rewrite</a> enabled.
                </td>
            </tr>
            <tr>
                <td style="width:120px;">Max bandwidth</td>
                <td>
                    <input type="text" name="settings[bw_max]" value="<?=$settings['bw_max']?>" size="5" id="mbw" style="text-align:center;" /> MB
                    (<span class="link" onclick="getObj('mbw').value=1*1024">1GB</span>,
                    <span class="link" onclick="getObj('mbw').value=3*1024">3GB</span>,
                    <span class="link" onclick="getObj('mbw').value=5*1024">5GB</span>,
                    <span class="link" onclick="getObj('mbw').value=10*1024">10GB</span>,
                    <span class="link" onclick="getObj('mbw').value=15*1024">15GB</span>,
                    <span class="link" onclick="getObj('mbw').value=20*1024">20GB</span>,
                    <span class="link" onclick="getObj('mbw').value=30*1024">30GB</span>)
                </td>
                <td class="cr"><span class="help" onclick="help('Specify the maximum bandwidth (in Megabyte) allowed for the user per month. This setting only applies if bandwidth limit is enabled (see Uploader Settings). Enter 0 for unlimited.');">Explain</span></td>
            </tr>
            <tr>
                <td>Bandwidth reset period</td>
                <td>
                    <input type="text" name="settings[bw_reset_period]" value="<?=$settings['bw_reset_period']?>" size="5" id="bwrp" style="text-align:center;" /> Days
                </td>
                <td class="cr"><span class="help" onclick="help('When a user reaches his/her bandwidth limit and Bandwidth Reset Rule is set to Automatic, the uploader checks to see when was the last time the counter was reset and will only reset it after the specified time has passed.');">Explain</span></td>
            </tr>
            <tr>
                <td>Bandwidth counter reset</td>
                <td>
                    <input type="radio" name="settings[bw_auto_reset]" value="1" <?=$settings['bw_auto_reset']==1?'checked="checked"':''?> class="radio" id="bw_auto_reset_1" /><label for="bw_auto_reset_1">Automatically</label><br />
                    <input type="radio" name="settings[bw_auto_reset]" value="0" <?=$settings['bw_auto_reset']==0?'checked="checked"':''?> class="radio" id="bw_auto_reset_2" /><label for="bw_auto_reset_2">Manually (by an Admin)</label>
                </td>
                <td class="cr"><span class="help" onclick="help('When a user reaches his/her bandwidth limit, specify whether the counter should be reset automatically or manually. If manually, the user will not be able to view his/her files outside of the uploader (MyFiles) until an admin resets the counter.');">Explain</span></td>
            </tr>
            <tr>
                <td>Transfer rate</td>
                <td>
                    <input type="text" name="settings[bw_xfer_rate]" value="<?=@$settings['bw_xfer_rate']?>" style="text-align:center;" size="5" /> KB/s
                </td>
                <td class="cr"><span class="help" onclick="help('This will set the transfer rate for files in this user\'s account. The actual speed is not exact but is very close. Enter zero for unlimited. This feature will NOT work on Windows server.');">Explain</span></td>
            </tr>
        </table>
    </fieldset>

    <div style="text-align:center; margin-top:15px;">
        <input type="submit" value="Save settings" class="blue_button" /> <input type="reset" class="blue_button" value="Undo changes" onclick="return confirm('Reset form?');" />
    </div>
</form>
<?php
// ######################################################################################
// THIS IS A SEPERATOR
// ######################################################################################
/* New user settings */ elseif ( $action == 'emails' ) : ?>
<p>The following email templates are used to send messages to users. Please edit them carefully. Each email message portion may
have some variables that you can insert into. Their names are self-explanatory. Use them wherever you want the actual value to be inserted.</p>
<form id="emails_form" method="post" action="admin.php?action=emails">
    <input type="hidden" name="action" value="emails" />
    <input type="hidden" name="task" value="save" />

    <fieldset>
        <legend>Email Header &amp; Footer</legend>
        <p style="padding:5px;">
            <strong class="s1">Message header</strong><br />
            To be entered at the beginning of all emails sent to the users. Ex: "Hello username". Variables: {username}<br />
            <textarea name="emails[email_header]" rows="5" cols="105"><?=$emails['email_header']?></textarea>
        </p>
        <p style="padding:5px;">
            <strong class="s1">Message footer</strong><br />
            To be entered at the end of all emails sent to the users. Ex: "Thanks, Your name". Variables: NONE<br />
            <textarea name="emails[email_footer]" rows="5" cols="105"><?=$emails['email_footer']?></textarea>
        </p>
    </fieldset>

    <fieldset>
        <legend>Email Templates</legend>
        <p style="padding:5px;">
            <strong class="s1">Activation email</strong><br />
            Sent when user registers. Variables for this email: {activation_url}<br />
            <textarea name="emails[activation]" rows="7" cols="105"><?=$emails['activation']?></textarea>
        </p>

        <p style="padding:5px;">
            <strong class="s1">Password reset email</strong><br />
            Sent when user's password has been reset. Variables for this email: {new_password}, {login_url}<br />
            <textarea name="emails[password_reset]" rows="5" cols="105"><?=$emails['password_reset']?></textarea>
        </p>

        <p style="padding:5px;">
            <strong class="s1">Password request email</strong><br />
            Sent when user forgot his/her password. Variables for this email: {ip}, {reset_url}<br />
            <textarea name="emails[password_request]" rows="7" cols="105"><?=$emails['password_request']?></textarea>
        </p>

        <p style="padding:5px;">
            <strong class="s1">User approved email</strong><br />
            Sent when user has been approved by admin. Variables for this email: {uploader_url}<br />
            <textarea name="emails[user_approved]" rows="5" cols="105"><?=$emails['user_approved']?></textarea>
        </p>

        <p style="padding:5px;">
            <strong class="s1">"Change email" email</strong><br />
            Sent when user changes his/her email address in the UserCP. Variables for this email: {ip}, {confirm_url}<br />
            <textarea name="emails[change_email]" rows="6" cols="105"><?=$emails['change_email']?></textarea>
        </p>

        <p style="text-align:center;">
            <input type="submit" value="Save Templates" class="blue_button" />
        </p>
    </fieldset>
</form>
<?php
// THIS IS A SEPERATOR
// ######################################################################################
/* New user settings */ elseif ( $action == 'public' ) : ?>
<?php /* If settings have been saved, display this little notification */ if ( isset ( $saved ) && $saved ) : ?>
<p class="note1" style="text-align:center;">All settings have been saved!</p>
<?php endif; ?>
<form id="public_form" method="post" action="admin.php?action=public">
    <input type="hidden" name="action" value="public" />
    <input type="hidden" name="task" value="save" />
	<fieldset>
        <legend>Public Uploader settings</legend>
        <table id="tbl_rr" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td colspan="3" style="line-height:1.5em;">
                    Remember to add a trailing slash after every path and URL
                </td>
            </tr>
            <tr>
                <td style="width:150px;">Public Uploader</td>
                <td colspan="2">
                    <input type="checkbox" name="psettings[enabled]" value="1" <?=$psettings['enabled']?'checked="checked"':''?> id="psettings_enabled" class="chkbox" />
                    <label for="psettings_enabled">Enable the public uploader</label>
                </td>
            </tr>
            <tr>
                <td >Public files directory</td>
                <td>
                    <input type="text" name="psettings[public_files_dir]" size="80" value="<?=@$psettings['public_files_dir']?>" />
                </td>
                <td class="cr"><span class="help" onclick="help('Similiar to the userfiles directory setting in the Uploader settings, this is the path to the directory were the files will be uploaded into.');">Explain</span></td>
            </tr>
            <tr>
                <td>Public files URL</td>
                <td>
                    <input type="text" name="psettings[public_files_url]" size="80" value="<?=@$psettings['public_files_url']?>" />
                </td>
                <td class="cr"><span class="help" onclick="help('This is the full URL to the user files directory above. Remember to add a trailing slash!');">Explain</span></td>
            </tr>
            <tr>
                <td>Allowed file types</td>
                <td>
                    <input type="text" name="psettings[allowed_filetypes]" size="80" value="<?=@$psettings['allowed_filetypes']?>" />
                </td>
                <td class="cr"><span class="help" onclick="help('Allowed file types, seperate with a comma and enter in lowercase. You don\'t have to enter uppercase. To allow all file types (except those set as illegal in the Uploader Settings), enter ANY');">Explain</span></td>
            </tr>
            <tr>
                <td>Max file size</td>
                <td>
                    <input type="text" name="psettings[max_file_size]" size="8" value="<?=@$psettings['max_file_size']?>" style="text-align:center;" /> KB
                </td>
                <td class="cr"><span class="help" onclick="help('Max file size in KB, enter 0 for unlimited.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max views</td>
                <td>
                    <input type="text" name="psettings[max_views]" size="8" value="<?=@$psettings['max_views']?>" style="text-align:center;" />
                </td>
                <td class="cr"><span class="help" onclick="help('Delete a file after it has had this many views. Enter 0 for no view limit. The file will be deleted when either it has had too many views or too many transfer, whichever comes first. If you set one limit (view or bandwidth) as unlimited, then it will depend on the other limit.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max bandwidth</td>
                <td>
                    <input type="text" name="psettings[max_bandwidth]" size="8" value="<?=@$psettings['max_bandwidth']?>" style="text-align:center;" /> MB
                </td>
                <td class="cr"><span class="help" onclick="help('Delete a file after it has had this much transfer. Enter 0 for no bandwidth limit. The file will be deleted when either it has had too many views or too many transfer, whichever comes first. If you set one limit (view or bandwidth) as unlimited, then it will depend on the other limit.');">Explain</span></td>
            </tr>
            <tr>
                <td>Images</td>
                <td colspan="2">
                    <input type="checkbox" name="psettings[images_only]" value="1" <?=$psettings['images_only']?'checked="checked"':''?> id="psettings_images_only" class="chkbox" />
                    <label for="psettings_images_only">Only images (JPEG, PNG, GIF) can be uploaded.</label><br />
                </td>
            </tr>
            <tr>
                <td>Thumbnails</td>
                <td colspan="2">
                    <input type="checkbox" name="psettings[allow_thumbnails]" value="1" <?=$psettings['allow_thumbnails']?'checked="checked"':''?> id="psettings_allow_thumbnails" class="chkbox" />
                    <label for="psettings_allow_thumbnails">Allow users to create thumbnails of uploaded images.</label>
                </td>
            </tr>

            <tr>
                <td>Thumbnails border</td>
                <td colspan="2">
                    <input type="checkbox" name="psettings[thumb_border]" value="1" <?=$psettings['thumb_border']?'checked="checked"':''?> id="psettings_thumbnail_border" class="chkbox" />
                    <label for="psettings_thumbnail_border">Add border to thumbnails plus original image dimension and size. Ex: 935x1234 124.5KB</label>
                </td>
            </tr>

            <tr>
                <td>Large &amp; small thumbnail size</td>
                <td>
                    <input type="text" name="psettings[thumb_large]" size="10" value="<?=@$psettings['thumb_large']?>" style="text-align:center;" /> &amp;
                    <input type="text" name="psettings[thumb_small]" size="10" value="<?=@$psettings['thumb_small']?>" style="text-align:center;" />
                </td>
                <td class="cr"><span class="help" onclick="help('Dimensions of large and small thumbnails. Example: 100x200');">Explain</span></td>
            </tr>

            <tr>
                <td>Watermark file</td>
                <td><input type="text" name="psettings[wm_path]" value="<?=$psettings['wm_path']?>" size="75" /></td>
                <td class="cr"><span class="help" onclick="help('The path to the PNG watermark image that is used to watermark public uploaded images. Watermarking can be disabled in the setting below.');">Explain</span></td>
            </tr>
            <tr>
                <td>Watermark options</td>
                <td colspan="2">
                    <select name="psettings[wm]">
                        <option value="1" <?=$psettings['wm']?'selected="selected"':''?>>Watermark all uploaded images</option>
                        <option value="0" <?=!$psettings['wm']?'selected="selected"':''?>>Don't watermark</option>
                    </select>
                    <select name="psettings[wm_pos]">
                        <option value="left,top" <?=$psettings['wm_pos']=='left,top'?'selected="selected"':''?>>Top Left</option>
                        <option value="center,top" <?=$psettings['wm_pos']=='center,top'?'selected="selected"':''?>>Top Center</option>
                        <option value="right,top" <?=$psettings['wm_pos']=='right,top'?'selected="selected"':''?>>Top Right</option>
                        <option value="left,center" <?=$psettings['wm_pos']=='left,center'?'selected="selected"':''?>>Center Left</option>
                        <option value="center,center" <?=$psettings['wm_pos']=='center,center'?'selected="selected"':''?>>Center</option>
                        <option value="right,center" <?=$psettings['wm_pos']=='right,center'?'selected="selected"':''?>>Center Right</option>
                        <option value="left,bottom" <?=$psettings['wm_pos']=='left,bottom'?'selected="selected"':''?>>Bottom Left</option>
                        <option value="center,bottom" <?=$psettings['wm_pos']=='center,bottom'?'selected="selected"':''?>>Bottom Center (default)</option>
                        <option value="right,bottom" <?=$psettings['wm_pos']=='right,bottom'?'selected="selected"':''?>>Bottom Right</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Uploader view</td>
                <td>
                    <input type="checkbox" name="psettings[uploader_view]" value="1" <?=@$psettings['uploader_view']?'checked="checked"':''?> id="uploader_view" class="chkbox" />
                    <label for="uploader_view">Use the uploader to view images.</label>
                </td>
                <td class="cr"><span class="help" onclick="help('If enabled, links to the uploaded images will be changed to force the image to be viewed in an html page. This will allow you to add contents (advertisement for example) around the image. This feature can be bypassed if you disable image hot linking protection. Edit tpl_public_view_image.php to change the layout of the mentioned page.');">Explain</span></td>
            </tr>
            <tr>
                <td>Upload log</td>
                <td>
                    <input type="checkbox" name="psettings[log_upload]" value="1" <?=@$psettings['log_upload']?'checked="checked"':''?> id="upload_log" class="chkbox" />
                    <label for="upload_log">Log upload actions. IP address, time, and file name</label>
                </td>
                <td  class="cr">
                    <span class="help" onclick="help('Similar to the non-public uploader, this will record the ip address, time and file name of every upload. You can view the log files in the Uploader log section.');">Explain</span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2"><input type="submit" value="Save" class="blue_button" /> <input type="reset" value="Reset" onclick="return confirm('Reset form?');" class="blue_button" /></td>
            </tr>
        </table>
	</fieldset>
</form>
<?php endif; ?>




<!-- alternating row color -->
<script type="text/javascript">
    alternateRowColor(getObj('tbl1'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('tbl2'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('tbl3'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('tbl_rr'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('emails_form'),'p','#F5F5F5','#FBFBFB');
</script>