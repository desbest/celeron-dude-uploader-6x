<?php global $UPL; ?>

<?php if ( $action == 'select' ) : ?>
<h1>Account Upgrade</h1>
<form method="post" action="<?=UPLOADER_URL?>upgrade.php?action=process">
    <?php if ( $UPL['USER']['logged_in'] ) : ?>
    <input type="hidden" name="payment[username]" value="<?=@$UPL['USER']['username']?>" />
    <?php endif; ?>


    <?php if ( isset ( $errors ) ) print $errors; ?>
    <p>Select an upgrade plan below. The payment process will be securely handled by Paypal.</p>
    <table cellspacing="1" cellpadding="3" style="margin-top:10px;">
	<tr>
	    <td>Upgrade option</td>
	    <td>
		<select name="upgrade_plan">
		    <option value="plan1">Plan 1</option>
		    <option value="plan2">Plan 2</option>
		    <option value="plan3">Plan 3</option>
		</select>
	    </td>
	</tr>
	<tr>
	    <td></td>
	    <td><input type="submit" class="blue_button" value="Submit" /></td>
	</tr>
    </table>
</form>

<?php elseif ( $action == 'process' ) : ?>
<h1>Please wait...</h1>
<p>Your order is being processed. You will be directed to the Paypal website momentarily. Click on the Continue button if you are not redirected.<br /><br /></p>
<form id="paypal_form" method="post" action="<?=$paypal_url?>">
    <?php while ( list ( $var, $val ) = each ( $paypal_info ) ) : ?>
    <input type="hidden" name="<?=$var?>" value="<?=str_replace('"','\"',$val)?>" />
    <?php endwhile; ?>
    <input type="submit" value="Continue" />
</form>
<script type="text/javascript">
<!--
function submit_pp ( )
{
    var f = getObj ( 'paypal_form' );
    if ( f ) f.submit();
}
addLoadEvent ( submit_pp );
-->
</script>
<?php elseif ( $action == 'success' ) : ?>
<!--	
<?php print_r ( $result ); ?>
-->
<h1>Thank you!</h1>
<?php if ( $result['payment_status'] == 'Completed' ) : ?>
<p>Payment received from <strong><?=$result['payer_email']?></strong> for <?=$result['item_number']?>.</p>
<p>Thank you for your purchase. Your account will be upgraded. This process may take from a few minutes to an hour. If your account
does not change within the next 24 hours, please contact the administrator.</p>
<?php elseif ( $result['payment_status'] == 'Pending' ) : ?>
    <?php if ( $result['pending_reason'] == 'echeck' ) : ?>
    Your account will be upgraded once your e-check payment clears.
    <?php else: ?>
    Unknown payment pending reason (reason:<?=$result['pending_reason']?>). Please contact the administrator.
    <?php endif; ?>   
<?php else: ?>
<p>Thank you for your purchase. Your account will be upgraded once the payment is completed.</p>
<?php endif; ?>



<?php elseif ( $action == 'cancelled' ) : ?>
<h1>Payment Cancelled</h1>
<p>Your paypment was cancelled.</p>
<?php else: ?>
<h1>Error!</h1>
Unknown step!
<?php endif; ?>