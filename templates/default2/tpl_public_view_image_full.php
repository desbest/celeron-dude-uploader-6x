<?php
global $UPL, $USER;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<base href="<?=$UPL['SETTINGS']['uploader_url']?>" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?=entities($file['file_name']);?> - Uploader v6</title>
<link rel="stylesheet" type="text/css" href="templates/default2/style_light.css?v=<?=UPLOADER_VERSION?>" media="all">
</head>
<body>
    <div id="info">
        <p>
            <a href="<?=$file['url']?>" class="special"><strong>Back</strong> to the image page</a>
            or
            <a href="<?=$upload_set['view_url']?>" class="special"><strong>Browse</strong> this set</a>
        </p>
        <p>
            Uploaded <?=get_date('M d, Y',$upload_set['upload_date'])?> by <strong><?=entities($upload_set['upload_name']==''?'Anonymous':$upload_set['upload_name'])?></strong>
        </p>
        <div class="spacer">&nbsp;</div>
    </div>

    <p id="image_container"><img src="<?=$file['direct_url']?>" alt="<?=entities($file['file_name'])?>" /></p>
</body>
</html>