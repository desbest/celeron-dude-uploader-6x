<?php
/*
  This is the password reset request email template. The following variables are available:
  $ip_address, $reset_url
*/
?>
You or someone with the IP address <?=$ip_address?> have requested to reset your account password.

If you did not perform this action, discard this email. Your password will not be reset.

If you do wish to reset your password, confirm this action by clicking on the link below. A new randomly generated password will be sent to you.

<?=$reset_url?>