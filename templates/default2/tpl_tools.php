<!-- tpl_tools.php -->
<?php /* Show tools menu */ if ( $action == 'tools' ) : ?>
<h1>Uploader Tools</h1>

<div style="margin-top:10px">
    <ul class="ls_menu2" style="float:left;width:50%;line-height:1.6em;">
        <li>
            <a href="admin.php?action=announcement" class="special">Announcements</a><br />
            Create and manage announcements.
        </li>
        <li>
            <a href="admin.php?action=pupload" class="special">Public uploader files</a><br />
            Manage public uploaded files.
        </li>
        <li>
            <a href="admin.php?action=emailer" class="special">Emailer</a><br />
            Send emails to (all) your registered users.
        </li>
        <li>
            <a href="admin.php?action=ban" class="special">Ban users</a><br />
            Ban users based on IP address
        </li>
    </ul>
    <ul class="ls_menu2" style="float:left;width:50%;line-height:1.6em;">
        <li>
            <a href="admin.php?action=logs" class="special">Uploader logs</a><br />
            View and manage uploader logs.
        </li>
        <li>
            <a href="admin.php?action=stats" class="special">Statistics</a><br />
            View statistics on users, bandwidth and usage.
        </li>
        <li>
            <a href="admin.php?action=optimize" class="special">Optimize MySQL</a><br />
            Run the mysql optimize command on all the uploader tables.
        </li>
    </ul>
    <div class="spacer"></div>
</div>
<?php /* End tools menu */ endif; ?>


<?php /*  email */ if ( $action == 'emailer' ) : ?>
<form method="post" action="admin.php?action=emailer">
    <input type="hidden" name="task" value="send" />
    <table style="width:100%" id="emailer_tbl" cellspacing="1" cellpadding="5" border="0">
        <tr>
            <td>Mass email</td>
            <td><input type="checkbox" name="email[mass_email]" value="1" class="chkbox" id="mass_email" onclick="massEmailToggle(this.checked);" />
            <label for="mass_email">Send this email to all registered users.</label> Note: Receiver(s) and BCCs field will be ignored if this is checked.</td>
        </tr>
        <tr>
            <td class="tt">Receiver(s)</td>
            <td><textarea name="email[send_to]" cols="100" rows="2" id="email_send_to"><?=$receivers?></textarea></td>
        </tr>
        <tr>
            <td class="tt">BCCs</td>
            <td>
                <textarea name="email[bcc]" cols="100" rows="2" id="email_bcc"><?=@$bcc?></textarea>
            </td>
        </tr>
        <tr>
            <td>Email Subject</td>
            <td><input type="text" name="email[subject]" size="103" maxlength="200" /></td>
        </tr>

        <tr>
            <td class="tt">Email message</td>
            <td><textarea name="email[message]" cols="100" rows="12"></textarea></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Send (click once)" onclick="this.disabled=true;this.value='Sending email...';" />
                <input type="button" onclick="go('admin.php?action=tools');" value="Cancel" />
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
function massEmailToggle ( enabled )
{
    Form.disable ( 'email_send_to', enabled );
    Form.disable ( 'email_bcc', enabled );
}
</script>
<?php /* End  emailer */ endif; ?>