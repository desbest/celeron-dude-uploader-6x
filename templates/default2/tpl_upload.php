<!-- tpl_upload.php -->
<?php global $UPL; ?>
<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'upload':'upload.php').'?action=upload'?>" enctype="multipart/form-data" id="upload_form" onsubmit="return doSubmit()">
    <div style="float:left; width:30%;">
        <div>
            <h1>Upload options</h1>
            <select name="folder_id" id="folder_id" style="width:170px; margin:3px 0px 5px 0px;">
                <?php reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : ?>
                <option value="<?=$folder['folder_id']?>" <?=$folder['folder_id']==$folder_id?'selected="selected"':''?><?=$folder['folder_isgallery']?' class="colgreen"':''?>><?=entities($folder['folder_name'])?> (<?=$folder['files_count']?>)</option>
                <?php endwhile; ?>
            </select>
            <br />
            <input type="checkbox" name="extract_zip_files" value="1" id="extract_zip_files" class="chkbox" />
            <label for="extract_zip_files">Extract Zip files</label> (<span class="help" onclick="go('help.php?topic=batch_upload');">?</span>)
            <br />
            <input type="checkbox" name="create_img_tags" value="1" id="create_img_tags" checked="checked" class="chkbox"/>
            <label for="create_img_tags">Show linking codes</label>
        </div>

    </div>
    <div style="float:right; width:70%">
        <h1>Select files</h1>
        <p class="rounded note2" style="margin:0 0 10px 0;">
            <img src="templates/default2/images/lightbulb.gif" alt="lightbulb" class="img1" />
            You can upload many files without selecting them individually with the
            <a href="help.php?topic=batch_upload" class="special">Batch uploading</a> feature.
        </p>
        <div class="rounded green" style="padding-bottom:5px">
            <div id="uploadFields"></div>
            <noscript>
                <ol class="ls_menu">
                    <li><input type="file" name="file_1" size="60" /></li>
                    <li><input type="file" name="file_2" size="60" /></li>
                    <li><input type="file" name="file_3" size="60" /></li>
                </ol>
            </noscript>
            <div style="font-size:0.9em;margin-top:5px;">
                <?php if($restrictions['images_only']) : ?>
                Images only.
                <?php elseif($restrictions['allowed_types']!=''&&$restrictions['allowed_types']!='any'):?>
                Allowed file types: <?=str_replace(',', ', ',$restrictions['allowed_types'])?>.
                <?php endif; ?>
                Maximum file size is <strong><?=get_size($restrictions['max_file_size'],'B',0)?></strong>
            </div>
        </div>
        <div id="selectedFiles"></div>
        <p id="selectedStatus" class="rounded gray" style="margin-top:5px;padding:6px 6px 4px 10px;color:#666;">Click on Browse to select a file. Repeat to add more files.</p>
        <p style="margin: 10px 0 0 0">
            <input type="submit" id="submit_button" value="Upload selected files" />
            <input type="button" value="Cancel" onclick="if(doCancel())go('<?=$cancel_url?>');" />
        </p>
    </div>
    <div class="spacer"></div>
</form>

<link rel="stylesheet" type="text/css" href="<?=UPLOADER_URL?>templates/default2/upload.css?v=<?=UPLOADER_VERSION?>" media="all">
<script type="text/javascript" charset="utf-8" src="<?=UPLOADER_URL?>templates/default2/upload.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8">
<!--
var upl = new uploadForm ( 'uploadFields', 'selectedFiles', 'selectedStatus' );
<?php if ( $restrictions['allowed_types'] != '' && $restrictions['allowed_types'] != 'any' ) : ?>
var allowed_types = <?=json_encode(explode(',',$restrictions['allowed_types']))?>;
<?php else : ?>
var allowed_types = [];
<?php endif; ?>
upl.setOptions({fieldSize:55, iconURL:'<?=UPLOADER_URL?>templates/default2/images/icons/', allowedTypes: allowed_types});
upl.display();

function doCancel()
{
    if(upl.getSelectedCount())
        return confirm('Looks like you have selected some files. Are you sure you want to discard them?');
    return true;
}

function doSubmit()
{
    if(!upl.getSelectedCount()){uplAlert('You did not select any files. Click on Browse to add a file. Repeat this process to add more files. Click on <strong>Upload selected files</strong> once you are done selecting your files.');return false;}
	var submit_button = $('submit_button');
	submit_button.value = 'Uploading files, please wait...';
	submit_button.disabled = true;
    return true;
}
function enableSubmitButton ( )
{
	var submit_button = $('submit_button');
	submit_button.value = 'Upload selected files';
	submit_button.disabled = false;
}
addLoadEvent(enableSubmitButton);
-->
</script>
