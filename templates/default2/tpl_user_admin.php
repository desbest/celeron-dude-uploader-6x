
<!-- tpl_user_admin.php -->

<div class="rounded green" style="padding:8px;margin-bottom:4px;">
    <h1><?=$userinfo['username']?>'s account</h1>
    <p>
        <strong class="s2">Actions:</strong> &nbsp;
        <?=$action=='user_info' ? 'View user info' : '<a href="'.$userinfo['info_url'].'" class="special">View user info</a>'?> -
        <?=$action=='edit_user' ? 'Edit user info' : '<a href="'.$userinfo['edit_url'].'" class="special">Edit user info</a>'?> -
        <?=$action=='user_files' ? 'Manage user files' : '<a href="'.$userinfo['manage_files_url'].'" class="special">Manage user files</a>'?>
    </p>
</div>


<?php /* View user info */ if ( $action == 'user_info' ) : ?>
<table id="user_info_tbl1" cellspacing="0" cellpadding="5" style="width: 100%;margin-top:1px;" class="rowlines">
    <tr class="header" skip_alternate="skip_alternate">
        <td colspan="2">
            <strong class="s1">Basic user info</strong>
        </td>
    </tr>
    <tr>
        <td style="width: 150px;">Username</td>
        <td><?=$userinfo['username']?></td>
    </tr>
    <tr>
        <td>Current e-mail</td>
        <td><a href="<?=$userinfo['email_url']?>"><?=$userinfo['email']?></a> (registered with email <?=$userinfo['reg_email']?>)</td>
    </tr>
    <tr>
        <td>Registered on</td>
        <td><?=$userinfo['reg_date']?> with IP address <?=$userinfo['reg_ip']?></td>
    </tr>
    <tr>
        <td style="vertical-align:top;">Last login</td>
        <td><?=$userinfo['last_login_time']?> from <?=$userinfo['last_login_ip']?></td>
    </tr>
    <tr>
        <td>Account level</td>
        <td>
            <?php switch ( $userinfo['level'] )
            {
                case LEVEL_ADMIN: print 'Administrator'; break;
                case LEVEL_MODERATOR: print 'Moderator'; break;
                case LEVEL_NORMAL: print 'Normal user'; break;
                case LEVEL_SUBSCRIBER: print 'General subscriber'; break;
                case LEVEL_DONATOR: print 'Donator'; break;
                case LEVEL_MONTHLY: print 'Monthly subscriber'; break;
                case LEVEL_QUARTERLY: print 'Quarterly subscriber'; break;
                case LEVEL_YEARLY: print 'Yearly subscriber'; break;
            } ?>
        </td>
    </tr>
    <tr>
        <td style="vertical-align:top;">Account info</td>
        <td>
            <?=$userinfo['is_activated'] ? '<span style="color:green;">Activated</span>' : '<span style="color:red;">Not activated</span>' ?> -
            <?=$userinfo['is_approved'] ? '<span style="color:green;">Approved</span>' : '<span style="color:red;">Not approved</span>' ?> -
            <?=$userinfo['is_suspended'] ? '<span style="color:red;">Suspended</span>' : '<span style="color:green;">Not suspended</span>' ?>
        </td>
    </tr>
    <tr>
        <td style="vertical-align:top;">Admin comments</td>
        <td><?=$userinfo['comments']?></tr>
    </tr>
</table>


<table id="user_info_tbl2" cellspacing="1" cellpadding="5" style="width: 100%;margin-top:15px;" class="rowlines">
    <tr class="header" skip_alternate="skip_alternate">
        <td colspan="2">
            <strong class="s1">Space usage</strong>
        </td>
    </tr>
    <tr>
        <td style="width: 150px;">Max file storage</td>
        <td><?=$userinfo['max_storage']?></td>
    </tr>
    <tr>
        <td>Max file size</td>
        <td><?=$userinfo['max_filesize']?></td>
    </tr>
    <tr>
        <td>Allowed filetypes</td>
        <td>
            <?php if ( $userinfo['images_only'] ) : ?>
            Images only
            <?php else: ?>
            <?=in_array($userinfo['filetypes'],array('','any'))?'All file types allowed.':$userinfo['filetypes']?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td>File rename permission</td>
        <td>
            <?php switch ( $userinfo['allow_rename'] )
            {
                case 2: print 'Filename and extension'; break;
                case 1: print 'Filename only'; break;
                case 0: default: print 'Cannot rename files';
            } ?>
        </td>
    </tr>
    <tr>
        <td>Watermarking</td>
        <td><?= $userinfo['fl_watermark'] ? 'Watermark images uploaded by this user' : 'Do not watermark' ?></td>
    </tr>
    <tr>
        <td>Folder creation</td>
        <td>
            <?php if ( $userinfo['fl_allow_folders'] ) : ?>
            <?=$userinfo['fl_max_folders']==0?'Unlimited folders':$userinfo['fl_max_folders'].' folders max.'?>
            <?php else: ?>
            Cannot create folders
            <?php endif; ?>
        </td>
    </tr>
</table>


<table id="user_info_tbl3" cellspacing="1" cellpadding="5" style="width: 100%;margin-top:15px;" class="rowlines">
    <tr class="header" skip_alternate="skip_alternate">
        <td colspan="2">
            <strong class="s1">Bandwidth usage</strong>
        </td>
    </tr>
    <tr>
        <td style="width: 150px;">Bandwidth limit</td>
        <td><?=$userinfo['max_bandwidth']?> (<strong><?=$userinfo['bw_used']?></strong> used)</td>
    </tr>
    <tr>
        <td>Last bandwidth counter reset</td>
        <td><?=$userinfo['bw_last_reset_days']?> days ago on <?=$userinfo['bw_last_reset']?></td>
    </tr>
    <tr>
        <td>Bandwidth counter reset</td>
        <td>
            <?php if ( $userinfo['bw_reset_auto'] ) : ?>
            Automatically every <?=$userinfo['bw_reset_period']?> days
            <?php else: ?>
            Manually by an Admin
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td>Transfer rate limit</td>
        <td><?=$userinfo['bw_xfer_rate']==0?'Unlimited':$userinfo['bw_xfer_rate'] . 'KB/s'?></td>
    </tr>
</table>

<!-- alternating row color -->
<script type="text/javascript">
    alternateRowColor(getObj('user_info_tbl1'),'tr','#fff','#fafafa');
    alternateRowColor(getObj('user_info_tbl2'),'tr','#fff','#fafafa');
    alternateRowColor(getObj('user_info_tbl3'),'tr','#fff','#fafafa');
</script>
<?php endif; ?>







<?php /* Edit user info */ if ( $action == 'edit_user' ) : ?>

<?php /* If settings have been saved, display this little notification */ if ( $saved ) : ?>
<p class="note1" style="text-align:center;">All settings have been saved!</p>
<?php endif; ?>


<form method="post" action="admin.php">
    <input type="hidden" name="action" value="edit_user" />
    <input type="hidden" name="userid" value="<?=$userinfo['userid']?>" />
    <input type="hidden" name="task" value="save" />

    <fieldset>
        <legend>Basic user info</legend>
        <table id="tbl1" style="width:100%" cellspacing="1" cellpadding="3">
            <tr>
                <td style="width:130px;">New Password</td>
                <td><input type="password" name="userinfo[new_password]" value="" size="50" /> (enter new password to change)</td>
            </tr>
            <tr>
                <td>Email address</td>
                <td><input type="text" name="userinfo[email]" value="<?=$userinfo['email']?>" size="50" /></td>
            </tr>
            <tr>
                <td style="vertical-align:top;">Account info</td>
                <td>
                    <select name="userinfo[level]" style="width:130px;">
                        <option value="null" <?=@!isset($userinfo['level'])||$userinfo['level']=='null'?'selected="selected"':''?>>Any</option>
                        <option value="<?=LEVEL_ADMIN?>" <?=@$userinfo['level']==LEVEL_ADMIN?'selected="selected"':''?>>Admin</option>
                        <option value="<?=LEVEL_MODERATOR?>" <?=@$userinfo['level']==LEVEL_MODERATOR?'selected="selected"':''?>>Moderators</option>
                        <option value="<?=LEVEL_NORMAL?>" <?=@$userinfo['level']!='null'&&@$userinfo['level']=='0'?'selected="selected"':''?>>Normal</option>
                        <option value="<?=LEVEL_SUBSCRIBER?>" <?=@$userinfo['level']==LEVEL_SUBSCRIBER?'selected="selected"':''?>>Normal: Subscriber</option>
                        <option value="<?=LEVEL_DONATOR?>" <?=@$userinfo['level']==LEVEL_DONATOR?'selected="selected"':''?>>Normal: Donator</option>
                        <option value="<?=LEVEL_MONTHLY?>" <?=@$userinfo['level']==LEVEL_MONTHLY?'selected="selected"':''?>>Normal: Monthly subscriber</option>
                        <option value="<?=LEVEL_QUARTERLY?>" <?=@$userinfo['level']==LEVEL_QUARTERLY?'selected="selected"':''?>>Normal: Quaterly subscriber</option>
                        <option value="<?=LEVEL_YEARLY?>" <?=@$userinfo['level']==LEVEL_YEARLY?'selected="selected"':''?>>Normal: Yearly subscriber</option>
                    </select>
                    &nbsp;&nbsp;
                    <input type="checkbox" id="is_activated" value="1" class="chkbox" name="userinfo[is_activated]" <?=$userinfo['is_activated'] ? 'checked="checked"' : ''?> /><label for="is_activated">Activated</label>&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" id="is_suspended" value="1" class="chkbox" name="userinfo[is_suspended]" <?=$userinfo['is_suspended'] ? 'checked="checked"' : ''?> /><label for="is_suspended">Suspended</label>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">Admin comments</td>
                <td>
                    <input type="text" name="userinfo[comments]" size="80" maxlength="100" value="<?=$userinfo['comments']?>" /><br />
                    <span style="font-size:0.9em;">This will not be visible to the user</span>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Space usage &amp; restrictions</legend>
        <table id="tbl2" style="width:100%" cellspacing="1" cellpadding="3">
            <tr>
                <td style="width:130px;">Max file storage</td>
                <td>
                    <input type="text" name="userinfo[fl_max_storage]" value="<?=$userinfo['fl_max_storage']?>" size="5" id="max_storage" style="text-align:center;" />
                    &nbsp;MB (Enter 0 for unlimited)
                </td>
            </tr>
            <tr>
                <td>Max file size</td>
                <td>
                    <input type="text" name="userinfo[fl_max_filesize]" value="<?=$userinfo['fl_max_filesize']?>" size="5" id="max_filesize" style="text-align:center;" />
                    &nbsp;KB (Enter 0 for unlimited)
                </td>
            </tr>
            <tr>
                <td>Max folders</td>
                <td>
                    <input type="text" name="userinfo[fl_max_folders]" value="<?=$userinfo['fl_max_folders']?>" size="5" id="max_folders" maxlength="3" style="text-align:center"  />
                    folders (Enter 0 for unlimited)
                </td>
            </tr>
            <tr>
                <td>Images only</td>
                <td>
                    <input type="radio" name="userinfo[fl_images_only]" id="images_only1" value="1" <?=$userinfo['fl_images_only']==1?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="images_only1">User can only upload images</label>
                    <br />
                    <input type="radio" name="userinfo[fl_images_only]" id="images_only0" value="0" <?=$userinfo['fl_images_only']==0?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="images_only0">Non-images allowed</label>
                </td>
            </tr>
            <tr>
                <td>Images watermark</td>
                <td>
                    <input type="radio" name="userinfo[fl_watermark]" id="fl_watermark1" value="1" <?=$userinfo['fl_watermark']==1?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="fl_watermark1">Watermark images uploaded by this user</label>
                    <br />
                    <input type="radio" name="userinfo[fl_watermark]" id="fl_watermark0" value="0" <?=$userinfo['fl_watermark']==0?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="fl_watermark0">Do not watermark</label>
                </td>
            </tr>
            <tr>
                <td>Rename permission</td>
                <td>
                    <input type="radio" name="userinfo[fl_rename_permission]" id="allow_rename0" value="0" <?=$userinfo['fl_rename_permission']=='0'?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="allow_rename0">Not allowed</label>
                    <br />
                    <input type="radio" name="userinfo[fl_rename_permission]" id="allow_rename1" value="1" <?=$userinfo['fl_rename_permission']=='1'?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="allow_rename1">Filename only</label>
                    <br />
                    <input type="radio" name="userinfo[fl_rename_permission]" id="allow_rename2" value="2" <?=$userinfo['fl_rename_permission']=='2'?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="allow_rename2">Filename and extension</label>
                </td>
            </tr>
            <tr>
                <td>Folder creation</td>
                <td>
                    <input type="checkbox"  class="chkbox" name="userinfo[fl_allow_folders]" value="1" <?=$userinfo['fl_allow_folders']==1?'checked="checked"':''?> id="fl_create_folder" />
                    <label for="fl_create_folder">User can create folders</label>
                </td>
            </tr>
            <tr>
                <td>Allowed file types</td>
                <td><input type="text" name="userinfo[fl_allowed_types]" value="<?=$userinfo['fl_allowed_types']?>" size="80" /></td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Bandwidth usage &amp; restrictions</legend>
        <table id="tbl3" style="width:100%" cellspacing="1" cellpadding="3">
            <tr>
                <td style="width:130px;">Bandwidth limit</td>
                <td>
                    <input type="text" size="5" name="userinfo[bw_max]" value="<?=$userinfo['bw_max']?>" id="max_bandwidth" style="text-align:center;"  />
                    MB (Enter 0 for unlimited)
                </td>
            </tr>
            <tr>
                <td>Bandwidth period</td>
                <td>
                    <input type="text" size="5" name="userinfo[bw_reset_period]" value="<?=$userinfo['bw_reset_period']?>" id="bwrp" style="text-align:center;" />
                    days
                </td>
            </tr>
            <tr>
                <td>Bandwidth counter reset</td>
                <td>
                    <input type="radio" name="userinfo[bw_reset_auto]" id="bw_reset_auto1" value="1" <?=$userinfo['bw_reset_auto']==1?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="bw_reset_auto1">Automatically</label>
                    <br />
                    <input type="radio" name="userinfo[bw_reset_auto]" id="bw_reset_auto0" value="0" <?=$userinfo['bw_reset_auto']==0?'checked="checked"':''?> style="margin-bottom:-2px; margin-left:-1px;"/>
                    <label for="bw_reset_auto0">Manually</label>
                </td>
            </tr>
            <tr>
                <td>Transfer rate</td>
                <td>
                    <input type="text" name="userinfo[bw_xfer_rate]" value="<?=@$userinfo['bw_xfer_rate']?>" style="text-align:center;" size="5" /> KB/s
                </td>
            </tr>
        </table>
    </fieldset>

    <div style="text-align:center; margin-top:15px;">
        <input type="submit" value="Save settings" class="blue_button" /> <input type="button" value="Cancel" onclick="go('admin.php?action=users');" class="blue_button" /> <input type="reset" value="Undo changes" onclick="return confirm('Reset form?');"  class="blue_button"/>
    </div>
</form>

<!-- alternating row color -->
<script type="text/javascript">
    alternateRowColor(getObj('tbl1'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('tbl2'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor(getObj('tbl3'),'tr','#F5F5F5','#FBFBFB');
</script>


<?php elseif ( $action == 'user_files' ) : require_once('tpl_icons.php');?>
<table style="width:100%;color:#505050;" id="user_folders_tbl" cellspacing="0" cellpadding="5" class="rowlines">
    <tr skip_alternate="skip_alternate" class="header">
        <td>&nbsp; Folder name</td>
        <td class="ct" style="width:100px;">Size</td>
        <td class="ct" style="width:100px;">Actions</td>
    </tr>
    <?php reset ( $userfolders ); while ( list ( , $folder ) = each ( $userfolders ) ) : ?>
    <tr>
        <td>
            <img src="<?=tpl_get_folder_icon(($folder['permission']))?>" alt="" class="img1" />
            &nbsp;<a href="<?=$folder['url']?>" class="<?=$folder['folder_isgallery']?'special_green':'special'?>"><?=entities($folder['folder_name'])?></a> - <?=$folder['folder_description']==''?'No Description':entities(str_preview($folder['folder_description'],100))?>
        </td>
        <td class="ct"><?=$folder['files_count']? number_format($folder['files_count']) . ' files ('.get_size($folder['size']).')':'Empty'?></td>
        <td class="cr">
            <?php if ( $folder['folder_deleteable'] ) : ?>
            <a href="<?=$folder['delete_url']?>" onclick="return confirm('Are you sure you want to delete this folder? This cannot be undone!');"><img src="templates/default2/images/b_delete.gif" alt="" class="img1" /></a>
            <?php endif; ?>
            <a href="<?=$folder['edit_url']?>"><img src="templates/default2/images/b_edit.gif" alt="" class="img1" /></a>
        </td>
    </tr>
    <?php endwhile; ?>
</table>

<form method="post" action="admin.php" id="userfiles_form">
    <input type="hidden" name="action" value="" />
    <input type="hidden" name="userid" value="<?=$userinfo['userid']?>" />
    <input type="hidden" name="current_folder_id" value="<?=$current_folder['folder_id']?>" />
    <input type="hidden" name="move_to_folder_id" value="" />
    <div class="rounded green" style="margin: 10px 0 2px 0;padding: 6px 10px 6px 10px; color:#505050;">
        <img src="templates/default2/images/<?=$current_folder['folder_isgallery']?'folder_image.gif':'folder_regular.gif'?>" alt="" class="img1" />
        &nbsp;<strong><?=entities($current_folder['folder_name'])?></strong>
        <p><?=entities($current_folder['folder_description'])?></p>
        <?php if ( $current_folder['files_count'] ) : ?>
        <p>
            <select name="folder_id" onchange="file_action_dropdown(this);">
                <option value="" selected="selected">Selected files actions...</option>
                <option value="delete">&nbsp; &nbsp;Delete</option>
                <option value="" disabled="disabled">Move to...</option>
                <?php reset ( $userfolders ); while ( list ( $i, $folder ) = each ( $userfolders ) ) : ?>
                <option value="move_<?=$folder['folder_id']?>"<?=$folder['folder_isgallery']?' class="colgreen"':''?>>&nbsp; &nbsp;<?=entities(str_preview($folder['folder_name'],50))?> (<?=$folder['files_count']?>)</option>
                <?php endwhile; ?>
            </select>
        </p>

        <div class="spacer"></div>
        <p style="float:left;width:70%;">
            <strong class="colgreen">Select:</strong>
            &nbsp;<span class="link" onmousedown="checkAllBoxes('userfiles_form','file_ids[]',1);return false;">All</span>,
            <span class="link" onmousedown="checkAllBoxes('userfiles_form','file_ids[]',0);return false;">None</span>
            &nbsp; &nbsp;
            <strong class="colgreen">Sort files:</strong>
            &nbsp;<a href="<?=$sort_url['type']?>" class="special" <?=$sort_by=='type'?'style="font-style:italic;"':''?>>Type</a>,
            <a href="<?=$sort_url['name']?>" class="special" <?=$sort_by=='name'?'style="font-style:italic;"':''?>>Name</a>
            <a href="<?=$sort_url['size']?>" class="special" <?=$sort_by=='size'?'style="font-style:italic;"':''?>>Size</a>
            <a href="<?=$sort_url['date']?>" class="special" <?=$sort_by=='date'?'style="font-style:italic;"':''?>>Date</a>
        </p>
        <p style="float:right;width:30%;text-align:right;">
            <?php if ( $current_page > 1 ) : ?><a href="<?=$prev_page_url?>" class="special">Previous</a><?php endif; ?>
            (<strong><?=$file_start?></strong> - <strong><?=$file_end?></strong> of <strong><?=$current_folder['files_count']?></strong>)
            <?php if ( $current_page < $total_pages ) : ?><a href="<?=$next_page_url?>" class="special">Next</a><?php endif; ?>
        </p>
        <div class="spacer"></div>
        <?php else: ?>
        <p>This folder is empty.</p>
        <?php endif; ?>
    </div>

    <table style="width:100%;color:#505050;margin:2px auto 0px auto;" id="user_files_tbl" cellspacing="0" cellpadding="5" class="rowlines">
        <?php reset ( $userfiles ); while ( list ( , $file ) = each ( $userfiles ) ) : ?>
        <tr>
            <td>
                <img src="templates/default2/images/icons/<?=tpl_myfiles_get_icon(get_extension($file['file_name']))?>" alt="icon" class="img1" />
                &nbsp;<a href="<?=$file['url']?>"><?=entities($file['file_name'])?></a>
            </td>
            <td class="ct" style="width:60px;"><?=get_size($file['file_size'],'B',0)?></td>
            <td class="ct" style="width:60px;"><?=get_date('M-d-y',$file['file_date'])?></td>
            <td class="ct" style="width:75px;"><a href="<?=$file['direct_url']?>">Direct URL</a>, <a href="<?=$file['edit_url']?>">Edit</a></td>
            <td class="ct" style="width:20px;"><input type="checkbox" class="chkbox" name="file_ids[]" value="<?=$file['file_id']?>" /> <a href="<?=$file['url']?>"></td>
        </tr>
        <?php endwhile; ?>
    </table>
</form>
<script type="text/javascript" charset="utf-8">
<!--
    alternateRowColor($('user_files_tbl'),'tr','#FBFBFB','#F5F5F5');
    alternateRowColor($('user_folders_tbl'),'tr','#FBFBFB','#F5F5F5');
    alternateRowColor($('files_options_tbl1'),'tr','#F5F5F5','#FBFBFB');
    alternateRowColor($('files_options_tbl2'),'tr','#F5F5F5','#FBFBFB');

    function file_action_dropdown(select)
    {
        var option = select.value;
        var form = select.form;

        if ( option != '' )
        {
            if ( option.substr ( 0, 5 ) == 'move_' )
            {
                form.elements.move_to_folder_id.value = option.substr ( 5 );
                form.elements.action.value = 'move_files';
                form.submit();
                return true;
            }
            else if ( option == 'delete' && confirm('Are you sure you wanted to delete the selected files? This action cannot be undone!') )
            {
                form.elements.action.value = 'delete_files';
                form.submit();
                return true;
            }
        }
        select.selectedIndex = 0;
    }
-->
</script>
<?php elseif ( $action == 'edit_folder' ) : ?>
<form method="post" action="<?=UPLOADER_URL?>admin.php?action=edit_folder&amp;userid=<?=$folder['userid']?>&amp;folder_id=<?=$folder['folder_id']?>">
    <input type="hidden" name="action" value="edit_folder" />
    <input type="hidden" name="task" value="edit" />
    <input type="hidden" name="folder_id" value="<?=$folder['folder_id']?>" />

    <?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>

    <table cellspacing="1" cellpadding="2" border="0" style="margin-top: 10px;">
        <tr>
            <td>Folder name</td>
            <td>
                <input type="text" name="folder[name]" value="<?=entities(@$folder['folder_name'])?>" size="40" maxlength="50" />
            </td>
        </tr>
        <tr>
            <td>Description&nbsp;(optional)</td>
            <td><input type="text" name="folder[description]" value="<?=entities(@$folder['folder_description'])?>" size="80" maxlength="255" /></td>
        </tr>
        <tr>
            <td class="tt">Access permission</td>
            <td>
                <ul>
                    <li>
                        <input type="radio" name="folder[access]" value="public" id="folder_access1" class="radio" <?=$folder['access']=='public'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access1">Public</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="hidden" id="folder_access3" class="radio" <?=$folder['access']=='hidden'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access3">Hidden</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="private" id="folder_access2" class="radio" <?=$folder['access']=='private'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', false);" />
                        <label for="folder_access2">Private</label>
                        <fieldset style="border:0px;margin:2px 0 0 18px;padding:0px;" id="folder_private_permission">
                            <ul id="folder_private_access">
                                <li>
                                    <input type="checkbox" name="folder[friend_access]" value="1" id="folder_friend_access" class="chkbox" <?=$folder['friend_access']?'checked="checked"':''?> />
                                    <label class="colgreen" for="folder_friend_access">user's friends</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="folder[family_access]" value="1" id="folder_family_access" class="chkbox" <?=$folder['family_access']?'checked="checked"':''?> />
                                    <label class="colfuscia" for="folder_family_access">user's family</label>
                                </li>
                            </ul>
                        </fieldset>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="tt">Advanced settings</td>
            <td>
                The following settings should NOT be enabled on the home directory "My Documents"<br />
                <input type="checkbox" name="folder[deleteable]" class="chkbox" value="1" id="folder_deletable" <?=$folder['folder_deleteable']?'checked="checked"':''?>/>
                <label for="folder_deletable">Folder can be deleted by user.</label>
                <br />
                <input type="checkbox" name="folder[renameable]" class="chkbox" value="1" id="folder_renameable" <?=$folder['folder_renameable']?'checked="checked"':''?> />
                <label for="folder_renameable">Folder can be renamed by user.</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Save changes" />&nbsp;<input type="button" value="Cancel" onclick="go('<?=$back_url?>');" /></td>
        </tr>
    </table>
</form>
<?php elseif ( $action == 'edit_file' ) :  ?>
<form method="post" action="<?=UPLOADER_URL?>admin.php?action=edit_file&amp;userid=<?=$file['userid']?>&amp;file_id=<?=$file['file_id']?>">
    <input type="hidden" name="action" value="edit_file" />
    <input type="hidden" name="task" value="save" />

    <table cellspacing="1" cellpadding="3">
        <tr>
            <td style="width:70px;">File name</td>
            <td><input type="text" size="50" name="file[name]" maxlength="128" value="<?=entities($file['file_name'])?>" /></td>
        </tr>
        <tr>
            <td>Extension</td>
            <td>
                <input type="text" size="5" name="file[extension]" maxlength="10" value="<?=entities($file['file_extension'])?>" />
                Used internally as mime type. Will not change the extension in the file name.
            </td>
        </tr>
        <tr>
            <td>Is image</td>
            <td>
                <select name="file[is_image]"/>
                    <option value="1" <?=$file['file_isimage']?'selected="selected"':''?>>Yes</option>
                    <option value="0" <?=!$file['file_isimage']?'selected="selected"':''?>>No</option>
                </select>
                This should not be changed, but there may be rare situations where it should be so the option is here.
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Save" /> <input type="button" value="Cancel" onclick="go('<?=$back_url?>');" /></td>
        </tr>
    </table>
</form>
<?php endif; ?>