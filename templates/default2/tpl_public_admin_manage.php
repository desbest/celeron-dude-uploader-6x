<div class="rounded green">
    <div id="edit_set">
        <form method="post" action="admin.php">
            <input type="hidden" name="action" value="edit_public_set_info" />
            <input type="hidden" name="upload_id" value="<?=(int)$upload_set['upload_id']?>" />
            <table cellspacing="3" border="0">
                <tr>
                    <td>Permission</td>
                    <td>
                        <input type="checkbox" class="chkbox" name="upload[public]" id="upload_public" value="1" <?=$upload_set['upload_ispublic']?'checked="checked"':''?> />
                        <label for="upload_public">Allow these files to be browsed by others</label>
                    </td>
                </tr>
                <tr>
                    <td>Upload key</td>
                    <td><input type="text" name="upload[key]" value="<?=entities($upload_set['upload_key'])?>" size="10" maxlength="32" /></td>
                </tr>
                <tr>
                    <td>Uploaded by</td>
                    <td><input type="text" name="upload[name]" value="<?=entities($upload_set['upload_name'])?>" autocomplete="off" maxlength="64" size="20" /> [IP Address: <span class="help" onclick="prompt('User IP',this.innerHTML);"><?=$upload_set['upload_ip']?></span>] Blank name is replaced with "Anonymous"</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input type="text" name="upload[description]" autocomplete="off" value="<?=entities($upload_set['upload_description'])?>" maxlength="255" size="100" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Save changes" /></td>
                </tr>
            </table>
        </form>
    </div>
    <div style="color:#576345;border-top:1px solid #f0f0f0;margin-top:10px">
        <div style="margin-top:10px">
            <strong class="s2">Select:</strong>
            <span class="link" onmousedown="checkAllBoxes('manage_form','file_ids[]',true);">All</span>,
            <span class="link" onmousedown="checkAllBoxes('manage_form','file_ids[]',false);">None</span>
            &nbsp; &nbsp;
            <select onchange="publicManageAction(this)">
                <option value="">Selected files actions</option>
                <option value="delete">&nbsp; Delete</option>
            </select>
            &nbsp; &nbsp;
            <a href="<?=UPLOADER_URL.'admin.php?action=pupload'?>" class="special">Back to uploaded sets</a>
        </div>
    </div>
</div>

<form method="post" action="admin.php" id="manage_form">
    <input type="hidden" name="action" value="" />
    <input type="hidden" name="upload_id" value="<?=(int)$upload_set['upload_id']?>" />
    <div class="gallery">
        <?php reset ( $files ); while ( list ( , $file ) = each ( $files ) ) : ?>
        <div class="cell" style="width:110px;margin:0 0 15px 7px;">
            <div class="inner_cell">
                <div class="top">Views: <?=number_format($file['file_views'])?></div>
                <div class="center"><a href="<?=$file['url']?>"><img src="<?=$file['file_isimage']?$file['square_thumb_url']:UPLOADER_URL . 'templates/default2/images/nothumb.gif'?>" alt="<?=entities($file['file_name'], ENT_QUOTES)?>" class="thumb" /></a></div>
                <div class="bottom">
                    <span class="name"><?=entities(wordwrap($file['file_name'], 15, "\r\n", true))?></span>
                    <div class="extra_options"><input type="checkbox" name="file_ids[]" value="<?=$file['file_id']?>" class="chkbox" /></div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <div class="spacer"></div>
</form>

<script type="text/javascript">
<!--
function publicManageAction(select)
{
    var value = select.value;
    var form = $('manage_form');
    var check_count = countCheckedBoxes ( form, 'file_ids[]' );

    if ( !check_count )
        help ( 'You did not select any files.' );
    else
    {
        if ( value == 'delete' )
        {
            form.elements.action.value = 'delete_public_files';
            form.submit();
            return false;
        }
        else if ( value == 'reset_view' )
        {
            form.elements.action.value = 'deletefiles';
            form.submit();
            return false;
        }
    }

    select.selectedIndex = 0;
}
-->
</script>