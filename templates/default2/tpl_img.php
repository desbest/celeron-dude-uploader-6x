<!-- tpl_img.php -->
<h1>Linking codes</h1>

<script type="text/javascript" charset="utf-8">
<!--
var img_files = {};
try
{
	img_files = <?=json_encode ( $files )?>;
}
catch ( error ) { img_files = {}; }


function write_tags ( type, where )
{
	if ( typeof where == 'undefined' ) return false;
	var target = $(where);
	if ( !target ) return false;

	var str = new Array();

	for ( var i = 0; i < img_files.length; ++i )
	{
		var file = img_files[i];

		switch ( type )
		{
			case 'link_bb':
			{
				if ( file.is_image )
					var code = '[url=' + file.url + '][img]' + file.small_thumb_url + '[/img][/url]';
				else
					var code = '[url=' + file.url + ']' + file.name + '[/url]';
				str.push(code);
			}
			break;

			case 'link_html':
			{
				if ( file.is_image )
					var code = '<a href="' + file.url + '">\n    <img src="' + file.small_thumb_url + '" alt="' + file.name + '" />\n</a>\n';
				else
					var code = '<a href="' + file.url + '">' + file.name + '</a>\n';
				str.push(code);
			}
			break;

			case 'direct_bb':
			{
				var code = file.is_image ? '[img]' + file.direct_url + '[/img]' : '[url=' + file.direct_url + ']' + file.name + '[/url]';
				str.push(code);
			}
			break;

			case 'direct_html':
			{
				var code = file.is_image ? '<img src="' + file.direct_url + '" alt="' + file.name + '" />' : '<a href="' + file.direct_url + '">' + file.name + '</a>';
				str.push(code);
			}
			break;

			case 'direct_url':
			{
				str.push ( file.direct_url );
			}
			break;
		}
	}
	target.value = str.join("\r\n");
}

function switch_bb_case ( bb_case )
{
	var target = $('link_codes');

	function toUpper(str){alert(str);}

	if ( bb_case == 'upper' )
	{
		target.value = target.value.replace ( /\[(\/)?img\]/gm, '[$1IMG]' );
		target.value = target.value.replace ( /\[(\/)?url\]/gm, '[$1URL]' );
		target.value = target.value.replace ( /\[url=/gm, '[URL=' );
	}
	else
	{
		target.value = target.value.replace ( /\[(\/)?IMG\]/gm, '[$1img]' );
		target.value = target.value.replace ( /\[(\/)?URL\]/gm, '[$1url]' );
		target.value = target.value.replace ( /\[URL=/gm, '[url=' );
	}
}

function imgInit ( )
{
	write_tags('link_bb','link_codes');
}
addLoadEvent ( imgInit );
-->
</script>

<p>
	Linking codes: <span class="link" onclick="write_tags('link_bb','link_codes');return false;">BB format</span> - <span class="link" onclick="write_tags('link_html','link_codes');return false;">HTML format</span>
	&nbsp; &nbsp; &nbsp;Direct link: <span class="link" onclick="write_tags('direct_bb','link_codes');return false;">BB format</span> -
	<span class="link" onclick="write_tags('direct_html','link_codes');return false;">HTML format</span> -
    <span class="link" onclick="write_tags('direct_url','link_codes');return false;">Just the direct URLs</span>
</p>

<p>
	BB Code casing:
	<span class="link" onclick="switch_bb_case('lower')">Lower case</span> -
	<span class="link" onclick="switch_bb_case('upper')">Upper case</span>
</p>

<p>
    <textarea rows="15" cols="135" id="link_codes" style="padding: 2px;"></textarea>
    <br /><br />
	<input type="button" value="Select all" onclick="$('link_codes').select();"/>
    <input type="button"  value="Copy to clipboard" onclick="copyTextArea('link_codes');"/>
    <input type="button"  value=" &nbsp;OK&nbsp; " onclick="go('<?=addslashes($back_url)?>');" />
</p>
