<?php if ( $action == 'add' ) : ?>
<h1>Add <?=$user['username']?> to your contact list</h1>
<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'contacts/add/'.$user['username']:'contacts.php?action=add&amp;userid='.$user['userid'])?>">
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="task" value="add" />
	<input type="hidden" name="userid" value="<?=$user['userid']?>" />

	<p>You may also set your relationship with <strong><?=$user['username']?></strong> by checking the appropriate boxes below.</p>
	<p>
		<input type="checkbox" class="chkbox" name="friend" value="1" id="mark_friend" />
		<label for="mark_friend"><?=$user['username']?> is a <span class="colgreen">friend</span></label>
		<br />
		<input type="checkbox" class="chkbox" name="family" value="1" id="mark_family" />
		<label for="mark_family"><?=$user['username']?> is <span class="colfuscia">family</span></label>
	</p>
	<p style="margin-top:5px">
		<input type="submit" value="Add <?=$user['username']?> to your contact list" />
		<input type="button" value="Cancel" onclick="go('<?=$back_url?>')" />
	</p>
</form>
<p><br /><a href="<?=UPLOADER_URL.(MOD_REWRITE?'contacts':'contacts.php')?>" class="special">See all of your contacts</a></p>
<?php elseif ( $action == 'edit' ) : ?>
<h1>Edit your relationship with <?=$user['username']?></h1>
<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'contacts/edit/'.$user['username']:'contacts.php?action=edit&amp;userid='.$user['userid'])?>">
	<input type="hidden" name="action" value="edit" />
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="userid" value="<?=$user['userid']?>" />
	<input type="hidden" name="back_url" value="<?=$back_url?>" />

	<p class="p1">Set your relationship with <strong><?=$user['username']?></strong>. If none is checked, <strong><?=$user['username']?></strong> will remain as a plain contact.</p>
	<fieldset id="contact_relationship" class="noposition">
		<p>
			<input type="checkbox" class="chkbox" name="friend" value="1" id="mark_friend" <?=$user['relationship']['is_friend']?'checked="checked"':''?> />
			<label for="mark_friend"><?=$user['username']?> is a <span class="colgreen">friend</span></label>
			<br />
			<input type="checkbox" class="chkbox" name="family" value="1" id="mark_family" <?=$user['relationship']['is_family']?'checked="checked"':''?> />
			<label for="mark_family"><?=$user['username']?> is <span class="colfuscia">family</span></label>
		</p>
	</fieldset>

	<p class="noposition">
		<input type="checkbox" name="remove" id="remove_contact" value="1" class="chkbox" />
		<label for="remove_contact">Remove <?=$user['username']?> from your contact list.</label>
	</p>

	<p style="margin-top:5px">
		<input type="submit" value="Save changes" />
		<input type="button" value="Cancel" onclick="go('<?=$back_url?>')" />
	</p>
</form>
<script type="text/javascript">
<!--
var remove_contact = $('remove_contact');
remove_contact.onclick = function ( )
{
	Form.disable ( $('contact_relationship'), this.checked );
}
-->
</script>
<?php else: ?>
<div class="rounded green">
	<h1>Your contacts</h1>
	<p class="p1">
		<strong class="s2">View:</strong>
		<?php if ( $filter_contact_type == '' || $filter_contact_type == 'all' ) : ?>All contacts<?php else:?><a href="<?=$filter_url['all']?>" class="special">All contacts</a><?php endif;?> -
		<?php if ( $filter_contact_type == 'friend' ) : ?>friends<?php else:?><a href="<?=$filter_url['friend']?>" class="special">friends</a><?php endif;?> -
		<?php if ( $filter_contact_type == 'family' ) : ?>family<?php else:?><a href="<?=$filter_url['family']?>" class="special">family</a><?php endif;?> -
		<?php if ( $filter_contact_type == 'both' ) : ?>Friends &amp; family<?php else:?><a href="<?=$filter_url['both']?>" class="special">friends &amp; family</a><?php endif;?>
	</p>
</div>
<?php if ( count ( $contacts ) ) : ?>
<table id="contacts_tbl" class="rowlines" style="width:100%;color:#888;margin-top:15px;" cellspacing="0" cellpadding="8">
	<tr class="header" skip_alternate="skip_alternate">
		<td>Contact's name</td>
		<td class="ct" style="width:130px">Relationship</td>
		<td class="cr" style="width:160px">Actions</td>
	</tr>
	<?php reset ( $contacts ); while ( list ( , $contact ) = each ( $contacts ) ) : ?>
	<tr>
		<td><a href="<?=$contact['info_url']?>" class="special"><?=entities($contact['username'])?></a></td>
		<td class="ct">
			<?=$contact['relationship']['is_friend']?'<span class="colgreen">Friend</span>':''?>
			<?=$contact['relationship']['is_family']?($contact['relationship']['is_friend']?'and ':'').'<span class="colfuscia">family</span>':''?>
			<?=!$contact['relationship']['is_friend']&&!$contact['relationship']['is_family']?'Contact':''?>
		</td>
		<td class="cr">
			<a href="<?=$contact['edit_contact_url']?>" class="special">Edit contact</a> -
			<a href="<?=$contact['del_contact_url']?>" class="special" onclick="return confirm('Are you sure you want to remove this user from your contact list?')">Remove contact</a>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<script type="text/javascript">
<!--
alternateRowColor($('contacts_tbl'),'tr','#fff','#fcfcfc');
-->
</script>
<?php else: ?>
<p class="ct p1" style="margin-top:10px">No contacts found</p>
<?php endif; ?>
<?php endif; ?>