var rowcolor1 = '#fff'; // color 1 for the rows
var rowcolor2 = '#fafafa'; // alternate color for the rows
var rowcolor3 = '#fbffdf'; // highlight
var images_per_row = 5;

var mf_current_folder = new Array();
var mf_files = new Array();
var mf_form = null;
var mf_files_container = null;
var mf_sort_type = 'date';
var mf_view_mode = 'details';

function mf_init()
{
	mf_form = $('mf_form');
	mf_files_container = $('mf_files_container');

	var folder_view_mode = getCookie ( 'uploader_folderview' );
	mf_view_mode = folder_view_mode ? folder_view_mode : mf_current_folder.folder_isgallery?'gallery':'details';
	mf_show(mf_view_mode);
}

function mf_addfile ( file_id, name, extension, size, date, url, direct_url, thumb_url, is_image, views, at_index )
{
	var index = typeof at_index == 'undefined' ? mf_files.length : at_index;
	var file = new Array();
	file.file_id = file_id;
	file.name = name;
	file.size = size;
	file.date = date;
	file.url = url;
	file.direct_url = direct_url;
	file.thumb_url = thumb_url;
	file.is_image = is_image;
	file.ext = extension;
	file.views = views;
	file.selected = typeof at_index != 'undefined' ? mf_files[index].selected : 0;
	mf_files[index] = file;
	return index;
}

function mf_addfile_info ( index, info )
{
	Object.extend(mf_files[index], info);
}

function mf_show ( mode )
{
	if ( typeof mode == 'undefined' || mode == 'details' ) mf_show_details();
	else mf_show_gallery();
}


function mf_show_details ( set_view_mode )
{
	if ( !mf_files.length ) return;
	mf_view_mode = 'details';
	if ( set_view_mode ) setCookie ( 'uploader_folderview', mf_view_mode );
	var is_gallery = mf_current_folder.folder_isgallery;
	var html = '<table style="width:100%;margin-top:5px;color:#545454;" cellspacing="0" cellpadding="4" class="rowlines" id="mf_table">';
	for ( var i = 0; i < mf_files.length; ++i )
	{
		var file = mf_files[i];
		var icon_url = base_url + 'templates/default2/images/icons/' + get_icon ( file.ext );
		var td = '<td><img src="' + icon_url + '" alt="icon" class="img1" /> &nbsp;<span id="mf_rename_'+i+'"><a href="' + file.url + '">' + str_preview(file.name.escapeHTML(),45) + '</a></span></td>';
		td += '<td style="width:40px;text-align:center;">' + get_size(file.size) + '</td>';
		td += '<td style="width:110px;text-align:center;">' + file.date + '</td>';
		td += '<td style="width:45px;text-align:right;"> <img src="'+(base_url+'templates/default2/images/icon_rename.gif')+'" alt="" onclick="mf_init_rename('+i+');" class="img1" style="cursor:pointer;" title="Quick rename" />';
		td += '&nbsp;&nbsp;<input type="checkbox" id="mf_checkbox_' + i + '" name="file_ids[]" value="' + file.file_id + '" class="chkbox" onclick="mf_check_onclick('+i+');" /></td>';
		html += '<tr id="mf_row_'+i+'">' + td + '</tr>';
	}
	html += '</table>';
	mf_files_container.innerHTML = html;
	alternateRowColor ( $('mf_table'), 'tr', rowcolor1, rowcolor2 );
}

function mf_show_gallery ( set_view_mode )
{
	if ( !mf_files.length ) return true;
	if ( !mf_current_folder.folder_isgallery ) return mf_show_details();
	mf_view_mode = 'gallery';
	if ( set_view_mode ) setCookie ( 'uploader_folderview', mf_view_mode );
	var i = 0;
	var rows = Math.ceil ( mf_files.length / images_per_row );
	var html = '<div class="gallery">';
	for ( var r = 0; r < rows; ++r )
	{
		html += '<div class="row">';
		for ( var c = 0; c < images_per_row; ++c )
		{
			if ( i < mf_files.length )
			{
				var file = mf_files[i];
				html += '<div class="cell" style="width:'+(100/images_per_row)+'%;">';
					html += '<div class="inner_cell">';
						html += '<div class="top"><input type="checkbox" id="mf_checkbox_' + i + '" name="file_ids[]" value="' + file.file_id + '" class="chkbox" /> Views: ' + file.views + '</div>';
						html += '<div class="center">';
							html += '<div class="thumb"><a href="' + file.url + '"><img src="' + file.thumb_url + '" alt="' + file.name + '" class="img1" /></a></div>';
						html += '</div>';
						html += '<div class="bottom"><span class="name" id="mf_rename_' + i + '">' + file.name.wordWrap(15,'<br />',true) + '</span></div>';
					html += '</div>';
				html += '</div>';
				++i;
			}
		}
		html += '<div class="spacer"></div></div>';
	}
	html += '</div><div class="spacer"></div>';
	mf_files_container.innerHTML = html;

	for ( var i = 0; i < mf_files.length; ++i )
	{
		var file = mf_files[i];
		new quickEditFileName ( 'mf_rename_' + i, file.name, file.file_id, i, {inputSize:35, maxInput:64, textAlign:'center', maxWidth:80, style:{fontSize:'0.9em'} } );
	}
}

function mf_show_options ( event, index )
{
	return false;

	var options = $('mf_file_options_'+index);
	if ( options ) return true;
	event = event || window.event;
	var options = document.createElement ( 'div' );
	var styles = { 'width' : '150px', 'position' : 'absolute' };
	setStyles ( options, styles );
	options.id = 'mf_file_options_' + index;
	options.style.left = ( event.clientX - ( 150 + 20 ) ) + 'px';
	options.style.top = event.clientY + 'px';

	var html = '<div class="mf_options">Edit<br />Download<br />Rename</div>';
	options.innerHTML = html;
	document.body.appendChild ( options );
}

function _mf_sort_size ( left, right ) { if ( left.size == right.size ) return 0; if ( left.size > right.size ) return 1; return -1; }
function _mf_sort_type ( left, right ) { if ( left.ext == right.ext ) return 0; if ( left.ext > right.ext ) return 1; return -1; }
function _mf_sort_date ( left, right ) { if ( left.date == right.date ) return 0; if ( left.date > right.date ) return 1; return -1; }
function _mf_sort_name ( left, right ) { if ( left.name.toLowerCase() == right.name.toLowerCase() ) return 0; if ( left.name.toLowerCase() > right.name.toLowerCase() ) return 1; return -1; }

function mf_sort ( column )
{
	if ( mf_sort_type == column ) mf_files.reverse();
	else
	{
		switch ( column )
		{
			case 'size': mf_files.sort ( _mf_sort_size ); break;
			case 'type': mf_files.sort ( _mf_sort_type ); break;
			case 'date': mf_files.sort ( _mf_sort_date ); break;
			case 'name': mf_files.sort ( _mf_sort_name ); break;
		}
	}
	mf_sort_type = column;
	$('sort_size').style.fontStyle = ( column == 'size' ? 'italic' : 'normal' );
	$('sort_type').style.fontStyle = ( column == 'type' ? 'italic' : 'normal' );
	$('sort_date').style.fontStyle = ( column == 'date' ? 'italic' : 'normal' );
	$('sort_name').style.fontStyle = ( column == 'name' ? 'italic' : 'normal' );
	mf_show(mf_view_mode);
}


var quickEditFileName = Class.create();
quickEditFileName.prototype = Object.extend ( new quickEditBase(), {
	initialize : function(container, defaultName, fileId, arrayIndex, options)
	{
		if(!Ajax.enabled)return false;
		this.ajax = new AjaxRequest(base_url+'myfiles.php?action=quickrename', {'method':'post', 'response':'json'});
		this.ajax.oncomplete = this.ajaxCallback.bind(this);
		this.fileId = fileId;
		this.arrayIndex = arrayIndex;
		this.value = defaultName;
		this.setOptions(Object.extend({inputSize:50, maxInput:128, maxCharsVisible:80}, options || {}));
		this.container = $(container);
		this.initContainer();
	},

	hideEditable : function(changed)
	{
		var file = mf_files[this.arrayIndex];
		if(this.editBox.parentNode) this.editBox.parentNode.removeChild(this.editBox);
		this.container.style.display = '';
		if ( mf_view_mode == 'gallery' )
		{
			this.container.innerHTML = file.name.wordWrap(15,'<br />',true);
		}
		else
		{
			this.container.innerHTML = '<a href="' + file.url + '">' + str_preview(file.name.escapeHTML(),45) + '</a>';
			this.restoreContainer();
		}
	},

	ajaxCallback : function()
	{
		var response = this.ajax.response;
		if ( response.result == 'success' )
		{
			var file = response.file;
			mf_addfile ( file.file_id, file.file_name, file.file_extension, file.file_size, file.date_string, file.url, file.direct_url, file.square_thumb_url,  file.file_isimage, file.file_views, this.arrayIndex );
			this.hideEditable();
		}
		else
		{
			switch ( response.message )
			{
				case 'NO_RENAME': uplAlert ( 'Sorry but you are not allowed to rename this file.' ); this.hideEditable(); break;
				case 'NO_RENAME_EXTENSION': uplAlert ( 'Sorry but you are not allowed to change the file extension.' ); this.hideEditable(); break;
				case 'LONG_NAME': uplAlert ( 'The file name is too long.' ); break;
				case 'SHORT_NAME': uplAlert ( 'The file name is too short.' ); break;
				case 'EMPTY_NAME': uplAlert ( 'The file name cannot be blank.' ); this.inputField.value = this.value; this.inputField.select(); this.inputField.focus(); break;
				default: uplAlert ( 'There was a problem updating the file name.' ); this.hideEditable(); break;
			}
		}
	},

	doUpdate : function()
	{
		if(this.inputField.value != this.value)
		{
			var postData = {action: 'quickrename', file_id: this.fileId, newname: this.inputField.value};
			this.ajax.setOptions({'postData':postData});
			this.ajax.request();
		}
		else this.hideEditable(false);
	}
});


function mf_init_rename ( index )
{
	var file = mf_files[index];
	var container = $('mf_rename_'+index);
	container.innerHTML = file.name;
	var qrename = new quickEditFileName ( 'mf_rename_' + index, file.name, file.file_id, index, {maxWidth:250, style:{fontSize:'1.0em'} } );
	qrename.showEditable();
}

function mf_check_onclick ( index )
{
	var file = mf_files[index];
	var checkbox = $('mf_checkbox_'+index);
	file.selected = checkbox.checked;
	mf_highlight_row ( index, checkbox.checked );
	return true;
}

function mf_gallery_check_onclick ( index )
{
	var file = mf_files[index];
	var checkbox = $('mf_checkbox_'+index);
	file.selected = checkbox.checked;
	mf_highlight_cell ( index, checkbox.checked );
	return true;
}

function mf_highlight_row ( index, on )
{
	var row = $('mf_row_'+index);
	if ( row ) row.style.backgroundColor = on ? rowcolor3 : ( index & 1 ? rowcolor2 : rowcolor1 );
}

function mf_highlight_cell ( index, on )
{
	var cell = $('mf_cell_'+index);
	if ( cell ) cell.className = on ? 'selected' : 'unselected';
}

function mf_check ( what )
{
	for ( i = 0; i < mf_files.length; ++i )
	{
		var box = $('mf_checkbox_'+i);
		if ( !box ) continue;
		var file = mf_files[i];

		switch ( what )
		{
			case 'all' : checkIt ( box, 1 ); break;
			case 'none' : checkIt ( box, 0 ); break;
			case 'images' : checkIt ( box, file.is_image ); break;
			case 'invert' : checkIt ( box, !box.checked ); break;
		}
	}
	$('select_all').style.fontStyle = ( what == 'all' ? 'italic' : 'normal' );
	$('select_none').style.fontStyle = ( what == 'none' ? 'italic' : 'normal' );
	$('select_images').style.fontStyle = ( what == 'images' ? 'italic' : 'normal' );
	$('select_invert').style.fontStyle = ( what == 'invert' ? 'italic' : 'normal' );
}

function mf_form_action ( action )
{
	var check_count = countCheckedBoxes ( mf_form, 'file_ids[]' );

	if ( !check_count && ( action == 'move' || action == 'delete' || action == 'get_code' ) )
		uplAlert ( "You did not select any files. At least one file must be selected to perform this action.", "No files selected!" );
	else
	{
		if ( action == 'delete' && !confirm ( "Are you sure you want to delete the selected files? This cannot be undone!" ) ) return false;
		mf_form.elements.action.value = action;
		mf_form.submit();
		return true;
	}
}

function mf_dropdown ( select )
{
	var option = select.value;

	if ( option != '' )
	{
		if ( option.substr ( 0, 5 ) == 'move_' )
		{
			mf_form.elements.move_to_folder_id.value = option.substr ( 5 );
			mf_form_action ( 'move' );
		}
		else mf_form_action ( option );
	}

	select.selectedIndex = 0;
}


function mf_show_share ( share_url )
{
	uplAlert ( 'Give this URL to your friend:<br /><a href="' + share_url + '" title="URL to this folder" class="special">' + share_url + '</a>', 'Share Folder' );
}