<?php
global $UPL, $USER, $PUB;
if ( !isset ( $page_title ) )   $page_title = 'Uploader';
if ( !isset ( $content ) )      $content    = 'No content specified.';
$runtime = timer ( $UPL['RUNTIME'], 5 );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<base id="base_url" href="<?=$UPL['SETTINGS']['uploader_url']?>" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?=entities($page_title);?> - Uploader v6</title>
<link rel="stylesheet" type="text/css" href="templates/default2/style.css?v=<?=UPLOADER_VERSION?>" media="all">
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="templates/default2/style_ie.css?v=<?=UPLOADER_VERSION?>" media="all">
<![endif]-->
<script type="text/javascript" charset="utf-8">
var base_url = "<?=$UPL['SETTINGS']['uploader_url']?>";
</script>
<script type="text/javascript" charset="utf-8" src="templates/default2/prototype.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default2/common.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default2/javascript.js?v=<?=UPLOADER_VERSION?>"></script>
</head>
<body>
<noscript><p style="text-align:center;font-size:1.2em;color:red;font-weight:bold;">Please enable Javascript to properly access this service.</p></noscript>
<div id="container">
    <div id="container2">
        <div id="top"> Welcome <span style="color:#FF8000;"><?=$USER['username']?>!</span></div>
        <div id="menu">
            <div id="menu_links">
                <div style="float:left;">
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'announcements':'announcements.php')?>">Announcements</a>
                    <?php if ( $PUB['enabled'] ) : ?>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'public':'public.php')?>">Public Upload</a>
                    <?php endif; ?>
                    <?php if(is_browse_enabled()): ?>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'members':'browse.php')?>">Members</a>
                    <?php endif; ?>
                    <?php /* If user is logged in, show this menu */ if($USER['logged_in']) : ?>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php')?>">My Files</a>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'upload':'upload.php')?>">Upload</a>
                    <?php /* Otherwise, show this menu instead */ else: ?>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'login':'account.php?action=login')?>">Login</a>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')?>">Register</a>
                    <?php endif; ?>
                </div>

                <?php /* If user is logged in, show this menu */ if($USER['logged_in']): ?>
                <div style="float:right;">
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp':'usercp.php')?>">UserCP</a>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'contacts':'contacts.php')?>">Contacts</a>
                    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'account?action=logout':'account.php?action=logout')?>" onclick="return confirm('Confirm logout?');">Logout</a>
                </div>
                <?php endif;?>
            </div>
        </div>

        <div id="message">
            <div style="float:left;width:98%;" id="message_content"><!-- --></div>
            <div style="float:right;width:10px;text-align:right;"><img src="templates/default2/images/close.gif" alt="" onclick="showIt('message',false);" style="cursor:pointer;" /></div>
            <div class="spacer"><!-- --></div>
        </div>

        <div id="content"><?=$content?></div>
    </div>
</div>

<?php if ( $USER['level'] == LEVEL_ADMIN ) : ?>
<div id="credit">
    <a href="admin.php">Admin section</a>
</div>
<?php endif; ?>

<?php if ( $USER['logged_in'] && CURRENT_PAGE != 'usercp.php' && $USER['unread_messages'] > 0 ) : ?>
<script type="text/javascript">
<!--
help('You have unread messages, <a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp':'usercp.php').'?action=pm'?>" class="special">click here</a> to view your inbox.');
-->
</script>
<?php endif; ?>
</body>
</html>