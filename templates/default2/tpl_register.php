<!-- tpl_register.php -->
<script type="text/javascript" charset="utf-8">
<!--
function check_username ( username, result_container )
{
	var postData = {'action':'checkname', 'name':username};
	result_container = $(result_container);

	var ajax = new AjaxRequest ( '<?=UPLOADER_URL?>account.php?action=checkname', {method:'post', 'postData':postData, response:'json'} );

	ajax.oncomplete = function()
	{
		var response = this.response;
		if(response.result!='success')
			result_container.innerHTML = '<img src="'+(base_url+'templates/default2/images/exclamation2.gif')+'" alt="" class="img1" /> ' + response.message;
		else result_container.innerHTML = '<img src="'+(base_url+'templates/default2/images/checkbox.gif')+'" alt="" class="img1" />';
	}
	ajax.request();
}

function register_init()
{
	var username_input = $('userinfo[name]');
	if(!username_input) return;
	username_input.focus();
	var username_input_timer = new Timer();

	if ( Ajax.enabled )
	{
		username_input.onchange = function(){check_username(this.value, 'check_username_result');}
		username_input.onkeyup = function ()
		{
			username_input_timer.stop();
			username_input_timer.start(check_username.bind(this, this.value, 'check_username_result'), 500);
			return true;
		}
	}

	var password1 = $('userinfo[pass1]');
	var password2 = $('userinfo[pass2]');
	var password_timer = new Timer();

	password1.onchange = password1.onkeyup = function()
	{
		var result = $('check_password1_result');

		if(password2.value != '' && this.value != password2.value)
		{
			result.innerHTML = '<img src="'+(base_url+'templates/default2/images/exclamation2.gif')+'" alt="" class="img1" /> The passwords you enterered do not match.';
		}
		else result.innerHTML = '<img src="'+(base_url+'templates/default2/images/checkbox.gif')+'" alt="" class="img1" />';
	}

	password2.onchange = password2.onkeyup = function()
	{
		var result = $('check_password1_result');

		if(password1.value == '' )
		{
			result.innerHTML = '<img src="'+(base_url+'templates/default2/images/exclamation2.gif')+'" alt="" class="img1" /> Please enter a password.';
		}
		else if ( this.value != password1.value )
		{
			result.innerHTML = '<img src="'+(base_url+'templates/default2/images/exclamation2.gif')+'" alt="" class="img1" /> The passwords you enterered do not match.';
		}
		else result.innerHTML = '<img src="'+(base_url+'templates/default2/images/checkbox.gif')+'" alt="" class="img1" />';
	}
}
addLoadEvent(register_init);
-->
</script>


<h1>User Registration</h1>
<p>Please fill out the form below. All fields are required. Please enter a valid email address as you may be required to activate your account.</p>


<?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>

<form action="<?=MOD_REWRITE?'register':'account.php?action=register'?>" method="post" id="registration_form">
    <input type="hidden" name="action" value="register" />
    <input type="hidden" name="task" value="register" />

    <table cellspacing="1" cellpadding="2" style="width:100%;margin-top:10px;">
        <tr>
            <td style="width:110px;" class="tt">Username</td>
            <td style="line-height:1.7em;">
				<input type="text" name="userinfo[name]" id="userinfo[name]" value="<?=$userinfo['name']?>" size="30" maxlength="<?=MAX_USERNAME_LEN > 0 ? MAX_USERNAME_LEN : 100 ?>" />
				<span id="check_username_result" style="padding:0 5px 5px 5px;position:absolute;width:400px">Letters and numbers only</span>
			</td>
        </tr>
        <tr>
            <td>Password</td>
            <td>
				<input type="password" name="userinfo[pass1]" id="userinfo[pass1]" value="" size="30" maxlength="30" />
				<span id="check_password1_result" style="padding:0 5px 5px 5px;position:absolute;width:400px"></span>
			</td>
        </tr>
        <tr>
            <td>Confirm password</td>
            <td>
				<input type="password" name="userinfo[pass2]" id="userinfo[pass2]" value="" size="30" maxlength="30" />
			</td>
        </tr>
        <tr>
            <td>Email address</td>
            <td><input type="text" name="userinfo[email]" id="userinfo[email]" value="<?=$userinfo['email']?>" size="50" maxlength="100" /></td>
        </tr>
        <tr>
            <td class="tt">Preferences</td>
            <td>
                <ul class="ls_menu">
                    <li><input type="checkbox" class="chkbox" name="userinfo[pemail]" id="userinfo[pemail]" value="1" <?=$userinfo['pemail']?'checked="checked"':''?>/> <label for="userinfo[pemail]">Allow other users to see my email address</label></li>
                    <li><input type="checkbox" class="chkbox" name="userinfo[pmessage]" id="userinfo[pmessage]" value="1" <?=$userinfo['pmessage']?'checked="checked"':''?>/> <label for="userinfo[pmessage]">Accept private messages from other users</label></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Register" /> <input type="button" value="Cancel" onclick="go('index.php');" /></td>
        </tr>
    </table>
</form>