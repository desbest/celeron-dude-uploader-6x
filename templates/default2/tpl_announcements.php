<!-- tpl_announcements.php -->

<?php if ( $action == 'view' ) : global $UPL, $USER; ?>
<div class="rounded announcement">
    <p>
        <strong class="subject"><?=$ann['announcement_subject']?></strong>
        <br /><span class="date"><?=get_date('M d, Y h:i A', $ann['announcement_date'])?></span>
    </p>
    <p class="content"><?=parse_message($ann['announcement_content'], 80, $ann['announcement_parsebb'], true)?></p>
</div>

<?php if ( count ( $comments ) ) : ?>
<div style="margin-top:20px">
    <h1>User comments</h1>
    <ul style="margin:0;padding:0">
        <?php reset ( $comments ); while ( list ( , $comment ) = each ( $comments ) ) : ?>
        <li style="margin:5px 0 20px 0;line-height:1.5em">
            <a href="<?=$comment['info_url']?>" class="special"><strong><?=entities($comment['username'])?></strong></a>
            <span style="color:#999">(<?=get_date('h:iA M d,Y', $comment['comment_date'])?>)</span>
            <?php if ( $comment['is_mine'] ) : ?><span style="color:#999">(<a href="<?=$comment['delete_url']?>" class="special">Delete</a>)</span><?php endif;?>
            <br />
            <?=parse_message($comment['comment_message'], 50)?>
        </li>
        <?php endwhile; ?>
    </ul>
</div>
<?php endif; ?>

<?php if ( $ann['announcement_allowcomment'] ) : ?>
<?php if($USER['logged_in']):?>
<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'comment':'comment.php')?>" style="margin-top:20px;">
    <input type="hidden" name="action" value="comment_announcement" />
    <input type="hidden" name="announcement_id" value="<?=$ann['announcement_id']?>" />
    <h1>Add your comment:</h1>
    <p>
        <textarea name="comment" cols="60" rows="5" style="margin: 0 0 5px 0;"></textarea><br />
        <input type="submit" value="Post comment" />
    </p>
</form>
<?php else: // logged in?>
<div>
    <h1>Login to comment</h1>
    <p>
        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'login':'account.php?action=login')?>" class="special">Click here</a> to login
        or <a href="<?=UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')?>" class="special">register</a> for an account.
    </p>
</div>
<?php endif;// logged in ?>
<?php endif; // comments allowed ?>


<?php else : // action ?>
<?php /* IF there are announcements, show them */ if ( count ( $announcements ) ) : ?>
<?php /* Loop through announcements */ reset ( $announcements ); while ( list ( $i, $ann ) = each ( $announcements ) ) : ?>
<div class="rounded announcement <?=$i&1?'odd':'even'?>">
    <p>
        <strong class="subject"><?=$ann['announcement_subject']?></strong>
        <br /><span class="date"><?=get_date('M d, Y h:i A', $ann['announcement_date'])?></span>
    </p>
    <p class="content"><?=parse_message($ann['announcement_content'], 80, $ann['announcement_parsebb'], true)?></p>
    <?php if ( $ann['announcement_allowcomment'] || $ann['comments_count'] ) : ?>
    <p class="comments"><a href="<?=$ann['view_url']?>" class="special"><?=$ann['comments_count']?$ann['comments_count']:'No'?> comments</a></p>
    <?php endif; ?>
</div>
<?php /* End loop */ endwhile; ?>
<?php /* No announcements, show this message instead */ else: ?>
<h1>No announcements</h1>
There are no announcements.
<?php endif; // count announcements ?>
<?php endif; // Action ?>
