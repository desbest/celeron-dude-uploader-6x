<!-- tpl_announcements_admin.php -->
<div class="rounded green">
    <h1>Uploader Announcements</h1>
    <p>
        <strong class="s2">Actions:</strong> &nbsp;
        <?php if ( $action == 'announcement' ) : ?>View all announcements<?php else:?><a href="admin.php?action=announcement" class="special">View all announcements</a><?php endif; ?> -
        <?php if ( $action == 'add_announcement' ) : ?>Add an announcement<?php else:?><a href="admin.php?action=add_announcement" class="special">Add an announcement</a><?php endif;?>
    </p>
</div>


<?php if ( $action == 'add_announcement' ) : ?>
<div style="padding:10px">
    <h1>Add announcement</h1>
    <?php /* Any errors will appear here */ if ( isset ( $errors ) ) print $errors; ?>
    <form method="post" action="admin.php?action=add_announcement">
        <input type="hidden" name="task" value="save" />
        <table cellspacing="1" cellpadding="3" style="margin-top:10px">
            <tr>
                <td>Announcement title/subject</td>
                <td><input type="text" size="70" maxlength="255" name="announcement[subject]" id="announcement_subject" value="<?=entities($announcement['subject'])?>" /></td>
            </tr>
            <tr>
                <td>Options</td>
                <td>
                    <input type="checkbox" id="announcement_parsebb" value="1" class="chkbox" name="announcement[parsebb]" <?=$announcement['parsebb']?'checked="checked"':''?> />
                    <label for="announcement_parsebb">Parse BB codes and URL in this announcement</label><br />
                    <input type="checkbox" id="announcement_allowcomment" value="1" class="chkbox" name="announcement[allowcomment]" <?=$announcement['allowcomment']?'checked="checked"':''?> />
                    <label for="announcement_allowcomment">Allow users to comment on this announcement</label>
                </td>
            </tr>
            <tr>
                <td class="tt">Content (HTML enabled)</td>
                <td>
                    <textarea cols="100" rows="8" name="announcement[content]" id="announcement_content"><?=entities($announcement['content'])?></textarea><br />
                    <span class="link" onmousedown="input.content.rows++">Increase textarea</span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="Add announcement" />
                    <input type="button" value="Cancel" onclick="go('admin.php?action=announcement')" />
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
<!--
var input = {};
input.subject = $('announcement_subject');
input.parsebb = $('announcement_parsebb');
input.content = $('announcement_content');

addLoadEvent ( function ( )
{
    input.subject.focus();
});
-->
</script>
<?php elseif ( $action == 'edit_announcement' ) : ?>
<div style="padding:10px">
    <h1>Edit announcement</h1>
    <?php /* Any errors will appear here */ if ( isset ( $errors ) ) print $errors; ?>
    <form method="post" action="admin.php?action=edit_announcement">
        <input type="hidden" name="task" value="save" />
        <input type="hidden" name="aid" value="<?=$announcement['aid']?>" />
        <table cellspacing="1" cellpadding="3" style="margin-top:10px">
            <tr>
                <td>Announcement title/subject</td>
                <td><input type="text" size="70" maxlength="255" name="announcement[subject]" id="announcement_subject" value="<?=entities($announcement['subject'])?>" /></td>
            </tr>
            <tr>
                <td>Options</td>
                <td>
                    <input type="checkbox" id="announcement_parsebb" value="1" class="chkbox" name="announcement[parsebb]" <?=$announcement['parsebb']?'checked="checked"':''?> />
                    <label for="announcement_parsebb">Parse BB codes and URL in this announcement</label><br />
                    <input type="checkbox" id="announcement_allowcomment" value="1" class="chkbox" name="announcement[allowcomment]" <?=$announcement['allowcomment']?'checked="checked"':''?> />
                    <label for="announcement_allowcomment">Allow users to comment on this announcement</label>
                </td>
            </tr>
            <tr>
                <td class="tt">Content (HTML enabled)</td>
                <td>
                    <textarea cols="100" rows="8" name="announcement[content]" id="announcement_content"><?=entities($announcement['content'])?></textarea><br />
                    <span class="link" onmousedown="input.content.rows++">Increase textarea</span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="Save announcement" />
                    <input type="button" value="Cancel" onclick="go('admin.php?action=announcement')" />
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
<!--
var input = {};
input.subject = $('announcement_subject');
input.parsebb = $('announcement_parsebb');
input.content = $('announcement_content');

addLoadEvent ( function ( )
{
    input.subject.focus();
});
-->
</script>
<?php else: ?>
<table id="announcements_tbl" cellspacing="0" cellpadding="4" class="rowlines" style="width:100%;margin-top:10px;">
    <tr skip_alternate="skip_alternate" class="header">
        <td>Subject</td>
        <td class="ct">Date added</td>
        <td class="ct">Comments</td>
        <td class="ct">Actions</td>
    </tr>
    <?php reset ( $announcements ); while ( list ( , $ann ) = each ( $announcements ) ) : ?>
    <tr>
        <td><?=entities(str_preview($ann['announcement_subject'],70))?></td>
        <td class="ct"><?=get_date('M d, Y h:iA', $ann['announcement_date'])?></td>
        <td class="ct"><?=$ann['comments_count']?></td>
        <td class="ct">
            <a href="<?=$ann['edit_url']?>" class="special">Edit</a> -
            <a href="<?=$ann['delete_url']?>" class="special" onclick="return confirm('Are you sure you want to delete this announcement?');">Delete</a>
        </td>
    </tr>
    <?php endwhile;?>
</table>
<script type="text/javascript">
<!--
alternateRowColor($('announcements_tbl'), 'tr', '#fff', '#fafafa');
-->
</script>
<?php endif; ?>