<!-- tpl_logs.php -->
<h1>Daily logs</h1>

<p style="background-color:#FAFAFA;padding:5px;margin-bottom:4px;color:#404040;">
    <strong class="s2">Actions:</strong> &nbsp;
    <a href="<?=$delete_all_url?>" class="special" onclick="return confirm('This will also delete archived log files. Are you sure?');">Delete all log files</a> -
	<a href="<?=$archive_url?>" class="special">Archive</a> (Combine all log files here into one single file)
</p>

<?php /* If log files found */ if ( count ( $log_files ) ) : ?>
<table id="log_tbl" cellspacing="1" cellpadding="6" style="width: 100%;">
    <tr class="color3" skip_alternate="skip_alternate" style="line-height:1.4em;">
        <td><strong class="s1">Log files</strong></td>
        <td class="ct" style="width:80px;"><strong class="s1">Size</strong></td>
        <td class="ct" style="width:130px;"><strong class="s1">Action</strong></td>
    </tr>
    <?php /* list log files */ reset ( $log_files ); while ( list ( $i, $file ) = each ( $log_files ) ) : ?>
    <tr class="<?=$i&1?'color1':'color2'?>">
        <td><a href="<?=$file['view_url']?>" onclick="window.open(this.href);return false;"><?=$file['name']?></a></td>
        <td class="ct"><?=$file['size']?></td>
        <td class="ct">
			<a href="<?=$file['download_url']?>" class="special">Download</a> -
			<a href="<?=$file['delete_url']?>" class="special" onclick="return confirm('Are you sure?');">Delete</a>
		</td>
    </tr>
    <?php endwhile; ?>
    <tr>
        <td colspan="3"><span class="smalltext">Total log files size: <?=$log_total_size?></span></td>
    </tr>
</table>
<?php /* no logs found */ else: ?>
<p style="text-align:center;">No log files found.</p>
<?php endif; ?>


<script type="text/javascript">
<!--
    alternateRowColor(getObj('log_tbl'),'tr','#F5F5F5','#FBFBFB');
-->
</script>