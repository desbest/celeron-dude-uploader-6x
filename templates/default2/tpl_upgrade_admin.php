<h1>Upgrade Plans</h1>
<a href="admin.php?action=newplan" class="special">Create upgrade plan</a>


<?php if ( !count ( $plans ) ) : ?>
<p>You do not have any upgrade plans. Click on the link above to create one.</p>
<?php else: ?>
<table id="plan_tbl" cellspacing="1" cellpadding="4" border="0" style="margin-top:10px;width:100%;color:#808080;text-align:center;">
	<tr skip_alternate="skip_alternate" style="color:#202020;background-color:#f0f0f0;line-height:1.8em;">
		<td>Plan</td>
		<td>Price</td>
		<td>Storage</td>
		<td>Max file size</td>
		<td>Folders</td>
		<td>Allowed files</td>
		<td>Rename</td>
		<td>Watermarked</td>
		<td>Bandwidth</td>
		<td>Transfer rate</td>
	</tr>
	<?php while ( list ( $i, $plan ) = each ( $plans ) ) : ?>
	<tr>
		<td><?=$plan['plan_name']?></td>
		<td>$<?=$plan['plan_price']?></td>
		<td><?=$plan['fl_max_storage']?get_size($plan['fl_max_storage'],'MB',1):'Unlimited'?></td>
		<td><?=$plan['fl_max_filesize']?get_size($plan['fl_max_filesize'],'KB',1):'Unlimited'?></td>
		<td><?=$plan['fl_create_folder']?($plan['fl_max_folders']?$plan['fl_max_folders']:'Unlimited'):'None'?></td>
		<td><?=$plan['fl_images_only']?'Images only':($plan['fl_allowed_filetypes']==''?'All types allowed':str_replace(',',', ',$plan['fl_allowed_filetypes']))?></td>
		<td><?=$plan['fl_allow_rename']?'Yes':'No'?></td>
		<td><?=$plan['fl_watermark']?'Yes':'No'?></td>
		<td><?=$plan['bw_max']?get_size($plan['bw_max'],'MB',1):'Unlimited'?></td>
		<td><?=$plan['bw_xfer_rate']?get_size($plan['bw_xfer_rate'],'KB',1).'/s':'Unlimited'?></td>
	</tr>
	<?php endwhile; ?>
</table>

<script type="text/javascript">
<!--
alternateRowColor(getObj('plan_tbl'),'tr','#fefefe','#fafafa');
-->
</script>

<?php endif; ?>