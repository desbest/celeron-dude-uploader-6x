<?php
/*
  This is the activation email template. The following variables are available:
  $activation_url
*/
?>
Thank you for registering! Your account must be activated before it can be used.

Please click on the link below to activate your account.

<?=$activation_url?>


If you did not register for an account, please discard this email message. We apologize for any inconveniences.