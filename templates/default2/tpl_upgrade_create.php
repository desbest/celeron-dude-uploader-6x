<h1>New upgrade plan</h1>
<a href="admin.php?action=plans" class="special">Back to plan list</a>

<form action="admin.php" method="post">
    <input type="hidden" name="action" value="newplan" />
    <input type="hidden" name="task" value="save" />

    <fieldset>
        <legend>Plan limit</legend>
        <table id="tbl1" style="width:100%;" cellpadding="5" cellspacing="1" border="0">
            <tr>
                <td style="width:120px;">Plan name</td>
                <td>
                    <input type="text" name="plan[plan_name]" size="20" maxlength="20" /> Letters and numbers only.
                </td>
                <td class="cr"><span class="help" onclick="help('Users will not see this. Use numbers and letters only, no special characters and must be unique. Good names are: plan1, plan2, plan3, etc.... If you enter a plan that already exists, it will be overwritten with this one.');">Explain</span></td>
            </tr>
            <tr>
                <td style="width:120px;">Description</td>
                <td>
                    <textarea name="plan[plan_description]" cols="70" rows="3"></textarea>
                </td>
                <td class="cr"><span class="help" onclick="help('Description that the users will see. Can be anything you want.');">Explain</span></td>
            </tr>
            <tr>
                <td>Plan price</td>
                <td>
                    <input type="text" name="plan[plan_price]" value="<?=@$plan['plan_price']?>" size="5" id="mst" style="text-align:center;"/> USD
                </td>
                <td class="cr"><span class="help" onclick="help('Cost of the plan in USD.');">Explain</span></td>
            </tr>
            <tr>
                <td>Plan period</td>
                <td>
                    <input type="text" name="plan[plan_period]" value="<?=@$plan['plan_period']?>" size="5" style="text-align:center;"/> days. Enter 0 for "Forever"
                </td>
                <td class="cr"><span class="help" onclick="help('The period that this plan applies. After this period has passed, all settings will revert back to the standard values set in the New User Settings.');">Explain</span></td>
            </tr>			
            <tr>
                <td>Max storage</td>
                <td>
                    <input type="text" name="plan[fl_max_storage]" value="<?=@$plan['fl_max_storage']?>" size="5" id="mst" style="text-align:center;" onkeyup=";" /> MB (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help" onclick="help('The maximum diskspace (in Megabyte) that the user can occupy.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max upload file size</td>
                <td>
                    <input type="text" name="plan[fl_max_filesize]" value="<?=@$plan['fl_max_filesize']?>" size="5" id="mfs" style="text-align:center;" onkeyup=";" /> KB (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help" onclick="help('The maximum file size (in Kilobyte) that the user can upload. Remember that PHP has a default size limit on uploaded files. That limit is usually 2MB. Even if you specify a value greater than the PHP default value, the user will still not be able to upload files greater than the value set by PHP. You will need to edit two PHP settings in php.ini, they are \'upload_max_filesize\' and \'post_max_size\'. You will either need to contact your webhost or edit php.ini if you are hosting yourself. Two php.ini settings have been inserted into the .htaccess file in the uploader directory (same location as index.php, admin.php, etc...) that will attempt to modify the settings mentioned above. If this method works, then you will not need to contact your web host.');">Explain</span></td>
            </tr>
            <tr>
                <td>Max folders</td>
                <td>
                    <input type="text" name="plan[fl_max_folders]" value="<?=@$plan['fl_max_folders']?>" size="5" style="text-align:center;" onkeyup=";" /> folders (enter 0 for unlimited)
                </td>
                <td class="cr"><span class="help"  onclick="help('Maximum number of folders the user can create. Enter 0 for unlimited.');">Explain</span></td>
            </tr>
            <tr>
                <td>Allowed filetypes</td>
                <td>
                    <input type="text" name="plan[fl_allowed_filetypes]" value="<?=@$plan['fl_allowed_filetypes']?>" size="75" />
                </td>
                <td class="cr"><span class="help" onclick="help('Specify the file types that the users are allowed to upload. Entery ANY or leave it blank for no limit (except for those specified in Uploader Settings).\nSeperate the extensions with commas, do NOT use spaces. Extension matching is case insensitive, you do not have to enter upper and lower case extensions.\nExample: gif,png,jpeg,zip,rar');">Explain</span></td>
            </tr>
            <tr>
                <td>Images only</td>
                <td>
                    <input type="radio" name="plan[fl_images_only]" value="1" <?=@$plan['fl_images_only']==1?'checked="checked"':''?> class="radio" id="fl_images_only_1" class="chkbox" /><label for="fl_images_only_1">Images only</label><br />
                    <input type="radio" name="plan[fl_images_only]" value="0" <?=@$plan['fl_images_only']==0?'checked="checked"':''?> class="radio" id="fl_images_only_2" /><label for="fl_images_only_2">Any file types</label>
                </td>
                <td class="cr"><span class="help" onclick="help('This setting will force the user to upload images only. Detecting whether the uploaded file is an image is done by reading the file, not just looking at the file extension (which can be renamed) so the Allowed Filetypes settings will be overridden by this one. For example, if you allow the user to upload .txt file but it is not an image file, it will not be uploaded.');">Explain</span></td>
            </tr>
            <tr>
                <td>File rename</td>
                <td>
                    <input type="radio" name="plan[fl_allow_rename]" value="2" <?=@$plan['fl_allow_rename']==2?'checked="checked"':''?> class="radio"  id="fl_allow_rename_2" /><label for="fl_allow_rename_2">File name and extension</label><br />
                    <input type="radio" name="plan[fl_allow_rename]" value="1" <?=@$plan['fl_allow_rename']==1?'checked="checked"':''?> class="radio" id="fl_allow_rename_1" /><label for="fl_allow_rename_1">File name only</label><br />
                    <input type="radio" name="plan[fl_allow_rename]" value="0" <?=@$plan['fl_allow_rename']==0?'checked="checked"':''?> class="radio" id="fl_allow_rename_0" /><label for="fl_allow_rename_0">User cannot rename files</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Allow user to rename files or not. If user is allowed to rename files, specify if the user can rename the file extension. Basically it would defeat the purpose of having file type limit when the user can rename a file that he/she is not allowed to one that is allowed (ex:EXE to JPG). Of course even if the user is allowed to rename the extension to anything, he/she cannot rename it to the extensions set in the Uploader Illegal Filetypes setting.');">Explain</span></td>
            </tr>
            <tr>
                <td>Folder creation</td>
                <td>
                    <input type="radio" name="plan[fl_create_folder]" value="1" <?=@$plan['fl_create_folder']==1?'checked="checked"':''?> class="radio" id="fl_create_folder_1" /><label for="fl_create_folder_1">User can create folders</label><br />
                    <input type="radio" name="plan[fl_create_folder]" value="0" <?=@$plan['fl_create_folder']==0?'checked="checked"':''?> class="radio" id="fl_create_folder_2" /><label for="fl_create_folder_2">User CANNOT create folders</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Allow users to create folders. All user created folders are created inside THAT user\'s folder. For example, if a user named \'cdude\' created a folder named \'images\', the folder created would be \'userfiles/cdude/images\'');">Explain</span></td>
            </tr>
            <tr>
                <td>Image watermark</td>
                <td>
                    <input type="radio" name="plan[fl_watermark]" value="1" <?=@$plan['fl_watermark']==1?'checked="checked"':''?> class="radio" id="fl_watermark1" /><label for="fl_watermark1">Watermark images uploaded by this user</label><br />
                    <input type="radio" name="plan[fl_watermark]" value="0" <?=@$plan['fl_watermark']==0?'checked="checked"':''?> class="radio" id="fl_watermark2" /><label for="fl_watermark2">Do not watermark</label>
                </td>
                <td class="cr"><span class="help" onclick="help('Specify whether images uploaded by this user should be watermarked or not. This setting CAN be overridden by the global uploader setting.');">Explain</span></td>
            </tr>
            <tr>
                <td style="width:120px;">Max bandwidth</td>
                <td>
                    <input type="text" name="plan[bw_max]" value="<?=@$plan['bw_max']?>" size="5" id="mbw" style="text-align:center;" /> MB
                    (<span class="link" onclick="getObj('mbw').value=1*1024">1GB</span>,
                    <span class="link" onclick="getObj('mbw').value=3*1024">3GB</span>,
                    <span class="link" onclick="getObj('mbw').value=5*1024">5GB</span>,
                    <span class="link" onclick="getObj('mbw').value=10*1024">10GB</span>,
                    <span class="link" onclick="getObj('mbw').value=15*1024">15GB</span>,
                    <span class="link" onclick="getObj('mbw').value=20*1024">20GB</span>,
                    <span class="link" onclick="getObj('mbw').value=30*1024">30GB</span>)
                </td>
                <td class="cr"><span class="help" onclick="help('Specify the maximum bandwidth (in Megabyte) allowed for the user per month. This setting only applies if bandwidth limit is enabled (see Uploader Settings). Enter 0 for unlimited.');">Explain</span></td>
            </tr>
            <tr>
                <td>Bandwidth reset period</td>
                <td>
                    <input type="text" name="plan[bw_reset_period]" value="<?=@$plan['bw_reset_period']?>" size="5" id="bwrp" style="text-align:center;" /> Days
                </td>
                <td class="cr"><span class="help" onclick="help('When a user reaches his/her bandwidth limit and Bandwidth Reset Rule is set to Automatic, the uploader checks to see when was the last time the counter was reset and will only reset it after the specified time has passed.');">Explain</span></td>
            </tr>
            <tr>
                <td>Bandwidth counter reset</td>
                <td>
                    <input type="radio" name="plan[bw_auto_reset]" value="1" <?=@$plan['bw_auto_reset']==1?'checked="checked"':''?> class="radio" id="bw_auto_reset_1" /><label for="bw_auto_reset_1">Automatically</label><br />
                    <input type="radio" name="plan[bw_auto_reset]" value="0" <?=@$plan['bw_auto_reset']==0?'checked="checked"':''?> class="radio" id="bw_auto_reset_2" /><label for="bw_auto_reset_2">Manually (by an Admin)</label>
                </td>
                <td class="cr"><span class="help" onclick="help('When a user reaches his/her bandwidth limit, specify whether the counter should be reset automatically or manually. If manually, the user will not be able to view his/her files outside of the uploader (MyFiles) until an admin resets the counter.');">Explain</span></td>
            </tr>
            <tr>
                <td>Transfer rate</td>
                <td>
                    <input type="text" name="plan[bw_xfer_rate]" value="<?=@$plan['bw_xfer_rate']?>" style="text-align:center;" size="5" /> KB/s
                </td>
                <td class="cr"><span class="help" onclick="help('This will set the transfer rate for files in this user\'s account. The actual speed is not exact but is very close. Enter zero for unlimited. This feature will NOT work on Windows server.');">Explain</span></td>
            </tr>
			<tr>
				<td colspan="3" class="ct"><input type="submit" value="Create" class="blue_button" /></td>
			</tr>
        </table>
    </fieldset>
</form>

<!-- alternating row color -->
<script type="text/javascript">
<!--
alternateRowColor(getObj('tbl1'),'tr','#F5F5F5','#FBFBFB');
-->
</script>