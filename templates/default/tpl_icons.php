<?php
// This function returns the icon file corresponding to the file type
function tpl_myfiles_get_icon ( $extension )
{
    $icons = array
    (
	'default'   => 'file.gif',
	// images
	'jpg'	=> 'images.gif',
	'jpeg'	=> 'images.gif',
	'png'	=> 'png.gif',
	'gif'	=> 'gif.gif',
	// media
	'avi'	=> 'video.gif',
	'wmv'	=> 'video.gif',
	'mpeg'	=> 'video2.gif',
	'mpg'	=> 'video2.gif',
	'mov'	=> 'quicktime.gif',
	'mp3'	=> 'mp3.gif',
	// Archives
	'zip'	=> 'zip.gif',
	'rar'	=> 'zip.gif',
	'tar'	=> 'zip.gif',
	// documents
	'pdf'	=> 'acrobat.gif',
	'ppt'	=> 'powerpoint.gif',
	'txt'	=> 'text.gif',
	'doc'	=> 'word.gif',
	'xls'	=> 'excel.gif',

    );
    return ( isset ( $icons[$extension] ) ? $icons[$extension] : $icons['default'] );
}

// returns the name for the icon corresponding to the folder's permission
function tpl_get_folder_icon ( $permission )
{
	switch ( $permission['access'] )
	{
		case 'public': $icon = 'icon_folder_public.gif'; break;
		case 'hidden': $icon = 'icon_folder_hidden.gif'; break;
		case 'private': $icon = 'icon_folder_private.gif'; break;
	}
	return (UPLOADER_URL . 'templates/default/images/' . $icon);
}
?>