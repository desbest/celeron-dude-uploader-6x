<!-- tpl_message.php -->

<?php if ( !isset ( $back_url ) ) { $back_url = UPLOADER_URL; } ?>
<?php if ( !isset ( $message_title ) ) { $message_title = 'Uploader Message'; } ?>

<?php /* Error display mode */ if ( isset ( $mode ) && $mode == 'error' ) : ?>
    <h1>Uploader Error</h1>
    <?=$message?>
<?php /* Normal mode */ else: ?>
    <h1><?=$message_title?></h1>
	<?php if ( is_array ( $message ) ) : ?>
	<ul class="ls_menu2">
		<?php while ( list ( , $msg ) = each ( $message ) ) : ?>
		<li><?=$msg?></li>
		<?php endwhile; ?>
	</ul>
	<?php else: ?>
    <p><?=$message?></p>
	<?php endif; ?>
<?php endif; ?>

<p><br /><input type="button" value="&nbsp; &nbsp; OK &nbsp; &nbsp;" onclick="go('<?=$back_url?>');" /></p>