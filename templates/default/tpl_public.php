<!-- tpl_public.php: This is the main page for public uploading -->

<?php if ( count ( $latest ) ) : ?>
<div class="rounded green" style="color:#708059;">
    <h1>Latest uploaded image sets</h1>
    <p><a href="<?=UPLOADER_URL . ( MOD_REWRITE ? 'public/browse' : 'public.php?action=browse' )?>" class="special no_underline2"><strong>Browse</strong> all uploaded files</a></p>
</div>
<div class="gallery">
    <?php reset ( $latest ); while ( list ( $row_num, $tmp ) = each ( $latest ) ) : $set = $tmp['set']; $file = $tmp['file']; ?>
	<?=$row_num%5==0?'<div class="spacer"></div>':''?>
    <div class="cell" style="width:110px;margin:0 0 10px 2px;">
        <div class="inner_cell">
            <div class="top"><?=$set['image_count']?> image<?=$set['image_count']>1?'s':''?></div>
            <div class="center"><a href="<?=$set['view_url']?>"><img src="<?=$file['square_thumb_url']?>" alt="<?=entities($file['file_name'], ENT_QUOTES)?>" class="img1 thumb" /></a></div>
            <div class="bottom"><span class="name">By <strong><?=$set['upload_name']==''?'Anonymous':wordwrap(entities(str_preview($set['upload_name'], 25)),20,'<br />',true)?></strong><br /><?=get_date('M d, Y', $set['upload_date'])?></span></div>
        </div>
    </div>
    <?php endwhile; ?>
</div>
<div class="spacer"></div>
<?php endif; ?>


<?php if ( !$user_is_banned ) : ?>
<link rel="stylesheet" type="text/css" href="<?=UPLOADER_URL?>templates/default/upload.css?v=<?=UPLOADER_VERSION?>" media="all">
<script type="text/javascript" charset="utf-8" src="<?=UPLOADER_URL?>templates/default/upload.js?v=<?=UPLOADER_VERSION?>"></script>

<form method="post" action="<?=MOD_REWRITE?'public':'public.php'?>" enctype="multipart/form-data" id="upload_form" onsubmit="return doSubmit()">
    <input type="hidden" name="action" value="upload" />
    <div class="rounded green" style="margin-top:10px">
        <h1>Upload your files</h1>
        <table cellspacing="3" border="0" style="margin-top:10px">
            <tr>
                <td>Public</td>
                <td>
                    <input type="checkbox" class="chkbox" name="upload[public]" id="upload_public" value="1" checked="checked" />
                    <label for="upload_public">Allow these files to be browsed by others</label>
                </td>
            </tr>
            <tr>
                <td style="width:80px;">Your name</td>
                <td><input type="text" name="upload[name]" value="" autocomplete="off" maxlength="64" size="30" /> (optional)</td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="upload[description]" autocomplete="off" value="" maxlength="255" size="70" /> (optional)</td>
            </tr>
        </table>
        <div style="padding:4px;width:81px;float:left">
            Select files
        </div>
        <div style="float:left;">
            <div id="uploadFields"></div>
            <noscript>
                <ul class="ls_menu">
                    <li><input type="file" name="file_1" size="50" /></li>
                    <li><input type="file" name="file_2" size="50" /></li>
                    <li><input type="file" name="file_3" size="50" /></li>
                </ul>
            </noscript>
            <div style="font-size:0.9em;margin-top:5px;">
                <?php if($restrictions['images_only']) : ?>
                Images only or a zip file named <strong>upload.zip</strong> containing your images.
                <?php elseif($restrictions['allowed_types']!=''&&$restrictions['allowed_types']!='any'):?>
                Allowed file types (or a zip file named <strong>upload.zip</strong> containing files of these types): <?=str_replace(',', ', ',$restrictions['allowed_types'])?>.
                <?php endif; ?>
                Maximum file size is <strong><?=get_size($restrictions['max_file_size'],'B',0)?></strong>
            </div>
        </div>
        <div class="spacer"></div>
     </div>
    <div id="selectedFiles"></div>
    <p id="selectedStatus" class="rounded gray" style="margin-top:5px;padding:6px 6px 4px 10px;color:#666;">Click on Browse to select a file. Repeat to add more files.</p>
    <p style="margin: 10px 0 0 1px"><input type="submit" id="submit_button" value="Upload selected files" /></p>
</form>

<script type="text/javascript" charset="utf-8">
<!--
<?php if ( $restrictions['allowed_types'] != '' && $restrictions['allowed_types'] != 'any' ) : ?>
var allowed_types = <?=json_encode(explode(',',$restrictions['allowed_types']))?>;
<?php else : ?>
var allowed_types = [];
<?php endif; ?>

var upl = new uploadForm ( 'uploadFields', 'selectedFiles', 'selectedStatus' );
upl.setOptions({fieldSize:35, iconURL:'<?=UPLOADER_URL?>templates/default/images/icons/', allowedTypes:allowed_types, publicUploader:true});
upl.display();

function doSubmit()
{
    if(!upl.getSelectedCount()){alert('You did not select any files.');return false;}
	var submit_button = $('submit_button');
	submit_button.value = 'Uploading files, please wait...';
	submit_button.disabled = true;
    return true;
}
function enableSubmitButton ( )
{
	var submit_button = $('submit_button');
	submit_button.value = 'Upload selected files';
	submit_button.disabled = false;
}
addLoadEvent(enableSubmitButton);
-->
</script>
<?php /* Endif for user is banned */ endif; ?>