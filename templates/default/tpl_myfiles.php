<?php
require_once('tpl_icons.php');
?>

<script type="text/javascript" charset="utf-8" src="<?=UPLOADER_URL?>templates/default/myfiles.js"></script>
<!-- LEFT SIDE -->
<div style="float: left; width: 20%; overflow: hidden;display:inline;" id="mf_leftside">
    <div style="color: #a0a0a0;">
        <img src="templates/default/images/folder2.gif" alt="" class="img1" />
        <a href="<?=UPLOADER_URL.(MOD_REWRITE?'folders':'folders.php')?>" class="special" style="font-weight:bold;">Folders</a>&nbsp;(<a href="<?=UPLOADER_URL.(MOD_REWRITE?'folders?action=create':'folders.php?action=create')?>" class="special">New</a>)
        <ul style="line-height: 1.6em; margin-top: 3px; white-space: nowrap;">
            <?php reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : ?>
            <li><a href="<?=$folder['url']?>" class="<?=$folder['folder_isgallery']?'special_green':'special'?>"><?=entities($folder['folder_name'])?> (<?=$folder['files_count']?>)</a></li>
            <?php endwhile; ?>
        </ul>
    </div>
    <div style="margin-top:15px;">
        <img src="templates/default/images/icon_stats.gif" alt="statistics icon" class="img1" />&nbsp; Usage Statistics
        <div style="font-size:0.8em; margin-top: 2px;color:#666666;margin:6px 0px 0px 0px;">
            Space: <?=get_size($stats['storage_used'])?> / <?=$stats['max_storage']?get_size($stats['max_storage']):'Unlimited'?>
            <div style="margin-bottom:3px;width: 100px; overflow: hidden; border: 1px solid #bbbbbb;padding:1px;">
                <div style="width: <?=$stats['storage_percentage']>100?100:($stats['storage_percentage']+1)?>%; background-color: <?=$stats['storage_percentage']>=100?'#BF3030':'#E0E0E0'?>; height: 7px; float: left;"><!-- --></div>
            </div>
            BW: <?=get_size($stats['bandwidth_used'],'KB')?> / <?=$stats['max_bandwidth']?get_size($stats['max_bandwidth'],'KB'):'Unlimited'?>
            <div style="width: 100px; overflow: hidden; border: 1px solid #bbbbbb;padding:1px;">
                <div style="width: <?=$stats['bandwidth_percentage']>100?100:($stats['bandwidth_percentage']+1)?>%; background-color: <?=$stats['bandwidth_percentage']>=100?'#BF3030':'#E0E0E0'?>; height: 7px; float: left;"><!-- --></div>
            </div>
        </div>
    </div>
    <?php if ( $current_folder['folder_isgallery'] && count ( $user_files ) ) : ?>
    <div style="margin-top:15px;">
        <a href="<?=$current_folder['slideshow_url']?>" class="special">View as slideshow</a>
    </div>
    <?php endif; ?>
    <div style="margin-top:15px; color:#555">
        <a href="<?=$current_folder['browse_url']?>" class="special">Share this <?=$current_folder['folder_isgallery']?'gallery':'folder'?></a>
		<br />
		<small>Send <span class="help" onclick="prompt('Copy this URL and send it to your friend','<?=$current_folder['browse_url']?>');">this URL</span> to your friends.</small>
		<small id="share_private_reminder" style="display:<?=$current_folder['permission']['access']=='private'?'inline':'none'?>">
			Remember to add them to your contact list because this folder is private.
		</small>
    </div>
</div>


<!-- RIGHT SIDE -->
<div style="float:right; width:76%;display:inline;" id="mf_rightside">
    <p style="margin:0; padding:0px; line-height:1.6em;">
        <img src="<?=tpl_get_folder_icon($current_folder['permission'])?>" class="img1" alt="" style="cursor:pointer;" onclick="togFolderPerm(this,'<?=$current_folder['tog_perm_url']?>','folder_permission_info');" />
        <a href="<?=$current_folder['upload_url']?>" class="<?=$current_folder['folder_isgallery']?'special_green':'special'?>" title="Upload into this folder"><strong><?=entities($current_folder['folder_name'])?></strong></a>
        &nbsp;<span style="color:#aaa;">(<a href="<?=$current_folder['edit_url']?>" class="special">Edit</a>)</span>
        <br /><span id="folder_description" style="padding: 2px;color:#808080;"><?=$current_folder['folder_description']==''?'No Description':entities(str_preview($current_folder['folder_description'],80))?></span>
    </p>
    <p id="folder_permission_info" style="margin:0 0 10px 0; padding:0px; line-height:1.6em; color:#444">
        This folder is set as <strong><?=$current_folder['permission']['access']?></strong>.
        <?php if($current_folder['permission']['access']=='public'):?>
        Anyone may browse and access the files in this folder.
		<?php elseif($current_folder['permission']['access']=='hidden'):?>
		Anyone may browse and access the files in this folder if they know <a href="<?=$current_folder['browse_url']?>" class="special">the URL</a>.
        <?php else:?>
        Only
        <?=$current_folder['permission']['friend']?'your <strong class="colgreen">friends</strong>':''?>
        <?=$current_folder['permission']['family']?($current_folder['permission']['friend']?'and <strong class="colfuscia">family</strong>':'your <strong class="colfuscia">family</strong>'):''?>
        <?=!$current_folder['permission']['family']&&!$current_folder['permission']['friend']?'<strong>you</strong>':''?>
        may browse and access the files in this folder.
		<?php endif;?>
    </p>

    <form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php')?>" id="mf_form" onsubmit="return false;">
        <input type="hidden" name="action" value="" />
        <input type="hidden" name="current_folder_id" value="<?=$current_folder['folder_id']?>" />
        <input type="hidden" name="move_to_folder_id" value="0" />

        <div class="rounded green" style="margin-top:10px;padding-bottom:4px;">
            <select onchange="mf_dropdown(this);">
                <option value="" selected="selected">Selected files actions...</option>
                <option value="get_code">&nbsp; &nbsp;Get linking codes</option>
                <option value="" disabled="disabled">Move to...</option>
                <?php reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : if($folder['folder_id'] == $current_folder['folder_id'])continue; ?>
                <option value="move_<?=$folder['folder_id']?>"<?=$folder['folder_isgallery']?' class="colgreen"':''?>>&nbsp; &nbsp;<?=entities(str_preview($folder['folder_name'],50))?> (<?=$folder['files_count']?>)</option>
                <?php endwhile; ?>
            </select>
            <input type="button" value="Delete" onclick="mf_form_action('delete');" />
            <input type="button" value="Upload" onclick="go('<?=$current_folder['upload_url']?>');return false;" />
            <div class="spacer" style="margin-top:5px;"></div>
            <p style="margin:0;float:left;">
                <strong class="s2">Select:</strong>
                <span class="link" id="select_all" onmousedown="mf_check('all');return false;">All</span>,
                <span class="link" id="select_none" onmousedown="mf_check('none');return false;">None</span>,
                <span class="link" id="select_images" onmousedown="mf_check('images');return false;">Images</span>,
                <span class="link" id="select_invert" onmousedown="mf_check('invert');return false;">Invert selection</span>
            </p>
            <p style="margin:0;float:left;margin-left:10px;">
                <strong class="s2">Sort:</strong>
                <a class="special" href="<?=$sort_url['type']?>" <?=$sort_by=='type'?'style="font-style:italic;"':''?>>Type</a>,
                <a class="special" href="<?=$sort_url['name']?>" <?=$sort_by=='name'?'style="font-style:italic;"':''?>>Name</a>,
                <a class="special" href="<?=$sort_url['size']?>" <?=$sort_by=='size'?'style="font-style:italic;"':''?>>Size</a>,
                <a class="special" href="<?=$sort_url['date']?>" <?=$sort_by=='date'?'style="font-style:italic;"':''?>>Date</a>
            </p>
            <?php if ( $current_folder['folder_isgallery'] ) : ?>
            <p style="margin:0;margin-left:10px;float:left;">
                <strong class="s2">View:</strong>&nbsp;
                <span class="link" id="sort_type" onmousedown="mf_show_details(1);return false;">Details</span>,
                <span class="link" id="sort_name" onmousedown="mf_show_gallery(1);return false;">Thumbnails</span>
            </p>
            <?php endif; ?>
            <div class="spacer"></div>
        </div>

        <div class="rounded gray" style="margin-top:10px;color:#606060;padding:6px 10px 6px 10px;">
            <?php if ( $current_folder['files_count'] ) : ?>
            <span style="float:left;">
                <img src="templates/default/images/folder_<?=$current_folder['folder_isgallery']?'image.gif':'regular.gif'?>" alt="" class="img1" />
                There are <?=$current_folder['files_count']?> <?=$current_folder['folder_isgallery']?'images':'files'?> in this folder.
            </span>
            <span style="float:right;padding-top:3px;">
                <?php if ( $current_page > 1 ) : ?><a href="<?=$prev_page_url?>" class="special">Previous</a><?php endif; ?>
                (<strong><?=$file_start?></strong> - <strong><?=$file_end?></strong> of <strong><?=$current_folder['files_count']?></strong>)
                <?php if ( $current_page < $total_pages ) : ?><a href="<?=$next_page_url?>" class="special">Next</a><?php endif; ?>
            </span>
            <div class="spacer"></div>
            <?php else: ?>
                <img src="templates/default/images/folder_<?=$current_folder['folder_isgallery']?'image.gif':'regular.gif'?>" alt="" class="img1" />
                This folder is empty. <a href="<?=$current_folder['upload_url']?>" class="special">Upload files into this folder</a>
            <?php endif; ?>
        </div>

        <div id="mf_files_container"><!-- NO CONTENTS IN HERE! --></div>
    </form>
</div>
<div class="spacer"><!-- --></div>


<script type="text/javascript" charset="utf-8">
<!--
<?php
// we only need a few information from the current folder
$current_folder_array = array
(
    'folder_isgallery'  => (int)$current_folder['folder_isgallery'],
    'folder_name'       => entities($current_folder['folder_name']),
);
?>
var mf_current_folder = <?=json_encode($current_folder_array)?>;

<?php
// Loop through the files and add it to the javascript array
while ( list ( , $file ) = each ( $user_files ) ) : ?>
mf_addfile(<?=$file['file_id']?>,'<?=addslashes($file['file_name'])?>', '<?=$file['file_extension']?>', <?=$file['file_size']?>,'<?=$file['date_string']?>','<?=addslashes($file['url'])?>','<?=addslashes($file['direct_url'])?>','<?=addslashes($file['square_thumb_url'])?>',<?=$file['file_isimage']?>, <?=$file['file_views']?>);
<?php endwhile; ?>

if ( Ajax.enabled )
    new quickEditFolderDescription ( <?=$current_folder['folder_id']?>, '<?=addslashes(($current_folder['folder_description']))?>', 'folder_description', 80 );

mf_init();
-->
</script>
