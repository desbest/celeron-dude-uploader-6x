<!-- tpl_users.php -->

<h1>User Management</h1>
<form method="post" action="admin.php?action=users" id="filter_form">
    <input type="hidden" name="task" value="setfilter" />
    <input type="hidden" name="action" value="users" />

    <p>
        Filter presets: <span class="link" onmousedown="setFilterPreset('bw');">Bandwidth limit reached</span> -
        <span class="link" onmousedown="setFilterPreset('approval');">Unapproved accounts</span> -
        <span class="link" onmousedown="setFilterPreset('inactive_users');">Inactive accounts (no login in 90 days)</span>
    </p>

    <fieldset>
        <table style="width:100%;" cellspacing="1" cellpadding="5" id="filters_tbl">
            <tr>
                <td>User level</td>
                <td>
                    <select name="filters[level]" style="width:130px;">
                        <option value="null" <?=@!isset($filters['level'])||$filters['level']=='null'?'selected="selected"':''?>>Any</option>
                        <option value="<?=LEVEL_ADMIN?>" <?=@$filters['level']==LEVEL_ADMIN?'selected="selected"':''?>>Admin</option>
                        <option value="<?=LEVEL_MODERATOR?>" <?=@$filters['level']==LEVEL_MODERATOR?'selected="selected"':''?>>Moderators</option>
                        <option value="<?=LEVEL_NORMAL?>" <?=@$filters['level']!='null'&&@$filters['level']=='0'?'selected="selected"':''?>>Normal</option>
                        <option value="<?=LEVEL_SUBSCRIBER?>" <?=@$filters['level']==LEVEL_SUBSCRIBER?'selected="selected"':''?>>Normal: Subscriber</option>
                        <option value="<?=LEVEL_DONATOR?>" <?=@$filters['level']==LEVEL_DONATOR?'selected="selected"':''?>>Normal: Donator</option>
                        <option value="<?=LEVEL_MONTHLY?>" <?=@$filters['level']==LEVEL_MONTHLY?'selected="selected"':''?>>Normal: Monthly subscriber</option>
                        <option value="<?=LEVEL_QUARTERLY?>" <?=@$filters['level']==LEVEL_QUARTERLY?'selected="selected"':''?>>Normal: Quaterly subscriber</option>
                        <option value="<?=LEVEL_YEARLY?>" <?=@$filters['level']==LEVEL_YEARLY?'selected="selected"':''?>>Normal: Yearly subscriber</option>
                    </select>
                </td>
                <td>Username</td>
                <td><input type="text" name="filters[username]" size="30" value="<?=@$filters['username']?>" /></td>

            </tr>

            <tr>
                <td>Approval status</td>
                <td>
                    <select name="filters[is_approved]" style="width:130px;">
                        <option value="null" <?=@$filters['is_approved']=='null'?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_approved']=='1'?'selected="selected"':''?>>Approved</option>
                        <option value="0" <?=@$filters['is_approved']=='0'?'selected="selected"':''?>>Not approved</option>
                    </select>
                </td>
                <td>Email address</td>
                <td><input type="text" name="filters[email]" size="60" value="<?=@$filters['email']?>" /></td>
            </tr>

            <tr>
                <td>Suspension</td>
                <td>
                    <select name="filters[is_suspended]" style="width:130px;">
                        <option value="null" <?=@$filters['is_suspended']=='null'?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_suspended']=='1'?'selected="selected"':''?>>Suspended</option>
                        <option value="0" <?=@$filters['is_suspended']=='0'?'selected="selected"':''?>>Not suspended</option>
                    </select>
                </td>
                <td>Admin comments</td>
                <td><input type="text" name="filters[admin_comments]" size="60" value="<?=@$filters['admin_comments']?>" /></td>
            </tr>

            <tr>
                <td>Activation</td>
                <td>
                    <select name="filters[is_activated]" style="width:130px;">
                        <option value="null" <?=@$filters['is_activated']=='null'?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_activated']=='1'?'selected="selected"':''?>>Activated</option>
                        <option value="0" <?=@$filters['is_activated']=='0'?'selected="selected"':''?>>Not activated</option>
                    </select>
                </td>
                <td>Registration time</td>
                <td><input type="text" name="filters[reg_time]" size="5" style="text-align:center;" value="<?=@$filters['reg_time']?>" /> days and longer.</td>
            </tr>

            <tr>
                <td>Bandwidth used</td>
                <td>
                    <select name="filters[bw_used_comp]">
                        <option value="<=" <?=@$filters['bw_used_comp']=='<='?'selected="selected"':''?>>&lt;=</option>
                        <option value=">=" <?=@$filters['bw_used_comp']=='>='?'selected="selected"':''?>>&gt;=</option>
                    </select>
                    <input type="text" name="filters[bw_used]" size="5" style="text-align:center;" value="<?=@$filters['bw_used']?>" /> %
                </td>
                <td>Last login</td>
                <td>
                    <select name="filters[last_login_comp]">
                        <option value="<=" <?=@$filters['last_login_comp']=='<='?'selected="selected"':''?>>has not logged-in in...</option>
                        <option value=">=" <?=@$filters['last_login_comp']=='>='?'selected="selected"':''?>>logged in within the past...</option>
                    </select>
                    <input type="text" name="filters[last_login]" size="5" style="text-align:center;" value="<?=@$filters['last_login']?>" /> days.
                </td>
            </tr>
        </table>
        <p style="text-align:center;margin:2px;padding:5px;background-color:#f0f0f0;">
            <input type="submit" value="Apply filters" class="blue_button" />
            <input type="button" value="Clear filters" class="blue_button" onclick="go('admin.php?action=users');" />
        </p>
    </fieldset>

</form>
<form method="post" action="admin.php?action=user_actions" id="users_form">
    <input type="hidden" name="action" value="user_actions" />

    <fieldset>
        <legend>Users: <?=$users_found?> / <?=$total_users?></legend>
        <table style="width:100%;text-align:center;color:#888888;" cellspacing="1" cellpadding="5" id="users_tbl">
            <tr skip_alternate="1" style="background-color:#f0f0f0;">
                <td style="width:10px;"><a href="<?=$sort_urls['sort_id']?>" class="special"><?=$sort_by=='userid'?'<strong>ID</strong>':'ID'?></a></td>
                <td style="text-align:left;"><a href="<?=$sort_urls['sort_name']?>" class="special"><?=$sort_by=='username'?'<strong>Username</strong>':'Username'?></a></td>
                <td>Email</td>
                <td><a href="<?=$sort_urls['sort_bw']?>" class="special"><?=$sort_by=='bw_used'?'<strong>BW</strong>':'BW'?></a></td>
                <td>Action</td>
                <td style="width:20px;"><input type="checkbox" class="chkbox" onclick="checkAllBoxes(this.form, 'users[]', this.checked);" /></td>
            </tr>
            <?php if ( count ( $users ) ) : reset ( $users ); while ( list ( , $user ) = each ( $users ) ) : ?>
            <tr>
                <td><?=$user['userid']?></td>
                <td style="text-align:left;"><a href="<?=$user['info_url']?>"><?=$user['username']?></a></td>
                <td><a href="<?=$user['email_url']?>"><?=$user['email']?></user></td>
                <td><?=get_size($user['bw_used'],'KB')?></td>
                <td>
                    <a href="<?=$user['edit_url']?>"><img src="templates/default/images/b_edit.gif" alt="Edit" class="img1" /></a>
                    <a href="<?=$user['files_url']?>"><img src="templates/default/images/b_files.gif" alt="Edit" class="img1" /></a>
                </td>
                <td><input type="checkbox" name="users[]" value="<?=$user['userid']?>" class="chkbox" /></td>
            </tr>
            <?php endwhile; else: ?>
            <tr>
                <td colspan="6">No users found matching the filters.</td>
            </tr>
            <?php endif; ?>
        </table>
    </fieldset>
    <p style="text-align:right;">
        <?php if ( $prev_page_url != '' ) : ?>
        <a href="<?=$prev_page_url?>" class="special">Previous</a>&nbsp;
        <?php endif; ?>
        <select onchange="go(this.value);">
        <?php reset ( $page_links ); while ( list ( , $page ) = each ( $page_links ) ) : ?>
            <option value="<?=$page['page_url']?>" <?=$page['page_num']==$current_page?'selected="selected"':''?>><?=$page['page_num']?></option>
        <?php endwhile; ?>
        </select>
        <?php if ( $next_page_url != '' ) : ?>
        &nbsp;<a href="<?=$next_page_url?>" class="special">Next</a>
        <?php endif; ?>
    </p>


    <fieldset>
        <p style="background-color:#f0f0f0;padding:8px;margin:10px 0px 5px 0px;">
            Do this
            <select name="target_action">
                <option value="">---Nothing---</option>
                <option value="approve">Approve</option>
                <option value="suspend">Suspend</option>
                <option value="unsuspend">Unsuspend</option>
                <option value="delete" style="color:red;">Delete</option>
                <option value="resetbw">Reset bandwidth counter</option>
                <option value="email">Send email (select users)</option>
                <option value="prune_nonimages">Prune non-images</option>
                <option value="setfield">Set field</option>
            </select>
            to
            <select name="target_users">
                <option value="selected" selected="selected">Selected users from the list above</option>
                <option value="normal">All Normal level users</option>
                <option value="mods_and_admin">Moderators and Admins</option>
                <option value="all">All registered users</option>
            </select>
            <input type="submit" value="Go" class="blue_button" />
        </p>

        <table style="width:100%;" cellspacing="1" cellpadding="4" id="fieldvalues_tbl">
            <tr>
                <td colspan="4">The following are required if you have selected the "Set field" option. Leave fields blank if you do not want to change. For restrictions, 0 means unlimited.</td>
            </tr>
            <tr>
                <td>Max filesize</td>
                <td><input type="text" name="user_field_values[fl_max_filesize]" style="text-align:center;" size="5" /> KB</td>
                <td>Max storage</td>
                <td><input type="text" name="user_field_values[fl_max_storage]" style="text-align:center;" size="5" /> MB</td>
            </tr>
            <tr>
                <td>Max bandwidth</td>
                <td><input type="text" name="user_field_values[bw_max]" style="text-align:center;" size="5" /> MB</td>
                <td>Folders</td>
                <td>
                    <select name="user_field_values[fl_allow_folders]">
                        <option value="">---</option>
                        <option value="1">Can create folders</option>
                        <option value="0">CANNOT create folders</option>
                    </select>
                    <input type="text" name="user_field_values[fl_max_folders]" style="text-align:center;" size="5" /> folders max.
                </td>
            </tr>
            <tr>
                <td>Bandwidth reset period</td>
                <td><input type="text" name="user_field_values[bw_reset_period]" style="text-align:center;" size="5" /> days</td>
                <td>Bandwidth reset mode</td>
                <td>
                    <select name="user_field_values[bw_reset_auto]">
                        <option value="">---</option>
                        <option value="1">Automatically</option>
                        <option value="0">Manually</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Images only</td>
                <td>
                    <select name="user_field_values[fl_images_only]">
                        <option value="">---</option>
                        <option value="1">Images only</option>
                        <option value="0">Non-images allowed</option>
                    </select>
                </td>
                <td>Watermark images</td>
                <td>
                    <select name="user_field_values[fl_watermark]">
                        <option value="">---</option>
                        <option value="1">Watermark images</option>
                        <option value="0">Don't watermark images</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Allowed file types</td>
                <td colspan="3"><input type="text" name="user_field_values[fl_allowed_types]" size="50" /> (Enter "ANY" for any file types allowed)</td>
            </tr>
        </table>
    </fieldset>
</form>
<script type="text/javascript">
<!--
    alternateRowColor ( getObj ( 'users_tbl' ), 'tr', '#fafafa', '#f5f5f5' );
    alternateRowColor ( getObj ( 'filters_tbl' ), 'tr', '#fafafa', '#f5f5f5' );
    alternateRowColor ( getObj ( 'fieldvalues_tbl' ), 'tr', '#fafafa', '#f5f5f5' );
    alternateRowColor ( getObj ( 'setfield_tbl' ), 'tr', '#fafafa', '#f5f5f5' );

    function validateForm ( form )
    {
        if ( form )
        {
            if ( form.user_action_type.value == 'delete' ) return confirm ( 'Are you sure you want to delete the selected users?' );
        }
        return true;
    }

    function clearFilters ( )
    {
        var form = getObj ( 'filter_form' );

        form['filters[username]'].value = '';
        form['filters[email]'].value = '';
        form['filters[admin_comments]'].value = '';
        form['filters[reg_time]'].value = '';
        form['filters[last_login]'].value = '';
        form['filters[bw_used]'].value = '';
        form['filters[level]'].selectedIndex = 0;
        form['filters[is_approved]'].selectedIndex = 0;
        form['filters[is_suspended]'].selectedIndex = 0;
        form['filters[is_activated]'].selectedIndex = 0;
    }

    function setFilterPreset ( presetName )
    {
        var form = getObj ( 'filter_form' );

        switch ( presetName )
        {
            case 'approval':
            {
                clearFilters();
                form['filters[is_approved]'].selectedIndex = 2;
            }
            break;

            case 'bw':
            {
                clearFilters();
                form['filters[bw_used]'].value = '100';
                form['filters[bw_used_comp]'].selectedIndex = 1;
            }
            break;

            case 'inactive_users':
            {
                clearFilters();
                form['filters[last_login]'].value = 90;
                form['filters[last_login_comp]'].selectedIndex = 0;
            }
        }
    }
-->
</script>