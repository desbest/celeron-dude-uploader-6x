<!-- tpl_browse.php -->
<h1>Registered users</h1>
<p>
    <?php if ( $public_only ) : ?>
    <img src="templates/default/images/icon_users.gif" alt="icon" class="img1" /> <a href="<?=MOD_REWRITE?'members':'browse.php'?>" class="special">Show all users</a>
    <?php else : ?>
    <img src="templates/default/images/icon_users_shared.gif" alt="icon" class="img1" /> <a href="<?=MOD_REWRITE?'members/public':'browse.php?public=1'?>" class="special">Show only users with shared folders</a>
    <?php endif; ?>
</p>
<?php if ( count ( $users ) ) : ?>
<table class="rowlines" id="user_tbl" style="text-align:center;width:100%;color:#505050; margin:8px 0px 8px 0px;" cellspacing="0" cellpadding="4">
    <tr skip_alternate="skip_alternate" class="header">
        <td style="text-align:left;padding-left:18px;">Username</td>
        <td style="width:180px;">Email</td>
        <td style="width:80px;">Join date</td>
        <td style="width:80px;"></td>
    </tr>
    <?php reset ( $users ); while ( list ( $i, $user ) = each ( $users ) ) : ?>
    <tr>
        <td style="text-align:left;">
            <img src="templates/default/images/usericon.gif" alt="" class="img1" />
            <a href="<?=$user['info_url']?>"><?=$user['username']?></a>
        </td>
        <td><?=ascii_str($user['email'])?></td>
        <td><?=$user['reg_date']?></td>
        <td style="text-align:right;">
            <?php if ( $user['has_public'] ) : ?>
            <a href="<?=$user['browse_url']?>" class="special"><img src="templates/default/images/b_browse.gif" alt="" class="img1" /></a>
            <?php else: ?>
            <img src="templates/default/images/b_browse_disabled.gif" alt="" class="img1" />
            <?php endif; ?>
            <?php if ( $user['pref_accepts_pm'] ) : ?>
            <a href="<?=$user['message_url']?>" class="special"><img src="templates/default/images/b_message.gif" alt="" class="img1" /></a>
            <?php else: ?>
            <img src="templates/default/images/b_message_disabled.gif" alt="" class="img1" />
            <?php endif; ?>
        </td>
    </tr>
<?php endwhile; ?>
</table>

<table style="width:100%;color:#404040;margin-top:5px;" cellspacing="0" cellpadding="0">
    <?php if ( count ( $pages ) ) : ?>
    <tr style="line-height:1.4em;">
        <td>
            <?php if ( $public_only ) : ?>
            <?=number_format($total_users)?> users with shared folders.
            <?php else : ?>
            There are <?=number_format($total_users)?> registered users.
            <?php endif; ?>
        </td>
        <td style="text-align:right;">
            <?php /* Show previous link */ if ( count ( $pages ) && $pages[0]['number'] == 'PREV' ) : ?>
            <a href="<?=$pages[0]['url']?>" class="special">&laquo; Previous</a>&nbsp;
            <?php endif; ?>

            <select onchange="go(this.value);">
            <?php reset ( $pages ); while ( list ( , $page ) = each ( $pages ) ) : if ( $page['number'] == 'NEXT' || $page['number'] == 'PREV' ) continue; ?>
            <option value="<?=$page['url']?>" <?=$current_page==$page['number']?'selected="selected" style="font-weight:bold;"':''?>><?=$page['number']?></option>
            <?php endwhile; ?>
            </select>

            <?php /* Show next link */ if ( count ( $pages ) && $pages[count($pages)-1]['number'] == 'NEXT' ) : ?>
            &nbsp;<a href="<?=$pages[count($pages)-1]['url']?>" class="special">Next &raquo;</a>
            <?php endif; ?>
        </td>
    </tr>
    <?php endif; ?>
</table>


<!-- alternating row color -->
<script type="text/javascript">
    alternateRowColor(getObj('user_tbl'),'tr','#fefefe','#fafafa');
</script>
<?php else: ?>
<p>No users found.</p>
<?php endif; ?>