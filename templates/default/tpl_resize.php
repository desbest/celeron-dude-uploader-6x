<!-- tpl_resize.php -->

<h1>Image resize</h1>

<p>
    This utility will allow you to resize JPEG and PNG format images (GIF images if available). Enter the new width or height in the form below.
    The image width and height will be automatically adjusted to maintain the original ratio. Note that the result
    image will override any existing one.
</p>

<?php /* Any errors will be shown here */ if ( isset ( $error ) ) print $error; ?>

<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'myfiles':'myfiles.php').'?action=resize&amp;file='.$file_path?>">
    <input type="hidden" name="action" value="resize" />
    <input type="hidden" name="task" value="resize" />
    <input type="hidden" name="file_path" value="<?=$file_path?>" />

    <table cellspacing="1" cellpadding="3" border="0" style="margin-top:10px;">
        <tr>
            <td style="width:130px;">Image name</td>
            <td><a href="<?=$file_url?>"><?=$file_name?></a></td>
        </tr>
        <tr>
            <td>Save resized image as</td>
            <td><input type="text" name="file_name" id="file_name" size="50" maxlength="100" value="<?php /* $file_thumb is the suggested file name for the resized image. */ print $file_thumb?>" /> Existing image will be overwritten.</td>
        </tr>
        <tr>
            <td>Dimension (Width x Height)</td>
            <td>
                <input type="text" name="file_width" id="file_width" size="3" maxlength="5" value="<?=$file_width?>" style="text-align:center;" onkeyup="adjustRatio ( event, this, getObj('file_height'), <?=$file_width/$file_height?>, <?=$file_width?> )" /> x
                <input type="text" name="file_height" id="file_height" size="3" maxlength="5" value="<?=$file_height?>" style="text-align:center;" onkeyup="adjustRatio ( event, this, getObj('file_width'), <?=$file_height/$file_width?>, <?=$file_height?> )" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Save" class="blue_button" />
                <input type="reset" class="blue_button" value="Reset" />
                <input type="button" class="blue_button" value="Cancel" onclick="go('<?=$cancel_url?>');" />
            </td>
        </tr>
    </table>
</form>