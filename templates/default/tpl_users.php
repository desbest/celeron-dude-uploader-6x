<!-- tpl_users.php -->
<form method="post" action="admin.php?action=users" id="users_form">
    <fieldset>
        <p style="background-color:#DBDBDB;color:#222;padding:5px;margin:1px 1px 4px 1px;">
            <strong>User filters:</strong>
        </p>
        <table cellspacing="1" cellpadding="3" border="0" style="width:100%" id="filters_tbl">
            <tr>
                <td style="width:70px">User Level</td>
                <td>
                    <select name="filters[level]" style="width:100px">
                        <option value="" <?=@!isset($filters['level'])||$filters['level']==''?'selected="selected"':''?>>All levels</option>
                        <option value="<?=LEVEL_ADMIN?>" <?=@$filters['level']==LEVEL_ADMIN?'selected="selected"':''?>>Admin</option>
                        <option value="<?=LEVEL_MODERATOR?>" <?=@$filters['level']==LEVEL_MODERATOR?'selected="selected"':''?>>Moderators</option>
                        <option value="<?=LEVEL_NORMAL?>" <?=@$filters['level']!=''&&@$filters['level']=='0'?'selected="selected"':''?>>Normal</option>
                        <option value="<?=LEVEL_SUBSCRIBER?>" <?=@$filters['level']==LEVEL_SUBSCRIBER?'selected="selected"':''?>>&nbsp; &nbsp;Subscriber</option>
                        <option value="<?=LEVEL_DONATOR?>" <?=@$filters['level']==LEVEL_DONATOR?'selected="selected"':''?>>&nbsp; &nbsp;Donator</option>
                        <option value="<?=LEVEL_MONTHLY?>" <?=@$filters['level']==LEVEL_MONTHLY?'selected="selected"':''?>>&nbsp; &nbsp;Monthly</option>
                        <option value="<?=LEVEL_QUARTERLY?>" <?=@$filters['level']==LEVEL_QUARTERLY?'selected="selected"':''?>>&nbsp; &nbsp;Quaterly</option>
                        <option value="<?=LEVEL_YEARLY?>" <?=@$filters['level']==LEVEL_YEARLY?'selected="selected"':''?>>&nbsp; &nbsp;Annually</option>
                    </select>
                </td>
                <td>Username</td>
                <td><input type="text" name="filters[username]" size="20" value="<?=@$filters['username']?>" /></td>
                <td>Last login</td>
                <td>
                    <select name="filters[last_login_operator]">
                        <option value="<=" <?=@$filters['last_login_operator']=='<='?'selected="selected"':''?>>more than</option>
                        <option value=">=" <?=@$filters['last_login_operator']=='>='?'selected="selected"':''?>>less than</option>
                    </select>
                    <input type="text" name="filters[last_login]" size="3" style="text-align:center;" value="<?=@$filters['last_login']?>" /> days
                </td>
            </tr>
            <tr>
                <td style="width:70px">Approval</td>
                <td style="width:110px">
                    <select name="filters[is_approved]" style="width:100px">
                        <option value="" <?=@$filters['is_approved']==''?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_approved']=='1'?'selected="selected"':''?>>Approved</option>
                        <option value="0" <?=@$filters['is_approved']=='0'?'selected="selected"':''?>>Not approved</option>
                    </select>
                </td>
                <td style="width:80px">Email Address</td>
                <td><input type="text" name="filters[email]" size="20" value="<?=@$filters['email']?>" /></td>
                <td>Bandwidth used</td>
                <td>
                    <select name="filters[bw_used_operator]">
                        <option value="<=" <?=@$filters['bw_used_operator']=='<='?'selected="selected"':''?>>&lt;=</option>
                        <option value=">=" <?=@$filters['bw_used_operator']=='>='?'selected="selected"':''?>>&gt;=</option>
                    </select>
                    <input type="text" name="filters[bw_used]" size="5" style="text-align:center;" value="<?=@$filters['bw_used']?>" />
                    <select name="filters[bw_used_unit]">
                        <option value="GB" <?=@$filters['bw_used_unit']=='GB'?'selected="selected"':''?>>GB</option>
                        <option value="MB" <?=@$filters['bw_used_unit']=='MB'?'selected="selected"':''?>>MB</option>
                        <option value="%" <?=@$filters['bw_used_unit']=='%'?'selected="selected"':''?>>%</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Suspension</td>
                <td>
                    <select name="filters[is_suspended]" style="width:100px">
                        <option value="" <?=@$filters['is_suspended']==''?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_suspended']=='1'?'selected="selected"':''?>>Suspended</option>
                        <option value="0" <?=@$filters['is_suspended']=='0'?'selected="selected"':''?>>Not suspended</option>
                    </select>
                </td>
                <td>Comments</td>
                <td><input type="text" name="filters[admin_comments]" size="20" value="<?=@$filters['admin_comments']?>" /></td>
                <td>Space used</td>
                <td>
                    <select name="filters[space_used_operator]">
                        <option value="<=" <?=@$filters['space_used_operator']=='<='?'selected="selected"':''?>>&lt;=</option>
                        <option value=">=" <?=@$filters['space_used_operator']=='>='?'selected="selected"':''?>>&gt;=</option>
                    </select>
                    <input type="text" name="filters[space_used]" size="5" style="text-align:center;" value="<?=@$filters['space_used']?>" />
                    <select name="filters[space_used_unit]">
                        <option value="KB" <?=@$filters['space_used_unit']=='KB'?'selected="selected"':''?>>KB</option>
                        <option value="MB" <?=@$filters['space_used_unit']=='MB'?'selected="selected"':''?>>MB</option>
                        <option value="%" <?=@$filters['space_used_unit']=='%'?'selected="selected"':''?>>%</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Activation</td>
                <td>
                    <select name="filters[is_activated]" style="width:100px">
                        <option value="" <?=@$filters['is_activated']==''?'selected="selected"':''?>>Any</option>
                        <option value="1" <?=@$filters['is_activated']=='1'?'selected="selected"':''?>>Activated</option>
                        <option value="0" <?=@$filters['is_activated']=='0'?'selected="selected"':''?>>Not activated</option>
                    </select>
                </td>
                <td>Registration</td>
                <td>
                    <select name="filters[reg_time_operator]">
                        <option value="<=" <?=@$filters['reg_time_operator']=='<='?'selected="selected"':''?>>more than</option>
                        <option value=">=" <?=@$filters['reg_time_operator']=='>='?'selected="selected"':''?>>less than</option>
                    </select>
                    <input type="text" name="filters[reg_time]" size="3" style="text-align:center;" value="<?=@$filters['reg_time']?>" maxlength="6" />
                    &nbsp;days
                </td>
                <td colspan="2">
                    <input type="button" class="colfuscia" onclick="submitFilters();return false;" value="Apply" />
                    <input type="button" onclick="go('admin.php?action=users')" value="Clear" />
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <p style="background-color:#efefef;padding:4px;margin:1px">
            Be VERY careful when performing a delete action. The deleted users or files cannot be recovered.
            <br /><strong>TIP:</strong> If you've selected "Users matched by the filters above" as your target, it is best to apply the filters
            first to make sure that the returned users are correc before performing any action.
        </p>
        <table cellspacing="1" cellpadding="3" border="0" style="width:100%" id="actions_tbl">
            <tr>
                <td>Apply action</td>
                <td>
                    <select name="action_type" onchange="$('set_fields_group').style.display=(this.value=='set_field'?'block':'none');">
                        <option value="" selected="selected">------------------------</option>
                        <option value="activate">Activate</option>
                        <option value="approve">Approve</option>
                        <option value="suspend">Suspend</option>
                        <option value="unsuspend">Un-Suspend</option>
                        <option value="delete">Delete user</option>
                        <option value="reset_bw">Reset bandwidth counter</option>
                        <option value="email">Send email</option>
                        <option value="set_field">Set custom fields</option>
                    </select>
                    to
                    <select name="action_target">
                        <option value="selected">Selected (checked) users from the list below</option>
                        <option value="filter">Users matched by the filters above</option>
                        <option value="all">All registered users</option>
                        <option value="level_<?=LEVEL_ADMIN?>">&nbsp; &nbsp;Admins only</option>
                        <option value="level_<?=LEVEL_MODERATOR?>">&nbsp; &nbsp;Moderators only</option>
                        <option value="level_<?=LEVEL_NORMAL?>">&nbsp; &nbsp;Normal users only</option>
                        <option value="level_<?=LEVEL_SUBSCRIBER?>">&nbsp; &nbsp;&nbsp; &nbsp;Subscriber</option>
                        <option value="level_<?=LEVEL_DONATOR?>">&nbsp; &nbsp;&nbsp; &nbsp;Donator</option>
                        <option value="level_<?=LEVEL_MONTHLY?>">&nbsp; &nbsp;&nbsp; &nbsp;Monthly</option>
                        <option value="level_<?=LEVEL_QUARTERLY?>">&nbsp; &nbsp;&nbsp; &nbsp;Quaterly</option>
                        <option value="level_<?=LEVEL_YEARLY?>">&nbsp; &nbsp;&nbsp; &nbsp;Annually</option>
                    </select>
                    <input type="button" value=" &nbsp;Go! &nbsp;" onclick="submitAction();" />
                </td>
            </tr>
        </table>

        <div id="set_fields_group" style="margin-top:10px;display:none;">
            <p style="background-color:#efefef;padding:4px;margin:1px">
                Enter the new values below. Blank fields will be ignored. For limit (Max size/bandwidth/etc..) fields, entering 0 will set
                that field as "Unlimited". For the "Allowed file types" field, enter <strong>all</strong> to allow all file types.
            </p>
            <table cellspacing="1" cellpadding="3" border="0" style="width:100%" id="setfields_tbl">
                <tr>
                    <td>Allowed file types</td>
                    <td>
                        <select name="fields[images_only]" style="width:170px">
                            <option value="">-----------</option>
                            <option value="1">Images only</option>
                            <option value="0">Specified types</option>
                        </select>
                        - <input type="text" name="fields[allowed_types]" size="35" />
                    </td>
                    <td>Max storage</td>
                    <td><input type="text" name="fields[max_storage]" size="3" style="text-align:center;" /> MB</td>
                </tr>
                <tr>
                    <td>Image watermarking</td>
                    <td>
                        <select name="fields[watermark]" style="width:170px">
                            <option value="">-----------</option>
                            <option value="1">Watermark images</option>
                            <option value="0">Don't watermark images</option>
                        </select>
                    </td>
                    <td>Max file size</td>
                    <td><input type="text" name="fields[max_filesize]" size="3" style="text-align:center;" /> KB</td>
                </tr>
                <tr>
                    <td>Create folder</td>
                    <td>
                        <select name="fields[create_folder]" style="width:170px">
                            <option value="">-----------</option>
                            <option value="1">Yes, allow folder creation</option>
                            <option value="0">No, cannot create folders</option>
                        </select>
                    </td>
                    <td>Max folders</td>
                    <td><input type="text" name="fields[max_folders]" size="3" style="text-align:center;" /> folders</td>
                </tr>
                <tr>
                    <td>Bandwidth reset</td>
                    <td>
                        <select name="fields[bw_reset_mode]" style="width:170px">
                            <option value="">-----------</option>
                            <option value="1">Automatically</option>
                            <option value="0">Manually</option>
                        </select>
                        every <input type="text" name="fields[bw_reset_period]" size="3" style="text-align:center;" /> days (Automatic only)
                    </td>
                    <td>Max bandwidth</td>
                    <td><input type="text" name="fields[max_bandwidth]" size="3" style="text-align:center;" /> MB</td>
                </tr>
                <tr>
                    <td>Transfer rate</td>
                    <td><input type="text" name="fields[transfer_rate]" size="3" style="text-align:center" /> KB/s</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </fieldset>

    <fieldset>
        <div style="background-color:#FFFFE5;padding:4px;color:#555;">
            <p style="width:20%;float:left;">
                Filtered: <strong><?=number_format($total_filtered_users)?></strong> | Total: <strong><?=number_format($total_users)?></strong>
            </p>
            <p style="width:50%;float:left">
                Sort by
                <select name="sort_by">
                    <option value="userid" <?=$sort_by=='userid'?'selected="selected"':''?>>User ID</option>
                    <option value="username" <?=$sort_by=='username'?'selected="selected"':''?>>User Name</option>
                    <option value="status" <?=$sort_by=='status'?'selected="selected"':''?>>Account status</option>
                    <option value="email" <?=$sort_by=='email'?'selected="selected"':''?>>Email address</option>
                    <option value="files" <?=$sort_by=='files'?'selected="selected"':''?>>Number of files</option>
                    <option value="space" <?=$sort_by=='space'?'selected="selected"':''?>>Space used</option>
                    <option value="bandwidth" <?=$sort_by=='bandwidth'?'selected="selected"':''?>>Bandwidth used</option>
                </select>
                <select name="sort_order">
                    <option value="asc" <?=$sort_order=='asc'?'selected="selected"':''?>>Ascending</option>
                    <option value="desc" <?=$sort_order=='desc'?'selected="selected"':''?>>Descending</option>
                </select>
                <input type="button" value="Go" onclick="this.form.submit()" />
            </p>
            <?php if ( $total_pages ) : ?>
            <p style="width:30%;float:right;text-align:right;">
                Pages: <strong><?=$total_pages?></strong> &nbsp;Jump to:
                <select name="current_page" onchange="this.form.submit()">
                    <?php for ( $i = 1; $i <= $total_pages; ++$i ) : ?>
                    <option value="<?=$i?>" <?=$i==$current_page?'selected="selected"':''?>>page <?=$i?></option>
                    <?php endfor; ?>
                </select>
            </p>
            <?php else: ?>
            <input type="hidden" name="current_page" value="1" />
            <?php endif; ?>
            <div class="spacer"></div>
        </div>
        <table class="rowlines" style="width:100%;color:#888;margin-top:4px;" id="users_tbl" cellspacing="0" cellpadding="5" border="0">
            <tr class="header" skip_alternate="skip_alternate">
                <td>UserID. Username</td>
                <td>Status</td>
                <td>Email</td>
                <td class="ct">Files</td>
                <td class="ct">Space</td>
                <td class="ct">Bandwidth</td>
                <td class="ct">Action</td>
                <td class="cr" style="width:25px"><input type="checkbox" class="chkbox" onclick="checkAllBoxes(this.form,'userids[]',this.checked);" /></td>
            </tr>
            <?php reset ( $users ); while ( list ( , $user ) = each ( $users ) ) : ?>
            <tr>
                <td><?=$user['userid']?>. <a href="<?=$user['info_url']?>"><?=entities($user['username'])?></a></td>
                <td style="color:#ccc">
                    <?=$user['is_approved']?'<span class="colgreen">Appr</span>':'<span class="colfuscia">Appr</span>'?> |
                    <?=$user['is_activated']?'<span class="colgreen">Act</span>':'<span class="colfuscia">Act</span>'?> |
                    <?=!$user['is_suspended']?'<span style="color:#ddd">Sus</span>':'<span class="colfuscia">Sus</span>'?>
                </td>
                <td><a href="<?=$user['email_url']?>"><?=entities($user['email'])?></a></td>
                <td class="ct"><?=$user['files_count']?number_format($user['files_count']):'-'?></td>
                <td class="ct"><?=$user['total_file_size']?'('.get_size($user['total_file_size'],'B',0).')':'-'?></td>
                <td class="ct"><?=$user['bw_used']?get_size($user['bw_used'],'KB',0):'-'?></td>
                <td class="ct">
                    <a href="<?=$user['edit_url']?>"><img src="templates/default/images/b_edit.gif" alt="Edit" class="img1" /></a>
                    <a href="<?=$user['files_url']?>"><img src="templates/default/images/b_files.gif" alt="Edit" class="img1" /></a>
                </td>
                <td class="cr"><input type="checkbox" class="chkbox" name="userids[]" value="<?=$user['userid']?>" /></td>
            </tr>
            <?php endwhile; ?>
            <?php if ( !count ( $users ) ) : ?>
            <tr>
                <td colspan="8" class="colfuscia">No users found</td>
            </tr>
            <?php endif; ?>
        </table>
    </fieldset>
</form>

<script type="text/javascript">
alternateRowColor ( $('users_tbl'), 'tr', '#fff', '#fafafa' );
alternateRowColor ( $('actions_tbl'), 'tr', '#fafafa', '#f5f5f5' );
alternateRowColor ( $('filters_tbl'), 'tr', '#fafafa', '#f5f5f5' );
alternateRowColor ( $('setfields_tbl'), 'tr', '#fafafa', '#f5f5f5' );

function submitFilters()
{
    var form = $('users_form');
    if ( !form ) return false;
    form.elements.action_type.selectedIndex = 0;
    form.elements.current_page.value = 1;
    form.submit();
}

function submitAction()
{
    var form = $('users_form');
    if ( !form ) return false;
    var action_type = form.elements.action_type;
    var action_target = form.elements.action_target;
    var checked_users_count = countCheckedBoxes ( form, 'userids[]' );

    if ( action_type.value == '' )
    {
        alert ( 'Please select an action to perform.' );
        return false;
    }

    if ( action_target.value == '' )
    {
        alert ( 'Please select the target users.' );
        return false;
    }

    if ( action_target.value == 'selected' && !checked_users_count )
    {
        alert ( 'No user selected.' );
        return false;
    }

    if ( action_type.value.indexOf('delete') != -1 )
    {
        if ( confirm ( 'You are about to perform a DELETE action. Are you sure?' ) )
            form.submit();
        else return false;
    }

    form.submit();
}
</script>