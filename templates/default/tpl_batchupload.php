<!-- tpl_batchupload.php -->
<h1>Batch file uploading</h1>

<p style="margin-bottom:10px;">
    Batch file uploading is a quick way to upload all your images without having to select them one by one.
    This is done by combining (Zipping) all your files into one single Zip archive and uploading it.
    Creating a Zip archive is easy if you are running Windows XP which has native support for Zip files.
    Follow the steps below to quickly create a Zip archive and uploading it to your account. If you are not
    running Windows XP, there are many other freeware and commercial Zip archiving utilities available.
    A great FREEWARE Zip utility is <a href="http://www.7-zip.org/" class="special">7-zip</a>. The following
    steps describe how to create a Zip archive on Windows XP.
</p>

<h1>Step 1: Select your files</h1>
<p>
    Drag and select all your files, then right click on them and select Send To, then select Compressed (Zipped) Folder.
    A new Zip file will appear momentarily that contains all the files you have selected. You can also do this with folders.
</p>
<p style="text-align:center;margin-bottom:20px"><img src="templates/default/images/batchupload_1.gif" alt="Step1" style="border:1px solid #ccc" /></p>

<h1>Step 2 (Optional): Renaming the archive</h1>
<p>
    All Zip files named either "batch.zip" or "upload.zip" will be extracted automatically, otherwise you must check on
    the "Extract Zip files" checkbox. If you do not do so any Zip file you upload
    that is not named "batch.zip" or "upload.zip" will be uploaded normally.
</p>
<p style="text-align:center"><img src="templates/default/images/batchupload_2.gif" alt="Step2" style="border:1px solid #ccc" /></p>


<h1>Step 3: Uploading the Zip file</h1>
<p>
    If the Zip file is not named either "batch.zip" or "upload.zip", you must tell the Uploader to extract the files in it.
    If you do not do so, the file will be uploaded normally.
</p>
<p style="text-align:center"><img src="templates/default/images/batchupload_3.gif" alt="Step3" style="border:1px solid #ccc" /></p>


<div class="spacer"></div>