<!-- tpl_error.php -->
<?php
if ( !isset ( $error ) || !is_array ( $error ) ) { $error = array ( $error ); }
if ( !isset ( $error_title ) ) $error_title = 'The following errors were found';
?>
<div style="background-color:#FFFFD6;padding: 3px 4px 3px 8px; margin:5px 0px 5px 0px;-moz-border-radius:5px;">
    <img src="templates/default/images/exclamation.gif" alt="Error!" class="img1" />
    <strong style="font-size:1.1em;color:#B32400;"><?=entities($error_title)?></strong>
    <ul style="padding: 3px 0px 3px 3px;color:#B32400;">
        <?php reset ( $error ); while ( list ( $i, $err ) = each ( $error ) ) : ?><li style="margin-top:3px;"><?=$err?></li><?php endwhile; ?>
    </ul>
</div>