<!-- tpl_usercp.php -->

<?php global $UPL; ?>


<div style="float:left; width: 18%;">
    <h1>UserCP Options</h1>
    <ul class="ls_menu">
        <li><a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp':'usercp.php')?>" class="special">Account info</a></li>
        <li><a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp?action=pm':'usercp.php?action=pm')?>" class="special">Messages <?=$new_messages_count > 0 ? "($new_messages_count unread)" : ''?></a></li>
        <li>&nbsp;&nbsp;<img src="templates/default/images/icon_sendpm.gif" alt="sendpm icon" class="img1" /> <a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp?action=sendpm':'usercp.php?action=sendpm')?>" class="special">Send message</a></li>
        <li><a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp?action=editoptions':'usercp.php?action=editoptions')?>" class="special">Preferences</a></li>
        <li><a href="<?=UPLOADER_URL.(MOD_REWRITE?'usercp?action=editprofile':'usercp.php?action=editprofile')?>" class="special">Edit email &amp; password</a></li>
    </ul>
</div>

<div style="float:right; width:80%;">
<?php /* Show user messages inbox */ if ( $action == 'pm' ) : ?>
    <?php if ( count ( $messages ) == 0 ) : ?>
    <p style="text-align:center;">You have no messages.</p>
    <?php else: ?>
    <div style="background-color:#F5FFE6;border-bottom: 1px solid #DCE6CF; padding: 10px 7px 10px 10px; margin: 5px 0px 5px 0px; -moz-border-radius:6px;">
        <strong class="s2">Select:</strong> &nbsp;
        <span class="link" onmousedown="checkAllBoxes(getObj('messages_form'),'messages[]',1);">All</span>,
        <span class="link" onmousedown="checkAllBoxes(getObj('messages_form'),'messages[]',0)">None</span>
        &nbsp; &nbsp;
        <strong class="s2">Options:</strong> &nbsp;
        <span class="link" onmousedown="PMaction('mark_read');">Mark Read</span> -
        <span class="link" onmousedown="PMaction('mark_unread');">Mark Unread</span> -
        <span class="link" onmousedown="PMaction('delete');">Delete selected messages</span>

        <?php if($UPL['USER']['max_messages']&&$UPL['USER']['messages_count']>=$UPL['USER']['max_messages']):?>
        Your inbox is currently full. You will not be able to receive new messages. Please clean out your inbox.
        <?php else: ?>
        <br /><br />
        You currently have <?=$UPL['USER']['messages_count']?> messages. Your inbox limit is <?=$UPL['USER']['max_messages']==0?'Unlimited':$UPL['USER']['max_messages'] . ' messages'?>.
        <?php endif; ?>
    </div>
    <form id="messages_form" method="post" action="usercp.php?action=pmactions">
        <input type="hidden" name="task" value="" />
        <table style="width:100%;color:#404040;" id="msg_tbl" cellpadding="3" cellspacing="0" border="0" class="rowlines">
            <?php reset ( $messages ); while ( list ( $i, $msg ) = each ( $messages ) ) : ?>
            <tr <?=$msg['is_read']?'':'skip_alternate="1" class="color4"'?>>
                <td style="width:16px;"><img src="templates/default/images/icon_pm.gif" alt="pm icon" class="img1" /></td>
                <td style="cursor:pointer;color:black;" onclick="go('<?=$msg['read_url']?>');"><?=$msg['from_username']?></td>
                <td style="cursor:pointer;" onclick="go('<?=$msg['read_url']?>');"><?=entities(str_preview($msg['subject'],30))?><br /><span style="color:#808080;font-size:0.9em"><?=entities(str_preview($msg['message'],40))?></span></td>
                <td style="cursor:pointer;width:70px;" onclick="go('<?=$msg['read_url']?>');"><?=get_date('M d, Y',$msg['date'])?></td>
                <td style="width:20px;"><input type="checkbox" name="messages[]" value="<?=$msg['messageid']?>" /></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </form>
    <script type="text/javascript">
    <!--
    function PMaction ( action )
    {
        var formObj = getObj ( 'messages_form' );
        if ( formObj )
        {
            var selected_msg = countCheckedBoxes ( formObj, 'messages[]' );
            if ( !selected_msg )
            {
                help ( 'Please select at least one message.' );
                return false;
            }
            if ( action == 'delete' && !confirm ( 'Are you sure you want to remove the selected messages from your inbox?' ) ) return false;
            formObj.task.value = action;
            formObj.submit();
        }
    }
    -->
    </script>
    <?php endif; ?>
<?php /* Send message */ elseif ( $action == 'sendpm' ) : ?>
    <h1>Send a message</h1>
    <form action="<?=UPLOADER_URL.(MOD_REWRITE?'usercp?action=sendpm':'usercp.php?action=sendpm')?>" method="post" onsubmit="return sendPM();">
        <input type="hidden" name="task" value="sendpm" />
        <?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>
        <table id="send_tbl" cellspacing="1" cellpadding="2" border="0" style="width:100%;">
            <tr>
                <td style="width:70px;">Username</td>
                <td>
                    <input type="text" name="newpm[to]" id="newpm_to" value="<?=$newpm['to']?>" maxlength="64" size="30" />
                </td>
            </tr>
            <tr>
                <td>Subject</td>
                <td><input type="text" name="newpm[subject]" id="newpm_subject" value="<?=$newpm['subject']?>" maxlength="255" size="58" /></td>
            </tr>
            <tr>
                <td class="tt">
                    Message
                </td>
                <td>
                    <textarea name="newpm[message]" id="newpm_message" cols="65" rows="5"><?=$newpm['message']!=''?entities($newpm['message']):''?></textarea><br />
                    <span style="font-size:0.9em;color:gray;">Message cannot be longer than 5,000 characters.</span>
                    <span class="help" style="font-size:0.9em;" onclick="help('Supported BB codes:\n[B]bold text[/B]\n[I]Italicized text[/I]\n[color=#xxxxxx]colored text[/color]\n[url]http://celerondude.com[/url]\n[url=http://celerondude.com]Go to my website[/url]\n[IMG]http://celerondude.com/image.jpg[/IMG]');">BB Codes</span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Send" /> <input type="button" value="Cancel" onclick="go('<?=previous_page((MOD_REWRITE?'usercp?action=pm':'usercp.php?action=pm'))?>');" /></td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
    <!--
        addLoadEvent(function(){$('newpm_to').focus();});
    -->
    </script>

<?php /* Read PM */ elseif ( $action == 'readpm' ) : ?>
    <p style="background-color:#F5FFE6;border-bottom: 1px solid #DCE6CF;color:#808080; padding: 10px 7px 10px 10px; margin: 5px 0px 5px 0px; -moz-border-radius:6px;">
        <span style="color:black;"><?=entities($message['subject'])?></span> (<?=$message['date']?>) -
        <span class="link" onclick="go('<?=$message['send_profile_url']?>');"><?=$message['from_username']?></span>
        <br /><br />
        <strong class="s2">Options:</strong>&nbsp; <span class="link" onclick="go('<?=$message['reply_url']?>');">Reply</span> -
        <span class="link" onclick="if(confirm('Confirm delete?'))go('<?=$message['delete_url']?>');return false;">Delete</span>
    </p>

    <p style="padding:8px;margin-bottom:15px;background-color:#fafafa;color:#505050;margin:1px;-moz-border-radius:4px;"><?=$message['message']?></p>

    <?php /* Edit user info, show this */ elseif ( $action == 'editprofile' ) : ?>
    <h1>Edit profile</h1>
    <p style="padding:0px;margin:0px;">
        Enter your current password before continuing.
        Enter a new password or email address if you want to change them, otherwise leave the fields alone.
        If you enter a new email address, an email will be sent to the new address to verify it. The email address
        on your account will NOT change until you verify the new address.
    </p>
    <form action="usercp.php?action=editprofile" method="post">
        <input type="hidden" name="task" value="save" />
        <?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>
        <table cellspacing="1" cellpadding="2" border="0" style="margin-top:8px;">
            <tr>
                <td style="width:150px;">Current password <span style="color:red;">*</span></td>
                <td><input type="password" id="current_password" name="current_password" value="" maxlength="30" size="30" /></td>
            </tr>
            <tr>
                <td>New password (optional)</td>
                <td><input type="password" name="new_password" value="" maxlength="30" size="30" /></td>
            </tr>
            <tr>
                <td>Confirm password</td>
                <td><input type="password" name="new_password_confirm" value="" maxlength="30" size="30" /></td>
            </tr>
            <tr>
                <td>New email address</td>
                <td><input type="text" name="email" value="<?=@$user['email']?>" maxlength="100" size="50" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Save" /> <input type="button" value="Cancel" onclick="go('usercp.php');" /></td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
    <!--
    var curpass = getObj ( 'current_password' );
    if ( curpass ) curpass.focus();
    -->
    </script>
<?php /* If edit options, show this section only */ elseif ( $action == 'editoptions' ) : ?>
    <h1>User preferences</h1>
    <form action="usercp.php?action=editoptions" method="post">
        <input type="hidden" name="task" value="save" />
        <ul class="ls_menu">
            <li>
                <input type="checkbox" class="chkbox" name="pemail" value="1" id="pemail" <?=$user['pemail']==1?'checked="checked"':''?>/>
                <label for="pemail">Allow other users to view my email.</label>
            </li>
            <li>
                <input type="checkbox" class="chkbox" name="pmessage" value="1" id="pmessage" <?=$user['pmessage']==1?'checked="checked"':''?>/>
                <label for="pmessage">Allow other users to message me.</label>
            </li>
            <li style="margin-top:10px;"><input type="submit" value="Save preferences" /></li>
        </ul>

    </form>
<?php /* Otherwise, display account info */  else: ?>
    <h1>User account information</h1>
    <table id="user_info" cellspacing="0" cellpadding="5" border="0" style="width:100%;color:#404040;">
        <tr>
            <td style="width:180px;">Email address</td>
            <td><?=$user['email']?></td>
        </tr>
        <tr>
            <td>User level</td>
            <td><?=$user['level']?></td>
        </tr>
        <tr>
            <td>Max bandwidth</td>
            <td><?=$user['bw_max']=='0.0 MB'?'Unlimited':$user['bw_max']?></td>
        </tr>
        <tr>
            <td>Bandwidth counter resets</td>
            <td><?=$user['bw_auto_reset'] ? ( $user['bw_reset_period'] ? 'Every ' . $user['bw_reset_period'] . ' days' : 'Automatically when you run out of bandwidth.' ) : 'By admin'?></td>
        </tr>
        <tr>
            <td>Max storage</td>
            <td><?=$user['max_storage']=='0.0 MB'?'Unlimited':$user['max_storage']?></td>
        </tr>
    </table>
<?php /* end if for usercp actions */ endif; ?>
</div>
<div class="spacer"><!-- --></div>

<script type="text/javascript">
    alternateRowColor(getObj('user_info'),'tr','#ffffff','#fafafa');
    alternateRowColor(getObj('msg_tbl'),'tr','#fafafa','#ffffff');
</script>
