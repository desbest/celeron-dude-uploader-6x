<!-- tpl_activate.php -->

<h1>Activate</h1>

<p>
    Enter the email associated with your account below.
	If your account is located an activation code will be sent to you.
</p>

<?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>

<form method="post" action="account.php?action=resend_activation">
    <input type="hidden" name="task" value="activate" />
    <p>
        <input type="text" name="email" value="" size="30" maxlength="200" />
        <input type="submit" value="Submit" />
        <input type="button" onclick="go('<?=UPLOADER_URL.(MOD_REWRITE?'login':'account.php?action=login')?>');" value="Cancel" />
    </p>
</form>