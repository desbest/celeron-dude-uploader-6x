<!-- tpl_public_uploaded.php -->
<h1>Upload result</h1>

<?php if ( count ( $errors ) ) : ?>
<p>The following file(s) were not uploaded because of the indicated reason(s):</p>
<ul class="ls_menu" style="margin:10px 0 10px 30px;list-style:disc">
    <?php reset ( $errors ); while ( list ( , $err_msg ) = each ( $errors ) ) : ?>
    <li><?=$err_msg?></li>
    <?php endwhile; ?>
</ul>
<?php endif; ?>

<?php if ( count ( $uploaded ) ) : ?>
<p>
   <strong><?=count($uploaded)?></strong> files were uploaded. Click on the following links to either view your uploaded files or manage them.
</p>
<p>
    <a href="<?=$upload_set['view_url']?>" class="special no_underline2"><strong>View</strong> your uploaded files</a> or
    <a href="<?=$upload_set['manage_url']?>" class="special no_underline2"><strong>Edit</strong> them</a>
</p>
<p>
    If you wish to share these files with your friends, send them the <strong>View</strong> link.
    Save the <strong>Edit</strong> link if you wish to delete these files later on.
</p>
<?php if ( !$upload_set['upload_ispublic'] ) : ?>
<p>
    Since you did not mark this set as <strong>Public</strong>, you will not be able to <strong>Browse</strong>
    for this set. So don't lose the URL to this set.
</p>
<?php endif; ?>
<?php else: /* No files uploaded */ ?>
<p>
    No files were uploaded. <a href="<?=UPLOADER_URL.(MOD_REWRITE?'public':'public.php')?>" class="special">Try again</a>
</p>
<?php endif; ?>