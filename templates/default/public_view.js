var per_row = 5;
var is_gallery = 0;
var show_views = 1;
var show_date = 0;
var show_name = 0;
var show_header = 1;
var files = [];

function addFile ( name, size, date, views, url, is_image, square, small )
{
    files.push({'name': name, 'size': size, 'date': date, 'views': views, 'url': url, 'square_thumb_url': square, 'small_thumb_url': small, 'is_image': is_image});
}

function showGallery ( )
{
    var html = '';
    var container = getObj ( 'view_contents' );
    if ( !container ) throw 'Could not find view_contents';
    var rows = Math.ceil ( files.length / per_row );
    html = '<div class="gallery">';
    for ( var row = 0; row < rows; ++row )
    {
        html += '<div class="row">';
        for ( var i = row * per_row; i < ( row + 1 ) * per_row && i < files.length; ++i )
        {
            file=files[i];
            file.thumb_url = file.is_image ? file.square_thumb_url : base_url +'templates/default/images/nothumb.gif';
            html += '<div class="cell" style="width:'+(100/per_row)+'%;">';
                html += '<div class="inner_cell">';
                    html += '<div class="top">Views: ' + file.views + '</div>';
                    html += '<div class="center">';
                        html += '<div class="thumb"><a href="' + file.url + '"><img src="' + file.thumb_url + '" alt="' + file.name + '" class="img1" /></a></div>';
                    html += '</div>';
                    html += '<div class="bottom"><span class="name">' + file.name.wordWrap(15,'<br />',true) + '</span></div>';
                html += '</div>';
            html += '</div>';

        }
        html += '<div class="spacer"></div></div>';
    }
    html += '</div><div class="spacer"></div>';
    container.innerHTML = html;
    setCookie( 'uploader_view_mode', 'gallery' )
}


function showDetails ( )
{
    var html = '';
    var container = getObj ( 'view_contents' );
    if ( !container ) throw 'Could not find view_contents';

    html = '<table cellspacing="0" cellpadding="5" style="width:100%;color:#505050;margin-top:8px;" id="details_tbl" class="rowlines">';

    if ( show_header )
    {
    html +=
        '<tr skip_alternate="1" class="header">' +
            '<td style="padding-left:26px;">File name</td>' +
            '<td class="ct" style="width:60px;">Size</td>' +
            ( show_views ? '<td style="width:110px;text-align:center;">Views</td>' : '' ) +
            ( show_date ? '<td style="width:120px;text-align:center;">Upload date</td>' : '' ) +
        '</tr>';
    }

    for ( var i = 0; i < files.length; ++i )
    {
        file=files[i];
        html +=
        '<tr>' +
            '<td><img src="' + icons_url + get_icon ( get_extension ( file.name ) ) + '" class="img1" alt="icon" /> &nbsp;<a href="' + file.url + '">' + file.name + '</a></td>' +
            '<td class="ct" style="width:60px;">' + file.size + '</td>' +
            ( show_views ? '<td style="width:110px;text-align:center;">' + ( file.views ? file.views + ' times' : 'Never' ) + '</td>' : '' ) +
            ( show_date ? '<td style="width:120px;text-align:center;">' + file.date + '</td>' : '' ) +
        '</tr>';
    }
    html += '</table>';
    container.innerHTML = html;
    alternateRowColor ( getObj ( 'details_tbl' ), 'tr', '#fefefe', '#fafafa' );
    setCookie( 'uploader_view_mode', 'details' )
}


function showBBCode ( )
{
    var html = '';
    var container = getObj ( 'view_contents' );
    if ( !container ) throw 'Could not find view_contents';
    html = '<p style="margin:5px;"><textarea cols="110" rows="25" style="padding:5px;">';
    for ( var i = 0; i < files.length; ++i )
    {
        file=files[i];
        html += '[url='+file.url+']' + ( file.is_image ? '[img]' + file.small_thumb_url + '[/img]': file.name ) + '[/url]' + "\r\n";
    }
    html += '</textarea></p>';
    container.innerHTML = html;
    setCookie( 'uploader_view_mode', 'bbcode' )
}

function showHTMLCode ( )
{
    var html = '';
    var container = getObj ( 'view_contents' );
    if ( !container ) throw 'Could not find view_contents';
    html = '<p style="margin:5px;"><textarea cols="130" rows="25" style="padding:5px;">';
    for ( var i = 0; i < files.length; ++i )
    {
        file=files[i];
        html += '<a href="' + file.url + '">\r\n   ' + ( file.is_image ? '<img src="' + file.small_thumb_url + '" alt="'+file.name+'" />': file.name ) + '\r\n</a>' + "\r\n";
    }
    html += '</textarea></p>';
    container.innerHTML = html;
    setCookie( 'uploader_view_mode', 'htmlcode' )
}