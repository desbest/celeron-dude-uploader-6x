<!-- tpl_upload.php -->
<?php
// DO NOT DELETE THIS
global $UPL;
?>
<form style="display:inline;" method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'upload':'upload.php').'?action=upload'?>" enctype="multipart/form-data" id="upload_form" onsubmit="return doUpload(this);">
    <!-- upload options -->
    <div style="float:left; width: 25%; margin-right: 10px;">
        <h1>Upload options</h1>
        <select name="upload_to" id="upload_to" style="width:170px; margin:3px 0px 5px 0px;" onclick="this.options[0].disabled=true;this.options[0].style.color='#808080';" onchange="showIt('photoFolderWarning',this.options[this.selectedIndex].getAttribute('is_gallery'));checkFileExists();return true;">
            <option value="">Upload selected files to...</option>
            <?php /* Display folders */ reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : $folder['spaces'] = str_repeat ( '&nbsp; &nbsp;', $folder['level'] ); ?>
            <option value="<?=$folder['path']?>" <?=$folder['selected'] ? 'selected="selected"' : '' ?> <?=$folder['is_gallery']?'is_gallery="1" style="color:green;"':''?> ><?=$folder['spaces']?><?=$folder['name']=='<MAIN_FOLDER>'?'Main Folder (default)':str_slice($folder['name'],25)?></option>
            <?php /* End folders loop */ endwhile; ?>
        </select>
        <select name="overwrite_option" style="width:170px; margin-bottom:5px;" onclick="this.options[0].disabled=true;this.options[0].style.color='#808080';">
            <option value="skip">If file exists...</option>
            <option value="skip">Skip (default)</option>
            <option value="rename">Rename</option>
            <option value="overwrite">Overwrite</option>
        </select>
	<br />
        <input type="checkbox" name="create_img_tags" value="1" id="create_img_tags" checked="checked" class="chkbox"/>
        <label for="create_img_tags">Create [IMG] tags</label><br />
        <input type="checkbox" name="create_thumbnails" value="1" id="create_thumbnails" class="chkbox" onclick="var box=getObj('thumbnail_size_box');box.style.display=this.checked?'block':'none';"/>
        <label for="create_thumbnails">Create thumbnails</label> (<span class="help" onclick="help('This will create thumbnails of uploaded images. Currently only JPEG, PNG and GIF formats are supported. Other types will be uploaded as normal.')">?</span>)<br />

        <div id="thumbnail_size_box" style="display:none;padding-left:20px;">
            <input type="radio" class="radio" name="thumbnail_size" id="thumbnail_size_small" value="small" checked="checked" />
            <label for="thumbnail_size_small">Small (<?=$UPL['SETTINGS']['thumb_sm']?>)</label>
            <br />
            <input type="radio" class="radio" name="thumbnail_size" id="thumbnail_size_large" value="large" />
            <label for="thumbnail_size_large">Large (<?=$UPL['SETTINGS']['thumb_lg']?>)</label>
        </div>

        <input type="checkbox" name="extract_zip_files" value="1" id="extract_zip_files" class="chkbox" />
        <label for="extract_zip_files">Extract Zip files</label> (<span class="help" onclick="go('help.php?topic=batch_upload');">?</span>)

        <p id="upload_restrictions" style="font-size:0.9em; color: #404040; line-height: 1.5em;">
            Maximum file size: <?=$restrictions['max_file_size']?><br />
            Allowed file types (<span class="help" onclick="help('You can only upload files with these extensions. If you are limited to Images Only, then you can only upload image files, usually JPEG, PNG, and GIF.');">?</span>): <?=$restrictions['images_only'] ? 'Images only' : ( $restrictions['file_types'] == '' ? 'Any' : $restrictions['file_types'] )?>
            <br />
            Please make sure that filenames do not contain <span class="help" onclick="help('Invalid characters include: &#92;&#32;&#47;&#32;&#58;&#32;&#42;&#32;&#63;&#32;&#60;&#32;&#62;&#32;&#124;&#32;&#38;\nPlease make sure filenames do not contain any of these characters. Also make sure that filenames start with alphanumeric characters (a-z, 0-9). Any violation will result in the file being discarded.');">invalid characters</span>
	    and start with a letter or number.
        </p>
    </div>

    <!-- upload fields -->
    <div style="width:70%; float:right;">
        <h1>Select files</h1>
        <p style="margin-bottom:10px;">
            Batch uploading is available if you are uploading many files. This feature will save you the trouble of selecting each file individually.
            Simply upload a Zip file containing your files and check "Extract Zip files" located on the left side of this page.
            Zip files named "upload.zip" or "batch.zip" will be extracted automatically.
            <a href="help.php?topic=batch_upload" class="special">Click here</a> for instructions on creating Zip files.
        </p>
        <p id="photoFolderWarning" style="display:none;padding:5px;color:#B32400;background-color:#FFFFD6;margin:2px 0px 4px 0px;-moz-border-radius:5px;">
            <img src="templates/default/images/exclamation.gif" alt="Error!" class="img1" />
            You have chosen to upload into a photo folder. Make sure that you select only images as other file types will be discarded.
        </p>
        <p id="fileExistsWarning" style="display:none;padding:5px;color:#B32400;background-color:#FFFFD6;margin:2px 0px 4px 0px;-moz-border-radius:5px;"><!-- do not edit --></p>
        <div style="background-color:#F5FFE6;border-bottom: 1px solid #DCE6CF; padding: 10px 7px 10px 10px; margin-bottom:5px; -moz-border-radius:6px;">
            <span id="fileSelect"></span>&nbsp; <input type="submit" id="upload_button" value="Upload" />
            <input type="button" value="Cancel" onclick="if(fileSelected&&!confirm('Looks like you have selected some files, do you really want to discard the selected files?'))return false;go('<?=$cancel_url?>');" />
        </div>
        <ul id="selectedFiles"><!-- do not edit in here unless you know what you're doing --></ul>
        <p id="fileSelectedStatus" style="margin-top:5px;background-color:#f0f0f0;padding:5px 5px 5px 8px;-moz-border-radius:5px;">
            No files selected. Click on Browse to add files. You can add more than one file.
        </p>
    </div>
    <div class="spacer"><!-- --></div>
</form>

<script type="text/javascript" src="templates/default/advupload.js?version=6.2.2"></script>
<script type="text/javascript">
var upload_to = getObj ( 'upload_to' );
if ( upload_to ) showIt('photoFolderWarning',upload_to.options[upload_to.selectedIndex].getAttribute('is_gallery'));

function doUpload(oForm)
{
    if ( !fileSelected )
    {
        alert ( 'You did not select any file.' );
        return false;
    }
    var uploadButton = getObj('upload_button');
    if ( uploadButton ) uploadButton.disabled = true;
    return true;
}
</script>
