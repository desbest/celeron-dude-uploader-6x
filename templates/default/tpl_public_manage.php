<div class="rounded green">
    <div style="color:#576345;" id="set_info">
        <p>
            This upload set is <strong><?=$upload_set['upload_ispublic']?'Public':'Private'?></strong>, was uploaded
            by <strong><?=$upload_set['upload_name']==''?'Anonymous':entities($upload_set['upload_name'])?></strong>
            <?=get_date('M d, Y', $upload_set['upload_date'])?>.
        </p>
        <?php if ( $upload_set['upload_description'] != '' ) : ?>
        <p><strong>Description:</strong> <?=parse_message($upload_set['upload_description'])?></p>
        <?php endif; ?>
        <div style="margin-top:10px">
            <strong class="s2">Action:</strong>
            <span class="link" onclick="togView('edit_set');togView('set_info');">Edit set info</span>
            &nbsp;
            <strong class="s2">Select:</strong>
            <span class="link" onmousedown="checkAllBoxes('manage_form','file_ids[]',true);">All</span>,
            <span class="link" onmousedown="checkAllBoxes('manage_form','file_ids[]',false);">None</span>
            &nbsp; &nbsp;
            <select onchange="publicManageAction(this)">
                <option value="">Selected files actions</option>
                <option value="delete">&nbsp; Delete</option>
            </select>
        </div>
    </div>

    <div id="edit_set" style="display:none">
        <form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'public':'public.php')?>">
            <input type="hidden" name="action" value="editsetinfo" />
            <input type="hidden" name="upload_id" value="<?=(int)$upload_set['upload_id']?>" />
            <input type="hidden" name="upload_key" value="<?=$upload_set['upload_key']?>" />
            <table cellspacing="3" border="0">
                <tr>
                    <td>Public</td>
                    <td>
                        <input type="checkbox" class="chkbox" name="upload[public]" id="upload_public" value="1" <?=$upload_set['upload_ispublic']?'checked="checked"':''?> />
                        <label for="upload_public">Allow these files to be browsed by others</label>
                    </td>
                </tr>
                <tr>
                    <td style="width:80px;">Your name</td>
                    <td><input type="text" name="upload[name]" value="<?=entities($upload_set['upload_name'])?>" autocomplete="off" maxlength="64" size="30" /> (optional)</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input type="text" name="upload[description]" autocomplete="off" value="<?=entities($upload_set['upload_description'])?>" maxlength="255" size="100" /> (optional)</td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Save changes" /> <input type="button" value="Cancel" onclick="togView('edit_set');togView('set_info');" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>

<form method="post" action="<?=UPLOADER_URL.(MOD_REWRITE?'public':'public.php')?>" id="manage_form">
    <input type="hidden" name="action" value="" />
    <input type="hidden" name="upload_id" value="<?=(int)$upload_set['upload_id']?>" />
    <input type="hidden" name="upload_key" value="<?=$upload_set['upload_key']?>" />
    <div class="gallery">
        <?php reset ( $files ); while ( list ( , $file ) = each ( $files ) ) : ?>
        <div class="cell" style="width:110px;margin:0 0 15px 7px;">
            <div class="inner_cell">
                <div class="top">Views: <?=number_format($file['file_views'])?></div>
                <div class="center"><a href="<?=$file['url']?>"><img src="<?=$file['file_isimage']?$file['square_thumb_url']:UPLOADER_URL . 'templates/default/images/nothumb.gif'?>" alt="<?=entities($file['file_name'], ENT_QUOTES)?>" class="thumb" /></a></div>
                <div class="bottom">
                    <span class="name"><?=entities(wordwrap($file['file_name'], 15, "\r\n", true))?></span>
                    <div class="extra_options"><input type="checkbox" name="file_ids[]" value="<?=$file['file_id']?>" class="chkbox" /></div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <div class="spacer"></div>
</form>

<script type="text/javascript">
<!--
function publicManageAction(select)
{
    var value = select.value;
    var form = $('manage_form');
    var check_count = countCheckedBoxes ( form, 'file_ids[]' );

    if ( value == 'delete' )
    {
        if ( !check_count )
            help ( 'You did not select any files.' );
        else
        {
            form.elements.action.value = 'deletefiles';
            form.submit();
            return false;
        }
    }

    select.selectedIndex = 0;
}
-->
</script>