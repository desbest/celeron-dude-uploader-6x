<!-- tpl_stats.php -->
<h1>Uploader Statistics</h1>

<table id="stats_tbl" cellspacing="1" cellpadding="7" border="0" style="width:100%; margin-top:10px;line-height:1.4em;color:#404040">
    <tr skip_alternate="skip_alternate" style="background-color:#eaeaea;">
        <td colspan="2"><strong>Registered Users</strong></td>
    </tr>
    <tr>
        <td style="width: 100px;">Registered users</td>
        <td><?=number_format($stats['users_count'])?> users</td>
    </tr>
        <tr>
            <td>Newest user</td>
            <td>
                <?php if ( $stats['users_count'] ) : ?>
                <a href="<?=$stats['newest_user_url']?>"><?=$stats['newest_user_name']?></a>
                <?php else: ?>
                None
                <?php endif; ?>
            </td>
        </tr>
</table>

<table id="stats_tbl2" cellspacing="1" cellpadding="7" border="0" style="width:100%; margin-top:10px;line-height:1.4em;color:#404040">
    <tr skip_alternate="skip_alternate" style="background-color:#eaeaea;">
        <td colspan="2"><strong>Uploader</strong></td>
    </tr>
    <tr>
        <td style="width: 100px;">Bandwidth usage</td>
        <td>
            <strong><?=$stats['total_bandwidth_used']?> total</strong>. Highest usage by <a href="<?=$stats['highest_bw_userinfo']?>" class="special"><?=$stats['highest_bw_username']?></a> with <?=$stats['highest_bw_value']?>.
        </td>
    </tr>
    <tr>
        <td>Disk Usage</td>
        <td>
            <strong><?=number_format($stats['userfiles_stats']['total_folders'])?></strong> folders,
            <strong><?=number_format($stats['userfiles_stats']['total_files'])?></strong> files,
            <strong><?=get_size($stats['userfiles_stats']['total_size'])?></strong> transferred
        </td>
    </tr>
</table>

<table id="stats_tbl3" cellspacing="1" cellpadding="7" border="0" style="width:100%; margin-top:10px;line-height:1.4em;color:#404040">
    <tr skip_alternate="skip_alternate" style="background-color:#eaeaea;">
        <td colspan="2"><strong>Public Uploader</strong></td>
    </tr>
    <tr>
        <td style="width: 100px;">Bandwidth Usage</td>
        <td><strong><?=get_size($stats['public_stats']['total_bandwidth_usage'])?></strong> transferred</td>
    </tr>
    <tr>
        <td>Disk Usage</td>
        <td>
            <strong><?=$stats['public_stats']['total_public_sets']?></strong> sets,
            <strong><?=$stats['public_stats']['total_public_files']?></strong> files,
            <strong><?=get_size($stats['public_stats']['total_public_size'])?></strong> total.
        </td>
    </tr>

</table>

<script type="text/javascript">
<!--
alternateRowColor(getObj('stats_tbl'),'tr','#fafafa','#f5f5f5');
alternateRowColor(getObj('stats_tbl2'),'tr','#fafafa','#f5f5f5');
alternateRowColor(getObj('stats_tbl3'),'tr','#fafafa','#f5f5f5');
-->
</script>