var Uploader = new Object();
Uploader.version = 6.3;

var userAgent = navigator.userAgent.toLowerCase();
var is_ie = document.all;
var is_opera = userAgent.indexOf('opera') > -1;

var timer = false;
var icons_url = 'templates/default/images/icons/';

if(typeof Array.prototype.push=='undefined'){Array.prototype.push=function(){var l=this.length;for(var i=0;i<arguments.length;++i)this[l+i]=arguments[i];return this.length;}}
Array.prototype.in_array=function(v){for(var i=0;i<this.length;++i){if(this[i]==v)return true;}return false;}
Array.prototype.remove=function(v){for(var i=0;i<this.length;++i){if(this[i]==v){this.splice(i,1);return true;}}return false;}
Object.prototype.toQueryString=function(){var q=[];for(var x in this){var t=typeof this[x];if(t!='function'&&t!='object')q.push([x,encodeURIComponent(this[x])].join('='));}return q.join('&');}

function rand(){return String(Math.floor(Math.random()*10000000000));}
function rand2(){return rand()+rand()+rand()+rand()+rand();}
function getObj(sId){return document.getElementById(sId);}
function checkIt(b,c){if(b){b.checked=!c;b.click();b.checked=c;}}
function showIt(sId,v){var x=$(sId);if(x)x.style.display=(v?'block':'none');}
function togView(sId){var el=$(sId);if(el){el.style.display=el.style.display=='none'?'':'none';}}
function checkAllBoxes(form,fieldName,chkVal){formObj=$(form);if(!formObj)return false;var checkBoxes=formObj.getElementsByTagName('input');for(var i=0;i<checkBoxes.length;i++){if(checkBoxes[i].name==fieldName&&!checkBoxes[i].disabled){checkBoxes[i].checked=chkVal;}}}
function countCheckedBoxes(form,fieldName){formObj=$(form);if(!formObj)return false;var checkBoxes=formObj.getElementsByTagName('input');var checkedBoxes=0;for(var i=0;i<checkBoxes.length;i++){checkedBoxes+=(checkBoxes[i].name==fieldName&&!checkBoxes[i].disabled&&checkBoxes[i].checked)?1:0;}return checkedBoxes;}
function pInt(oF){if(oF){oF.value=parseInt(oF.value);if(oF.value=='NaN'){oF.value=0;}}}
function pop(sURL){return!window.open(sURL);}
function go(url){if(url.substr(0,7)!='http://')url=base_url+url;window.location=url;return false;}
function htmlspecialchars(s){var t={'&':'&amp;','"':'&quot;',"'":'&#039;','<':'&lt;','>':'&gt;'};for(var k in t){rx=new RegExp(k,'g');s=s.replace(rx,t[k]);}return s;}
function addslashes(s){s=s.replace(/'/gi,"&#039;");s=s.replace(/"/gi,"&quot;");return s;}
function alternateRowColor(obj,el,c1,c2){if(obj){var els=obj.getElementsByTagName(el);var j=0;for(var i=0;i<els.length;i++)if(!els[i].getAttribute('skip_alternate')){els[i].style.backgroundColor=(j&1?c2:c1);j++;}}}
function basename(s){var p=-1;for(var i=0;i<s.length;i++){if( s.charAt(i)=='\\'||s.charAt(i)=='/')p=i;}if(p<0)return s;return s.substr(p+1,s.length-p);}
function dirname(s){var p=-1;for(var i=0;i<s.length;i++){if( s.charAt(i)=='\\'||s.charAt(i)=='/')p=i;}if(p<0)return s;return s.substr(0,p+1);}
function togImage(im,n){if(!im)return false;var alt=im.getAttribute('alternateImage');if(!alt)alt=dirname(im.src)+'/'+n;var cur=im.src;im.src=alt;im.setAttribute('alternateImage',cur);}
function get_extension(n){n=n.substr(n.lastIndexOf('.')+1);return n.toLowerCase();}
function str_slice(s,l){if(s.length>l)return s.substr(0,l/2)+'...'+s.substr(s.length-(l/2));return s;}
function str_preview(s,l){return s.length>l?s.substr(0,l)+'...':s;}
function setStyles(obj,styles){if(!obj)return false;for(var k in styles)obj.style[k]=styles[k];}
function number_format(n){n+='';x=n.split('.');x1=x[0];x2=x.length>1?'.'+x[1]:'';var rgx=/(\d+)(\d{3})/;while(rgx.test(x1)){x1 = x1.replace(rgx,'$1'+','+'$2');}return x1+x2;}
function setParentSize(i,s){if(!s)s=2;i.parentNode.parentNode.style.width=i.width+s+'px';i.parentNode.parentNode.style.height=i.height+s+'px';}
function set_border(i,p,s){if(typeof s=='undefined')s=2;p.style.width=i.width+s+'px';p.style.height=i.height+s+'px';}
function trim(v,p){var t=Math.pow(10,p);return Math.round(v*t)/t;}
function get_size(v,u){if(!u)u='B';if(v>1024){if(u=='B')return get_size(v/1024,'KB');if(u=='KB')return get_size(v/1024,'MB');if(u=='MB')return get_size(v/1024,'GB');if(u=='GB')return get_size(v/1024,'TB');}return trim(v,0)+'&nbsp;'+u;}
function deleteNode(n){return n.parentNode.removeChild(n);}
function confirmGo(s,url){if(confirm(s))go(url);return false;}

// from: http://jsfromhell.com/string/wordwrap
String.prototype.wordWrap = function(m, b, c){ //v1.0
    var i, j, l, s, r = this.split("\n");
    if(m > 0) for(i = -1, l = r.length; ++i < l;){
        for(s = r[i], r[i] = ""; s.length > m;
            j = c ? m : (j = s.substr(0, m).match(/\S*$/)).input.length - j[0].length
            || j.input.length + (j = s.substr(m).match(/^\S*/)).input.length + j[0].length,
            r[i] += s.substr(0, j) + ((s = s.substr(j)).length ? b : "")
        );
        r[i] += s;
    }
    return r.join("\n");
};

function Timer()
{
	this.t=false;
	this.intv=false;
	this.start=function(func,delay){this.stop();this.t=window.setTimeout(func,delay);};
	this.stop=function(){if(this.t)window.clearTimeout(this.t);}

	this.startInterval=function(func,delay){this.intv=window.setInterval(func,delay);};
	this.stopInterval=function(){if(this.intv)window.clearInterval(this.intv);}
}

function get_icon ( ext )
{
    var icons =
	{
		'default':'file.gif',
		// images
		'jpg'	: 'images.gif',
		'jpeg'	: 'images.gif',
		'png'	: 'png.gif',
		'gif'	: 'gif.gif',
		// media
		'avi'	: 'video.gif',
		'wmv'	: 'video.gif',
		'mpeg'	: 'video2.gif',
		'mpg'	: 'video2.gif',
		'mov'	: 'quicktime.gif',
		'mp3'	: 'mp3.gif',
		// Archives
		'zip'	: 'zip.gif',
		'rar'	: 'zip.gif',
		'tar'	: 'zip.gif',
		// documents
		'pdf'	: 'acrobat.gif',
		'ppt'	: 'powerpoint.gif',
		'txt'	: 'text.gif',
		'doc'	: 'word.gif',
		'xls'	: 'excel.gif'
	}
    return icons[ext] ? icons[ext] : icons['default'];
}

function copyTextArea ( id )
{
	var field = $ ( id );
	if ( field )
	{
		if ( is_ie ) field.createTextRange ( ).execCommand ( 'copy' );
		else alert ( 'Sorry, this feature is only enabled for Internet Explorer at the moment. Select the text and press Ctrl+C to copy and Ctrl+V to paste.' );
	}
}

function help ( str, container )
{
	container = container || 'message_content';
	var x = $(container);
	if(!x)return false;
	x.innerHTML = str.replace(/\n/g,'<br />');
	showIt ( 'message', true );
}

function uplAlert ( str, title )
{
	if ( !title ) title = 'Message';
	var alert_box = $ ( 'alert_box' );
	if ( !alert_box ) var alert_box = document.createElement ( 'div' );
	alert_box.id = 'alert_box';
	alert_box.style.height = '200px';
	alert_box.innerHTML = '<h1>'+title+'</h1>' +
	'<p><img src="templates/default/images/exclamation.gif" alt="" class="img1" /> ' + str + '</p>';
	document.body.appendChild ( alert_box );
}


function togFolderPerm(img, url, update_container)
{
	if ( !Ajax.enabled )
	{
		go(url);
		return true;
	}
	var ajax = new AjaxRequest ( url, {method:'get', response:'json'} );
	ajax.oncomplete = function ( )
	{
		var response = this.response;
		if ( response.result == 'success' )
		{
			if ( response.current_perm == 'public' ) img.src = base_url + 'templates/default/images/icon_folder_public.gif';
			else if ( response.current_perm == 'hidden' ) img.src = base_url + 'templates/default/images/icon_folder_hidden.gif';
			else img.src = base_url + 'templates/default/images/icon_folder_private.gif';

			if(typeof update_container != 'undefined')
			{
				// remind user that this folder is public
				var share_private_reminder = $('share_private_reminder');
				var permission = response.permission;
				update_container = $(update_container);
				var str = "This folder is set as <strong>" + permission['access'] + "</strong>.";
				if ( permission['access'] == 'public' )
				{
					str += " Anyone may browse and access the files in this folder.";
					share_private_reminder.style.display = 'none';
				}
				else if ( permission['access'] == 'hidden' )
				{
					str += ' Anyone may browse and access the files in this folder if they know <a href="' + response.browse_url + '" class="special">the URL</a>.';
					share_private_reminder.style.display = 'none';
				}
				else
				{
					str += " Only " +
					(permission['friend']?'your <strong class="colgreen">friends</strong>':'') +
					(permission['family']?(permission['friend']?' and <strong class="colfuscia">family</strong>':'your <strong class="colfuscia">family</strong>'):'') +
					(!permission['family']&&!permission['friend']?'<strong>you</strong>':'') +
					' may browse and access the files in this folder.';
					share_private_reminder.style.display = 'inline';
				}
				update_container.innerHTML = str;
			}
		}
	}
	ajax.request();
}

var quickEditBase = Class.create();

quickEditBase.prototype =
{
	initialize : function()
	{
		this.options = {inputSize:50, maxInput:255, maxCharsVisible:100, textAlign:'left', maxWidth:100, style:{}};
	},

	initContainer : function()
	{
		if(!this.container) return false;
		this.oldStyles = {backgroundColor:this.container.style.backgroundColor,color:this.container.style.color};
		this.container.onmouseover = function(){Object.extend(this.style, {backgroundColor:'#FFFFE6',cursor:'text',color:'#000'});};
		this.container.onmouseout  = function(){Object.extend(this.container.style, this.oldStyles);}.bind(this);
		this.container.onclick = this.showEditable.bind(this);
	},

	restoreContainer: function()
	{
		if(!this.container) return false;
		Object.extend(this.container.style, this.oldStyles);
		this.container.onmouseover = null;
		this.container.onmouseout  = null;
		this.container.onclick = null;
	},

	setOptions : function(options)
	{
		Object.extend(this.options, options||{});
	},

	showEditable : function()
	{
		this.editBox = document.createElement('span');
		this.inputField = document.createElement('input');
		if(!this.editBox||!this.inputField)return false;
		this.inputField.value = this.value;
		this.inputField.size = this.options.inputSize;
		this.inputField.onkeypress = this.keyPressHandler.bindAsEventListener(this);
		this.inputField.onblur = this.doUpdate.bind(this);
		this.inputField.maxLength = this.options.maxInput;
		this.inputField.style.textAlign = this.options.textAlign;
		this.editBox.className = 'qinput';
		Object.extend(this.editBox.style, this.options.style||{});
		this.editBox.appendChild(this.inputField);
		this.container.parentNode.insertBefore(this.editBox, this.container);
		this.container.style.display = 'none';
		this.inputField.setAttribute('autocomplete', 'off');

		var width = this.inputField.value.getTextWidth();
		this.inputField.style.width = ( width > this.options.maxWidth ? this.options.maxWidth : ( width < 40 ? 40 : width ) ) + 'px';

		this.inputField.focus();
		this.inputField.select();
		//this.inputField.focus();
	},

	hideEditable : function(value)
	{
		var str = value || this.value;
		str = str_slice ( str.escapeHTML(), this.options.maxCharsVisible );
		this.container.innerHTML = str;
		this.container.style.display = '';
		if(this.editBox.parentNode) this.editBox.parentNode.removeChild(this.editBox);
	},

	keyPressHandler : function(event)
	{
		event = event || window.event;
		var key = event.keyCode;
		if(key == 13)
		{
			this.inputField.onblur=function(){};
			window.setTimeout(this.doUpdate.bind(this), 10);
			return false;
		}
		if(key == 27)
		{
			this.hideEditable();
			return false;
		}

		var str = this.inputField.value + String.fromCharCode(key);
		var width = str.getTextWidth();
		this.inputField.style.width = ( width > this.options.maxWidth ? this.options.maxWidth : ( width < 40 ? 40 : width ) ) + 'px';

		return true;
	},

	doUpdate : function()
	{
		alert('doUpdate() is abstract');
		this.hideEditable();
	}
};


var quickEditFolderDescription = Class.create();
quickEditFolderDescription.prototype = Object.extend(new quickEditBase(), {
	initialize : function(folderId, description, container, maxLength)
	{
		this.folderId = folderId;
		this.setOptions({inputSize:50, maxInput:255, maxCharsVisible:maxLength, maxWidth:400});
		this.container = $(container);
		this.value = description;
		this.initContainer();
		this.ajax = null;
	},

	ajaxCallback : function()
	{
		var response = this.ajax.response;
		if(response.result != 'success') alert(response.message);
		this.hideEditable(this.value==''?'No Description':this.value);
	},

	doUpdate : function()
	{
		if(this.inputField.value != this.value)
		{
			this.value = this.inputField.value;
			var postData = {action:'update_description', folder_id:this.folderId, description:this.value};
			this.ajax = new AjaxRequest(base_url+'folders.php?action=update_description', {method:'post', 'postData':postData, response:'json'});
			this.ajax.oncomplete = this.ajaxCallback.bind(this);
			this.ajax.request();
		}
		else this.hideEditable(this.value==''?'No Description':this.value);
	}
});