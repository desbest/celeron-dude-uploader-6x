<!-- tpl_public_view.php -->
<script type="text/javascript" src="templates/default/public_view.js"></script>
<script type="text/javascript">
/* Number of thumbnails per row. Decrease this number if you increase the thumbnail width. */
var per_row = 5;
var show_views = 1;
var show_date = 0;
var is_gallery = 1;
var files = [];
<?php while ( list ( , $file ) = each ( $files ) ) : ?>
addFile('<?=addslashes($file['file_name'])?>', '<?=get_size($file['file_size'],'B',0)?>', '', <?=(int)$file['file_views']?>, '<?=addslashes($file['url'])?>', <?=(bool)$file['file_isimage']?'true':'false'?>, '<?=addslashes($file['square_thumb_url'])?>', '<?=addslashes($file['small_thumb_url'])?>');
<?php endwhile; ?>
</script>

<div class="rounded green" style="color:#708059;">
    <p>Uploaded by <strong><?=$upload_set['upload_name'] == '' ? 'Anonymous' : entities($upload_set['upload_name']) ?></strong>
        <?=get_date('M d, Y', $upload_set['upload_date'])?>

    </p>
    <?php if ( $upload_set['upload_description'] != '' ) : ?>
    <p><strong>Description:</strong> <?=parse_message($upload_set['upload_description'])?></p>
    <?php endif;?>
	<p>
	<a href="<?=$upload_set['view_url']?>" class="special">Link to this set</a>
	- <a href="<?=$upload_set['slideshow_url']?>" class="special">View images in this set as a slideshow</a>
	</p>
    <p>
        Switch viewing mode:
        <span class="link" onmousedown="showGallery();">Gallery</span> -
        <span class="link" onmousedown="showDetails();">Details</span> -
        <span class="link" onmousedown="showBBCode();">BB Code</span> -
        <span class="link" onmousedown="showHTMLCode();">HTML Code</span>
    </p>
</div>

<div id="view_contents"><!-- do not edit in here --></div>
<script type="text/javascript">
function setViewMode ( )
{
    switch ( getCookie ( 'uploader_view_mode' ) )
    {
        case 'details': showDetails(); break;
        case 'bbcode': showBBCode(); break;
        case 'htmlcode': showHTMLCode(); break;
        case 'gallery': default:
        {
            if ( is_gallery )
                showGallery();
            else
                showDetails();
        }
        break;
    }
}
addLoadEvent ( setViewMode );
</script>