<?php
// Do not edit within this section unless you know what you're doing
$page_title = isset ( $page_title ) ? $page_title : 'Uploader';
global $UPL, $USER;
$runtime = timer ( $UPL['RUNTIME'], 5 );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?=$page_title;?> - Uploader v6 (Admin)</title>
<style type="text/css" media="all">@import "templates/default/style.css";</style>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="templates/default/style_ie.css?v=<?=UPLOADER_VERSION?>" media="all">
<![endif]-->
<script type="text/javascript">
<!--
var base_url = "<?=$UPL['SETTINGS']['uploader_url']?>";
-->
</script>
<script type="text/javascript" charset="utf-8" src="templates/default/prototype.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default/common.js?v=<?=UPLOADER_VERSION?>"></script>
<script type="text/javascript" charset="utf-8" src="templates/default/javascript.js?v=<?=UPLOADER_VERSION?>"></script>
</head>
<body>
    <div id="container" style="width:750px">
        <div id="container2" style="width:732px">
			<div id="menu">
				<strong>Hello <?=$UPL['USER']['username']?></strong>
				<ul>
					<li><a href="admin.php?action=tools">Tools</a></li>
                    <li><a href="admin.php?action=users" style="font-weight:bold;">Users</a></li>
                    <li><a href="account.php?action=logout" onclick="return confirm('Confirm logout?');">Logout</a></li>
                    <li><a href="index.php">Uploader</a></li>
				</ul>
			</div>

			<div id="message">
				<div style="float:left;width:98%;" id="message_content"><!-- --></div>
				<div style="float:right;width:10px;text-align:right;"><img src="templates/default/images/close.gif" alt="" onclick="showIt('message',false);" style="cursor:pointer;" /></div>
				<div class="spacer"><!-- --></div>
			</div>

            <div id="content">
                <?=$content?>
            </div>

        </div>
    </div>
    <div id="credit">
        <!-- This script is copyrighted, please don't remove -->
        Uploaderv<?=UPLOADER_VERSION?> &copy; <a href="http://www.celerondude.com/page_php">www.celerondude.com</a><br />
        Generated in <?=$runtime?>
    </div>
</body>
</html>