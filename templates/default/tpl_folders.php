<?php
require_once('tpl_icons.php');
?>
<!-- tpl_folders.php -->

<?php /* Create a new folder */ if ( $action == 'create' ) : ?>
<form method="post" action="<?=MOD_REWRITE?'folders':'folders.php'?>?action=create">
    <input type="hidden" name="action" value="create" />
    <input type="hidden" name="task" value="create" />
    <input type="hidden" name="return_url" value="<?=$return_url?>" />

    <h1>New folder</h1>

    <?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>
    <table cellspacing="1" cellpadding="2" border="0" style="margin: 5px 0 0 -3px;">
        <tr>
            <td>Folder name</td>
            <td><input type="text" name="folder[name]" id="folder_name" value="<?=entities(@$folder['name'])?>" size="40" maxlength="50" /></td>
        </tr>
        <tr>
            <td>Description&nbsp;(optional)</td>
            <td><input type="text" name="folder[description]" value="<?=entities(@$folder['description'])?>" size="80" maxlength="255" /></td>
        </tr>
        <tr>
            <td class="tt">Folder properties</td>
            <td style="line-height:1.4em;">
                <input type="checkbox" name="folder[is_gallery]" id="folder_gallery" value="1" class="chkbox" <?=@$folder['is_gallery']?'checked="checked"':''?> />
                <label for="folder_gallery">Create this folder as a </label> <span class="help" onclick="help('This will set the folder as a picture album. You can only upload and move images (JPEG, PNG, and GIF) into this folder. If you wish to share the photos with your friends, you should also make the folder public.');">photo folder</span>.
            </td>
        </tr>
        <tr>
            <td class="tt">Access permission</td>
            <td>
                <ul>
                    <li>
                        <input type="radio" name="folder[access]" value="public" id="folder_access1" class="radio" <?=$folder['access']=='public'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access1">Public, anyone can access files in this folder.</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="hidden" id="folder_access3" class="radio" <?=$folder['access']=='hidden'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access3">Hidden, anyone can access files in this folder if they know the URL.</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="private" id="folder_access2" class="radio" <?=$folder['access']=='private'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', false);" />
                        <label for="folder_access2">Private, only my contacts below may access files in this folder.</label>
                        <fieldset style="border:0px;margin:2px 0 0 18px;padding:0px;" id="folder_private_permission">
                            <ul id="folder_private_access">
                                <li>
                                    <input type="checkbox" name="folder[friend_access]" value="1" id="folder_friend_access" class="chkbox" <?=$folder['friend_access']?'checked="checked"':''?> />
                                    <label class="colgreen" for="folder_friend_access">my friends</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="folder[family_access]" value="1" id="folder_family_access" class="chkbox" <?=$folder['family_access']?'checked="checked"':''?> />
                                    <label class="colfuscia" for="folder_family_access">my family</label>
                                </li>
                            </ul>
                        </fieldset>
                        <p>
                            If friends and family are both unchecked,
                            only <span class="colblue">you</span> can access files in this folder.
                        </p>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Create new folder" />
                <input type="button" value="Cancel" onclick="go('<?=$return_url?>');" />
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
<!--
$('folder_name').focus();
Form.disable('folder_private_permission', <?=(int)($folder['access']!='private')?>);
-->
</script>
<?php /* Rename */ elseif ( $action == 'edit' ) : ?>
<h1>Folder properties</h1>
<form method="post" action="<?=MOD_REWRITE?'folders':'folders.php'?>?action=edit&amp;folder_id=<?=$folder['folder_id']?>">
    <input type="hidden" name="action" value="edit" />
    <input type="hidden" name="task" value="edit" />
    <input type="hidden" name="folder_id" value="<?=$folder['folder_id']?>" />
    <input type="hidden" name="return_url" value="<?=$return_url?>" />

    <?php /* Errors will be printed here */ if ( isset ( $error ) ) print $error; ?>
    <table cellspacing="1" cellpadding="2" border="0" style="margin: 5px 0 0 -3px;">
        <tr>
            <td style="width:120px;">Folder name</td>
            <td><input type="text" name="folder[name]" id="folder_name" value="<?=entities(@$folder['name'])?>" size="40" maxlength="50" <?=!$folder['renameable']?'disabled="disabled"':''?> /></td>
        </tr>
        <tr>
            <td>Description&nbsp;(optional)</td>
            <td><input type="text" name="folder[description]" value="<?=entities(@$folder['description'])?>" size="80" maxlength="255" /></td>
        </tr>
        <tr>
            <td class="tt">Access permission</td>
            <td>
                <ul>
                    <li>
                        <input type="radio" name="folder[access]" value="public" id="folder_access1" class="radio" <?=$folder['access']=='public'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access1">Public, anyone can access files in this folder.</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="hidden" id="folder_access3" class="radio" <?=$folder['access']=='hidden'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', true);" />
                        <label for="folder_access3">Hidden, anyone can access files in this folder if they know the URL.</label>
                    </li>
                    <li>
                        <input type="radio" name="folder[access]" value="private" id="folder_access2" class="radio" <?=$folder['access']=='private'?'checked="checked"':''?> onclick="Form.disable('folder_private_permission', false);" />
                        <label for="folder_access2">Private, only my contacts below may access files in this folder.</label>
                        <fieldset style="border:0px;margin:2px 0 0 18px;padding:0px;" id="folder_private_permission">
                            <ul id="folder_private_access">
                                <li>
                                    <input type="checkbox" name="folder[friend_access]" value="1" id="folder_friend_access" class="chkbox" <?=$folder['friend_access']?'checked="checked"':''?> />
                                    <label class="colgreen" for="folder_friend_access">my friends</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="folder[family_access]" value="1" id="folder_family_access" class="chkbox" <?=$folder['family_access']?'checked="checked"':''?> />
                                    <label class="colfuscia" for="folder_family_access">my family</label>
                                </li>
                            </ul>
                        </fieldset>
                        <p>
                            If friends and family are both unchecked,
                            only <span class="colblue">you</span> can access files in this folder.
                        </p>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Save changes" />
                <input type="button" value="Cancel" onclick="go('<?=$return_url?>');" />
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
<!--
$('folder_name').focus();
Form.disable('folder_private_permission', <?=(int)($folder['access']!='private')?>);
-->
</script>
<?php elseif ( $action == 'delete' ) : ?>
<form method="post" action="<?=UPLOADER_URL?>folders.php">
    <input type="hidden" name="action" value="delete" />
    <input type="hidden" name="task" value="confirm" />
    <input type="hidden" name="folder_id" value="<?=$folder['folder_id']?>" />

    <p>
        Are you sure you want to delete <strong class="colblue"><?=entities($folder['folder_name'])?></strong>?
        <br /><br />
        <input type="radio" name="delete_mode" value="delete" id="delete_mode_delete" checked="checked" class="radio" />
        <label for="delete_mode_delete">Delete this folder and all files inside this folder.</label>
        <br />
        <input type="radio" name="delete_mode" value="move" id="delete_mode_move" class="radio"/>
        <label for="delete_mode_move">Delete just this folder and move all files inside this folder to My Documents.</label>
        <br /><br />
        <input type="submit" value="Confirm Delete" />
        <input type="button" value="Cancel" onclick="go('<?=UPLOADER_URL.(MOD_REWRITE?'folders':'folders.php')?>');" />
    </p>
</form>

<?php /* No action, just list folders */ else: ?>
<p class="rounded green">
    You have <?=count($user_folders)?> folders.
    <img src="templates/default/images/icon_folder_create.gif" alt="new folder icon" class="img1" />&nbsp;
    <a href="<?=UPLOADER_URL.(MOD_REWRITE?'folders?action=create':'folders.php?action=create')?>" class="special">New folder</a>
</p>

<table class="rowlines" id="tbl1" style="width:99%;margin:0px auto 0px auto;color:#606060;" cellpadding="3" cellspacing="0" border="0">
    <tr skip_alternate="1">
        <td></td>
        <td style="width:50px;"></td>
        <td style="width:55px;"></td>
        <td style="width:75px;"></td>
    </tr>
    <?php /* Loop through folders and list them */ reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : ?>
    <tr>
        <td style="line-height:1.5em;">
            <img src="<?=tpl_get_folder_icon($folder['permission'])?>" class="img1" alt="" style="cursor:pointer;" onmousedown="togFolderPerm(this,'<?=$folder['tog_perm_url']?>');" />
            &nbsp;<a href="<?=$folder['url']?>" <?=$folder['folder_isgallery']?'class="special_green"':'class="special"'?>><?=entities($folder['folder_name'])?></a>
            <span id="folder_description_<?=$i?>" style="padding:2px;"><?=$folder['folder_description']==''?'No description':entities(str_preview($folder['folder_description'],55));?></span>
        </td>
        <td class="ct"><?=$folder['files_count'] ? $folder['files_count'] . ' files' : 'empty' ?></td>
        <td class="ct"><?=$folder['size']  ? get_size ( $folder['size'], 'B', 0 ) : 'empty' ?></td>
        <td class="cr">
            <?php if ( $folder['folder_deleteable'] ) : ?><a href="<?=$folder['delete_url']?>" title="Delete this folder"><img src="templates/default/images/b_delete.gif" alt="" /></a><?php endif; ?>
            <a href="<?=$folder['edit_url']?>" title="Edit this folder's properties"><img src="templates/default/images/b_edit.gif" alt="" /></a>
        </td>
    </tr>
    <?php endwhile; ?>
</table>

<p style="padding:5px 0px 0px 3px;color:#505050;">
    Click on the folder icons to quickly toggle folder permissions.
    (<img src="templates/default/images/icon_folder_public.gif" alt="Public" class="img1"/> Public,
	<img src="templates/default/images/icon_folder_hidden.gif" alt="Hidden" class="img1"/> Hidden,
    <img src="templates/default/images/icon_folder_private.gif" alt="Private" class="img1"/> Private)
</p>

<script type="text/javascript" charset="utf-8">
<!--
addLoadEvent(function()
{
    alternateRowColor($('tbl1'),'tr','#fafafa','#ffffff');
    if(Ajax.enabled)
    {
        <?php reset ( $user_folders ); while ( list ( $i, $folder ) = each ( $user_folders ) ) : ?>
        new quickEditFolderDescription ( <?=$folder['folder_id']?>, '<?=addslashes($folder['folder_description'])?>', 'folder_description_<?=$i?>', 55 );
        <?php endwhile; ?>
    }
});
-->
</script>
<?php /* For action */  endif; ?>