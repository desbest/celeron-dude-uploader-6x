<!-- tpl_user.php -->
<?php
require_once ( 'tpl_icons.php' );
global $UPL, $USER;
?>
<?php /* Show user info */ if ( $action == 'info' ) : ?>
<div class="rounded green">
    <h1>User info for <?=$user['username']?></h1>
    <?php if ( $user['relationship']['is_contact'] ) : ?>
    <p>
        <strong><?=$user['username']?></strong> is on your contact list
        <?php if($user['relationship']['is_friend']):?>as a <span class="colgreen">friend</span><?php endif;?>
        <?php if($user['relationship']['is_family']):?><?=$user['relationship']['is_friend']?'and as':'as'?> <span class="colfuscia">family</span><?php endif;?>
        (<a href="<?=$user['edit_contact_url']?>" class="special">Change</a>)
    </p>
    <?php endif;?>
    <?php if ( $user['reverse_relationship']['is_contact'] ) : ?>
    <p>
        You are on <strong><?=$user['username']?></strong>'s contact list
        <?php if($user['reverse_relationship']['is_friend']):?>as a <span class="colgreen">friend</span><?php endif;?>
        <?php if($user['reverse_relationship']['is_family']):?><?=$user['reverse_relationship']['is_friend']?'and as':'as'?> <span class="colfuscia">family</span><?php endif;?>
    </p>
    <?php endif; ?>
    <p>
        <a href="<?=$user['browse_url']?>" class="special"><?=$user['username']?>'s shared files</a>
        <?php if ( $user['pref_accepts_pm'] ) : ?>
        - <a href="<?=$user['message_url']?>" class="special">Send a message to <?=$user['username']?></a>
        <?php endif; ?>
        <?php /* If user is logged AND user is not browsing his/her own profile */ if ( !$user['relationship']['is_contact'] &&  $UPL['USER']['logged_in'] && $USER['userid'] != $user['userid'] ) : ?>
        - <a href="<?=$user['add_contact_url']?>" class="special">Add <?=$user['username']?> to your contact list</a>
        <?php endif; ?>
    </p>
</div>

<table id="info_tbl" cellspacing="0" cellpadding="6" border="0" style="margin-top:10px;">
    <tr>
        <td>Join date</td>
        <td><?=$user['reg_date']?> (<?=ceil($user['reg_date_ago'])?> days ago)</td>
    </tr>
    <tr>
        <td>Last login</td>
        <td><?=$user['last_login']?> (<?=ceil($user['last_login_ago'])?> days ago)</td>
    </tr>
    <?php /* show email only if user allows it */ if ( $user['pref_show_email'] ) : ?>
    <tr>
        <td>Email address</td>
        <td><?=ascii_str($user['email'])?></td>
    </tr>
    <?php /* endif for show email */ endif ; ?>
</table>
<p style="padding:5px">
    <input type="button" value="&nbsp; OK &nbsp;" onclick="go('<?=UPLOADER_URL.(MOD_REWRITE?'members':'browse.php')?>');" />
</p>
<?php endif; ?>





<?php
/* Browse user files */
if ( $action == 'list_folders' ) :
include_once ( 'tpl_icons.php' ); // required
?>
<div style="background-color:#F5FFE6;border-bottom: 1px solid #DCE6CF; padding: 10px 7px 10px 10px; margin: 0px 0px 5px 0px; -moz-border-radius:6px;">
    <h1><?=ucfirst($user['username'])?>'s shared folders</h1>
    <p>
        <img src="templates/default/images/icon_user_gray.gif" alt="userinfo icon" class="img1" /> <a href="<?=$user['info_url']?>" class="special">View <?=$user['username']?>'s info</a> -
        <img src="templates/default/images/icon_sendpm.gif" alt="sendpm icon" class="img1" /> <a href="<?=$user['message_url']?>" class="special">Send a message to <?=$user['username']?></a>
    </p>
</div>

<div id="shared_folders_list" style="margin:5px;">
    <ul id="user_folders_tbl">
        <?php reset ( $public_folders ); while ( list ( , $folder ) = each ( $public_folders ) ) : ?>
        <li style="padding:5px;">
            <img src="<?=tpl_get_folder_icon($folder['permission'])?>" alt="" class="img1" />
            <a href="<?=$folder['browse_url']?>" <?=$folder['folder_isgallery']?'class="special_green"':'class="special"'?>><?=entities($folder['folder_name'])?></a>
            <span style="color:#808080;">(<?=$folder['files_count']?$folder['files_count'].' files':'Empty'?>)</span>
            <span style="color:#909090;"><?=$folder['folder_description']!=''?' - '.entities(str_preview($folder['folder_description'],100)):''?></span>
        </li>
        <?php endwhile; ?>
    </ul>
</div>
<script type="text/javascript">
<!--
alternateRowColor(getObj('user_folders_tbl'),'li','#fafafa','#fefefe');
-->
</script>
<?php /* End action */ endif; ?>





<?php
/* Browse user files */
if ( $action == 'browse' ) : include_once ( 'tpl_icons.php' ); // required ?>
<div class="rounded green" style="padding:5px 10px 5px 10px;line-height:1.5em;">
	<p>
		<img src="<?=tpl_get_folder_icon($current_folder['permission'])?>" alt="folder icon" class="img1" />
		<span class="head1"><?=entities($current_folder['folder_name'])?></span>
		<br />
		<?php if ( $current_folder['folder_description'] != '' ) : ?>
		<span><?=entities($current_folder['folder_description'])?></span>
		<?php endif; ?>
	</p>
	<p>
		This folder belongs to <a href="<?=$user['info_url']?>" class="special"><?=entities($user['username'])?></a> <span style="color:#aaa">(<a href="<?=$user['browse_url']?>" class="special">View all</a>)</span>
	</p>

    <?php if ( count ( $user_files ) ) : ?>
    <p style="margin:0;">
        <strong class="s2">View mode:</strong>
        <?php if ( $current_folder['folder_isgallery'] ) : ?>
        <span class="link" onmousedown="showGallery();">Gallery</span> -
        <?php endif; ?>
        <span class="link" onmousedown="showDetails();">Details</span> -
        <span class="link" onmousedown="showBBCode();">BB Code</span> -
        <span class="link" onmousedown="showHTMLCode();">HTML Code</span> -
		<a href="<?=$current_folder['slideshow_url']?>" class="special">Slideshow</a>
    </p>
	<?php else: ?>
	<p>This folder is empty.</p>
    <?php endif; ?>
</div>

<?php if ( count ( $user_files ) ) : ?>
<div class="rounded gray" style="margin-top:10px;color:#606060;padding:6px 10px 6px 10px;">
    <?php if ( $current_folder['files_count'] ) : ?>
    <span style="float:left;">
        <img src="templates/default/images/folder_<?=$current_folder['folder_isgallery']?'image.gif':'regular.gif'?>" alt="" class="img1" />
        There are <?=$current_folder['files_count']?> <?=$current_folder['folder_isgallery']?'images':'files'?> in this folder.
    </span>
    <span style="float:right;padding-top:3px;">
        <?php if ( $current_page > 1 ) : ?><a href="<?=$prev_page_url?>" class="special">Previous</a><?php endif; ?>
        (<strong><?=$file_start?></strong> - <strong><?=$file_end?></strong> of <strong><?=$current_folder['files_count']?></strong>)
        <?php if ( $current_page < $total_pages ) : ?><a href="<?=$next_page_url?>" class="special">Next</a><?php endif; ?>
    </span>
    <div class="spacer"></div>
    <?php else: ?>
        <img src="templates/default/images/folder_<?=$current_folder['folder_isgallery']?'image.gif':'regular.gif'?>" alt="" class="img1" />
        This folder is empty. <a href="<?=$current_folder['upload_url']?>" class="special">Upload files into this folder</a>
    <?php endif; ?>
</div>

<script type="text/javascript" src="templates/default/public_view.js"></script>
<script type="text/javascript">
/* Number of thumbnails per row. Decrease this number if you increase the thumbnail width. */
var per_row = 6;
var show_views = 1;
var show_date = 1;
var show_header = 1;
var is_gallery = <?=$current_folder['folder_isgallery']?1:0?>;
var files = [];
<?php while ( list ( , $file ) = each ( $user_files ) ) : ?>
addFile ( '<?=addslashes($file['file_name'])?>', '<?=get_size($file['file_size'],'B',0)?>', '<?=get_date('M-d-y',$file['file_date'])?>', <?=(int)$file['file_views']?>, '<?=addslashes($file['url'])?>', <?=(bool)$file['file_isimage']?'true':'false'?>, '<?=$file['square_thumb_url']?>', '<?=$file['small_thumb_url']?>' );
<?php endwhile; ?>
</script>
<div id="view_contents"><!-- do not edit in here --></div>
<script type="text/javascript">
<!--
function setViewMode ( )
{
    if ( is_gallery )
    {
        showGallery();
        return;
    }
    switch ( getCookie ( 'uploader_view_mode' ) )
    {
        case 'details': showDetails(); break;
        case 'bbcode': showBBCode(); break;
        case 'htmlcode': showHTMLCode(); break;
        case 'gallery': default:
        {
            if ( is_gallery )
                showGallery();
            else
                showDetails();
        }
        break;
    }
}
addLoadEvent ( setViewMode );
-->
</script>
<?php /* end folder empty */ endif; ?>

<script type="text/javascript">
<!--
alternateRowColor(getObj('user_folders_tbl'),'li','#fafafa','#fefefe');
alternateRowColor(getObj('user_files_tbl'),'tr','#fafafa','#ffffff');
-->
</script>
<?php /* End action */ endif; ?>