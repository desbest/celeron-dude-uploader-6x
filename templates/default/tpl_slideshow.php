<link rel="stylesheet" type="text/css" href="templates/default/slideshow.css?v=<?=UPLOADER_VERSION?>" media="all">
<script type="text/javascript" charset="utf-8" src="templates/default/slideshow.js?v=<?=UPLOADER_VERSION?>"></script>
<div id="upl_slideshow" style="margin-top:10px">
	<div id="nav_container">
		<div style="float:left;width:50%"><a href="<?=isset($back_url)?$back_url:UPLOADER_URL?>">Back</a></div>
		<div style="float:right;text-align:right;width:40%">
			<div style="float:left">
				<img id="play_button" src="templates/default/images/slideshow_play.gif" alt="Play" class="img1" />
				<img id="pause_button" src="templates/default/images/slideshow_pause.gif" alt="Pause" class="img1" style="display:none" />
				<img id="prev_button" src="templates/default/images/slideshow_prev.gif" alt="Previous" class="img1" />
				<img id="next_button" src="templates/default/images/slideshow_next.gif" alt="Next" class="img1" />
			</div>
			<div style="float:left;width:175px;">
				<div id="sliderTrack" style="float:left;width:100px;height:10px;background-image:url('templates/default/images/slider_track.gif');margin-left:10px;margin-top:3px;">
					<div id="sliderHandle" style="position:absolute;width:6px;height:12px;background-image:url('templates/default/images/slider_bar.gif');background-repeat:no-repeat"><!-- --></div>
				</div>
				<div id="delayValue" style="float:right;color:white;margin:3px 0 0 0;text-align:left;">1 sec</div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
	</div>
	<div id="image_container">
		<div id="image_container_left">&nbsp;</div>
		<div id="image_container_right"></div>
	</div>
	<div id="thumb_container"><div class="spacer"></div></div>
</div>

<script type="text/javascript" charset="utf-8">
<!--
var g = new Slideshow('nav_container', 'image_container', 'image_container_left', 'image_container_right', 'thumb_container', 'play_button', 'pause_button', 'prev_button', 'next_button');
<?php reset($images); while(list(,$image) = each($images)): ?>
g.addImage(<?=intval($image['file_id'])?>, '<?=addslashes(entities($image['file_name']))?>', '<?=addslashes($image['url'])?>', '<?=addslashes($image['square_thumb_url'])?>', '<?=addslashes($image['large_thumb_url'])?>');
<?php endwhile; ?>
function changeDelay ( percentage )
{
	var delayValue = $('delayValue');
	var seconds = percentage * 10;
	seconds = Math.round(seconds*10)/10;
	if(seconds < 1)seconds = 1;
	g.setOptions({playDelay:seconds*1000});
	delayValue.innerHTML = seconds + '&nbsp; secs';
}

function loadSlideshow ( )
{
	var slider = new Slider('sliderTrack', 'sliderHandle');
	slider.onslide = changeDelay;
	slider.onchange = changeDelay;

	changeDelay(0.3);
	slider.setValue(0.3);
	g.display();
}
addLoadEvent(loadSlideshow);
-->
</script>