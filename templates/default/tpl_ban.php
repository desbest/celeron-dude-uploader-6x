<?php if ( !isset ( $action ) ) $action = ''; ?>

<?php if ( $action == 'ban_edit' ) : ?>
<div style="margin-top:10px;">
    <form method="post" action="admin.php?action=ban_edit">
        <input type="hidden" name="action" value="ban_edit" />
        <input type="hidden" name="task" value="save" />
        <input type="hidden" name="ip" value="<?=$ban['ban_ip']?>" />
        <h1>Edit banning option</h1>
        <table cellspacing="4">
            <tr>
                <td>IP address</td>
                <td><?=$ban['real_ip']?></td>
            </tr>
            <tr>
                <td class="tt">Ban options</td>
                <td>
                    <input type="checkbox" name="ban_uploader" id="ban_uploader" value="1" class="chkbox" <?=$ban['ban_uploader']?'checked="checked"':''?> />
                    <label for="ban_uploader">Ban user from registering</label><br />
                    <input type="checkbox" name="ban_public" id="ban_public" value="1" class="chkbox" <?=$ban['ban_public']?'checked="checked"':''?> />
                    <label for="ban_public">Ban user from uploading files to the Public Uploader</label>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Save changes" /> <input type="button" onclick="go('admin.php?action=ban');" value="Cancel" /></td>
            </tr>
        </table>
    </form>
</div>
<?php else: ?>
<div class="rounded green">
    <form method="post" action="admin.php?action=ban_add">
        <h1>Add a new IP address</h1>
        <table cellspacing="4">
            <tr>
                <td>IP address</td>
                <td><input type="text" name="ban_ip" value="" size="15" maxlength="15" style="text-align:center" autocomplete="off" /> &nbsp;Ex: 192.168.0.1</td>
            </tr>
            <tr>
                <td class="tt">Ban options</td>
                <td>
                    <input type="checkbox" name="ban_uploader" id="ban_uploader" value="1" class="chkbox" checked="checked" />
                    <label for="ban_uploader">Ban user from registering</label><br />
                    <input type="checkbox" name="ban_public" id="ban_public" value="1" class="chkbox" checked="checked" />
                    <label for="ban_public">Ban user from uploading files to the Public Uploader</label>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Add IP to ban list" /></td>
            </tr>
        </table>
    </form>
</div>

<?php if ( count ( $bans ) ) : ?>
<table id="ban_tbl" class="rowlines" cellspacing="0" cellpadding="4" style="width:100%;margin-top:10px">
    <tr class="header" skip_alternate="skip_alternate">
        <td>IP Address</td>
        <td class="ct">Register</td>
        <td class="ct">Public Uploader</td>
        <td class="ct">Actions</td>
    </tr>
    <?php reset ( $bans ); while ( list ( , $ban ) = each ( $bans ) ) : ?>
    <tr>
        <td><?=$ban['ban_ip']?></td>
        <td class="ct"><?=$ban['ban_uploader']?'Banned':'&nbsp;'?></td>
        <td class="ct"><?=$ban['ban_public']?'Banned':'&nbsp;'?></td>
        <td class="ct">
            <a href="<?=$ban['unban_url']?>" class="special" onclick="return confirm('Confirm unban on <?=$ban['ban_ip']?>?');">Unban</a> -
            <a href="<?=$ban['edit_url']?>" class="special">Edit</a>
        </td>
    </tr>
    <?php endwhile; ?>
</table>
<script type="text/javascript">
alternateRowColor($('ban_tbl'), 'tr', '#fff', '#fafafa');
</script>
<?php else: ?>
<p>No banned IPs</p>
<?php endif; ?>
<?php endif; ?>