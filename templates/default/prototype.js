/*  Prototype JavaScript framework, version 1.4.0
 *  (c) 2005 Sam Stephenson <sam@conio.net>
 *
 *  Prototype is freely distributable under the terms of an MIT-style license.
 *  For details, see the Prototype web site: http://prototype.conio.net/
 *
 *  Stripped down and modifed by Tuan Do
/*--------------------------------------------------------------------------*/
var userAgent = navigator.userAgent.toLowerCase();
var is_ie = document.all;
var is_opera = userAgent.indexOf('opera') > -1;

function $(){var els=new Array();for(var i=0;i<arguments.length;++i){var el=arguments[i];if(typeof el=='string')el=document.getElementById(el);if(arguments.length==1)return el;els.push(el);}return els;}
function $A(arr){if(!arr)return [];if(arr.toArray){return arr.toArray();}else{var results=[];for(var i=0;i<arr.length;++i)results.push(arr[i]);return results;}}
var Class={create:function(){return function(){this.initialize.apply(this,arguments);}}}
var Try = {these:function(){var retval;for(var i=0;i<arguments.length;++i){var lambda=arguments[i];try{retval=lambda();break;}catch(e){}}return retval;}}
Object.extend=function(dest,src){for(p in src){dest[p] = src[p];}return dest;}
Object.prototype.shift = function(){var result=this[0];this.length--;for(var i=0;i<this.length;++i)this[i]=this[i+1];return result;}
Function.prototype.bind = function(){var __method=this,args=$A(arguments),object=args.shift();return function(){return __method.apply(object,args.concat($A(arguments)));}}
Function.prototype.bindAsEventListener = function(object){var __method=this;return function(event){return __method.call(object,event||window.event);}}

Object.extend(String.prototype,
{
	escapeHTML: function()
	{
		var div = document.createElement('div');
		var text = document.createTextNode(this);
		div.appendChild(text);
		return div.innerHTML;
	},

	getTextWidth: function()
	{
		var span = document.createElement('span');
		Object.extend(span.style, {visibiliy:'hidden', position:'absolute', left:'0px', top:'0px'});
		span.innerHTML = this;
		document.body.appendChild(span);
		var width = span.offsetWidth;
		document.body.removeChild(span);
		return width;
	}
});

var Ajax = {
	events: ['uninitialized', 'loading', 'loaded', 'interactive', 'complete'],
	enabled: ( typeof XMLHttpRequest != 'undefined' || typeof ActiveXObject != 'undefined' ),
	getXMLHttpObject: function ( ) { return Try.these ( function() {return new ActiveXObject('Msxml2.XMLHTTP')}, function() {return new ActiveXObject('Microsoft.XMLHTTP')}, function() {return new XMLHttpRequest()} ) || false; }
};

AjaxRequest = Class.create();
AjaxRequest.prototype =
{
	initialize: function ( url, options )
	{
		this.options = Object.extend ( { method:'post', async:true, postData:{}, response:'json' }, options || { } );
		this.onloading = null;
		this.onloaded = null;
		this.oncomplete = null;
		this.response = null;
		this.xmlhttp = Ajax.getXMLHttpObject();
		this.url = url;
	},

	setOptions: function ( options )
	{
		this.options = Object.extend ( this.options, options || {} );
	},

	request: function ( url )
	{
		this.url = url || this.url;
		this.xmlhttp.open ( this.options.method, this.url + '&ajax=' + this.getRand(), this.options.async );
		if ( this.options.async )
			this.xmlhttp.onreadystatechange = this.stateChange.bind(this);
		if ( this.options.method == 'post' )
			this.xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded; charset=utf-8')
		this.xmlhttp.send ( this.options.method == 'post' ? this.options.postData.toQueryString() : null );
	},

	stateChange: function ( )
	{
		var state = Ajax.events[this.xmlhttp.readyState];

		if ( state == 'loading' )
			(this.onloading || function(){}).bind(this)();
		if ( state == 'loaded' )
			(this.onloaded || function(){}).bind(this)();
		if ( state == 'complete' && this.xmlhttp.status == 200 )
		{
			this.response = this.getResponse();
			(this.oncomplete || function(){}).bind(this)();
			this.xmlhttp.onreadystatechange = function(){};
		}
	},

	getResponse: function (type)
	{
		var type = type || this.options.response;
		if ( type == 'json' )
		{
			eval ( 'var response = ' + this.xmlhttp.responseText );
			return response;
		}
		if ( type == 'xml' ) return this.xmlhttp.responseXML;
		return this.xmlhttp.responseText;
	},

	getRand: function ( )
	{
		var rand = '';
		for ( var i = 0; i < 5; ++i )
			rand += String(Math.floor(Math.random()*10000000000));
		return rand;
	}
};


Element = new Object();
Object.extend(Element,
{
	setOpacity: function(element, opacity)
	{
		//if ( is_opera ) return false;
		element = $(element);
		if(!element) return false;
		if(element.filters)
		{
			try { element.filters.alpha.opacity = opacity; }
			catch (error) { element.style.filter = "alpha(opacity=" + opacity + ")"; }
		}
		element.style.opacity = opacity / 100;
		element.style.MozOpacity = opacity / 100;
		element.style.visibility = opacity == 0 ? 'hidden' : 'visible';
		if(is_opera)return true;
	},

	getOffset: function(element, absolutePositioned)
	{
		absolutePositioned = absolutePositioned || false;
		element = $(element);
		if(!element) return false;
		var retval = {offsetWidth:element.offsetWidth, offsetHeight:element.offsetHeight, offsetLeft: 0, offsetTop: 0};

		if(false && element.getBoundingClientRect)
		{
			var info = element.getBoundingClientRect();
			retval.offsetLeft += info['left'] - 2;
			retval.offsetTop += info['top'] - 2;
		}
		else
		{
			do
			{
				retval.offsetLeft += element.offsetLeft || 0;
				retval.offsetTop += element.offsetTop || 0;
				element = element.offsetParent;
			}
			while ( element && !absolutePositioned );
		}
		retval.offsetRight = retval.offsetLeft + retval.offsetWidth;
		retval.offsetBottom = retval.offsetTop + retval.offsetHeight;
		return retval;
	},

	fade: function(element, from, to, step)
	{
		element = $(element);
		if(!element) return false;
		if(from == null)from = 100;
		step = step || 10;
		if(from == to )
		{
			if(typeof element.onfadefinish == 'function') element.onfadefinish();
			return true;
		}
		from = Element.__getNextStepValue(from, to, step);
		Element.setOpacity(element, from);
		window.setTimeout(Element.fade.bind(this, element, from, to, step), 10);
	},

	__getNextStepValue: function(from, to, step)
	{
		if(from > to) from -= (from - to) > step ? step : from - to;
		else from += (to - from) > step ? step : to - from;
		return from;
	}
});





var Drag = Class.create();
Drag.prototype = {
	initialize: function(element, options)
	{
		this.dragObj = $(element);
		if ( !this.dragObj ) return false;

		this.options = { zIndex: 1, horizontal: true, vertical: true, leftBound: 0, rightBound: -1, topBound: 0, bottomBound: -1 };
		this.setOptions ( options || this.options );
		this.lastMousePos = [0, 0]; // [x, y]

		this.mouseMove = this.mouseMoveListener.bindAsEventListener(this);
		this.mouseDown = this.startDrag.bindAsEventListener(this);
		this.mouseUp = this.stopDrag.bindAsEventListener(this) ;

		this.ondrag = null;
		this.ondragstart = null;
		this.ondragend = null;

		addEvent ( this.dragObj, 'mousedown', this.mouseDown );
		addEvent ( this.dragObj, 'mouseup', this.mouseUp );
	},

	setBoundary: function(top, right, bottom, left)
	{
		this.setOptions({leftBound:left, rightBound:right, topBound:top, bottomBound:bottom});
	},

	setBoundingObject: function(element)
	{
		element = $(element);
		if(!element) return false;
		var offset = Element.getOffset(element);
		this.setBoundary(offset.offsetTop, offset.offsetRight, offset.offsetBottom, offset.offsetLeft);
	},

	setOptions: function(options)
	{
		Object.extend(this.options, options || {});
	},

	startDrag: function(event)
	{
		Object.extend ( this.dragObj.style, { zIndex: this.options.zIndex + 1 } );
		addEvent ( document, 'mousemove', this.mouseMove );
		addEvent ( document, 'mouseup', this.mouseUp );
		document.onselectstart = function ( ) { return false; };

		var offset = Element.getOffset ( this.dragObj, true );
		var mousePosX = ( event.pageX || event.pageY ) ? event.pageX : event.clientX + document.body.scrollLeft;
		var mousePosY = ( event.pageY || event.pageY ) ? event.pageY : event.clientY + document.body.scrollTop;
		this.relativeX = mousePosX - offset.offsetLeft;
		this.relativeY = mousePosY - offset.offsetTop;
		this.dragObjOffset = offset;
		if ( typeof this.ondragstart == 'function' )
			this.ondragstart ( Element.getOffset ( this.dragObj, true ) );
		return false;
	},

	stopDrag: function(event)
	{
		Object.extend ( this.dragObj.style, { zIndex: this.options.zIndex } );
		removeEvent ( document, 'mousemove', this.mouseMove );
		removeEvent ( document, 'mouseup', this.mouseUp );
		document.onselectstart = function ( ) { return true; };

		if ( typeof this.ondragend == 'function' )
			this.ondragend ( Element.getOffset ( this.dragObj, true ) );
	},

	mouseMoveListener: function(event)
	{
		var mousePosX = ( event.pageX || event.pageY ) ? event.pageX : event.clientX + document.body.scrollLeft;
		var mousePosY = ( event.pageY || event.pageY ) ? event.pageY : event.clientY + document.body.scrollTop;

		if ( this.lastMousePos[0] == mousePosX && this.lastMousePos[1] == mousePosY )
			return;

		var posLeft 	= mousePosX - this.relativeX;
		var posRight 	= posLeft + this.dragObjOffset.offsetWidth;
		var posTop 		= mousePosY - this.relativeY;
		var posBottom 	= posTop + this.dragObjOffset.offsetHeight;

		// boundary constraints
		if ( posLeft < this.options.leftBound ) posLeft = this.options.leftBound;
		else if ( this.options.rightBound > -1 && ( posRight > this.options.rightBound ) ) posLeft = ( this.options.rightBound  - this.dragObjOffset.offsetWidth );

		if ( posTop < this.options.topBound ) posTop = this.options.topBound;
		else if ( this.options.bottomBound > -1 && ( posBottom > this.options.bottomBound ) ) posTop = ( this.options.bottomBound - this.dragObjOffset.offsetHeight );

		// direction constrains
		if ( this.options.horizontal )
			this.dragObj.style.left = posLeft + 'px';

		if ( this.options.vertical )
			this.dragObj.style.top = posTop + 'px';

		// update drag progress
		if ( typeof this.ondrag == 'function' )
			this.ondrag ( Element.getOffset ( this.dragObj, true ) );

		this.lastMousePos = [mousePosX, mousePosY];
	}
};


var Slider = Class.create();
Slider.prototype = {
	initialize: function(track, handle, options)
	{
		this.track = $(track);
		if(!this.track)return false;
		this.handle = $(handle);
		if(!this.handle) return false;

		this.options = { barBackgroundColor: '#aaa' };
		Object.extend(this.options, options || {});

		this.onslide = null;
		this.onchange = null;

		this.dragger = new Drag(this.handle);
		this.dragger.setOptions({vertical:false});
		this.dragger.setBoundingObject(this.track);
		this.dragger.ondrag = this.startSlide.bind(this);
		this.dragger.ondragend = this.endSlide.bind(this);

		this.trackOffset = Element.getOffset ( this.track );
	},

	getValue: function(barOffset)
	{
		var barPosition = barOffset.offsetLeft - this.trackOffset.offsetLeft;
		return barPosition / ( this.trackOffset.offsetWidth - this.handle.offsetWidth );
	},

	setValue: function(percentage)
	{
		var barPosition = (percentage * this.trackOffset.offsetWidth) + this.trackOffset.offsetLeft;
		this.handle.style.left = barPosition + 'px';
	},

	endSlide: function(barOffset)
	{
		var scaleOffset = Element.getOffset ( this.track );
		var barPosition = barOffset.offsetLeft - scaleOffset.offsetLeft;
		if( typeof this.onchange == 'function' )
			this.onchange(this.getValue(barOffset));
	},

	startSlide: function(barOffset)
	{
		if ( typeof this.onslide == 'function' ) this.onslide ( this.getValue(barOffset) );
	}
};

var Form = {
	disable: function(element, disabled)
	{
		element = $(element);
		if(!element) return false;
		var tagName = element.tagName.toLowerCase();

		if(tagName == 'form' || tagName == 'input' || tagName == 'textarea' || (tagName == 'fieldset' && document.all))
		{
			element.disabled = disabled;
			return true;
		}
		if(typeof disabled == 'undefined') disabled = true;
		var inputs = element.getElementsByTagName('input');
		for(var i = 0; i < inputs.length; ++i)
			inputs[i].disabled = disabled;
		var inputs = element.getElementsByTagName('textarea');
		for(var i = 0; i < inputs.length; ++i)
			inputs[i].disabled = disabled;
	}
};