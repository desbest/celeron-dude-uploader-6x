<?php
set_time_limit(0);
define ( 'NO_AUTH_CHECK', 1 );
require_once 'includes/commons.inc.php';

if ( !is_file ( 'includes/image.class.php' ) )
	exit ( 'Woops, you forgot to upload image.class.php (in the "includes" folder) from the new version to the "includes" folder of the current version.' );
require_once 'includes/image.class.php';

// EDIT HERE

$UPL['CONFIGS'] = array_merge ( $UPL['CONFIGS'], array
(
	// Large thumb
	'LARGE_THUMB'		=> '350x500',

	// small thumb
	'SMALL_THUMB'		=> '80x120',

	// square thumb
	'SQUARE_THUMB'		=> '70x70',

	// image info (dimension - size)
	'INFO_SMALL_THUMB'		=> 0,

	// max image dimension
	'MAX_IMAGE_DIMENSION'	=> '5000x5000',
) );

// END EDIT


$go = 0;

// old library, gotta do this
$mysqlDB2 = new mysqlDB($host,$username,$password,$database,defined('NO_PERSISTENT')?0:$persistent);

if ( !isset ( $_GET['start_update'] ) )
{
	print <<<HTML
<h1>Update script for version 6.2.1 to version 6.3</h1>
<h3>You must be running version 6.2.1. Click continue if you are.</h3>
<p><a href="update.php?start_update=true">Continue</a></p>
HTML;
	exit;
}

print "<h1>Starting update process, this may take a while.....</h1>";

if ( is_dir ( 'files' ) )
{
	print 'The user files directory "files" still exists. You must rename it to another name, say "ufiles", and edit the settings to reflect this change. Please see the update instructions for an explaination.';
	exit;
}

if ( is_dir ( 'public' ) )
{
	print 'The public files directory "public" still exists. You must rename it to another name, say "pfiles", and edit the settings to reflect this change. Please see the update instructions for an explaination.';
	exit;
}


// Update public uploader
$db = new DB;
if ( !$db->open ( PUBLIC_SETTINGS ) ) exit ( 'Unable to load public settings ' . PUBLIC_SETTINGS );
$pub_settings = $db->all();
$db->close();
$pub_dir = $pub_settings['public_files_dir'];

if ( !$mysqlDB2->query ( "ALTER TABLE uploader_pfiles ADD file_location VARCHAR(255) NOT NULL" ) )
	exit ( $mysqlDB2->error ( __LINE__, __FILE__ ) );

print "<h3>Updating the public uploader...</h3>";
$mysqlDB->query ( "SELECT COUNT(*) AS files_count FROM uploader_pfiles" );
$result = $mysqlDB->getAssoc();
print "Found " . $result['files_count'] . " public files. Processing them: <br />";
ob_flush();flush();
$count = 0;
while ( 1 )
{
	$mysqlDB->query ( "SELECT * FROM uploader_pfiles LIMIT $count, 3" );
	if ( !$mysqlDB->getRowCount() ) break;
	while ( false !== ( $file = $mysqlDB->getAssoc() ) )
	{
		print ($count+1) . ', ';
		ob_flush();flush();
		$newname = uniqid('',true);
		rename ( $pub_dir . '/' . $file['file_rname'], $pub_dir . '/' . $newname );
		$file_location = realpath ( $pub_dir . '/' . $newname );
		if ( !$mysqlDB2->query ( "UPDATE uploader_pfiles SET file_location='" . $mysqlDB->escape ( $file_location ) . "' WHERE file_id={$file['file_id']} LIMIT 1" ) )
			exit ( $mysqlDB2->error ( __LINE__, __FILE__ ) );

		// delete old thumbnails
		unlink ( $pub_dir . '/' . 'thumb_' . $file['file_rname'] );

		// create new thumbnails
		$image = new image ( $file_location );
		$image_info = getimagesize ( $file_location );

		// large version
		list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['LARGE_THUMB'] );
		if ( $image_info[0] > $w || $image_info[1] > $h )
		{
			$image->resizeTo ( $w, $h, true, false );
			$image->export ( $file_location . '_large' );
		}

		// small version
		list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SMALL_THUMB'] );
		if ( $image_info[0] > $w || $image_info[1] > $h )
		{
			$image->resizeTo ( $w, $h, true, false );
			if( $UPL['CONFIGS']['INFO_SMALL_THUMB'] )
			{
				$copy = new image ( $image );
				$copy->addInfo();
				$copy->export ( $file_location . '_small' );
				$copy->destroy();
				unset($copy);
			}
			else $image->export ( $file_location . '_small' );
		}

		// square version
		list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SQUARE_THUMB'] );
		if ( $image_info[0] > $w || $image_info[1] > $h )
		{
			$image->resizeTo ( $w, $h, true, true );
			$image->export ( $file_location . '_square' );
		}
		$image->destroy();
		$count++;
	}
	$mysqlDB->free();
}


// public files table
$str = "ALTER TABLE uploader_pfiles DROP file_hasthumb, DROP file_rname, ADD file_lastview INT UNSIGNED NOT NULL";
if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
$str = "UPDATE uploader_pfiles SET file_lastview=" . time() . " WHERE 1";
if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print '<h4>Table "uploader_pfiles" altered!</h4>';
ob_flush();flush();

// public upload table
$str = "ALTER TABLE uploader_puploads ADD upload_ispublic TINYINT(1) DEFAULT '0', CHANGE upload_comments upload_description VARCHAR(255) NULL DEFAULT ''";
if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_pfiles" altered!<br />';
ob_flush();flush();

print "<h3>Public uploader updated!</h3>";
ob_flush();flush();


// Update previous tables for utf8
print "Updated old tables!<br />";
ob_flush(); flush();

// update some columns
if ( !$mysqlDB->query ( "ALTER TABLE uploader_users CHANGE userid userid INT UNSIGNED NOT NULL AUTO_INCREMENT" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_userfiles (
  file_id BIGINT UNSIGNED AUTO_INCREMENT,
  file_name VARCHAR(255) NOT NULL DEFAULT '',
  file_description TEXT NOT NULL DEFAULT '',
  file_location VARCHAR(255) NOT NULL DEFAULT '',
  file_size INT(10) UNSIGNED NOT NULL DEFAULT '0',
  file_isimage TINYINT(1) DEFAULT '0',
  file_extension VARCHAR(10) NOT NULL DEFAULT '',
  file_date INT(10) UNSIGNED NOT NULL DEFAULT '0',
  file_key VARCHAR(5) NOT NULL DEFAULT '',
  file_views INT UNSIGNED DEFAULT '0',
  file_last_view INT(10) UNSIGNED NULL DEFAULT '0',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  folder_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY  (file_id),
  KEY (userid),
  KEY (folder_id)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error() );

print 'Table "uploader_userfiles" created!<br />';
ob_flush();flush();

$str = "CREATE TABLE uploader_userfolders (
  folder_id BIGINT UNSIGNED AUTO_INCREMENT,
  folder_name VARCHAR(255) NOT NULL DEFAULT '',
  folder_description VARCHAR(255) DEFAULT '',
  folder_isgallery TINYINT(1) DEFAULT '0',
  folder_ispublic TINYINT(1) DEFAULT '0',
  folder_key VARCHAR(5) NOT NULL DEFAULT '',
  folder_permission TINYINT DEFAULT '0',
  folder_deleteable TINYINT(1) DEFAULT '1',
  folder_homefolder TINYINT(1) DEFAULT '0',
  folder_renameable TINYINT(1) DEFAULT '1',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (folder_id),
  KEY (userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );

print 'Table "uploader_userfolders" created!<br />';
ob_flush();flush();

$str = "CREATE TABLE uploader_usercontacts (
  contact_id BIGINT UNSIGNED AUTO_INCREMENT,
  contact_userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  contact_type TINYINT UNSIGNED NOT NULL DEFAULT '0',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (contact_id),
  KEY(contact_userid),
  KEY (userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );

print 'Table "uploader_usercontacts" created!<br />';
ob_flush();flush();



// comments table
$str = "CREATE TABLE uploader_usercomments (
  comment_id BIGINT UNSIGNED AUTO_INCREMENT,
  comment_date INT UNSIGNED NOT NULL,
  comment_ip VARCHAR(15) DEFAULT '0.0.0.0',
  comment_type TINYINT UNSIGNED NOT NULL,
  comment_message TEXT NOT NULL,
  object_id BIGINT UNSIGNED NOT NULL DEFAULT '0',
  userid INT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (comment_id),
  KEY(object_id)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_usercomments" created!<br />';
ob_flush();flush();

// announcements table

$str = "CREATE TABLE uploader_announcements (
  announcement_id INT UNSIGNED AUTO_INCREMENT,
  announcement_date INT UNSIGNED NOT NULL,
  announcement_subject VARCHAR(255) NOT NULL,
  announcement_parsebb TINYINT(1) DEFAULT '1',
  announcement_allowcomment TINYINT(1) DEFAULT '1',
  announcement_content TEXT NOT NULL,
  userid INT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (announcement_id),
  KEY(userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );

print 'Table "uploader_announcements" created!<br />';
ob_flush();flush();

// banned users table
$str = "CREATE TABLE uploader_banned (
  ban_ip INT UNSIGNED NOT NULL,
  ban_uploader TINYINT(1) NOT NULL,
  ban_public TINYINT(1) NOT NULL,
  PRIMARY KEY(ban_ip)
);";
if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_banned" created!<br />';
ob_flush();flush();

// set charset and ignore error if it fails
$mysqlDB->query ( "ALTER TABLE uploader_announcements CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_banned CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_messages CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_pfiles CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_puploads CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_usercomments CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_usercontacts CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_userfiles CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_userfolders CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_users CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
print 'New tables created.<br />';
ob_flush();flush();

print "<h3>Reading userfiles metadata and inserting them to the database</h3>";
ob_flush();flush();

$mysqlDB->query ( "SELECT COUNT(userid) AS user_count FROM uploader_users" );
$result = $mysqlDB->getAssoc();
print "Total users {$result['user_count']}. Users processed so far: ";
ob_flush();flush();

$count = 0;
while ( 1 )
{
	$mysqlDB->query ( "SELECT * FROM uploader_users LIMIT $count, 10" );
	if ( !$mysqlDB->getRowCount() ) break;
	while ( false !== ( $user  = $mysqlDB->getAssoc() ) )
	{
		$userid = $user['userid'];
		print ($count+1) . ", ";
		ob_flush();flush();

		$usercontents = get_contents ( USERFILES_ROOT );
		$userfiles = $usercontents['files'];

		while ( list ( $folder_path, $folder_files ) = each ( $userfiles ) )
		{
			$folder_info = get_folder_info ( USERFILES_ROOT . $folder_path );
			if ( $folder_path == '<MAIN_FOLDER>' )
				$folder_path = '';

			$folder_name = $folder_path == '' ? 'My Documents' : basename ( $folder_path );
			if ( $folder_name == 'thumbs' )
			{
				// remove thumb folder
				delete_dir ( USERFILES_ROOT . $folder_path );
				continue;
			}


			$insert = array
			(
				'folder_id'			=> null,
				'folder_name'			=> $mysqlDB->escape ( $folder_name ),
				'folder_description'		=> $mysqlDB->escape ( $folder_info['description'] ),
				'folder_isgallery'		=> (int)(bool)$folder_info['is_gallery'],
				'folder_ispublic'		=> 2,
				'folder_key'			=> get_rand ( 5 ),
				'folder_permission'		=> 0,
				'folder_deleteable'		=> (int)($folder_name != 'My Documents'),
				'folder_renameable'		=> (int)($folder_name != 'My Documents'),
				'folder_homefolder'		=> (int)($folder_name == 'My Documents'),
				'userid'			=> $user['userid']
			);

			if ( !$mysqlDB2->query ( "INSERT INTO uploader_userfolders SET " . $mysqlDB2->buildInsertStatement ( $insert )  ) ) exit ( $mysqlDB2->error() );
			$folder_id = $mysqlDB2->getInsertId();
			#$folder_id = 0;

			while ( list ( , $file ) = each ( $folder_files ) )
			{
				if ( !is_file ( USERFILES_ROOT . $folder_path . '/' . $file['name'] ) ) continue;
				$current_location = realpath ( USERFILES_ROOT . $folder_path . '/' . $file['name'] );
				$current_location = str_replace ( '\\', '/', $current_location );
				$file_location = realpath ( USERFILES_ROOT ) . '/' . uniqid('',true);
				$file_location = str_replace ( '\\', '/', $file_location );

				// get rid of thumbs
				if ( preg_match ( '#_thumb\.(jpeg|jpg|gif|png)$#i', $file['name'] ) )
				{
					print $current_location;
					unlink ( $current_location );
					continue;
				}

				$is_image = is_image ( $current_location );
				$insert = array
				(
					'file_id'		=> null,
					'file_name'		=> $mysqlDB->escape ( $file['name'] ),
					'file_location'		=> $file_location,
					'file_size'		=> $file['size'],
					'file_isimage'		=> $is_image,
					'file_date'		=> $file['date'],
					'file_extension'	=> substr ( get_extension ( $file['name'] ), 0, 10 ),
					'file_key'		=> strtolower(get_rand(5)),
					'userid'		=> $userid,
					'folder_id'		=> $folder_id,
				);

				if ( !$mysqlDB2->query ( "INSERT INTO uploader_userfiles SET " . $mysqlDB->buildInsertStatement ( $insert ) ) )
				{
					exit ( $mysqlDB2->error ( __LINE__, __FILE__ ) );
				}

				// change file name
				rename ( $current_location, $file_location );

				// check image dimension
				if ( $is_image )
				{
					$image_info = @getimagesize ( $file_location );
					if ( is_array ( $image_info ) )
					{
						list ( $max_width, $max_height ) = explode ( 'x', $UPL['CONFIGS']['MAX_IMAGE_DIMENSION'] );
						if ( $image_info[0] > $max_width || $image_info[1] > $max_height )
						{
							exit ( 'max image dim' );
							continue;
						}
					}
					else
					{
						#exit ( '$image_info is not an array: ' . $file_location );
						continue;
					}
				}

				// create thumbs for images
				if ( $is_image )
				{
					$image = new image ( $file_location );
					if ( $image )
					{
						// large version
						list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['LARGE_THUMB'] );
						if ( $image_info[0] > $w || $image_info[1] > $h )
						{
							$image->resizeTo ( $w, $h, true, false );
							$image->export ( $file_location . '_large' );
						}

						// small version
						list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SMALL_THUMB'] );
						if ( $image_info[0] > $w || $image_info[1] > $h )
						{
							$image->resizeTo ( $w, $h, true, false );
							if($UPL['CONFIGS']['INFO_SMALL_THUMB'])
							{
								$copy = new image ( $image );
								$copy->addInfo();
								$copy->export ( $file_location . '_small' );
								$copy->destroy();
								unset($copy);
							}
							else $image->export ( $file_location . '_small' );
						}

						// square version

						list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SQUARE_THUMB'] );
						$dim = $image->getDimension();
						if ( $dim[0] < $w || $dim[1] < $h )
						{
							$image->destroy();
							$image = new image ( $file_location . '_large' );
						}

						if ( $image_info[0] > $w || $image_info[1] > $h )
						{
							$image->resizeTo ( $w, $h, true, true );
							$image->export ( $file_location . '_square' );
						}
						$image->destroy();
					}
				}

			}
			if ( $folder_path != '' )
			{
				delete_dir ( USERFILES_ROOT . $folder_path );
			}
		}
		$count++;
	}
	$mysqlDB2->free();
}
print '<h3>Update completed!</h3>';
print '<p>You can now start uploading the new version onto your server. Be sure to follow the update instructions!</p>';
?>