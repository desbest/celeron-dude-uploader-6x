<?php
require_once 'includes/commons.inc.php';
require_once 'includes/messages_upload.inc.php';
require_once 'includes/image.class.php';
require_once 'includes/functions_upload.inc.php';
require_once 'includes/functions_userfiles.inc.php';
require_once 'includes/upload.class.php';

$tpl_upload = new Template ( TPL_DIR . 'tpl_upload.php' );
$tpl_error  = new Template ( TPL_DIR . 'tpl_error.php' );

$userid = $UPL['USER']['userid'];

// Get user statistics
$storage_used = 0;
$mysqlDB->query("SELECT SUM(file_size) AS total FROM uploader_userfiles WHERE userid={$userid}");
if($mysqlDB->getRowCount())
{
	$result = $mysqlDB->getAssoc();
	$storage_used = $result['total'];
	$mysqlDB->free();
}
$max_storage = $UPL['USER']['fl_max_storage'] * 1024 * 1024;
$images_only = $UPL['USER']['fl_images_only'];
$max_filesize = $UPL['USER']['fl_max_filesize'] * 1024;

if ( $max_storage > 0 && $storage_used >= $max_storage )
{
	$tpl_message->set ( 'message', $lang_upload['upl_storage_limit'] );
	$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) );
	$tpl_uploader->set ( 'content', $tpl_message, 1 );
	exit;
}

// Restrictions
$upload_max = get_byte_value ( ini_get ( 'upload_max_filesize' ) );
$post_max = get_byte_value ( ini_get ( 'post_max_size' ) );
$max_php_file_size = min ( $upload_max, $post_max );
$max_user_file_size = $max_filesize > 0 ? $max_filesize : $max_php_file_size;

$restrictions = array
(
	'max_file_size'	=> $max_user_file_size,
	'allowed_types'	=> $images_only ? 'jpg,jpeg,gif,png' : $UPL['USER']['fl_allowed_types'],
	'images_only'	=> $images_only
);

$tpl_upload->set ( 'restrictions', $restrictions );

// Load user folders
$user_folders = get_user_folders ( $userid, 0, 1 );
$tpl_upload->set ( 'user_folders', $user_folders );

if ( $action == 'upload' )
{
	// detect upload failure due to large files
	if ( !count ( $_POST ) && !count ( $_FILES ) )
	{
		$tpl_message->set ( 'message', parse ( $lang_upload['upl_max_php_size'], '{max}', get_size ( $max_php_file_size, 'B', 0 ) ) );
		$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'upload' : 'upload.php' ) );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
		exit;
	}

	// init
	$errors   = array ( );
	$uploaded = array ( );
	$userfiles_dir = str_replace ( '\\', '/', realpath ( $UPL['SETTINGS']['userfiles_dir'] ) ) . '/';

	// determine file location
	$dir_limit = 32000; //  max sub directories in a directory
	$paths = array();

	$paths[] = $userid % $dir_limit;
	$paths[] = ((int)($userid/$dir_limit) % $dir_limit);
	$paths[] = $userid;

	$path = implode ( '/', $paths );
	if ( !is_dir ( $userfiles_dir . $path ) )
		make_dir_recursive ( $userfiles_dir, $path );

	// upload options
	$folder_id 			= (int)gpc ( 'folder_id', 'P', 0 );
	$create_img_tags	= gpc ( 'create_img_tags', 'P', 0 );
	$extract_zip_files 	= gpc ( 'extract_zip_files', 'P', 0 );

	// check if folder is valid and get its info
	$target_folder = get_user_folders ( $userid, $folder_id );
	if ( !count ( $target_folder ) )
	{
		$tpl_message->set ( 'message', $lang_upload['upl_invalid_folder'] );
		$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'upload' : 'upload.php' ) );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
		exit;
	}

	// upload validating class
	$upload = new upload();
	$upload->set ( 'allowed_extensions', ($UPL['USER']['fl_allowed_types']==''||$UPL['USER']['fl_allowed_types']=='any') ? '' : $UPL['USER']['fl_allowed_types'] );
	$upload->set ( 'unallowed_extensions', $UPL['SETTINGS']['filetypes'] );
	$upload->set ( 'max_file_size', $max_filesize );

	// Process zip files
	while ( list ( $name , $file ) = each ( $_FILES ) )
	{
		if ( !isset($file['extracted_from_zip']) && ($file['name'] == 'batch.zip' || $file['name'] == 'upload.zip' || $extract_zip_files) && is_zip ( $file['tmp_name'] ) )
		{
			process_zip_file ( $file['tmp_name'] );
			unset ( $_FILES[$name] );
			@unlink ( $file['tmp_name'] );
		}
	}
	reset ( $_FILES );

	// Process all files
	while ( list ( $name, $file ) = each ( $_FILES ) )
	{
		if ( get_magic_quotes_gpc ( ) )
			$file['name'] = stripslashes ( $file['name'] );

		$file['name'] = str_replace ( '/', '_', $file['name'] );
		if ( preg_match ( '#\.php\..+#i', $file['name'] ) )
			$file['name'] = str_replace ( '.php', '_php', $file['name'] );

		// validate file first
		$error = 'none';
		if ( !$upload->validate ( $file, $error ) )
		{
			// report error
			switch ( $error )
			{
				case 'extension_not_allowed': $errors[] = parse ( $lang_upload['upl_bad_extension'], '{filename}', $file['name'] ); break;
				case 'http_error': case 'file_not_uploaded': $errors[] = parse ( $lang_upload['upl_partial_upload'], '{filename}', $file['name'] ); break;
				case 'max_file_size': $errors[] = parse ( $lang_upload['upl_max_size'], array ( '{filename}' => $file['name'], '{max_file_size}' => get_size ( $max_filesize, 'B', 1 ) ) ); break;
				case 'file_empty': continue; break; // just in case
				default: $errors[] = 'Unknown error: ' . $error; break;
			}
			// get rid of uploaded file
			if ( @is_file ( $file['tmp_name'] ) ) @unlink ( $file['tmp_name'] );
			// skip this file
			continue;
		}

		$is_image = is_image ( $file['tmp_name'] );

		// reached storage limit?
		if ( $max_storage && $storage_used > $max_storage )
		{
			$errors[] = parse ( $lang_upload['upl_storage_full'], '{filename}', $file['name'] );
			continue;
		}

		// trying to upload into a gallery folder?
		if ( $target_folder['folder_isgallery'] && !$is_image )
		{
			$errors[] = parse ( $lang_upload['upl_photo_folder'], array ( '{filename}' => $file['name'], '{folder}' => $target_folder['folder_name'] ) );
			continue;
		}

		// image upload only?
		if ( $images_only && !$is_image )
		{
			$errors[] = parse ( $lang_upload['upl_not_image'], '{filename}', $file['name'] );
			continue;
		}

		// check image dimension
		if ( $is_image )
		{
			$image_info = @getimagesize ( $file['tmp_name'] );
			if ( is_array ( $image_info ) )
			{
				list ( $max_width, $max_height ) = explode ( 'x', $UPL['CONFIGS']['MAX_IMAGE_DIMENSION'] );
				if ( $image_info[0] > $max_width || $image_info[1] > $max_height )
				{
					$errors[] = parse ( $lang_upload['upl_large_image'], array ( '{filename}' => $file['name'], '{max_dimension}' => $UPL['CONFIGS']['MAX_IMAGE_DIMENSION'] ) );
					continue;
				}
			}
			else continue;
		}

		// determine unique file name
		do
		{
			clearstatcache();
			$name = uniqid ( '', true );
			$file_location = $userfiles_dir . $path . '/' . $name;
		}
		while ( is_file ( $file_location ) );

		//  move file to that location
		if ( !@rename ( $file['tmp_name'], $file_location ) && !@copy ( $file['tmp_name'], $file_location ) )
		{
			exit ( 'Fatal error: Could not move or copy uploaded files to the user files directory. Check directory permission!' );
		}

		// insert
		$file_extension = get_extension ( $file['name'] );

		$insert = array
		(
			'file_id'			=> null,
			'file_name'			=> $mysqlDB->escape ( $file['name'] ),
			'file_location'		=> $file_location,
			'file_size'			=> filesize ( $file_location ),
			'file_isimage'		=> $is_image,
			'file_date'			=> time(),
			'file_extension'	=> substr ( $file_extension, 0, 10 ),
			'file_key'			=> strtolower(get_rand(5)),
			'userid'			=> $userid,
			'folder_id'			=> $folder_id,
		);

		if ( $mysqlDB->query ( "INSERT INTO uploader_userfiles SET " . $mysqlDB->buildInsertStatement ( $insert ) ) )
		{
			$file_id = $mysqlDB->getInsertId();
			// Successfully uploaded, do anything else?

			// if it's an image
			if ( $is_image )
			{
				$image = new image ( $file_location );
				if ( $image )
				{
					// watermark?
					if ( $UPL['SETTINGS']['wm'] == 'always' || ( $UPL['SETTINGS']['wm'] == 'user' && $UPL['USER']['fl_watermark'] ) )
					{
						list ( $hor, $ver ) = explode ( ',', $UPL['SETTINGS']['wm_pos'] );
						$copy = new image ( $image );
						$copy->watermark ( $UPL['SETTINGS']['wm_path'], $hor, $ver );
						$copy->export ( $file_location );
						$copy->destroy();
						unset($copy);
					}

					// large version
					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['LARGE_THUMB'] );
					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, false );
						$image->export ( $file_location . '_large' );
					}

					// small version
					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SMALL_THUMB'] );
					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, false );
						if($UPL['CONFIGS']['INFO_SMALL_THUMB'])
						{
							$copy = new image ( $image );
							$copy->addInfo();
							$copy->export ( $file_location . '_small' );
							$copy->destroy();
							unset($copy);
						}
						else $image->export ( $file_location . '_small' );
					}

					// square version

					list ( $w, $h ) = explode ( 'x', $UPL['CONFIGS']['SQUARE_THUMB'] );
					$dim = $image->getDimension();
					if ( $dim[0] < $w || $dim[1] < $h )
					{
						$image->destroy();
						$image = new image ( is_file ( $file_location . '_large' ) ? $file_location . '_large' : $file_location );
					}

					if ( $image_info[0] > $w || $image_info[1] > $h )
					{
						$image->resizeTo ( $w, $h, true, true );
						$image->export ( $file_location . '_square' );
					}
					$image->destroy();
				}
			}

			// add to list
			$insert['file_id'] = $file_id;
			$insert['username'] = $UPL['USER']['username'];
			processFile ( $insert );
			$uploaded[] = $insert;
			$storage_used += $insert['file_size'];

			// log file
			if ( $UPL['SETTINGS']['log'] >= 1 ) upload_log ( "Uploaded: {$file['name']}" );
		}
		else
		{
			@unlink ( $file['tmp_name'] );
			delete_file($file_location);
			exit ( 'Fatal error: Could not insert into database. upload.php line ' . __LINE__ );
		}
	} // end uploaded files loop

	// Report errors if any
	if ( count ( $errors ) )
	{
		$tpl_error->set ( 'error_title', $lang_upload['upl_files_not_uploaded'] );
		$tpl_error->set ( 'error', $errors );
		$tpl_message->set ( 'message', $tpl_error );
		$tpl_message->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . ( $folder_id ? '?folder_id=' . $folder_id : '' ) );
		$tpl_uploader->set ( 'content', $tpl_message, 1 );
	}
	elseif ( !count ( $uploaded ) )
	{
		// nothing uploaded, show upload page again
		go_to ( UPLOADER_URL . ( MOD_REWRITE ? 'upload' : 'upload.php' ) . ( $folder_id ? '?folder_id=' . $folder_id : '' ) );
	}
	else
	{
		if ( $create_img_tags )
		{
			$tpl_img = new Template ( TPL_DIR . 'tpl_img.php' );
			// just want a few info, not all
			$files = array();
			$count = count ( $uploaded );
			for ( $i = 0; $i < $count; ++$i )
			{
				$file = $uploaded[$i];
				$files[] = array
				(
					'name' 				=> $file['file_name'],
					'is_image' 			=> (int)$file['file_isimage'],
					'url' 				=> $file['file_isimage'] ? $file['fullsize_url'] : $file['url'],
					'direct_url' 		=> $file['direct_url'],
					'small_thumb_url' 	=> $file['small_thumb_url']
				);
			}
			// and display
			$tpl_img->set ( 'files', $files );
			$tpl_img->set ( 'back_url', UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . ( $folder_id ? '?folder_id=' . $folder_id : '' ) );
			$tpl_uploader->set ( 'content', $tpl_img, 1 );
		}
		else go_to ( UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . ( $folder_id ? '?folder_id=' . $folder_id : '' ) );
	}

}
else
{
	// display upload form
	$folder_id = (int)gpc ( 'folder_id', 'G', 0 );
	$tpl_upload->set ( 'cancel_url', UPLOADER_URL . ( MOD_REWRITE ? 'myfiles' : 'myfiles.php' ) . ( $folder_id ? '?folder_id=' . $folder_id : '' ) );
	$tpl_upload->set ( 'folder_id', $folder_id );
	$tpl_uploader->set ( 'page_title', $lang_upload['upl_title'] );
	$tpl_uploader->set ( 'content', $tpl_upload, 1 );
}
?>