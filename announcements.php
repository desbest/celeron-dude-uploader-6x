<?php
@define ( 'NO_AUTH_CHECK', 1 );
require_once 'includes/commons.inc.php';
$tpl_ann = new Template ( TPL_DIR . 'tpl_announcements.php' );
$tpl_ann->set ( 'action', $action );

if ( $action == 'view' )
{
	$aid = (int)gpc ( 'aid', 'G', 0 );

	// get announcement
	$ann = array();
	$result = $mysqlDB->query2 ( "SELECT * FROM uploader_announcements WHERE announcement_id=$aid LIMIT 1" );
	if ( !$result->isGood() ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	if ( !$result->rowCount() )
	{
		go_to(UPLOADER_URL);
		exit;
	}
	$ann = $result->fetchAssoc();

	// get comments
	$comments = array();
	$result = $mysqlDB->query2("SELECT comment.*, user.username FROM uploader_usercomments AS comment LEFT JOIN uploader_users AS user USING(userid) WHERE object_id={$ann['announcement_id']} AND comment_type=" . COMMENT_ANNOUNCEMENT . " ORDER BY comment_id ASC");
	if ( $result->isGood() )
	{
		if ( $result->rowCount() )
		{
			while ( false !== ( $comment = $result->fetchAssoc() ) )
			{
				processUser ( $comment, false );
				$comment['is_mine'] = ( $comment['userid'] == $USER['userid'] );
				$comment['delete_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'comment' : 'comment.php' ) . '?action=delete&amp;comment_id=' . $comment['comment_id'];
				$comments[] = $comment;
			}
		}
		$result->free();
	}
	else exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

	$tpl_ann->set ( 'ann', $ann );
	$tpl_ann->set ( 'comments', $comments );
	$tpl_uploader->set ( 'content', $tpl_ann, true );
}
else
{
	// read in announcements
	$announcements = array();
	$result = $mysqlDB->query2 ( "SELECT *, COUNT(comment_id) AS comments_count FROM uploader_announcements AS a LEFT "
	. " JOIN uploader_usercomments AS uc ON uc.object_id=a.announcement_id AND uc.comment_type=" . COMMENT_ANNOUNCEMENT
	. " GROUP BY announcement_id ORDER BY announcement_id DESC" );
	if ( !$result->isGood() ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	while ( false !== ( $ann = $result->fetchAssoc() ) )
	{
		$ann['view_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'announcements?action=view&amp;aid=' . $ann['announcement_id'] : 'announcements.php?action=view&amp;aid=' . $ann['announcement_id'] );
		$announcements[] = $ann;
	}
	$result->free();

	$tpl_ann->set('announcements', $announcements);
	$tpl_uploader->set('content', $tpl_ann, true);
}
?>