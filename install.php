<?php
error_reporting ( E_ALL );
define ( 'CHMOD_VALUE', 0777 );
ob_start();
set_magic_quotes_runtime ( 0 );
require_once 'includes/license.inc.php';
require_once 'includes/configs.inc.php';
require_once 'includes/constants.inc.php';
require_once 'includes/functions_base.inc.php';
require_once 'includes/mysql.class.php';
require_once 'includes/upl_settings.inc.php';
require_once 'includes/pub_settings.inc.php';

// init mysql
extract($UPL['MYSQL'],EXTR_OVERWRITE);
$mysqlDB=new mysqlDB($host,$username,$password,$database,defined('NO_PERSISTENT')?0:$persistent);

if ( get_magic_quotes_gpc ( ) )
{
    $_GET = strip_gpc ( $_GET );
    $_POST = strip_gpc ( $_POST );
}

function file_perm ( $path )
{
    return substr ( sprintf ( '%o', fileperms ( $path ) ), -4);
}

function get_dir_status ( $path )
{
    return is_writable ( $path ) ? 'OK (' . file_perm ( $path ) . ')' : 'Please CHMOD ' . $path . ' to 0' . decoct ( CHMOD_VALUE );
}

function get_file_status ( $file )
{
    if ( !is_file ( $file ) )
    {
	if ( !touch ( $file ) )
	{
	    return 'File does not exist and attempt to create it has failed. Please create ' . $file . ' manually.';
	}
	else
	{
	    return 'OK, file was created.';
	}
    }
    elseif ( is_writable ( $file ) )
    {
	return 'OK (' . file_perm ( $file ) . ')';
    }
    else
    {
	if ( !change_mode ( $file, CHMOD_VALUE ) )
	{
	    return 'File permission changed. Reload this page to re-test.';
	}
	else
	{
	    return 'Unable to change file permission. Please CHMOD ' . $file;
	}
    }
}

$step = isset ( $_GET['step'] ) ? $_GET['step'] : 1;
?>



<?php if ( $step == 1 ) : ?>
<h3>The following directories must be CHMODed to writeable (0777, or 0755).</h3>
<table cellspadding="3" cellspacing="1" border="1">
    <tr>
        <td>Directory description</td>
        <td>Path</td>
        <td>Status</td>
    </tr>
    <tr>
        <td>Uploader logs</td>
        <td><?=LOGS_DIR?></td>
        <td><?=get_dir_status(LOGS_DIR)?></td>
    </tr>
    <tr>
        <td>Uploader settings</td>
        <td><?=UPLSETTINGS_DIR?></td>
        <td><?=get_dir_status(UPLSETTINGS_DIR)?></td>
    </tr>
    <tr>
        <td>Public uploader files directory</td>
        <td><?=$UPL['PUBLIC_SETTINGS']['files_dir']?></td>
        <td><?=get_dir_status($UPL['PUBLIC_SETTINGS']['files_dir'])?></td>
    </tr>
</table>

<p>
    The settings have been changed. Edit includes/upl_settings.inc.php, includes/configs.inc.php, includes/pub_settings.inc.php to edit these settings.
    You MUST edit the configs.inc.php before proceeding.
</p>

<p>
    If everything is OK, <a href="install.php?step=2">go to step 2</a>.
</p>
<?php endif; ?>


<?php if ( $step == 2 )
{
    print '<h3>Trying to create mysql tables:</h3>';
    $mysqlDB->query ( 'DROP TABLE IF EXISTS uploader_users;' );

    $str = "
CREATE TABLE uploader_users
(
    userid INT UNSIGNED AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL,
    password VARCHAR(32) NOT NULL,
    email VARCHAR(255) NOT NULL,
    level TINYINT UNSIGNED DEFAULT '0',
    max_messages TINYINT(1) DEFAULT '20',
    is_activated TINYINT(1) DEFAULT '0',
    is_approved TINYINT(1) DEFAULT '0',
    is_suspended TINYINT(1) DEFAULT '0',
    last_login_time INT DEFAULT '0',
    last_login_ip VARCHAR(15),
    reg_email VARCHAR(255) DEFAULT '',
    reg_date INT UNSIGNED DEFAULT '0',
    reg_ip VARCHAR(15) DEFAULT '0.0.0.0',
    pref_accepts_pm TINYINT(1) DEFAULT '1',
    pref_show_email TINYINT(1) DEFAULT '0',
    bw_used DOUBLE UNSIGNED NOT NULL DEFAULT '0',
    bw_max INT UNSIGNED NOT NULL,
    bw_reset_last INT UNSIGNED DEFAULT '0',
    bw_reset_period TINYINT UNSIGNED DEFAULT '30',
    bw_reset_auto TINYINT(1) DEFAULT '1',
    bw_xfer_rate SMALLINT UNSIGNED DEFAULT '0',
    fl_max_storage SMALLINT UNSIGNED DEFAULT '5',
    fl_max_filesize MEDIUMINT UNSIGNED DEFAULT '2048',
    fl_allowed_types VARCHAR(255),
    fl_images_only TINYINT(1) DEFAULT '1',
    fl_rename_permission TINYINT(1) DEFAULT '0',
    fl_allow_folders TINYINT(1) DEFAULT '1',
    fl_max_folders SMALLINT DEFAULT '10',
    fl_watermark TINYINT(1) DEFAULT '1',
    fl_has_public TINYINT(1) DEFAULT '0',
    xtr_admin_comments VARCHAR(255),
    xtr_new_email_address VARCHAR(255),
    xtr_activation_code VARCHAR(32),
    xtr_password_reset_code VARCHAR(32),
    xtr_change_email_code VARCHAR(32),
    PRIMARY KEY (userid),
    KEY (username),
    KEY (fl_has_public)
)";

    if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error (__LINE__,__FILE__) );
    print 'Table uploader_users created.<br />';

    $mysqlDB->query ( 'DROP TABLE IF EXISTS uploader_messages;' );
    $str = "
CREATE TABLE uploader_messages
(
	messageid INT UNSIGNED AUTO_INCREMENT,
	userid MEDIUMINT UNSIGNED NOT NULL,
	from_id MEDIUMINT UNSIGNED NOT NULL,
	from_username VARCHAR(64) NOT NULL,
	subject VARCHAR(255) NOT NULL,
	message TEXT DEFAULT '',
	folder TINYINT NOT NULL,
	date INT UNSIGNED NOT NULL,
	is_read TINYINT(1) DEFAULT '0',
	PRIMARY KEY (messageid),
	KEY(userid)
)";

    if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error (__LINE__,__FILE__) );
    print 'Table uploader_messages created.<br />';
    $mysqlDB->query ( "DROP TABLE IF EXISTS uploader_puploads" );
    $str = "
CREATE TABLE uploader_puploads
(
	upload_id INT UNSIGNED AUTO_INCREMENT,
	upload_name VARCHAR(64) DEFAULT '',
	upload_date INT UNSIGNED NOT NULL,
	upload_description VARCHAR(255) DEFAULT '',
	upload_key VARCHAR(32) NOT NULL,
	upload_ispublic TINYINT(1) DEFAULT '1',
	upload_ip VARCHAR(16) NOT NULL,
	PRIMARY KEY (upload_id)
)";

    if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error (__LINE__,__FILE__) );
    print 'Table uploader_puploads created.<br />';
    $mysqlDB->query ( "DROP TABLE IF EXISTS uploader_pfiles" );
    $str = "
CREATE TABLE uploader_pfiles
(
	file_id INT UNSIGNED AUTO_INCREMENT,
	file_name VARCHAR(128) NOT NULL,
	file_location VARCHAR(255) NOT NULL,
	file_size INT UNSIGNED NOT NULL,
	file_views INT UNSIGNED DEFAULT 0,
	file_lastview INT UNSIGNED NOT NULL,
	file_isimage TINYINT(1) DEFAULT 0,
	upload_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (file_id),
	KEY(upload_id)
)";

    if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error (__LINE__,__FILE__) );
    print 'Table uploader_pfiles created.<br />';




// Userfiles table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_userfiles;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_userfiles (
  file_id BIGINT UNSIGNED AUTO_INCREMENT,
  file_name VARCHAR(255) NOT NULL DEFAULT '',
  file_description TEXT NOT NULL DEFAULT '',
  file_location VARCHAR(255) NOT NULL DEFAULT '',
  file_size INT(10) UNSIGNED NOT NULL DEFAULT '0',
  file_isimage TINYINT(1) DEFAULT '0',
  file_extension VARCHAR(10) NOT NULL DEFAULT '',
  file_date INT(10) UNSIGNED NOT NULL DEFAULT '0',
  file_key VARCHAR(5) NOT NULL DEFAULT '',
  file_views INT UNSIGNED DEFAULT '0',
  file_last_view INT(10) UNSIGNED NULL DEFAULT '0',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  folder_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY  (file_id),
  KEY (userid),
  KEY (folder_id)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error() );

print 'Table "uploader_userfiles" created!<br />';

// userfolders table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_userfolders;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_userfolders (
  folder_id BIGINT UNSIGNED AUTO_INCREMENT,
  folder_name VARCHAR(255) NOT NULL DEFAULT '',
  folder_description VARCHAR(255) DEFAULT '',
  folder_isgallery TINYINT(1) DEFAULT '0',
  folder_ispublic TINYINT(1) DEFAULT '0',
  folder_key VARCHAR(5) NOT NULL DEFAULT '',
  folder_permission TINYINT DEFAULT '0',
  folder_deleteable TINYINT(1) DEFAULT '1',
  folder_homefolder TINYINT(1) DEFAULT '0',
  folder_renameable TINYINT(1) DEFAULT '1',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (folder_id),
  KEY (userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );

print 'Table "uploader_userfolders" created!<br />';

// contact table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_usercontacts;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_usercontacts (
  contact_id BIGINT UNSIGNED AUTO_INCREMENT,
  contact_userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  contact_type TINYINT UNSIGNED NOT NULL DEFAULT '0',
  userid INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (contact_id),
  KEY(contact_userid),
  KEY (userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );

print 'Table "uploader_usercontacts" created!<br />';


// comments table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_usercomments;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_usercomments (
  comment_id BIGINT UNSIGNED AUTO_INCREMENT,
  comment_date INT UNSIGNED NOT NULL,
  comment_ip VARCHAR(15) DEFAULT '0.0.0.0',
  comment_type TINYINT UNSIGNED NOT NULL,
  comment_message TEXT NOT NULL,
  object_id BIGINT UNSIGNED NOT NULL DEFAULT '0',
  userid INT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (comment_id),
  KEY(object_id)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_usercomments" created!<br />';

// announcements table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_announcements;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_announcements (
  announcement_id INT UNSIGNED AUTO_INCREMENT,
  announcement_date INT UNSIGNED NOT NULL,
  announcement_subject VARCHAR(255) NOT NULL,
  announcement_parsebb TINYINT(1) DEFAULT '1',
  announcement_allowcomment TINYINT(1) DEFAULT '1',
  announcement_content TEXT NOT NULL,
  userid INT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (announcement_id),
  KEY(userid)
)";

if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_announcements" created!<br />';


// banned users table
if ( !$mysqlDB->query ( "DROP TABLE IF EXISTS uploader_banned;" ) ) exit ( $mysqlDB->error() );

$str = "CREATE TABLE uploader_banned (
  ban_ip INT UNSIGNED NOT NULL,
  ban_uploader TINYINT(1) NOT NULL,
  ban_public TINYINT(1) NOT NULL,
  PRIMARY KEY(ban_ip)
);";
if ( !$mysqlDB->query ( $str ) ) exit ( $mysqlDB->error( __FILE__, __LINE__ ) );
print 'Table "uploader_banned" created!<br />';


// set charset and ignore error if it fails
$mysqlDB->query ( "ALTER TABLE uploader_announcements CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_banned CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_messages CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_pfiles CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_puploads CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_usercomments CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_usercontacts CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_userfiles CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_userfolders CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );
$mysqlDB->query ( "ALTER TABLE uploader_users CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;" );



    print '<a href="install.php?step=3">Proceed to the next step</a>';
}

?>


<?php if ( $step == 3 ) :
if ( count ( $_POST ) )
{
    $userid = 1;
    $submit = $_POST['user'];

    if ( $submit['password'] != $submit['password2'] )
	exit ( 'The passwords you entered do not match. Click Back.' );

    $new_user_info = array
    (
	    'userid'			=> NULL,
	    'username'			=> $submit['name'],
	    'password'			=> md5 ( $submit['password'] ),
	    'email'			=> $submit['email'],
	    'level'			=> LEVEL_ADMIN,
	    'max_messages' 		=> 0,
	    'is_activated'		=> 1,
	    'is_approved'		=> 1,
	    'is_suspended'		=> 0,
	    'last_login_time'		=> 0,
	    'last_login_ip'		=> '0.0.0.0',
	    'reg_email'			=> $submit['email'],
	    'reg_date'			=> time(),
	    'reg_ip'			=> $_SERVER['REMOTE_ADDR'],
	    'pref_accepts_pm'		=> 1,
	    'pref_show_email'		=> 0,
	    'bw_used'			=> 0,
	    'bw_max'			=> 0,
	    'bw_reset_last'		=> 0,
	    'bw_reset_period'		=> 30,
	    'bw_reset_auto'		=> 1,
	    'bw_xfer_rate'		=> 0,
	    'fl_max_storage'		=> 0,
	    'fl_max_filesize'		=> 0,
	    'fl_allowed_types'		=> '',
	    'fl_images_only'		=> 0,
	    'fl_rename_permission' 	=> 2,
	    'fl_allow_folders'		=> 1,
	    'fl_max_folders'		=> 0,
	    'fl_watermark'		=> 0,
	    'xtr_admin_comments'	=> '',
	    'xtr_new_email_address'	=> '',
	    'xtr_activation_code'	=> md5 ( get_rand ( 1024 ) ),
	    'xtr_password_reset_code'	=> md5 ( get_rand ( 1024 ) ),
	    'xtr_change_email_code'	=> md5 ( get_rand ( 1024 ) ),
    );


    if ( !$mysqlDB->query ( "INSERT INTO uploader_users SET " . $mysqlDB->buildInsertStatement ( $new_user_info ) . ";" ) ) exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

    if ( !$mysqlDB->getAffectRowCount ( ) )
        exit ( 'Could not add admin user to the database.' );
    else
    {
        $userid = $mysqlDB->getInsertId();

        // create default folder
        $insert = array
        (
                'folder_id'	        => null,
                'folder_name'		=> 'My Documents',
                'folder_description'    => '',
                'folder_isgallery'	=> 0,
                'folder_ispublic'	=> 0,
                'folder_permission'	=> 0,
                'folder_deleteable'	=> 0,
                'folder_homefolder'	=> 1,
                'folder_renameable'	=> 0,
                'userid'		=> $userid
        );
        if ( !$mysqlDB->query ( "INSERT INTO uploader_userfolders SET " . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error() );

    }

    print 'Congrats, installation successful. <a href="install.php?step=5">Click here</a> to remove the installer and login. If the installer fails to remove itself, please delete it manually.';
}
else
{?>
<h3>Create admin account</h3>
<form method="post" action="install.php?step=3">
    <table cellspadding="3" cellspacing="1" border="1">
	<tr>
	    <td>Username</td>
	    <td><input type="text" name="user[name]" size="50" /></td>
	</tr>
	<tr>
	    <td>Password</td>
	    <td><input type="password" name="user[password]" size="50" /></td>
	</tr>
	<tr>
	    <td>Confirm password</td>
	    <td><input type="password" name="user[password2]" size="50" /></td>
	</tr>
	<tr>
	    <td>Email address</td>
	    <td><input type="text" name="user[email]" size="50" /></td>
	</tr>
	<tr>
	    <td></td>
	    <td><input type="submit" name="submit" value="Submit" /></td>
	</tr>
    </table>
</form>
<?php } endif; ?>


<?php if ( $step == 4 )
{
    if ( unlink ( 'install.php' ) )
    {
	header ( 'Location: account.php?action=login' );
    }
    else
    {
	exit ( 'Unable to remove install.php, please remove this file manually.' );
    }
}
?>