<?



  class template
  {
    var $_t = '';
    var $_v = array ();
    var $_par = false;
    function template ($t)
    {
      if (!(is_file ($t)))
      {
        exit ('The template file \'' . $t . '\' does not exist.');
      }

      $this->_t = $t;
    }

    function set ($n, $v = '', $d = 0)
    {
      if (is_array ($n))
      {
        while (list ($a, $b) = each ($n))
        {
          $this->_v[$a] = $b;
        }
      }
      else
      {
        $this->_v[$n] = $v;
      }

      if ($d)
      {
        $this->display ();
      }

    }

    function setr ($n, &$v, $d = 0)
    {
      $this->_v[$n] = &$v;
      if ($d)
      {
        $this->display ();
      }

    }

    function display ($r = 0)
    {
      while (list ($k) = each ($this->_v))
      {
        if (is_object ($this->_v[$k]))
        {
          $$k = $this->_v[$k]->display (1);
          continue;
        }
        else
        {
          $$k = &$this->_v[$k];
          continue;
        }
      }

      if ($r)
      {
        ob_start ();
        include $this->_t;
        $x = ob_get_contents ();
        ob_end_clean ();
        return $x;
      }

      include $this->_t;
    }
  }

?>