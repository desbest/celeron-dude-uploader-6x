<?php
// shared between public.php and upload.php
$lang_upload = array
(
	'upl_folder_no_exists'	=> 'Unable to access user folder for {username}. This could be due to the userfiles folder not being CHMODed correctly.',
	'upl_invalid_folder'	=> 'Invalid folder',
	'upl_storage_limit'		=> 'You have reached the storage limit on your account. Delete old files to make room.',
	'upl_partial_upload'	=> '{filename} was partially uploaded and discarded.',
	'upl_empty_file'		=> '{filename} is empty or invalid.',
	'upl_php_exceed'		=> '{filename} exceeds PHP\'s max_file_size limit of {max_file_size} and was not uploaded.',
	'upl_storage_full'		=> '{filename} was not uploaded because you have reached the storage limit on your account.',
	'upl_bad_extension'		=> '{filename}, this file type is not allowed.',
	'upl_ext_not_alllowed'	=> '{filename}, you are not allowed to upload this file type.',
	'upl_bad_chars'			=> '{filename}, file name contains invalid character(s).',
	'upl_bad_name_start'	=> '{filename}, file name must start with a letter or a number.',
	'upl_max_size'			=> '{filename}, file size exceeds your limit of {max_file_size}.',
	'upl_not_image'			=> '{filename} does not appear to be a valid image file (JPEG, PNG, and GIF). You can only upload images.',
	'upl_large_image'		=> '{filename}, the image dimension is too large. The maximum dimension is {max_dimension}',
	'upl_file_exists_warn'	=> '<strong>Warning</strong>: The file <span style="color:#444444;">{file}</span> already exists in <span style="color:#444444;">{folder}</span>.<br />',
	'upl_cant_move'			=> 'Unable to move {file} to user folder.',
	'upl_skipped'			=> 'The file "{filename}" exists and was skipped.',
	'upl_photo_folder'  	=> 'The folder "{folder}" is a photo folder. The file "{filename}" does not appear to be a valid image and was discarded.',
	'upl_files_not_uploaded'	=> 'Some files were not uploaded due to the following reasons:',
	'upl_max_php_size'		=> 'We are sorry but one or more of your files is too large. The maximum file size is {max}. Although you may be allowed to upload files larger than this, the server is incapable of accepting file(s) larger than this limit.',
	'upl_title'				=> 'File upload',


);

$lang_public = array
(
	'file_not_found'	=> 'File not found. It may have been removed due to excessive bandwidth usage or other reasons.',
	'disabled'			=> 'Public uploading has been disabled.',
	'set_not_found'		=> 'The upload set you are looking for does not exist. Perhaps it was deleted.',
	'invalid_set_key'	=> 'You have specified an invalid key. You cannot edit this set.',
	'set_deleted'		=> 'All the files in this set has been removed and the set has been deleted.',
	'slideshow_no_image'=> 'The slideshow cannot be viewed because there are no images in this set.',

	'ptitle1'			=> 'Public file upload',
	'title_invalid_set'	=> 'Invalid set',
	'title_invalid_key'	=> 'Invalid key',
	'title_manage'		=> 'Manage upload set',
	'title_set_deleted' => 'Set deleted',
	'title_file_not_found' => 'Invalid file',
);
?>