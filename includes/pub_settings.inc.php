<?php
// Unless specified, 1 means yes/on/enabled and 0 means no/off/disabled

$UPL['PUBLIC_SETTINGS'] = array
(
	// Absolute or relative path to the directory where public uploaded files will be stored.
	// The script will try to resolve relative paths to absolute path when necessary.
	'files_dir' => 'pfiles/',

	// Enable/Disable the public uploader
	'enabled' => 1,

	// Enable/Disable images only restriction.
	'images_only' => 1,

	// If images_only is OFF, only these file types can be uploaded. Comma seperated format. No spaces
	'allowed_filetypes' => 'jpeg,jpg,png,gif,bmp,mp3,txt,avi,wmv,mpg,mpeg,doc',

	// Max file size of uploaded files. Enter 0 for unlimited. Value is in KB.
	'max_file_size' => 5 * 1024, // 5 MB

	// Max views before file is deleted. Enter 0 for unlimited. If unlimited, file will be deleted depending on max_bandwidth
	'max_views' => 5,

	// Max bandwidth transferred before file is deleted. Enter 0 for unlimited. Value is in MB. If unlimited, file will be deleted depending on max_views.
	'max_bandwidth' => 500, // 500 MB

	// transfer rate limit, this setting will NOT work on windows servers. Enter 0 for unlimited. Value is in KB/s
	'transfer_rate'	=> 0,

	// Use the uploader to view images. If disabled, URLs to the images will be the direct URLs. 1 is VERY recommended
	'uploader_view' => 1,

	// Add the original image's info (size and dimension) to the SMALL thumbnail.
	'image_info' => 0,

	// Log upload actions.
	'log_upload' => 1,

	// Watermark uploaded images
	'wm' => 0,

	// Path to png watermark image.
	'wm_path' => 'data/watermark.png',

	// Position of watermark image relative to original image. 9 positions available.
	// First setting: left, center, right
	// Second setting: top, center, bottom
	'wm_pos' => 'center,bottom',

	// how many latest image sets to show on the main public uploader page. Enter 0 to disable this feature.
	'latest_limit'	=> 12,

	// sets per page on the browse page
	'sets_per_page'	=> 24,
);
?>