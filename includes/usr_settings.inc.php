<?php
// Unless specified, 1 means yes/on/enabled and 0 means no/off/disabled

$UPL['USER_SETTINGS'] = array
(
	'new_user_settings' => array
	(
		// Max file storage. Value is in MB
		'fl_max_storage' 		=> 20,

		// Max file size for uploaded files. Value is in KB
		'fl_max_filesize' 		=> 1024,

		// Create folder permission.
		'fl_create_folder' 		=> 1,

		// Max number of folders user can create. Enter 0 for unlimited.
		'fl_max_folders' 		=> 5,

		// File types user can upload. Enter BLANK or 'any' to allow all file types. Comma seperated format.
		// Example: jpeg,gif,mp3
		'fl_allowed_filetypes' 	=> 'any',

		// If enabled, users can only upload images. The checking is NOT by the file's extension but by the file header.
		// If this is set, the file types setting is ignored.
		'fl_images_only' 		=> true,

		// Renaming permission. The options are:
		// 2: Both name and extension (recommended in this version [6.3])
		// 1: Name only, no extension
		// 0: No rename
		'fl_allow_rename' 		=> 0,

		// Watermark all images uploaded by user.
		'fl_watermark' 			=> 1,

		// Maximum bandwidth allowed. Enter 0 for unlimited. Value is in MB
		'bw_max' 				=> 30 * 1024, // 30 GB

		// Automatically reset user's bandwidth counter when the limit is reached and the reset period has passed.
		'bw_auto_reset' 		=> 1,

		// This period must pass before user's bandwidth counter can be reset. Value is in DAYS.
		'bw_reset_period' 		=> 30,

		// Maximum transfer rate. Enter 0 for unlimited. Value is in KB/s
		'bw_xfer_rate' 			=> 0,
	),

	'restrictions' => array
	(
		// Names that users cannot register under. Comma serperated format.
		'disallowed_names' => 'admin,administrator'
	)
);
?>