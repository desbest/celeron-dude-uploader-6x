<?



  function get_folder_info ($p)
  {
    if (!(is_dir ($p)))
    {
      return false;
    }

    $f = $p . '/.folder.php';
    $db = new DB ();
    if ($db->open ($f))
    {
      $info = $db->all ();
      $db->close ();
      if (!(isset ($info['users'])))
      {
        $info['users'] = array ();
        set_folder_info ($p, $info);
      }

      if (!(isset ($info['permission'])))
      {
        $info['permission'] = ($info['is_public'] ? 'public' : 'private');
        set_folder_info ($p, $info);
      }

      return $info;
    }

    $info = array ('is_gallery' => 0, 'permission' => (is_file ($p . '/.public') ? 'public' : 'private'), 'users' => array (), 'icon' => '', 'description' => (is_file ($p . '/.description') ? implode ('', file ($p . '/.description')) : ''));
    if ($db->create ($f))
    {
      $db->open ($f);
      $db->set ($info, 1);
      $db->save ();
    }

    @unlink ($p . '/.public');
    @unlink ($p . '/.description');
    return $info;
  }

  function set_folder_info ($p, $info)
  {
    if (!(is_dir ($p)))
    {
      return false;
    }

    $f = $p . '/.folder.php';
    $db = new DB ();
    if (!(is_file ($f)))
    {
      if (!($db->create ($f)))
      {
        return false;
      }
    }

    if ($db->open ($f))
    {
      $db->set ($info, 1);
      $db->save ();
      return true;
    }

    return false;
  }

  function is_folder_empty ($path)
  {
    if (false === $h = @opendir ($path))
    {
      return false;
    }

    $ignore = array ('.', '..', '.cache.php', '.folder.php', 'thumbs', '.htaccess');
    while (false !== $f = readdir ($h))
    {
      if (!(in_array ($f, $ignore)))
      {
        return false;
      }
    }

    return true;
  }

  function has_public_folders ($path)
  {
    $contents = get_contents ($path);
    $folders = &$contents['dirs'];
    $count = count ($folders);
    for ($i = 0; $i < $count; ++$i)
    {
      $info = get_folder_info ($path . '/' . $folders[$i]['path']);
      if ($info !== false)
      {
        if ($info['permission'] == 'public')
        {
          return true;
        }

        continue;
      }
    }

    return false;
  }

  function clear_contents_cache ($dir)
  {
    return @unlink ($dir . '/.cache.php');
  }

  function get_contents ($dir, $ret = array (), $parent = '', $level = 0)
  {
    $ignore = array ('.', '..', '.cache.php', '.folder.php', '.htaccess');
    if (!(count ($ret)))
    {
      $ret = array ('dirs' => array (array ('name' => '<MAIN_FOLDER>', 'path' => '', 'level' => 0, 'size' => 0)), 'files' => array (), 'total_dirs' => 0, 'total_files' => 0, 'total_size' => 0);
    }

    $cache_file = $dir . '/.cache.php';
    if ($level == 0)
    {
      $DB = new DB ();
      if ($DB->open ($cache_file))
      {
        return $DB->all ();
      }
    }

    $fidx = ($parent == '' ? '<MAIN_FOLDER>' : $parent);
    if (false !== $h = @opendir ($dir))
    {
      $ret['files'][$fidx] = array ();
      $index = count ($ret['dirs']) - 1;
      $ret['dirs'][$index]['files'] = 0;
      $ret['dirs'][$index]['size'] = 0;
      while (false !== $f = readdir ($h))
      {
        if (!(in_array ($f, $ignore)))
        {
          if (is_dir ($dir . '/' . $f))
          {
            $path = trim ($parent . '/' . $f, '/');
            ++$ret['total_dirs'];
            $ret['dirs'][] = array ('name' => $f, 'path' => $path, 'level' => $level, 'size' => 0);
            $ret = get_contents ($dir . '/' . $f, $ret, $path, $level + 1);
            continue;
          }
          else
          {
            $size = filesize ($dir . '/' . $f);
            ++$ret['total_files'];
            $ret['total_size'] += $size;
            $ret['dirs'][$index]['size'] += $size;
            ++$ret['dirs'][$index]['files'];
            $ret['files'][$fidx][] = array ('name' => $f, 'size' => $size, 'date' => filemtime ($dir . '/' . $f), 'is_image' => (int)is_image ($dir . '/' . $f));
            continue;
          }

          continue;
        }
      }

      closedir ($h);
      if ($level == 0)
      {
        $DB = new DB ();
        if ($DB->create ($cache_file))
        {
          if ($DB->open ($cache_file))
          {
            $DB->set ($ret, 1);
            $DB->save ();
          }
        }
      }
    }

    return $ret;
  }

?>