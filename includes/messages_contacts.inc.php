<?php

$lang_contacts = array
(
	'contact_added'	=> '{username} has been added to your contact list. <a href="{contact_page_url}" class="special">See all of your contacts</a>',
	'title_main'	=> 'Your contacts',
	'title_add'		=> 'Add new contact',
	'title_edit'	=> 'Edit contact relationship',
);

?>