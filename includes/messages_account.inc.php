<?php
$lang_account = array
(
	'reg_disabled'		=> '{reason}',
	'reg_no_name'		=> 'Please enter a username.',
	'reg_short_name'	=> 'Your name must be at least {min_length} characters long.',
	'reg_long_name'		=> 'Your name cannot be longer than {max_length} characters long.',
	'reg_bad_name'		=> 'Your name contains one or more invalid characters. Please use letters and numbers only.',
	'reg_name_taken'	=> 'The name <strong>{username}</strong> has been taken. Please enter a different name.',
	'reg_disallowed_name'	=> 'The name you entered is not allowed.',
	'reg_no_pass'		=> 'Please enter a password in both fields.',
	'reg_pass_no_match'	=> 'The passwords you entered do not match.',
	'reg_no_email'		=> 'Enter a valid working email address.',
	'reg_invalid_email'	=> 'The email address you entered appears to be invalid. Example: someone@domain.com',
	'reg_email_exists'	=> 'The email address you entered already exists in our database.',
	'reg_act_email_subj'	=> 'Account activation',
	'reg_success1'		=> 'You have been registered <strong>{username}</strong>.',
	'reg_success2'		=> 'Your account must be activated. An email has been sent to <strong>{email}</strong> containing instructions on how to activate your account.',
	'reg_success3'		=> 'Your account must be approved by the administrator. You will be notified when your account has been approved.',

	'chk_no_name'		=> 'Please enter a username.',
	'chk_avail'		=> '<span style="color:green;">{username}</span> is available!',
	'chk_taken'		=> '<span style="color:red;">{username}</span> has been taken.',

	'act_no_email'		=> 'Please enter your email address.',
	'act_email_not_found'	=> 'That email address does not exist in our database.',
	'act_already_activated'	=> 'Your account has already been activated.',
	'act_email_sent'	=> 'An email has been sent to the address you entered. Please check it for further instructions.',
	'act_activated'		=> 'Congratulation <strong>{username}</strong>, your account has been activated!',
	'act_invalid_code'	=> 'The activation code you specified is invalid. To obtain a valid code, <a href="account.php?action=resend_activation" class="special">click here</a>.',
	'act_email_subj'	=> 'Uploader account activation',

	'pass_email_subj'	=> 'Your password has been reset',
	'pass_email_sub2'	=> 'Your uploader password',
	'pass_no_email'		=> 'Please enter your email address.',
	'pass_email_not_found'	=> 'That email address does not exist in our database.',
	'pass_sent'			=> 'Hello <strong>{username}</strong>, an email has been sent to you. Please check it for instructions on how to reset your password.',
	'pass_reset'		=> 'Your password has been successfully reset. Please check your email.',
	'pass_invalid'		=> 'You did not supply a valid password reset code. To obtain a valid code, <a href="account.php?action=password" class="special">click here</a>.',

	'log_no_input'		=> 'Please fill out both your username and password.',
	'log_bad_user'		=> 'That user does not exists.',
	'log_bad_password'	=> 'The password you entered is incorrect.',

	'email_invalid_code'	=> 'Invalid code specified.',
	'email_changed'		=> 'Your email address has been updated.',
);
?>