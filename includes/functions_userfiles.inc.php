<?



  function get_user_files_in_folder ($userid, $folder_id, $offset = 0, $limit = 0, $order_by = 'file_id', $order = 'DESC', $additional_where = '')
  {
    global $mysqlDB;
    $user_files = array ();
    if ($offset < 0)
    {
      $offset = 0;
    }

    $limit_str = ($limit ? 'LIMIT ' . $offset . ', ' . $limit : '');
    $order_str = 'ORDER BY ' . $order_by . ' ' . $order;
    if ($additional_where != '')
    {
      $additional_where = 'AND ' . $additional_where;
    }

    $query = 'SELECT files.*, users.username FROM uploader_userfiles AS files LEFT JOIN uploader_users AS users USING(userid) WHERE files.userid=' . $userid . ' AND files.folder_id=' . $folder_id . ' ' . $additional_where . ' ' . $order_str . ' ' . $limit_str;
    $result = $mysqlDB->query2 ($query);
    if (!($result->isGood ()))
    {
      exit ($mysqlDB->error (21, __FILE__));
    }

    $user_files = $result->fetchAllAssoc ();
    return $user_files;
  }

  function get_user_folders ($userid, $folder_id = 0, $extended_info = false)
  {
    global $mysqlDB;
    $folders = array ();
    $result = $mysqlDB->query2 ('SELECT username FROM uploader_users WHERE userid=' . $userid . ' LIMIT 1');
    if (!(($result->isGood () AND $result->rowCount ())))
    {
      return $folders;
    }

    $user = $result->fetchAssoc ();
    $result->free ();
    $where = ($folder_id ? 'AND folders.folder_id=' . $folder_id . ' ' : '');
    if ($extended_info)
    {
      $query = 'SELECT folders.*, COUNT(files.file_id) AS files_count, SUM(files.file_size) AS size FROM uploader_userfolders AS folders LEFT JOIN uploader_userfiles AS files USING(folder_id) ' . ('WHERE folders.userid=' . $userid . ' ' . $where . ' GROUP BY folders.folder_id ORDER BY folder_homefolder DESC, folder_name ASC');
    }
    else
    {
      $query = 'SELECT * FROM uploader_userfolders AS folders WHERE userid=' . $userid . ' ' . $where . ' ORDER BY folder_homefolder DESC, folder_name ASC';
    }

    $result = $mysqlDB->query2 ($query);
    if ($result->isGood ())
    {
      if ($result->rowCount ())
      {
        while (false !== $folder = $result->fetchAssoc ())
        {
          $folder['username'] = $user['username'];
          $folders[] = $folder;
        }
      }
    }
    else
    {
      if (!($result->isGood ()))
      {
        exit ($mysqlDB->error (62, __FILE__));
      }
    }

    $result->free ();
    if (count ($folders))
    {
      if ($folder_id)
      {
        return $folders[0];
      }
    }

    return $folders;
  }

?>