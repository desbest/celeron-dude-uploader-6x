<?php
// For ALL URLs and PATHs, a trailing slash MUST be added to the end of the string.
$UPL['SETTINGS'] = array
(
	// The absolute URL to the uploader directory.
	'uploader_url' => 'http://localhost/projects/cdupload/',

	// The absolute or relative path to the directory where user files will be stored.
	// The script will try to resolve relative paths to absolute path when necessary.
	'userfiles_dir' => 'ufiles/',

	// Use the uploader to view images. If disabled, links to images will be the direct link to the image file itself.
	// 1 is VERY recommended
	'uploader_view' => 1,

	// Enable/Disable user registration
	'reg' => 1,

	// This message will be displayed if registration is disabled.
	'regmsg' => 'Registration has been disabled',

	// Enable/Disable maintenance mode. If on, the users will not be able to access the uploader.
	// The admin section will still be available.
	'm' => 0,

	// This message will be displayed if maintenance mode is enabled.
	'm_msg' => 'The uploader is down for maintenance, we will be back up shortly. Sorry for the inconvenience.',

	// Enable/Disable automatic approval of NEW users. This will NOT approve existing users.
	'approval' => 1,

	// Require new users to active their account through email. This will NOT activate existing users.
	'activation_req' => 1,

	// Set who can access the member browsing feature.
	// The options are:
	// any: anyone, registered or unregistered
	// reg: only registered users
	// none: no one
	'browsing' => 'reg',

	// These file types CANNOT be uploaded by users even if their account permission is set to allow all file types.
	'filetypes' => 'php,php3,phtml,htaccess,htpasswd,cgi,pl,asp,aspx,cfm',

	// List of emails of admins
	'email' => 'admin@domain.com',

	// Notify the admins when a new user registers.
	'notify_reg' => 0,

	// User action logging modes:
	// 2: All relavent actions such as upload/delete/rename
	// 1: Just upload actions
	// 0: No logging
	'log' => 2,

	// Watermarking setting. The options are:
	// always: Always watermark user uploaded images.
	// user: Only watermark if a user account is set to do so.
	// never: Never watermark regardless of user account settings.
	'wm' => 'user',

	// Path to the watermark PNG file.
	'wm_path' => 'data/watermark.png',

	// The position of the watermark image relative to the original image.
	// The first setting is for horizontal alignment. The options are: left, center, bottom
	// The second setting is for vertical alignment. The options are: top, center, bottom
	'wm_pos' => 'center,bottom',

	// What to display on the "index" page. Choices are
	// public_uploader: the public uploader
	// announcement: uploader announcements. This is the default setting.
	'index_page'	=> 'public_uploader'
);
?>