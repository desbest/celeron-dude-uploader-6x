<?



  class db
  {
    var $_d = null;
    var $_f = null;
    var $_p = null;
    function db ()
    {
      $this->_d = array ();
      $this->_f = '';
      $p = false;
    }

    function open ($f, $c = false)
    {
      if (!(is_file ($f)))
      {
        if ($c)
        {
          return $this->create ($f);
        }

        return 0;
      }

      $this->_p = @fopen ($f, 'ab+');
      if (!(($this->_p AND flock ($this->_p, LOCK_SH))))
      {
        return 0;
      }

      include $f;
      $this->_f = $f;
      @set_file_buffer ($this->_p, 0);
      return 1;
    }

    function create ($f)
    {
      if (!(touch ($f)))
      {
        return 0;
      }

      chmod ($f, 511);
      return 1;
    }

    function remove ($f)
    {
      if (is_file ($f))
      {
        return @unlink ($f);
      }

      return 1;
    }

    function uset ($f)
    {
      unset ($this->_d[$f]);
    }

    function set ($f, $v = 0)
    {
      if (is_array ($f))
      {
        if ($v)
        {
          $this->_d = $f;
          return null;
        }

        while (list ($k, $v) = each ($f))
        {
          $this->set ($k, $v);
        }
      }
      else
      {
        $this->_d[$f] = $v;
      }

    }

    function get ($f)
    {
      if (isset ($this->_d[$f]))
      {
        return $this->_d[$f];
      }

      exit ('Field \'' . $f . '\' does not exist in ' . $this->_f);
    }

    function all ()
    {
      return $this->_d;
    }

    function close ()
    {
      if ($this->_p)
      {
        fclose ($this->_p);
      }

      $this->DB ();
    }

    function save ()
    {
      $p = &$this->_p;
      if (!($p))
      {
        exit ('Can\'t save, no file opened.');
      }

      $buf = sprintf ('<?php 
$this->_d=%s;
?>', var_export ($this->_d, 1));
      if ($p)
      {
        if (flock ($p, LOCK_EX))
        {
          ftruncate ($p, 0);
          fseek ($p, 0);
          fwrite ($p, $buf);
          fflush ($p);
          fclose ($p);
          return null;
        }
      }

      exit ('Could not open ' . $this->_f . ' for writting.');
    }
  }

  require_once 'constants.inc.php';
?>