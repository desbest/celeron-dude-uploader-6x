<?



  function mime_type ($f, $is_ext = 0)
  {
    $ext = ($is_ext ? $f : $ext = strtolower (trim (strrchr ($f, '.'), '.')));
    $mime = array ('avi' => 'video/x-msvideo', 'bmp' => 'image/bmp', 'css' => 'text/css', 'js' => 'application/x-javascript js', 'doc' => 'application/msword', 'gif' => 'image/gif', 'htm' => 'text/html', 'html' => 'text/html', 'jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'mov' => 'video/quicktime', 'mpeg' => 'video/mpeg', 'mp3' => 'audio/mpeg mpga mp2 mp3', 'pdf' => 'application/pdf', 'php' => 'text/html', 'png' => 'image/png', 'ppt' => 'application/vnd.ms-powerpoint', 'qt' => 'video/quicktime', 'rar' => 'application/x-rar', 'swf' => 'application/x-shockwave-flash swf', 'txt' => 'text/plain', 'torrent' => 'application/x-bittorrent', 'wmv' => 'video/x-ms-wmv', 'xml' => 'text/xml', 'xsl' => 'text/xml', 'xls' => 'application/msexcel x-excel', 'zip' => 'application/zip x-zip');
    if (isset ($mime[$ext]))
    {
      return $mime[$ext];
    }

    return 'application/octet-stream';
  }

?>