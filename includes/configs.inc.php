<?php
define ( "LICENSE_KEY", "ba18a35db8fe0adb2ce037dc7fcc6a25" );

// Unless specified, 1 means yes/on/enabled and 0 means no/off/disabled

// MySQL info
$UPL['MYSQL'] = array
(
	// this is the hostname to the server, it almost always is localhost
	'host'		=> 'localhost',

	// username for the database
	'username' 	=> 'root',

	// password for the database
	'password' => 'password',

	// the database name, you should have created a database with this name.
	'database' => 'database_name',

	// use persistent connection. 0 is the safe value.
	// you might get the "too many connections" error if you turn this on.
	'persistent' => 0

);


$UPL['CONFIGS'] = array
(
	// Name of the template directory
	'TEMPLATE_DIR'		=> 'default2',

	// Set this to 1 if your server supports mod_rewrite
	'MOD_REWRITE'		=> 1,

	// format for file upload date
	'TIME_FORMAT'   	=> 'M d, Y',

	// Other time formats
	'TIME_FORMAT2'  	=> 'm/d/y h:iA',

	// Simple time format
	'TIME_FORMAT3'		=> 'M d, Y',

	// SET THE DOMAIN PARAMETER OF COOKIES SET BY THE UPLOADER, IN MOST CASES YOU'D WANT TO LEAVE THIS ALONE
	'COOKIE_DOMAIN' 	=> NULL,

	// MUST BE IN THIS FORMAT 0xxx where x is an octal number. If 0755 doesn't work, use 0777 or 0666
	'CHMOD_TO'		=> 0777,

	// max folder name length, this should match the column size in the mysql table definition
	'FOLDER_MAX_LEN'        => 128,

	// min folder name length, should be at least 1
	'FOLDER_MIN_LEN'        => 4,

	// max file name length including the extension, this should match the column size in the mysql table definition
	'FILE_MAX_LEN'          => 128,

	// min file name length including extension, should be at least 1
	'FILE_MIN_LEN'          => 1,

	// default file list sorting: 'none', 'name', 'type', 'size', 'date'
	// Note: the default template tpl_myfiles.php sorts the files by date through javascript, so this option doesn't apply unless
	// you disable the sorting by editing myfiles.js.
	'DEFAULT_FILE_SORT_BY'	=> 'none',

	// default file list sorting order: 'asc' or 'dsc'
	// Note: same as the setting above, you must disable the javascript sorting in order for this setting to apply.
	'DEFAULT_FILE_SORT_ORDER'=> 'asc',

	// invalid characters in file name. Regex pattern
	'REGEX_INVALID_CHARS'	=> '#[\\\/\:\*\?\<\>\|\"]#',

	// These folder names are reserved. Users cannot rename or create folders with this name.
	// Enter folder names in lower case, checking will be case-INSENSITIVE.
	// You shouldn't change this unless you know why.
	'RESERVED_FOLDERNAMES'	=> array ( 'thumbs', 'main folder' ),

	// Default message inbox size
	'DEFAULT_MAX_MESSAGE'	=> 20,

	// How many users to show per page on the Browse page.
	'USERLIST_PERPAGE'	=> 25,

	// how many upload sets to show per page on the public upload Browse page
	'SETS_PERPAGE'		=> 15,

	// Maximum dimension of uploaded images. If either the width or the height of the image is greater than the
	// values below, the image will be discarded.
	'MAX_IMAGE_DIMENSION'	=> '5000x5000',

	// The following thumbnail dimensions should not be changed and applies to all thumbnails created by the
	// public uploader or registered uploader. You should
	// pick an appropiate set of dimensions for your layout and they should be finalized.
	// For large & small thumbs, the image will be resized in such a way that
	// it will fit within the dimensions specified. Either the width or the height
	// will be reduced and the image will maintain its proportionality.
	// The square thumbnail will be cropped so it will always be exactly the dimension entered.

	// Large thumb
	'LARGE_THUMB'		=> '350x500',

	// small thumb
	'SMALL_THUMB'		=> '80x120',

	// square thumb
	'SQUARE_THUMB'		=> '70x70',

	// Add original image info to SMALL thumbnails?
	// The small thumbnail is used to link to images so only it should display these info.
	// This applies to user images only, not public images. See pub_settings.inc.php if you want to do this to public images.
	'INFO_SMALL_THUMB'	=> 0,
);
?>