<?php
$lang_folders = array
(
	'folder_invalid'		=> 'That folder does not exist.',
	'folder_bad_char'		=> 'The folder name contains invalid characters.',
	'folder_no_name'		=> 'Please enter a folder name',
	'folder_long_name'		=> 'The folder name you entered is too long, please shorten it to within {length} characters.',
	'folder_short_name'     => 'The folder name you entered is too short. It must be at least {length} characters long.',
	'folder_exists'			=> 'There is already a folder with the name you entered. Please enter a different name.',
	'folder_no_perm_create'	=> 'You do not have the permission to create folders.',
	'folder_limit'			=> 'You have reached your folder limit, you cannot create more folders.',
	'folder_no_exists'		=> 'Unable to access user folder for {username}. This could be due to the userfiles folder not being CHMODed correctly.',
	'folder_name_reserved'	=> 'The folder name "{folder}" is reserved. Please enter another name.',
	'folder_cant_delete' 	=> 'This folder cannot be deleted.',
);
?>