<?



  class image
  {
    var $_im = null;
    var $_type = null;
    var $_width = null;
    var $_height = null;
    var $_path = null;
    function image ($p)
    {
      if (is_object ($p))
      {
        $w = imagesx ($p->_im);
        $h = imagesy ($p->_im);
        $this->_im = imagecreatetruecolor ($w, $h);
        if (!($this->_im))
        {
          exit ('create failed');
        }

        if (!(imagecopy ($this->_im, $p->_im, 0, 0, 0, 0, $w, $h)))
        {
          exit ('copy failed');
        }

        $this->_type = $p->_type;
        $this->_width = $p->_width;
        $this->_height = $p->_height;
        $this->_path = $p->_path;
        return 1;
      }

      $this->_im = false;
      $this->_type = '';
      $this->_width = 0;
      $this->_height = 0;
      $this->_path = '';
      if (!(is_file ($p)))
      {
        return 0;
      }

      if (false === $fp = @fopen ($p, 'rb'))
      {
        return 0;
      }

      $h = fread ($fp, 8);
      fclose ($fp);
      if ($h == '�PNG

')
      {
        $this->_im = imagecreatefrompng ($p);
        $this->_type = 'png';
      }
      else
      {
        if (substr ($h, 0, 2) == '��')
        {
          $this->_im = imagecreatefromjpeg ($p);
          $this->_type = 'jpeg';
        }
        else
        {
          if (substr ($h, 0, 3) == 'GIF')
          {
            $this->_im = imagecreatefromgif ($p);
            $this->_type = 'gif';
          }
          else
          {
            return 0;
          }
        }
      }

      $this->_width = imagesx ($this->_im);
      $this->_height = imagesy ($this->_im);
      $this->_path = $p;
      return 1;
    }

    function display ()
    {
      header ('Content-disposition: inline;filename="' . basename ($this->_path) . '"');
      header ('Content-type: image/' . $this->_type);
      switch ($this->_type)
      {
        case 'png':
        {
          imagepng ($this->_im);
          break;
        }

        case 'jpeg':
        {
          imagejpeg ($this->_im, null, 85);
          break;
        }

        case 'gif':
        {
          imagegif ($this->_im);
        }
      }

    }

    function resizeto ($new_width, $new_height, $keep_ratio = true, $crop = false)
    {
      $original_width = imagesx ($this->_im);
      $original_height = imagesy ($this->_im);
      $original_ratio = $original_width / $original_height;
      $new_ratio = $new_width / $new_height;
      $src_x = 0;
      $src_y = 0;
      $src_w = $original_width;
      $src_h = $original_height;
      if ($keep_ratio)
      {
        if (1 <= $original_ratio)
        {
          $resized_new_width = $new_height * $original_ratio;
          if ($new_width < $resized_new_width)
          {
            if ($crop)
            {
              $original_resized_width = $original_height * $new_ratio;
              $width_diff = $original_width - $original_resized_width;
              $src_x = $width_diff / 2;
              $src_w = $original_resized_width;
            }
            else
            {
              $new_height = $new_width / $original_ratio;
            }
          }
          else
          {
            $new_width = $resized_new_width;
          }
        }
        else
        {
          $resized_new_height = $new_width / $original_ratio;
          if ($new_height < $resized_new_height)
          {
            if ($crop)
            {
              $original_resized_height = $original_width / $new_ratio;
              $height_diff = $original_height - $original_resized_height;
              $src_y = floor ($height_diff / 2);
              $src_h = $original_height - ceil ($height_diff);
            }
            else
            {
              $new_width = $new_height * $original_ratio;
            }
          }
          else
          {
            $new_height = $resized_new_height;
          }
        }
      }

      $new_im = imagecreatetruecolor (ceil ($new_width), ceil ($new_height));
      if (!($new_im))
      {
        echo 'NW: ' . $new_width . ', NH: ' . $new_height;
        return false;
      }

      imagecopyresampled ($new_im, $this->_im, 0, 0, $src_x, $src_y, $new_width, $new_height, $src_w, $src_h);
      imagedestroy ($this->_im);
      $this->_im = &$new_im;
    }

    function crop ($width, $height, $horizontal_anchor = 'center', $vertical_anchor = 'center')
    {
      $original_width = imagesx ($this->_im);
      $original_height = imagesy ($this->_im);
      $width_diff = $original_width - $width;
      $height_diff = $original_height - $height;
      switch ($horizontal_anchor)
      {
        case 'left':
        {
          $src_x = 0;
        }

        case 'center':
        {
          $src_x = $width_diff / 2;
        }

        case 'right':
        {
          $src_x = $width_diff;
          switch ($vertical_anchor)
          {
            case 'top':
            {
              $src_y = 0;
            }

            case 'center':
            {
              $src_y = $height_diff / 2;
            }

            case 'bottom':
            {
              $src_y = $height_diff;
              $new_im = imagecreatetruecolor ($width, $height);
              imagecopy ($new_im, $this->_im, 0, 0, $src_x, $src_y, $width, $height);
              imagedestroy ($this->_im);
              $this->_im = &$new_im;
              return null;
            }
          }
        }
      }
    }

    function addinfo ($s = 1, $d = 1)
    {
      $w = imagesx ($this->_im);
      $h = imagesy ($this->_im);
      $font = 2;
      if ($s)
      {
        if ($d)
        {
          $str = sprintf ('%dx%d - %s', $this->_width, $this->_height, $this->_getsize (filesize ($this->_path)));
        }
        else
        {
          if ($s)
          {
            $str = $this->_getsize (filesize ($this->_path));
          }
        }
      }
      else
      {
        if ($d)
        {
          $str = sprintf ('%dx%d', $this->_width, $this->_height);
        }
      }

      $strw = imagefontwidth ($font) * strlen ($str);
      if ($w <= $strw)
      {
        $str = sprintf ('%dx%d', $this->_width, $this->_height);
        $strw = imagefontwidth ($font) * strlen ($str);
      }

      $white = imagecolorallocate ($this->_im, 255, 255, 255);
      $black = (function_exists ('imagecolorallocatealpha') ? imagecolorallocatealpha ($this->_im, 0, 0, 0, 80) : imagecolorallocate ($this->_im, 0, 0, 0));
      imagefilledrectangle ($this->_im, 0, $h - imagefontheight ($font), $w, $h, $black);
      imagestring ($this->_im, $font, ($w - $strw) / 2, $h - imagefontheight ($font), $str, $white);
    }

    function export ($d, $jpeg_quality = 80)
    {
      $r = 0;
      switch ($this->_type)
      {
        case 'png':
        {
          $r = imagepng ($this->_im, $d);
          break;
        }

        case 'jpeg':
        {
          $r = imagejpeg ($this->_im, $d, $jpeg_quality);
          break;
        }

        case 'gif':
        {
          $r = imagegif ($this->_im, $d);
          break;
        }
      }

      exit ('Unknown type');
    }

    function watermark ($wm, $wo = 'center', $ho = 'bottom')
    {
      if ($this->_type == 'gif')
      {
        return 1;
      }

      $w = imagesx ($this->_im);
      $h = imagesy ($this->_im);
      $inf_wm = getimagesize ($wm);
      if (is_array ($inf_wm))
      {
        if (!((!($this->_width <= $inf_wm[0]) AND !($this->_height <= $inf_wm[1]))))
        {
          return 1;
        }

        $im_wm = imagecreatefrompng ($wm);
        if (!($im_wm))
        {
          return 0;
        }

        switch ($wo)
        {
          case 'left':
          {
            $x = 0;
          }

          case 'right':
          {
            $x = $w - $inf_wm[0];
          }

          case 'top':
          {
            $y = 0;
            break;
          }

          case 'bottom':
          {
            $y = $h - $inf_wm[1];
            break;
          }

          case 'center':
          {
          }
        }

        while (true)
        {
          $y = ($h - $inf_wm[1]) / 2;
          if (true)
          {
            $r = imagecopy ($this->_im, $im_wm, $x, $y, 0, 0, $inf_wm[0], $inf_wm[1]);
            imagedestroy ($im_wm);
            return $r;
          }
        }
      }

      return 0;
    }

    function destroy ()
    {
      @imagedestroy ($this->_im);
      $this->_im = null;
    }

    function getdimension ()
    {
      return array (imagesx ($this->_im), imagesy ($this->_im));
    }

    function _getsize ($s, $u = 'B')
    {
      while (1024 < $s)
      {
        $s /= 1024;
        switch ($u)
        {
          case 'B':
          {
            $u = 'KB';
          }

          case 'KB':
          {
            $u = 'MB';
            break;
          }

          case 'MB':
          {
            $u = 'GB';
            break;
          }

          case 'GB':
          {
            $u = 'TB';
            break;
          }
        }
      }
    }
  }

?>