<?php
$lang_myfiles = array
(
	'invalid_file'		=> 'Invalid file, perhaps it was deleted.',
	'folder_no_exists'	=> 'Unable to access user folder for {username}. This could be due to the userfiles folder not being CHMODed correctly.',
	'folder_invalid'	=> 'That folder does not exist.',
	'no_gd'				=> 'This feature is disabled because the server does not have the required tools. Need <a href="http://www.boutell.com/gd/">GD 2.0</a>',
	'file_not_supported'	=> 'File not found or not specified.',
	'type_not_supported'	=> 'File is not an image or image type is not supported.',
	'zero_dimension'	=> 'The image dimension cannot be zero.',
	'smaller_only'		=> 'The resized image dimensions cannot be greater than or equal the original image dimensions. You can only shrink the image, not enlarge it.',
	'resize_success'	=> 'A new image has been created. You will be taken to the image.',
	'no_resize_perm'	=> 'You do not have the permission to resize files.',
	'no_rename_perm'	=> 'You do not have the permission to rename files.',
	'blank_filename'	=> 'Please enter the file name.',
	'long_name'			=> 'The file name is too long. It must be no more than {length} characters in length.',
	'short_name'		=> 'The file name is too short. It must be at least {length} characters in length.',
	'file_exists'		=> 'There is already a file with that same name. Please enter a different name.',
	'filename_only'		=> 'You cannot change the file extension. You are allowed to change only the filename.',
	'move_non_image'	=> '{filename}, this file is not a recognizable image. You can only move and upload images into a photo folder.',
	'move_exists'		=> '{filename}, this file already exists in the destination folder.',
	'files_not_moved'	=> 'Some files were not moved due the following reasons:',
	'title_rename'		=> 'File rename',
	'title_rename_error'=> 'File rename error',
	'title_default'		=> 'My files',
	'title_invalid_folder' => 'Invalid folder',
);
?>