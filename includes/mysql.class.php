<?



  class mysqldb
  {
    var $_l = null;
    var $_r = null;
    var $_q = null;
    function mysqldb ($h, $u, $p, $db, $ps = 0)
    {
      $this->_l = false;
      $this->_r = false;
      $this->_q = 0;
      $this->_r = 0;
      $t = 'mysql_' . ($ps ? 'p' : '') . 'connect';
      $this->_l = $t ($h, $u, $p);
      if (!(($this->_l AND mysql_select_db ($db))))
      {
        exit ($this->error (5, __FILE__));
      }

    }

    function query ($q)
    {
      trim ($q, ';');
      ++$this->_q;
      $this->_r = @mysql_query ($q, $this->_l);
      return $this->_r !== false;
    }

    function getrow ()
    {
      if ($this->_r)
      {
        return mysql_fetch_row ($this->_r);
      }

      return false;
    }

    function getassoc ()
    {
      if ($this->_r)
      {
        return mysql_fetch_array ($this->_r, MYSQL_ASSOC);
      }

      return false;
    }

    function getarray ()
    {
      if ($this->_r)
      {
        return mysql_fetch_array ($this->_r, MYSQL_NUM);
      }

      return false;
    }

    function getinsertid ()
    {
      return mysql_insert_id ($this->_l);
    }

    function getrowcount ()
    {
      if ($this->_r)
      {
        return mysql_num_rows ($this->_r);
      }

      return 0;
    }

    function getaffectrowcount ()
    {
      if ($this->_r)
      {
        return mysql_affected_rows ($this->_l);
      }

      return 0;
    }

    function error ($l = -1, $f = '')
    {
      return 'mySQL: ' . mysql_error () . (' on line ' . $l . ' in ' . $f);
    }

    function escape ($s)
    {
      if (function_exists ('mysql_real_escape_string'))
      {
        return mysql_real_escape_string ($s, $this->_l);
      }

      return addslashes ($s);
    }

    function buildinsertstatement ($arr)
    {
      $ins = array ();
      reset ($arr);
      while (list ($c, $v) = each ($arr))
      {
        $ins[] = ($v === NULL ? sprintf ('`%s`=NULL', $c) : sprintf ('`%s`=\'%s\'', $c, $v));
      }

      return implode (', ', $ins);
    }

    function free ()
    {
      return @mysql_free_result ($this->_r);
    }

    function query2 ($q)
    {
      trim ($q, ';');
      ++$this->_q;
      return new mysqlResult (mysql_query ($q, $this->_l));
    }
  }

  class mysqlresult
  {
    var $_rsrc = null;
    function mysqlresult ($rsrc)
    {
      $this->_rsrc = &$rsrc;
    }

    function error ()
    {
      return $this->_rsrc === false;
    }

    function isgood ()
    {
      return $this->_rsrc !== false;
    }

    function rowcount ()
    {
      if ($this->_rsrc)
      {
        return mysql_num_rows ($this->_rsrc);
      }

      return 0;
    }

    function fetchassoc ()
    {
      if ($this->_rsrc)
      {
        return mysql_fetch_array ($this->_rsrc, MYSQL_ASSOC);
      }

      return false;
    }

    function fetcharray ()
    {
      if ($this->_rsrc)
      {
        return mysql_fetch_array ($this->_rsrc, MYSQL_NUM);
      }

      return false;
    }

    function fetchallassoc ()
    {
      $r = array ();
      if (!($this->_rsrc))
      {
        return $r;
      }

      while (false !== $c = $this->fetchAssoc ())
      {
        $r[] = $c;
      }

      $this->free ();
      return $r;
    }

    function fetchallarray ()
    {
      $r = array ();
      if (!($this->_rsrc))
      {
        return $r;
      }

      while (false !== $c = $this->fetchArray ())
      {
        $r[] = $c;
      }

      $this->free ();
      return $r;
    }

    function free ()
    {
      @mysql_free_result ($this->_rsrc);
      $this->_rsrc = false;
    }
  }

?>