<?php
// shared between slideshows.php and view.php

$lang_view = array
(
	'file_unavailable'		=> 'The file you have requested is not available. It may have been removed or you do not have the permission to view it.',
	'folder_unavailable'	=> 'The folder you have requested is not available. It may have been removed or you do not have the permission to view it.',
	'folder_not_gallery'	=> 'This folder is not a photo folder and cannot be viewed as a slideshow. Would you like to <a href="{browse_url}" class="special">Browse</a> its contents instead?',
	'no_images'				=> 'There are no images in this folder. The slideshow cannot start.',

	'title_slideshow'	=> 'Slideshow of <{folder_name}>',
);
?>