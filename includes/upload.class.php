<?



  class upload
  {
    var $_s = null;
    var $_f = null;
    function upload ()
    {
      $this->_s = array ('allowed_extensions' => 'all', 'unallowed_extensions' => 'php,php3,phtml,htaccess,htpasswd,cgi,pl,asp,aspx,cfm', 'max_file_size' => 512000);
    }

    function set ($n, $v)
    {
      $this->_s[$n] = $v;
    }

    function ext ($f)
    {
      return strtolower (trim (strrchr ($f, '.'), '.'));
    }

    function validate ($f, &$e)
    {
      if (!(isset ($this->_s['aa'])))
      {
        $this->_s['aa'] = explode (',', $this->_s['allowed_extensions']);
      }

      if (!(isset ($this->_s['ua'])))
      {
        $this->_s['ua'] = explode (',', $this->_s['unallowed_extensions']);
      }

      $e = 'none';
      if (!(is_array ($f)))
      {
        $e = 'not_an_array';
      }
      else
      {
        if ($f['name'] == '')
        {
          $e = 'file_empty';
        }
        else
        {
          if ($f['size'] == '')
          {
            $e = 'file_empty';
          }
          else
          {
            if ($f['error'] != 0)
            {
              $e = 'http_error';
            }
            else
            {
              if ($this->_s['allowed_extensions'] != '')
              {
                if ($this->_s['allowed_extensions'] != 'all')
                {
                  if (!(in_array ($this->ext ($f['name']), $this->_s['aa'])))
                  {
                    $e = 'extension_not_allowed';
                  }
                }
              }
              else
              {
                if ($this->_s['unallowed_extensions'] != '')
                {
                  if ($this->_s['unallowed_extensions'] != 'none')
                  {
                    if (in_array ($this->ext ($f['name']), $this->_s['ua']))
                    {
                      $e = 'extension_not_allowed';
                    }
                  }
                }
                else
                {
                  if (!(is_file ($f['tmp_name'])))
                  {
                    $e = 'file_not_uploaded';
                  }
                  else
                  {
                    if ($this->_s['max_file_size'])
                    {
                      if ($this->_s['max_file_size'] < filesize ($f['tmp_name']))
                      {
                        $e = 'max_file_size';
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      return $e == 'none';
    }
  }

?>