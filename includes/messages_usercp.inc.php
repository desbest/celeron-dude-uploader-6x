<?php
$lang_usercp = array
(
	'ucp_no_pass'		=> 'Please enter your current password.',
	'ucp_incorrect_pass'=> 'The password you entered is incorrect.',
	'ucp_pass_no_match'	=> 'The new passwords do not match',
	'ucp_bad_email'		=> 'The email address appears to be invalid. Expecting someone@domain.',
	'ucp_email_exists'	=> 'The email you have entered already exists in our database. Please note that multiple accounts are not permitted.',
	'ucp_saved'			=> 'Your account has been updated. If you have changed your email address, an email will be sent to this new address in order to verify it. Please read it for further instructions.',
	'ucp_no_user'		=> 'Please enter the name of the recipient.',
	'ucp_no_subject'	=> 'Please enter a subject.',
	'ucp_no_message'	=> 'Please enter the message you want to send.',
	'ucp_message_sent'	=> 'Message has been delivered.',
	'ucp_bad_user'		=> 'That user does not exist.',
	'ucp_message_denied'	=> 'The user <strong>{username}</strong> does not wish to receive messages.',
	'ucp_inbox_full'	=> 'The recipient\'s inbox is currently full.',
	'ucp_long_message'	=> 'Your message is too long, please shorten it.',
	'ucp_long_subject'	=> 'Your subject is too long, please shorten it.',
	'ucp_email_subject'	=> 'Email change confirmation required',
	'ucp_bad_id'		=> 'Invalid message ID',
);
?>