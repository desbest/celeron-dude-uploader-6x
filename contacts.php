<?php
define ( 'CONTACTS_PHP', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_contacts.inc.php';
$tpl_contacts = new Template ( TPL_DIR . 'tpl_contacts.php' );
$tpl_contacts->set('action', $action);
$tpl_contacts->set('back_url', previous_page(UPLOADER_URL.(MOD_REWRITE?'contacts':'contacts.php')));
$tpl_uploader->set('page_title', $lang_contacts['title_main'] );

switch ( $action )
{
	case 'add':
	{
		//$use = trim(gpc('use', 'G', 'userid'));
		$userid = (int)gpc ( 'userid', 'GP', -1 );
		$username = $mysqlDB->escape ( gpc ( 'username', 'GP', '' ) );
		$userinfo = array();

		// get user info
		$mysqlDB->query("SELECT userid, username FROM uploader_users WHERE userid=$userid OR username='$username' LIMIT 1");
		if($mysqlDB->getRowCount())
		{
			// found user!
			$userinfo = $mysqlDB->getAssoc();
			processUser($userinfo, false);
			$mysqlDB->free();

			// check user
			$relationship = compute_contact_relationship ( $UPL['USER']['userid'], $userinfo['userid'] );

			if ( $relationship['is_contact'] )
			{
				// already a contact, edit
				go_to($userinfo['edit_contact_url']);
				exit;
			}
			else
			{
				if ( $task == 'add' )
				{
					$is_friend = (int)gpc('friend', 'P', 0);
					$is_family = (int)gpc('family', 'P', 0);

					$contact_type = 0;
					if($is_friend) $contact_type |= CONTACT_FRIEND;
					if($is_family) $contact_type |= CONTACT_FAMILY;

					$insert = array
					(
						'contact_id'	=> NULL,
						'contact_userid'=> $userinfo['userid'],
						'contact_type'	=> $contact_type,
						'userid'		=> $USER['userid'],
					);

					if ( !$mysqlDB->query ( "INSERT INTO uploader_usercontacts SET " . $mysqlDB->buildInsertStatement ( $insert ) ) ) exit ( $mysqlDB->error() );

					$tpl_message->set('message_title', 'Contact added!');
					$msg = parse ( $lang_contacts['contact_added'], array ( '{username}' => $userinfo['username'], '{contact_page_url}' => ( UPLOADER_URL . ( MOD_REWRITE ? 'contacts' : 'contacts.php' ) ) ) );
					$tpl_message->set('message', $msg );
					$tpl_message->set('back_url', $userinfo['info_url']);
					$tpl_uploader->set('content', $tpl_message, 1);
				}
				else
				{
					// Show form to user
					$tpl_uploader->set('page_title', $lang_contacts['title_add']);
					$tpl_contacts->set('user', $userinfo);
					$tpl_uploader->set('content', $tpl_contacts, 1);
				}
			}
		}
		else
		{
			// invalid user
			$tpl_message->set('message', 'Invalid user');
			$tpl_uploader->set('content', $tpl_message, 1);
		}
	}
	break;

	case 'delete':
	{
		$userid = (int)gpc ( 'userid', 'GP', -1 );
		$username = $mysqlDB->escape ( gpc ( 'username', 'GP', '' ) );
		$userinfo = array();

		// get user info
		$result = $mysqlDB->query2("SELECT userid, username FROM uploader_users WHERE userid=$userid OR username='$username' LIMIT 1");

		if ( $result->isGood() )
		{
			if ( $result->rowCount() )
			{
				$userinfo = $result->fetchAssoc();
				$result->free();
				$result = $mysqlDB->query2("DELETE FROM uploader_usercontacts WHERE userid={$USER['userid']} AND contact_userid={$userinfo['userid']}");
				if(!$result->isGood())exit($mysqlDB->error(__LINE_,__FILE__));
			}
			go_to();
		}
		else exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	}
	break;

	case 'edit':
	{
		$userid = (int)gpc ( 'userid', 'GP', -1 );
		$username = $mysqlDB->escape ( gpc ( 'username', 'GP', '' ) );
		$userinfo = array();

		// get user info
		$result = $mysqlDB->query2 ( "SELECT c.contact_id, c.contact_userid, c.contact_type, u.userid, u.username FROM uploader_usercontacts AS c "
		. "LEFT JOIN uploader_users AS u ON u.userid=c.contact_userid "
		. "WHERE c.userid={$USER['userid']} AND (c.contact_userid=$userid OR u.username='$username')" );

		if ( $result->isGood() )
		{
			if ( $result->rowCount() )
			{
				$userinfo = $result->fetchAssoc();
				$result->free();

				if ( $task == 'save' )
				{
					// save changes
					$back_url = gpc ( 'back_url', 'P', UPLOADER_URL . (MOD_REWRITE?'contacts':'contacts.php') );
					$is_friend = (int)gpc('friend', 'P', 0);
					$is_family = (int)gpc('family', 'P', 0);
					$do_remove = (int)gpc('remove', 'P', 0);

					if ( $do_remove )
					{
						$mysqlDB->query ( "DELETE FROM uploader_usercontacts WHERE contact_id={$userinfo['contact_id']}");
					}
					else
					{
						$contact_type = 0;
						if($is_friend) $contact_type |= CONTACT_FRIEND;
						if($is_family) $contact_type |= CONTACT_FAMILY;
						$mysqlDB->query ( "UPDATE uploader_usercontacts SET contact_type=$contact_type WHERE contact_id={$userinfo['contact_id']}" );
					}
					go_to($back_url);
				}
				else
				{
					$userinfo['relationship'] = array
					(
						'is_contact'	=> true,
						'is_friend'		=> (bool)($userinfo['contact_type'] & CONTACT_FRIEND),
						'is_family'		=> (bool)($userinfo['contact_type'] & CONTACT_FAMILY)
					);
					$tpl_uploader->set('page_title', $lang_contacts['title_edit']);
					$tpl_contacts->set ( 'back_url', previous_page ( UPLOADER_URL . (MOD_REWRITE?'contacts':'contacts.php') ) );
					$tpl_contacts->set ( 'user', $userinfo );
					$tpl_uploader->set ( 'content', $tpl_contacts, 1 );
					// show form
				}
			}
			else exit ( 'Invalid user' );
		}
		else exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );
	}
	break;

	default:
	{
		$filter_contact_type = gpc ( 'type', 'G', 'all' );

		switch ( $filter_contact_type )
		{
			case 'friend': $contact_type = 'AND contact_type & ' . CONTACT_FRIEND; break;
			case 'family': $contact_type = 'AND contact_type & ' . CONTACT_FAMILY; break;
			case 'both': $contact_type = 'AND contact_type = ' . (CONTACT_FRIEND|CONTACT_FAMILY); break;
			default: case 'all': $contact_type = ''; break;
		}

		// Show all contacts
		$contacts = array();
		$result = $mysqlDB->query2 ( "SELECT c.contact_id, c.contact_userid, c.contact_type, u.userid, u.username FROM uploader_usercontacts AS c "
		. "LEFT JOIN uploader_users AS u ON c.contact_userid=u.userid "
		. "WHERE c.userid={$USER['userid']} $contact_type GROUP BY c.contact_id" );

		if ( $result->isGood() )
		{
			if ( $result->rowCount() )
				while ( false !== ( $contact = $result->fetchAssoc() ) )
				{
					$contact['relationship'] = array
					(
						'is_contact'	=> true,
						'is_friend'		=> (bool)($contact['contact_type'] & CONTACT_FRIEND),
						'is_family'		=> (bool)($contact['contact_type'] & CONTACT_FAMILY)
					);
					processUser ( $contact );
					$contacts[] = $contact;
				}
			$result->free();
		}
		else exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

		$filter_base_url = UPLOADER_URL . ( MOD_REWRITE ? 'contacts' : 'contacts.php' );
		$filter_url = array
		(
			'all'		=> $filter_base_url,
			'friend'	=> $filter_base_url . '?type=friend',
			'family'	=> $filter_base_url . '?type=family',
			'both'		=> $filter_base_url . '?type=both',
		);

		$tpl_contacts->set ( 'filter_contact_type', $filter_contact_type );
		$tpl_contacts->set ( 'filter_url', $filter_url );
		$tpl_contacts->setr ( 'contacts', $contacts );
		$tpl_uploader->set('content', $tpl_contacts, 1);
	}
}
?>