<?php
define ( 'NO_PERSISTENT', 1 );
define ( 'NO_AUTH_CHECK', 1 );
require_once 'includes/commons.inc.php';
require_once 'includes/messages_view.inc.php';

if ( $action == 'full' )
{
	$tpl_view = new Template ( TPL_DIR . 'tpl_view_image_full.php' );
	$file_id = (int)gpc ( 'file_id', 'G', 0 );
	$file_key = trim(gpc('file_key', 'G', ''));

	$mysqlDB->query ( "SELECT * FROM uploader_userfiles WHERE file_id=$file_id;" );

	if($mysqlDB->getRowCount())
	{
		$file = $mysqlDB->getAssoc();

		$mysqlDB->query ( "SELECT * FROM uploader_userfolders WHERE folder_id={$file['folder_id']} LIMIT 1" );
		$folder = $mysqlDB->getAssoc();

		$mysqlDB->query ( "SELECT * FROM uploader_users WHERE userid={$file['userid']} LIMIT 1" );
		$user = $mysqlDB->getAssoc();
		$folder['username'] = $user['username'];

		// check key first
		if($file['file_key'] != $file_key) exit(go_to(UPLOADER_URL));

		// is the owner of this image viewing it?
		$owner_viewing = ( $USER['logged_in'] && $USER['userid'] == $file['userid'] );

		// process
		processFile ( $file );
		processUser ( $user, false );
		processFolder ( $folder, true, false );

		// is file viewable? public?
		if( !$owner_viewing && $folder['permission']['access']=='private')
		{
			if ( !$USER['logged_in'] )
			{
				$msg = parse($lang_commons['not_logged_in'], array('{login_url}'=>UPLOADER_URL.(MOD_REWRITE?'login?':'account.php?action=login&amp;').'return_url='.rawurlencode(current_page()), '{register_url}'=>UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')));
				$tpl_message->set ( 'message', $msg );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}

			$relationship = compute_contact_relationship ( $user['userid'], $USER['userid'] );
			$allowed = ( $folder['permission']['family'] && $relationship['is_family'] ) || ( $folder['permission']['friend'] && $relationship['is_friend'] );
			if ( !$allowed )
			{
				$tpl_message->set ( 'message', $lang_view['file_unavailable'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}
		}

		// is file really an image?
		if(!$file['file_isimage'])exit(go_to($file['url']));

		$tpl_view->set ( 'folder', $folder );
		$tpl_view->set ( 'user', $user );
		$tpl_view->set ( 'file', $file, 1 );
	}
	else go_to(UPLOADER_URL);

}
else
{
	$tpl_view = new Template ( TPL_DIR . 'tpl_view_image.php' );

	$file_id = (int)gpc ( 'file_id', 'G', 0 );
	$file_key = trim(gpc('file_key', 'G', ''));

	$mysqlDB->query ( "SELECT * FROM uploader_userfiles WHERE file_id=$file_id;" );

	if($mysqlDB->getRowCount())
	{
		$file = $mysqlDB->getAssoc();

		$mysqlDB->query ( "SELECT * FROM uploader_userfolders WHERE folder_id={$file['folder_id']} LIMIT 1" );
		$folder = $mysqlDB->getAssoc();

		$mysqlDB->query ( "SELECT * FROM uploader_users WHERE userid={$file['userid']} LIMIT 1" );
		$user = $mysqlDB->getAssoc();
		$folder['username'] = $user['username'];

		// check key first
		if($file['file_key'] != $file_key) exit(go_to(UPLOADER_URL));

		// is the owner of this image viewing it?
		$owner_viewing = ( $USER['logged_in'] && $USER['userid'] == $file['userid'] );

		// process
		processFile ( $file );
		processUser ( $user, false );
		processFolder ( $folder, true, false );

		// is file viewable? public?
		if( !$owner_viewing && $folder['permission']['access']=='private')
		{
			if ( !$USER['logged_in'] )
			{
				$msg = parse($lang_commons['not_logged_in'], array('{login_url}'=>UPLOADER_URL.(MOD_REWRITE?'login?':'account.php?action=login&amp;').'return_url='.rawurlencode(current_page()), '{register_url}'=>UPLOADER_URL.(MOD_REWRITE?'register':'account.php?action=register')));
				$tpl_message->set ( 'message', $msg );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}
			$relationship = compute_contact_relationship ( $user['userid'], $USER['userid'] );
			$allowed = ( $folder['permission']['family'] && $relationship['is_family'] ) || ( $folder['permission']['friend'] && $relationship['is_friend'] );
			if ( !$allowed )
			{
				$tpl_message->set ( 'message', $lang_view['file_unavailable'] );
				$tpl_uploader->set ( 'content', $tpl_message, 1 );
				exit;
			}
		}

		// is file really an image?
		if(!$file['file_isimage'])exit(go_to($file['url']));

		// get total images in this folder
		$folder['total_images'] = 0;
		$mysqlDB->query("SELECT COUNT(file_id) AS total_images FROM uploader_userfiles WHERE folder_id={$file['folder_id']} AND file_isimage=1");
		if($mysqlDB->getRowCount())
		{
			$result = $mysqlDB->getAssoc();
			$folder['total_images'] = $result['total_images'];
		}

		// get position
		$file['position'] = 0;
		$mysqlDB->query("SELECT COUNT(file_id) AS position FROM uploader_userfiles WHERE folder_id={$file['folder_id']} AND file_isimage=1 AND file_id<{$file['file_id']}");
		if($mysqlDB->getRowCount())
		{
			$result = $mysqlDB->getAssoc();
			$file['position'] = $result['position']+1;
		}

		$previous_file = array();
		$next_file = array();

		// previous
		$mysqlDB->query("SELECT * FROM uploader_userfiles WHERE file_id < {$file['file_id']} AND folder_id={$file['folder_id']} AND file_isimage=1 ORDER BY file_id DESC LIMIT 1");
		if($mysqlDB->getRowCount())
		{
			$previous_file = $mysqlDB->getAssoc();
			processFile($previous_file);
		}
		// next
		$mysqlDB->query("SELECT * FROM uploader_userfiles WHERE file_id > {$file['file_id']} AND folder_id={$file['folder_id']} AND file_isimage=1 ORDER BY file_id ASC LIMIT 1");
		if($mysqlDB->getRowCount())
		{
			$next_file = $mysqlDB->getAssoc();
			processFile($next_file);
		}

		// get comments
		$comments = array();
		$result = $mysqlDB->query2("SELECT comment.*, user.username FROM uploader_usercomments AS comment LEFT JOIN uploader_users AS user USING(userid) WHERE object_id={$file['file_id']} AND comment_type=" . COMMENT_FILE . " ORDER BY comment_id ASC");
		if ( $result->isGood() )
		{
			if ( $result->rowCount() )
			{
				while ( false !== ( $comment = $result->fetchAssoc() ) )
				{
					processUser ( $comment, false );
					$comment['is_mine'] = ( $comment['userid'] == $USER['userid'] );
					$comment['delete_url'] = UPLOADER_URL . ( MOD_REWRITE ? 'comment' : 'comment.php' ) . '?action=delete_image_comment&amp;comment_id=' . $comment['comment_id'];
					$comments[] = $comment;
				}
			}
			$result->free();
		}
		else exit ( $mysqlDB->error ( __LINE__, __FILE__ ) );

		$tpl_view->setr ( 'comments', $comments );
		$tpl_view->set ( 'owner_viewing', $owner_viewing );
		$tpl_view->set ( 'next_file', $next_file );
		$tpl_view->set ( 'previous_file', $previous_file );
		$tpl_view->set ( 'folder', $folder );
		$tpl_view->set ( 'user', $user );
		$tpl_view->set ( 'file', $file, 1 );
	}
	else go_to(UPLOADER_URL);
}
?>