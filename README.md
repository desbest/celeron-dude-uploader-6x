# Celeron Dude Uploader 6.3.0

This script was made by [Celeron Dude](http://celerondude.com) and as his website is gone for years I thought I'd upload his software for free.

Uploader version 6.3.0 is not backwards compatible with Uploader version 5.3, they are both completely different.

## About

This script allows you to run a "file hosting" service on your web server.

## Release Date

6.3.0 -----> 7/17/06 [older versions are here](https://archive.is/UIMum)

## Features

This is a short list of important features. The full feature list for the latest version is available here. For a demo of these features in action, check out CDUpload.com.

* User registration with individual account space and bandwidth allocation/limit. Each account has its own seperate storage space. You can manage them with the web based admin tools.
* Bandwidth tracking and limiting (requires mod_rewrite which means Apache)
* IIS servers with ISAPI_Rewrite will need to port the rewrite rules. The syntaxes are VERY similar to each other so if you can do it you can ignore this requirement.
* Batch file uploading, either by selection or by Zip file (requires gzip in PHP).
* Version 6.1 requires the additional Zip extension. Version 6.2 and after do not. Versions prior to 6.1 does not support batch uploading through Zip files.
* Image galleries, or photo folder, with thumbnails and slideshows. The GD graphics library (version 2) extension for PHP is required for thumbnail creation. Photo folder feature is only available in version 6.2 and up. Slideshow feature is only available in version 6.3 and up.
* Fully skinnable via template files. Two basic template sets are included.

## Requirements

The following are required to run this script. You may be able to get by with older versions but you may need to do some modifications.

* Apache web server with mod_rewrite or a one with URL rewriting capability.
* PHP 4.1.0 and up (minimally). Version 4.3.0 and up is recommended. PHP 5 untested but based on reports the script runs just fine.
* Safe mode in PHP must be OFF
* GD2 for thumbnail creation.
* Zip extension required for batch file uploading. Version 6.2 and up does not require this extension.
* Version 6.2 and up requires MySQL 3.0 and up. Although I only tested with 4.0, I didn't use any new features that are only available in 4.0 so 3.0 should be fine. I don't think any host is stupid enough to still run anything under 4.0
* Version 6.3 and up only: Zend optimizer


