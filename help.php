<?php
define ( 'NO_AUTH_CHECK', 1 );
require_once ( 'includes/commons.inc.php' );

$topic = gpc ( 'topic', 'G', '' );

if ( $topic == 'batch_upload' )
{
	$tpl_help = new Template ( TPL_DIR . 'tpl_batchupload.php' );
	$tpl_uploader->set ( 'content', $tpl_help, 1 );
}
else header ( 'Location: index.php' );
?>