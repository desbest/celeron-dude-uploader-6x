<?
/***********************************************************************/
/*                                                                     */
/*  Decoded by DGT and Friends                                         */
/*                                                                     */
/***********************************************************************/


  define ('ADMIN', 1);
  require_once 'includes/commons.inc.php';
  require_once 'includes/functions_userfiles.inc.php';
//  if (isset ($_GET['show_license']))
 // {
 //   exit ('License to: ' . LICENSE_URL . '<br />Type: ' . LICENSE_TYPE);
 // }
//
 // if (!((defined ('LICENSE_KEY') AND !(encode_key (get_server_key ()) != LICENSE_KEY))))
 // {
  //  exit ('Invalid license key!');
  }

  $tpl_error = new Template (TPL_DIR . 'tpl_error.php');
  $tpl_admin = new Template (TPL_DIR . 'tpl_admin.php');
  $tpl_admin->set ('current_user', $UPL['USER']['username']);
  $allowed = ($UPL['USER']['level'] OR $UPL['USER']['level'] == LEVEL_ADMIN);
  if (!($demo))
  {
    if (!($allowed))
    {
      $tpl_message->set ('message', 'You do not have the permission to access this page.');
      $tpl_admin->setr ('content', $tpl_message, 1);
      exit ();
    }
  }

  switch ($action)
  {
    case 'announcement':
    {
      $tpl_ann = new Template (TPL_DIR . 'tpl_announcements_admin.php');
      $tpl_ann->set ('action', $action);
      $tpl_admin->set ('page_title', 'Uploader announcements');
      $announcements = array ();
      $result = $mysqlDB->query2 ('SELECT *, COUNT(comment_id) AS comments_count FROM uploader_announcements AS a LEFT  JOIN uploader_usercomments AS uc ON uc.object_id=a.announcement_id AND uc.comment_type=' . COMMENT_ANNOUNCEMENT . ' GROUP BY announcement_id ORDER BY announcement_id DESC');
      if (!($result->isGood ()))
      {
        exit ($mysqlDB->error (40, __FILE__));
      }

      while (false !== $ann = $result->fetchAssoc ())
      {
        $ann['edit_url'] = 'admin.php?action=edit_announcement&amp;aid=' . $ann['announcement_id'];
        $ann['delete_url'] = 'admin.php?action=delete_announcement&amp;aid=' . $ann['announcement_id'];
        $ann['view_url'] = 'admin.php?action=view_announcement&amp;aid=' . $ann['announcement_id'];
        $announcements[] = $ann;
      }

      $result->free ();
      $tpl_ann->set ('announcements', $announcements);
      $tpl_admin->set ('page_title', 'Uploader announcements');
      $tpl_admin->set ('content', $tpl_ann, 1);
      return 1;
    }

    case 'add_announcement':
    {
      $tpl_ann = new Template (TPL_DIR . 'tpl_announcements_admin.php');
      $tpl_ann->set ('action', $action);
      $tpl_admin->set ('page_title', 'Add announcement');
      if ($task == 'save')
      {
        $new_ann = gpc ('announcement', 'P', array ('subject' => '', 'parsebb' => 1, 'allowcomment' => 1, 'content' => ''));
        $errors = array ();
        $new_ann['parsebb'] = (isset ($new_ann['parsebb']) ? $new_ann['parsebb'] : 0);
        $new_ann['allowcomment'] = (isset ($new_ann['allowcomment']) ? $new_ann['allowcomment'] : 0);
        $new_ann['subject'] = trim ($new_ann['subject']);
        if ($new_ann['subject'] == '')
        {
          $errors[] = 'The announcement subject CANNOT be blank. Please enter a subject.';
        }
        else
        {
          255 < strlen ($new_ann['subject']);
        }

        $new_ann['content'] = trim ($new_ann['content']);
        if ($new_ann['content'] == '')
        {
          $errors[] = 'The announcement content CANNOT be blank. Please enter some content.';
        }

        if (!(count ($errors)))
        {
          $insert = array ('announcement_id' => NULL, 'announcement_date' => time (), 'announcement_subject' => $mysqlDB->escape ($new_ann['subject']), 'announcement_parsebb' => (int)$new_ann['parsebb'], 'announcement_allowcomment' => (int)$new_ann['allowcomment'], 'announcement_content' => $mysqlDB->escape ($new_ann['content']), 'userid' => $USER['userid']);
          $result = $mysqlDB->query2 ('INSERT INTO uploader_announcements SET ' . mysqldb::buildInsertStatement ($insert));
          if (!($result->isGood ()))
          {
            exit ($mysqlDB->error (95, __FILE__));
          }

          go_to (UPLOADER_URL . 'admin.php?action=announcement');
          return 1;
        }

        $tpl_error->set ('error', $errors);
        $tpl_ann->set ('errors', $tpl_error);
        $tpl_ann->set ('announcement', $new_ann);
        $tpl_admin->set ('content', $tpl_ann, 1);
        return 1;
      }

      $tpl_ann->set ('announcement', array ('subject' => '', 'parsebb' => 1, 'allowcomment' => 1, 'content' => ''));
      $tpl_admin->set ('content', $tpl_ann, 1);
      return 1;
    }

    case 'delete_announcement':
    {
      $aid = (int)gpc ('aid', 'G', 0);
      $mysqlDB->query ('DELETE FROM uploader_announcements WHERE announcement_id=' . $aid);
      $mysqlDB->query ('DELETE FROM uploader_usercomments WHERE object_id=' . $aid . ' AND comment_type=' . COMMENT_ANNOUNCEMENT);
      go_to (UPLOADER_URL . 'admin.php?action=announcement');
      return 1;
    }

    case 'edit_announcement':
    {
      $tpl_ann = new Template (TPL_DIR . 'tpl_announcements_admin.php');
      $tpl_admin->set ('page_title', 'Edit announcements');
      $tpl_ann->set ('action', $action);
      $aid = (int)gpc ('aid', 'GP', 0);
      $result = $mysqlDB->query2 ('SELECT * FROM uploader_announcements WHERE announcement_id=' . $aid);
      if (!($result->rowCount ()))
      {
        $tpl_message->set ('message', 'Invalid announcement');
        $tpl_message->set ('back_url', UPLOADER_URL . 'admin.php?action=announcement');
        $tpl_admin->set ('content', $tpl_message, 1);
        exit ();
      }

      $ann = $result->fetchAssoc ();
      $result->free ();
      if ($task == 'save')
      {
        $new_ann = gpc ('announcement', 'P', array ('subject' => '', 'parsebb' => 1, 'allowcomment' => 1, 'content' => ''));
        $errors = array ();
        $new_ann['parsebb'] = (isset ($new_ann['parsebb']) ? $new_ann['parsebb'] : 0);
        $new_ann['allowcomment'] = (isset ($new_ann['allowcomment']) ? $new_ann['allowcomment'] : 0);
        $new_ann['subject'] = trim ($new_ann['subject']);
        if ($new_ann['subject'] == '')
        {
          $errors[] = 'The announcement subject CANNOT be blank. Please enter a subject.';
        }
        else
        {
          255 < strlen ($new_ann['subject']);
        }

        $new_ann['content'] = trim ($new_ann['content']);
        if ($new_ann['content'] == '')
        {
          $errors[] = 'The announcement content CANNOT be blank. Please enter some content.';
        }

        if (!(count ($errors)))
        {
          $insert = array ('announcement_subject' => $mysqlDB->escape ($new_ann['subject']), 'announcement_parsebb' => (int)$new_ann['parsebb'], 'announcement_allowcomment' => (int)$new_ann['allowcomment'], 'announcement_content' => $mysqlDB->escape ($new_ann['content']), 'userid' => $USER['userid']);
          $result = $mysqlDB->query2 ('UPDATE uploader_announcements SET ' . mysqldb::buildInsertStatement ($insert) . ('WHERE announcement_id=' . $aid));
          if (!($result->isGood ()))
          {
            exit ($mysqlDB->error (177, __FILE__));
          }

          go_to (UPLOADER_URL . 'admin.php?action=announcement');
          return 1;
        }

        $tpl_error->set ('error', $errors);
        $tpl_ann->set ('errors', $tpl_error);
        $tpl_ann->set ('announcement', $new_ann);
        $tpl_admin->set ('content', $tpl_ann, 1);
        return 1;
      }

      $announcement = array ('aid' => intval ($ann['announcement_id']), 'subject' => $ann['announcement_subject'], 'parsebb' => intval ($ann['announcement_parsebb']), 'allowcomment' => intval ($ann['announcement_allowcomment']), 'content' => $ann['announcement_content']);
      $tpl_ann->set ('announcement', $announcement);
      $tpl_admin->set ('content', $tpl_ann, 1);
      return 1;
    }

    case 'view_announcement':
    {
    }

    case 'cleanpuploads':
    {
      $r = $mysqlDB->query ('SELECT uploader_puploads.upload_id, COUNT(file_id) AS files FROM uploader_puploads LEFT JOIN uploader_pfiles USING (upload_id) GROUP BY uploader_puploads.upload_id;');
      $list = array ();
      while (false !== $upload = $mysqlDB->getAssoc ())
      {
        if ($upload['files'] == 0)
        {
          $list[] = $upload['upload_id'];
          continue;
        }
      }

      $mysqlDB->free ();
      $mysqlDB->query ('DELETE FROM uploader_puploads WHERE upload_id IN (' . implode (',', $list) . ')');
      $tpl_message->set ('message', count ($list) . ' uploads removed.');
      $tpl_message->set ('back_url', 'admin.php?action=pupload');
      $tpl_admin->set ('content', $tpl_message, 1);
      return 1;
    }

    case 'pupload':
    {
      $per_page = $PUB['sets_per_page'];
      $current_page = (int)gpc ('page', 'G', 1);
      $result = $mysqlDB->query2 ('SELECT COUNT(p.upload_id) AS total_sets FROM uploader_puploads AS p WHERE 1');
      if ($result->error ())
      {
        exit ($mysqlDB->error ());
      }

      $r = $result->fetchAssoc ();
      $total_sets = $r['total_sets'];
      $total_pages = ceil ($total_sets / $per_page);
      if ($current_page < 1)
      {
        $current_page = 1;
      }

      if ($total_pages < $current_page)
      {
        $current_page = $total_pages;
      }

      $start_offset = (1 < $current_page ? ($current_page - 1) * $per_page : 0);
      $next_page_url = 'admin.php?action=pupload&amp;page=' . ($current_page + 1);
      $prev_page_url = 'admin.php?action=pupload&amp;page=' . ($current_page - 1);
      $sets = array ();
      $result = $mysqlDB->query2 ('SELECT *, COUNT(file_id) AS image_count FROM uploader_puploads JOIN uploader_pfiles USE INDEX(upload_id) USING(upload_id) WHERE 1 GROUP BY uploader_puploads.upload_id ORDER BY file_id DESC LIMIT ' . $start_offset . ', ' . $per_page);
      if ($result->error ())
      {
        exit ($mysqlDB->error (257, __FILE__));
      }

      while (false !== $file = $result->fetchAssoc ())
      {
        $set = $file;
        processpublicfile ($file);
        processpublicset ($set, true);
        $sets[] = array ('set' => $set, 'file' => $file);
      }

      $result->free ();
      $tpl_pbrowse = new Template (TPL_DIR . 'tpl_public_admin.php');
      $tpl_pbrowse->set ('total_pages', $total_pages);
      $tpl_pbrowse->set ('total_sets', $total_sets);
      $tpl_pbrowse->set ('current_page', $current_page);
      $tpl_pbrowse->set ('next_page_url', $next_page_url);
      $tpl_pbrowse->set ('prev_page_url', $prev_page_url);
      $tpl_pbrowse->setr ('upload_sets', $sets);
      $tpl_admin->set ('content', $tpl_pbrowse, 1);
      return 1;
    }

    case 'delete_public_set':
    {
      $upload_id = (int)gpc ('upload_id', 'G', 0);
      $result = $mysqlDB->query2 ('SELECT * FROM uploader_puploads WHERE upload_id=' . $upload_id . ' LIMIT 1');
      if ($result->rowCount ())
      {
        $set = $result->fetchAssoc ();
        $result->free ();
        $result = $mysqlDB->query2 ('SELECT file_location FROM uploader_pfiles WHERE upload_id=' . $upload_id);
        if ($result->rowCount ())
        {
          while (false !== $file = $result->fetchAssoc ())
          {
            delete_public_file ($file['file_location']);
          }

          $result->free ();
        }

        $mysqlDB->query ('DELETE FROM uploader_pfiles WHERE upload_id=' . $upload_id);
        $mysqlDB->query ('DELETE FROM uploader_puploads WHERE upload_id=' . $upload_id);
      }

      go_to (previous_page (UPLOADER_URL . 'admin.php?action=pupload'));
      return 1;
    }

    case 'edit_public_set':
    {
      $upload_id = (int)gpc ('upload_id', 'G', 0);
      $result = $mysqlDB->query2 ('SELECT * FROM uploader_puploads WHERE upload_id=' . $upload_id . ' LIMIT 1');
      if ($result->rowCount ())
      {
        $upload_set = $result->fetchAssoc ();
        processpublicset ($upload_set);
        $result->free ();
        $files = array ();
        $result = $mysqlDB->query2 ('SELECT * FROM uploader_pfiles WHERE upload_id=' . $upload_id . ' ORDER BY file_id ASC');
        if ($result->rowCount ())
        {
          while (false !== $file = $result->fetchAssoc ())
          {
            processpublicfile ($file);
            $files[] = $file;
          }

          $result->free ();
        }

        $tpl_manage = new Template (TPL_DIR . 'tpl_public_admin_manage.php');
        $tpl_manage->setr ('files', $files);
        $tpl_manage->setr ('upload_set', $upload_set);
        $tpl_uploader->set ('content', $tpl_manage, true);
        return 1;
      }

      $tpl_message->set ('message', $lang_public['set_not_found']);
      $tpl_uploader->set ('content', $tpl_message, true);
      return 1;
    }

    case 'edit_public_set_info':
    {
      $upload_id = (int)gpc ('upload_id', 'P', 0);
      $upload_set_input = gpc ('upload', 'P', array ('public' => 1, 'name' => '', 'description' => ''));
      $upload_set_input['public'] = (isset ($upload_set_input['public']) ? (int)$upload_set_input['public'] : 0);
      $upload_set_input['description'] = trim ($upload_set_input['description']);
      $result = $mysqlDB->query2 ('SELECT * FROM uploader_puploads WHERE upload_id=' . $upload_id . ' LIMIT 1');
      if ($result->rowCount ())
      {
        $upload_set = $result->fetchAssoc ();
        processpublicset ($upload_set);
        $result->free ();
        $insert = array ('upload_name' => (isset ($upload_set_input['name']) ? $mysqlDB->escape (substr ($upload_set_input['name'], 0, 64)) : ''), 'upload_description' => (isset ($upload_set_input['description']) ? $mysqlDB->escape (substr ($upload_set_input['description'], 0, 255)) : ''), 'upload_ispublic' => (int)$upload_set_input['public']);
        if (!($mysqlDB->query ('UPDATE uploader_puploads SET ' . $mysqlDB->buildInsertStatement ($insert) . (' WHERE upload_id=' . $upload_id))))
        {
          exit ($mysqlDB->error (370, __FILE__));
        }

        go_to ();
        return 1;
      }

      exit ('invalid upload set');
    }

    case 'delete_public_files':
    {
      $upload_id = (int)gpc ('upload_id', 'P', 0);
      $file_ids = gpc ('file_ids', 'P', array ());
      $result = $mysqlDB->query2 ('SELECT * FROM uploader_puploads WHERE upload_id=' . $upload_id . ' LIMIT 1');
      if ($result->rowCount ())
      {
        $upload_set = $result->fetchAssoc ();
        processpublicset ($upload_set);
        $result->free ();
        for ($i = 0; $i < count ($file_ids); ++$i)
        {
          $file_ids[$i] = intval ($file_ids[$i]);
        }

        $list = implode (',', $file_ids);
        $result = $mysqlDB->query2 ('SELECT file_location FROM uploader_pfiles WHERE file_id IN (' . $list . ')');
        if ($result->rowCount ())
        {
          while (false !== $file = $result->fetchAssoc ())
          {
            delete_public_file ($file['file_location']);
          }

          $result->free ();
        }

        $mysqlDB->query ('DELETE FROM uploader_pfiles WHERE file_id IN(' . $list . ')');
        $result = $mysqlDB->query2 ('SELECT COUNT(file_id) AS files_count FROM uploader_pfiles WHERE upload_id=' . $upload_id);
        $row = $result->fetchAssoc ();
        if (!($row['files_count']))
        {
          $mysqlDB->query ('DELETE FROM uploader_puploads WHERE upload_id=' . $upload_id);
          $tpl_message->set ('back_url', UPLOADER_URL . 'admin.php?action=pupload');
          $tpl_message->set ('message', 'All files in the set have been deleted. The set has also been deleted.');
          $tpl_uploader->set ('content', $tpl_message, 1);
          exit ();
        }

        go_to ();
        return 1;
      }

      exit ('invalid set');
    }

    case 'logs':
    {
      $tpl_logs = new Template (TPL_DIR . 'tpl_logs.php');
      $log_files = array ();
      $log_total_size = 0;
      if (false !== $h = opendir (LOGS_DIR))
      {
        while (false !== $f = readdir ($h))
        {
          if (get_extension ($f) == 'log')
          {
            $log_size = filesize (LOGS_DIR . $f);
            $log_total_size += $log_size;
            $log_files[] = array ('name' => $f, 'size' => get_size ($log_size, 'B', 0), 'download_url' => UPLOADER_URL . 'admin.php?action=logs&amp;task=download&amp;log_file=' . $f, 'view_url' => UPLOADER_URL . 'admin.php?action=logs&amp;task=view&amp;log_file=' . $f, 'delete_url' => UPLOADER_URL . 'admin.php?action=logs&amp;task=del&amp;log_file=' . $f);
            continue;
          }
        }

        reset ($log_files);
      }
      else
      {
        exit (sprintf ('Unable to open the logs directory "%s"', LOGS_DIR));
      }

      closedir ($h);
      $log_files = multi_sort ($log_files, 'name');
      switch ($task)
      {
        case 'del':
        {
          if ($demo)
          {
            exit ('Demo only!');
          }

          $log_file = gpc ('log_file', 'G', '');
          if (is_file (LOGS_DIR . $log_file))
          {
            if (unlink (LOGS_DIR . $log_file))
            {
              header ('Location: admin.php?action=logs');
            }
          }

          exit ('Could not delete ' . $log_file);
        }

        case 'delall':
        {
          if ($demo)
          {
            exit ('Demo only!');
          }

          for ($i = 0; $i < count ($log_files); ++$i)
          {
            if (is_file (LOGS_DIR . $log_files[$i]['name']))
            {
              if (!(unlink (LOGS_DIR . $log_files[$i]['name'])))
              {
                exit ('Could not delete log file.');
              }

              continue;
            }
          }

          header ('Location: admin.php?action=logs');
        }

        case 'clear':
        {
          if ($demo)
          {
            exit ('Demo only!');
          }

          $log_file = gpc ('log_file', 'G', '');
          if (is_file (LOGS_DIR . $log_file))
          {
            fclose (fopen (LOGS_DIR . $log_file, 'w'));
            header ('Location: admin.php?action=logs');
          }

          exit ('Could not delete ' . $log_file);
        }

        case 'view':
        {
        }

        case 'download':
        {
          $log_file = gpc ('log_file', 'G', '');
          $fp = @fopen (LOGS_DIR . $log_file, 'rt');
          if (!($fp))
          {
            exit ('Invalid log file');
          }

          header ('Content-type: text/plain');
          if ($task == 'download')
          {
            header ('Content-disposition: attachment;filename="' . $log_file . '"');
          }

          while (true)
          {
            echo fread ($fp, 102400);
            if (feof ($fp))
            {
              fclose ($fp);
              return 1;
            }
          }
        }

        case 'archive':
        {
          $archive_file = 'Archive_' . date ('Y_M_d') . '.log';
          $fp = fopen (LOGS_DIR . $archive_file, 'at');
          if (!($fp))
          {
            exit ('Error creating log file, check that log directory is chmodded');
          }

          for ($i = 0; $i < count ($log_files); ++$i)
          {
            $log_file = $log_files[$i]['name'];
            if (!(preg_match ('#archive#i', $log_file)))
            {
              $fh = fopen (LOGS_DIR . $log_file, 'rt');
              if ($fh)
              {
                while (!(feof ($fh)))
                {
                  fwrite ($fp, fread ($fh, 1024000));
                }

                fclose ($fh);
              }

              unlink (LOGS_DIR . $log_file);
              continue;
            }
          }

          fclose ($fp);
          go_to (UPLOADER_URL . 'admin.php?action=logs');
          return 1;
        }
      }

      while (true)
      {
        $tpl_logs->set ('log_total_size', get_size ($log_total_size, 'B'));
        $tpl_logs->setr ('log_files', $log_files);
        $tpl_logs->set ('archive_url', 'admin.php?action=logs&amp;task=archive');
        $tpl_logs->set ('delete_all_url', 'admin.php?action=logs&amp;task=delall');
        $tpl_logs->setr ('log_data', $log_data);
        $tpl_admin->setr ('content', $tpl_logs);
        $tpl_admin->set ('page_title', 'Uploader logs');
        $tpl_admin->display ();
        for (; true; )
        {
        }
      }
    }

    case 'user_info':
    {
      $tpl_userinfo = new Template (TPL_DIR . 'tpl_user_admin.php');
      $tpl_userinfo->set ('action', $action);
      $userid = (int)gpc ('userid', 'GP', 0);
      $userinfo = get_user_info ($userid);
      if (count ($userinfo))
      {
        processuser ($userinfo, true);
        $userinfo['comments'] = (trim ($userinfo['xtr_admin_comments']) == '' ? 'No comments' : nl2br ($userinfo['xtr_admin_comments']));
        $userinfo['reg_date'] = date ($UPL['CONFIGS']['TIME_FORMAT2'], $userinfo['reg_date']);
        $userinfo['last_login_time'] = date ($UPL['CONFIGS']['TIME_FORMAT2'], $userinfo['last_login_time']);
        $userinfo['last_login_ip'] = $userinfo['last_login_ip'];
        $userinfo['max_storage'] = ($userinfo['fl_max_storage'] == 0 ? 'Unlimited' : get_size ($userinfo['fl_max_storage'], 'MB', 1));
        $userinfo['max_filesize'] = ($userinfo['fl_max_filesize'] == 0 ? 'Unlimited' : get_size ($userinfo['fl_max_filesize'], 'KB', 1));
        $userinfo['filetypes'] = str_replace (',', ', ', $userinfo['fl_allowed_types']);
        $userinfo['bw_used'] = get_size ($userinfo['bw_used'], 'KB');
        $userinfo['max_bandwidth'] = get_size ($userinfo['bw_max'], 'MB');
        $userinfo['bw_last_reset_days'] = floor ((time () - $userinfo['bw_reset_last']) / 86400);
        $userinfo['bw_last_reset'] = date ($UPL['CONFIGS']['TIME_FORMAT2'], $userinfo['bw_reset_last']);
        $userinfo['allow_rename'] = $userinfo['fl_rename_permission'];
        $userinfo['allow_create_folder'] = $userinfo['fl_allow_folders'];
        $userinfo['images_only'] = $userinfo['fl_images_only'];
        $tpl_userinfo->setr ('userinfo', $userinfo);
        $tpl_admin->setr ('content', $tpl_userinfo);
        $tpl_admin->set ('page_title', 'Account info', 1);
        return 1;
      }

      $tpl_message->set ('message', 'Unable to load user data, perhaps user does not exists.');
      $tpl_message->set ('back_url', 'admin.php?action=users');
      $tpl_admin->setr ('content', $tpl_message, 1);
      return 1;
    }

    case 'user_files':
    {
      $tpl_userfiles = new Template (TPL_DIR . 'tpl_user_admin.php');
      $tpl_userfiles->set ('action', $action);
      $files_per_page = 25;
      $folder_id = (int)gpc ('folder_id', 'G', 0);
      $userid = abs (intval (gpc ('userid', 'G', 0)));
      $sort = trim (gpc ('sort', 'G', 'date_desc'));
      $current_page = abs ((int)gpc ('page', 'G', 1));
      $userinfo = get_user_info ($userid);
      if (!(count ($userinfo)))
      {
        exit ('Invalid user');
      }

      processuser ($userinfo, true);
      $tpl_userfiles->set ('userinfo', $userinfo);
      $userfolders = get_user_folders ($userid, 0, 1);
      $current_folder = array ();
      $count = count ($userfolders);
      for ($i = 0; $i < $count; ++$i)
      {
        processfolder ($userfolders[$i], 0, 1);
        if (!($userfolders[$i]['folder_id'] == $folder_id))
        {
          if (!($folder_id))
          {
            if ($i === 0)
            {
            }

            continue;
          }

          continue;
        }
      }

      if (!(count ($current_folder)))
      {
        exit ('Invalid folder');
      }

      $folder_id = $current_folder['folder_id'];
      $total_pages = ceil ($current_folder['files_count'] / $files_per_page);
      if ($current_page < 1)
      {
        $current_page = 1;
      }

      if ($total_pages < $current_page)
      {
        $current_page = $total_pages;
      }

      $start_offset = ($current_page - 1) * $files_per_page;
      if (!(strstr ($sort, '_')))
      {
        $sort = 'date_desc';
      }

      list ($sort_by, $sort_order) = explode ('_', $sort);
      switch ($sort_by)
      {
        case 'type':
        {
          $sort_column = 'file_extension';
          break;
        }

        case 'name':
        {
          $sort_column = 'file_name';
          break;
        }

        case 'size':
        {
          $sort_column = 'file_size';
          break;
        }
      }

      $sort_column = 'file_id';
      $sort_by = 'date';
      if ($sort_order != 'asc')
      {
        if ($sort_order != 'desc')
        {
          $sort_order = 'asc';
        }
      }

      $userfiles = get_user_files_in_folder ($userid, $folder_id, $start_offset, $files_per_page, $sort_column, $sort_order);
      $count = count ($userfiles);
      for ($i = 0; $i < $count; ++$i)
      {
        processfile ($userfiles[$i], 1);
      }

      $base_url = UPLOADER_URL . 'admin.php?action=user_files&amp;userid=' . $userid . '&amp;folder_id=' . $folder_id;
      if ($sort_by == 'type')
      {
        (true ? $sort_order == 'asc' : 'desc');
      }

      if ($sort_by == 'name')
      {
        (true ? $sort_order == 'asc' : 'desc');
      }

      if ($sort_by == 'size')
      {
        (true ? $sort_order == 'asc' : 'desc');
      }

      if ($sort_by == 'date')
      {
        (true ? $sort_order == 'asc' : 'desc');
      }

      $sort_url = array ('type' => $base_url . '&amp;sort=type_' . 'asc', 'name' => $base_url . '&amp;sort=name_' . 'asc', 'size' => $base_url . '&amp;sort=size_' . 'asc', 'date' => $base_url . '&amp;sort=date_' . 'asc');
      $next_page_url = UPLOADER_URL . 'admin.php?action=user_files&amp;userid=' . $userid . '&amp;folder_id=' . $folder_id . '&amp;page=' . ($current_page + 1) . ('&amp;sort=' . $sort_by . '_' . $sort_order);
      $prev_page_url = UPLOADER_URL . 'admin.php?action=user_files&amp;userid=' . $userid . '&amp;folder_id=' . $folder_id . '&amp;page=' . ($current_page - 1 <= 0 ? 1 : $current_page - 1) . ('&amp;sort=' . $sort_by . '_' . $sort_order);
      $tpl_vars = array ('per_page' => $files_per_page, 'file_start' => $start_offset + 1, 'file_end' => $start_offset + count ($userfiles), 'current_page' => $current_page, 'total_pages' => $total_pages, 'sort_url' => $sort_url, 'sort_by' => $sort_by, 'next_page_url' => $next_page_url, 'prev_page_url' => $prev_page_url);
      $tpl_userfiles->set ($tpl_vars);
      $tpl_userfiles->setr ('current_folder', $current_folder);
      $tpl_userfiles->setr ('userfiles', $userfiles);
      $tpl_userfiles->setr ('userfolders', $userfolders);
      $tpl_admin->setr ('content', $tpl_userfiles, 1);
      return 1;
    }

    case 'edit_user':
    {
      $tpl_user = new Template (TPL_DIR . 'tpl_user_admin.php');
      $tpl_user->set ('action', $action);
      $tpl_user->set ('saved', isset ($_GET['saved']));
      $userid = abs ((int)gpc ('userid', 'GP', 0));
      $userinfo = get_user_info ($userid);
      if (count ($userinfo))
      {
        if ($task == 'save')
        {
          if ($demo)
          {
            exit ('Demo only!');
          }

          $userinfo = (isset ($_POST['userinfo']) ? $_POST['userinfo'] : array ());
          $new_settings = array ('email' => $mysqlDB->escape (trim ($userinfo['email'])), 'level' => (int)$userinfo['level'], 'is_activated' => (int)$userinfo['is_activated'], 'is_suspended' => (int)$userinfo['is_suspended'], 'xtr_admin_comments' => $mysqlDB->escape (substr ($userinfo['comments'], 0, 255)), 'fl_max_storage' => (double)$userinfo['fl_max_storage'], 'fl_max_filesize' => (double)$userinfo['fl_max_filesize'], 'fl_max_folders' => (int)$userinfo['fl_max_folders'], 'fl_images_only' => (int)$userinfo['fl_images_only'], 'fl_rename_permission' => (int)$userinfo['fl_rename_permission'], 'fl_watermark' => (int)$userinfo['fl_watermark'], 'fl_allow_folders' => (int)$userinfo['fl_allow_folders'], 'fl_allowed_types' => $mysqlDB->escape (strtolower (trim ($userinfo['fl_allowed_types']))), 'bw_max' => (double)$userinfo['bw_max'], 'bw_reset_period' => (double)$userinfo['bw_reset_period'], 'bw_reset_auto' => (int)$userinfo['bw_reset_auto'], 'bw_xfer_rate' => (int)$userinfo['bw_xfer_rate']);
          if (isset ($userinfo['new_password']))
          {
            if ($userinfo['new_password'] != '')
            {
              $new_settings['password'] = md5 ($userinfo['new_password']);
            }
          }

          $r = $mysqlDB->query ('UPDATE uploader_users SET ' . $mysqlDB->buildInsertStatement ($new_settings) . ('  WHERE userid=' . $userid . '; '));
          if (!($r))
          {
            exit ($mysqlDB->error ());
          }

          header ('Location: admin.php?action=edit_user&userid=' . $userid . '&saved=1');
          return 1;
        }

        $userinfo['comments'] = trim ($userinfo['xtr_admin_comments']);
        processuser ($userinfo, true);
        $tpl_user->setr ('userinfo', $userinfo);
        $tpl_admin->setr ('content', $tpl_user);
        $tpl_admin->set ('page_title', 'Edit account');
        $tpl_admin->display ();
        return 1;
      }

      $tpl_message->set ('message', 'Unable to load user data, perhaps user does not exists.');
      $tpl_message->set ('back_url', 'admin.php?action=users');
      $tpl_admin->setr ('content', $tpl_message);
      $tpl_admin->display ();
      return 1;
    }

    case 'users':
    {
      $tpl_users = new Template (TPL_DIR . 'tpl_users.php');
      $per_page = 25;
      $current_page = (int)gpc ('current_page', 'P', 1);
      $sort_by = trim (gpc ('sort_by', 'P', 'userid'));
      $sort_order = trim (gpc ('sort_order', 'P', 'asc'));
      $where = array ();
      $having = array ();
      $result = $mysqlDB->query2 ('SELECT COUNT(userid) FROM uploader_users');
      $row = $result->fetchArray ();
      $tpl_users->set ('total_users', $row[0]);
      if ($sort_order != 'desc')
      {
        if ($sort_order != 'asc')
        {
          $sort_order = 'asc';
        }
      }

      switch ($sort_by)
      {
        case 'userid':
        {
          $order_str = 'ORDER BY u.userid';
        }

        case 'username':
        {
          $order_str = 'ORDER BY u.username';
        }

        case 'status':
        {
          $order_str = 'ORDER BY status';
        }

        case 'email':
        {
          $order_str = 'ORDER BY u.email';
        }

        case 'files':
        {
          $order_str = 'ORDER BY files_count';
        }

        case 'space':
        {
          $order_str = 'ORDER BY total_file_size';
        }

        case 'bandwidth':
        {
          $order_str = 'ORDER BY u.bw_used';
          $order_str .= ' ' . $sort_order;
          $filters = gpc ('filters', 'P', array ());
          while (list ($fvar, $fval) = each ($filters))
          {
            $fvar = strtolower (trim ($fvar));
            $fval = $mysqlDB->escape (trim ($fval));
            if (!($fval == ''))
            {
              if (!($fval == 'null'))
              {
                switch ($fvar)
                {
                  case 'level':
                  {
                    $where[] = 'u.level=' . (int)$fval;
                  }

                  case 'username':
                  {
                    $where[] = 'u.username LIKE \'%' . $fval . '%\'';
                    break;
                  }

                  case 'email':
                  {
                    $where[] = 'u.email LIKE \'%' . $fval . '%\'';
                    break;
                  }

                  case 'admin_comments':
                  {
                    $where[] = 'u.xtr_admin_comments LIKE \'%' . $fval . '%\'';
                    break;
                  }

                  case 'is_approved':
                  {
                    $where[] = 'u.is_approved=' . (int)$fval;
                    break;
                  }

                  case 'is_activated':
                  {
                    $where[] = 'u.is_activated=' . (int)$fval;
                    break;
                  }

                  case 'is_suspended':
                  {
                    $where[] = 'u.is_suspended=' . (int)$fval;
                    break;
                  }

                  case 'last_login':
                  {
                    $operator = (isset ($filters['last_login_operator']) ? $filters['last_login_operator'] : '');
                    if (!(($operator != '<=' AND $operator != '>=')))
                    {
                      $time = time () - (int)$fval * 3600;
                      $where[] = 'u.last_login_time ' . $operator . ' ' . $time;
                      break;
                    }

                    break;
                  }

                  case 'bw_used':
                  {
                    $operator = (isset ($filters['bw_used_operator']) ? $filters['bw_used_operator'] : '');
                    $unit = (isset ($filters['bw_used_unit']) ? $filters['bw_used_unit'] : '%');
                    if (!(($operator != '<=' AND $operator != '>=')))
                    {
                      if ($unit == 'MB')
                      {
                        $where[] = 'bw_max>0&&bw_used ' . $operator . ' ' . $fval * 1024;
                        break;
                      }
                      else
                      {
                        if ($unit == 'GB')
                        {
                          $where[] = 'bw_max>0&&bw_used ' . $operator . ' ' . $fval * 1024 * 1024;
                          break;
                        }
                        else
                        {
                          $where[] = 'bw_max>0&&((bw_used/1024)/bw_max) ' . $operator . ' ' . $fval / 100;
                          break;
                        }

                        break;
                      }

                      break;
                    }

                    break;
                  }

                  case 'reg_time':
                  {
                    $operator = (isset ($filters['reg_time_operator']) ? $filters['reg_time_operator'] : '');
                    if (!(($operator != '<=' AND $operator != '>=')))
                    {
                      $where[] = 'u.reg_date ' . $operator . ' ' . (time () - (int)$fval * 86400);
                      break;
                    }

                    break;
                  }

                  case 'space_used':
                  {
                    $operator = (isset ($filters['space_used_operator']) ? $filters['space_used_operator'] : '');
                    $unit = (isset ($filters['space_used_unit']) ? $filters['space_used_unit'] : '%');
                    if (!(($operator != '<=' AND $operator != '>=')))
                    {
                      if ($unit == 'KB')
                      {
                        $having[] = 'total_file_size ' . $operator . ' ' . $fval * 1024;
                        break;
                      }
                      else
                      {
                        if ($unit == 'MB')
                        {
                          $having[] = 'total_file_size ' . $operator . ' ' . $fval * 1024 * 1024;
                          break;
                        }
                        else
                        {
                          $having[] = '((total_file_size/1048576)/fl_max_storage) ' . $operator . ' ' . $fval / 100;
                          break;
                        }

                        break;
                      }

                      break;
                    }

                    break;
                  }
                }
              }

              continue;
            }
          }
        }
      }
    }
  }
?>