<?


  class zip
  {
    var $_fp = null;
    var $_cdr = null;
    function zip ()
    {
      $this->_fp = false;
      $this->_cdr = array ();
    }

    function open ($f)
    {
      if (false === $this->_fp = @fopen ($f, 'rb'))
      {
        return false;
      }

      $this->_getCDR ();
      if (!((is_array ($this->_cdr) AND count ($this->_cdr))))
      {
        return false;
      }

      return true;
    }

    function getfile ($name = '')
    {
      if ($name == '')
      {
        list ($k, $v) = each ($this->_cdr);
        if (is_array ($v))
        {
          return $this->_getEntry ($v['rel_offset']);
        }
      }
      else
      {
        $count = count ($this->_cdr);
        for ($i = 0; $i < $count; ++$i)
        {
          if ($this->_cdr[$i]['filename'] == $name)
          {
            return $this->_getEntry ($this->_cdr[$i]['rel_offset']);
          }
        }
      }

      return false;
    }

    function getfilelist ()
    {
      $count = count ($this->_cdr);
      $files = array ();
      for ($i = 0; $i < $count; ++$i)
      {
        $files[] = $this->_cdr[$i]['filename'];
      }

      return $files;
    }

    function _getcdr ()
    {
      fseek ($this->_fp, -22, SEEK_END);
      $stop_pos = ftell ($this->_fp);
      $d = fread ($this->_fp, 22);
      if (strlen ($d) != 22)
      {
        return false;
      }

      $cdr = unpack ('Ncdr_sig/nignore1/nignore2/nignore3/nignore4/Vcdr_size/Vdisk_num/ncomment_len', $d);
      if ($cdr['cdr_sig'] !== 1347093766)
      {
        return false;
      }

      fseek ($this->_fp, 0 - ($cdr['cdr_size'] + 22), SEEK_END);
      while (ftell ($this->_fp) < $stop_pos)
      {
        $data = fread ($this->_fp, 46);
        if (strlen ($data) != 46)
        {
          return false;
        }

        $fh = unpack ('Vcfh_sig/vversion_made_by/vv_extract/ngen_flag/vcomppression_method/vmod_time/vmode_date/Ncrc32/Vcompressed_size/Vuncompressed_size/vfilename_len/nxtr_len/nfilecomment_len/ndisk_num_start/ninternal_attrib/Vext_attrib/Vrel_offset', $data);
        if (!((is_array ($fh) AND !($fh['cfh_sig'] != 33639248))))
        {
          return false;
        }

        $fh['filename'] = ($fh['filename_len'] ? fread ($this->_fp, $fh['filename_len']) : $fh['filename_len']);
        $fh['extra'] = ($fh['xtr_len'] ? fread ($this->_fp, $fh['xtr_len']) : '');
        $fh['comments'] = ($fh['filecomment_len'] ? fread ($this->_fp, $fh['filecomment_len']) : '');
        $this->_cdr[] = $fh;
      }

      return true;
    }

    function _getentry ($lfh_offset)
    {
      fseek ($this->_fp, $lfh_offset, SEEK_SET);
      $lfh = unpack ('Vheader_sig/Sversion_extract/Sgen_purpose/Scompression_method/Slast_mod_time/Slast_mod_date/Vcrc_32/Vcompressed_size/Vuncompressed_size/Sfile_name_length/Sextra_field_length', fread ($this->_fp, 30));
      $lfh['name'] = ($lfh['file_name_length'] ? fread ($this->_fp, $lfh['file_name_length']) : '');
      $lfh['extra_field'] = ($lfh['extra_field_length'] ? fread ($this->_fp, $lfh['extra_field_length']) : '');
      if ($lfh['compression_method'] == 8)
      {
        $file_content = ($lfh['compressed_size'] ? @gzinflate (@fread ($this->_fp, $lfh['compressed_size'])) : '');
      }
      else
      {
        if ($lfh['compression_method'] == 0)
        {
          $file_content = ($lfh['compressed_size'] ? fread ($this->_fp, $lfh['compressed_size']) : '');
        }
        else
        {
          return false;
        }
      }

      $ldd = ($lfh['gen_purpose'] & 8 ? unpack ('Vcrc_32/Vcompressed_size/Vuncompressed_size', fread ($this->_fp, 12)) : '');
      return array ('name' => $lfh['name'], 'size' => $lfh['uncompressed_size'], 'content' => $file_content);
    }

    function close ()
    {
      @fclose ($this->_fp);
    }
  }

  if (!(function_exists ('gzinflate')))
  {
    exit ('Zip.class.php cannot be used because the zlib library is not available.');
  }

?>