<?



  function gpc ($n, $w = 'GPC', $d = '')
  {
    for ($i = 0; $i < strlen ($w); ++$i)
    {
      if ($w[$i] == 'G')
      {
        if (isset ($_GET[$n]))
        {
          return $_GET[$n];
        }
      }

      if ($w[$i] == 'P')
      {
        if (isset ($_POST[$n]))
        {
          return $_POST[$n];
        }
      }

      if ($w[$i] == 'C')
      {
        if (isset ($_COOKIE[$n]))
        {
          return $_COOKIE[$n];
        }

        continue;
      }
    }

    return $d;
  }

  function strip_gpc ($v)
  {
    if (is_array ($v))
    {
      while (list ($k, $x) = each ($v))
      {
        $v[$k] = strip_gpc ($x);
      }

      return $v;
    }

    return stripslashes ($v);
  }

  function str_slice ($s, $l)
  {
    if ($l < strlen ($s))
    {
      return substr ($s, 0, $l / 2 - 1) . '...' . substr ($s, 0 - ($l / 2 - 2));
    }

    return $s;
  }

  function path_encode ($p)
  {
    return rawurlencode (trim (str_replace ('//', '/', $p), ' /.'));
  }

  function path_decode ($p)
  {
    return str_replace ('../', '', trim (rawurldecode ($p), ' /.'));
  }

  function clean_url ($u)
  {
    return str_replace (' ', '%20', trim (str_replace ('//', '/', $u), ' '));
  }

  function rtrim2 ($s, $c = ' /\\"')
  {
    $i = strlen ($s) - 1;
    while (0 <= $i)
    {
      for (; strstr ($c, $s[$i]); )
      {
        --$i;
        break;
      }

      break;
    }

    return $s;
  }

  function go_to ($url = '')
  {
    exit (header ('Location: ' . ($url == '' ? (isset ($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : UPLOADER_URL) : $url)));
  }

  function last_location ($url)
  {
    if (isset ($_SERVER['HTTP_REFERER']))
    {
      return $_SERVER['HTTP_REFERER'];
    }

    return $url;
  }

  function get_size ($s, $u = 'B', $p = 1)
  {
    while (1024 <= $s)
    {
      $s /= 1024;
      switch ($u)
      {
        case 'B':
        {
          $u = 'KB';
        }

        case 'KB':
        {
          $u = 'MB';
          break;
        }

        case 'MB':
        {
          $u = 'GB';
          break;
        }

        case 'GB':
        {
          $u = 'TB';
          break;
        }

        case 'TB':
        {
          $u = 'PB';
          break;
        }

        case 'PB':
        {
          $u = 'XB';
          break;
        }
      }
    }
  }

  function make_file ($f)
  {
    if (!(touch ($f)))
    {
      exit ('Unable to create ' . $f);
    }

    return change_mode ($f);
  }

  function change_mode ($f, $m = 511)
  {
    $o = umask (0);
    $r = chmod ($f, $m);
    umask ($o);
    return $r;
  }

  function make_dir ($p, $m = 511)
  {
    $o = umask (0);
    $r = mkdir ($p, $m);
    umask ($o);
    return $r;
  }

  function make_dir_recursive ($base, $path)
  {
    if (!(is_dir ($base)))
    {
      return false;
    }

    $x = trim ($path, '/');
    $x = explode ('/', $path);
    for ($i = 0; $i < count ($x); ++$i)
    {
      $a = $base . '/' . implode ('/', array_slice ($x, 0, $i));
      $b = $a . '/' . $x[$i];
      if (!(is_dir ($b)))
      {
        make_dir ($b);
        continue;
      }
    }

    return true;
  }

  function get_rand ($len = 8, $p = '')
  {
    $c = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $x = strlen ($c) - 1;
    for ($i = 0; $i < $len; ++$i)
    {
      $p .= $c[mt_rand (0, $x)];
    }

    return $p;
  }

  function get_filename ($f)
  {
    if (false !== $pos = strrpos ($f, '.'))
    {
      return substr ($f, 0, $pos);
    }

    return $f;
  }

  function get_extension ($n)
  {
    return strtolower (trim (strrchr ($n, '.'), '.'));
  }

  function multi_sort ($arr, $c, $o = SORT_ASC, $t = SORT_STRING, $case = false)
  {
    reset ($arr);
    $tmp = array ();
    while (list ($k, $v) = each ($arr))
    {
      if (isset ($v[$c]))
      {
        if (!($case))
        {
          (true ? $t == SORT_STRING : strtolower ($v[$c]));
        }

        $tmp[$k] = $v[$c];
        continue;
      }
    }

    if (count ($tmp))
    {
      array_multisort ($tmp, $o, $t, $arr);
    }

    return $arr;
  }

  function parse ($str, $var, $val = '')
  {
    if (is_array ($var))
    {
      while (list ($k, $v) = each ($var))
      {
        $str = str_replace ($k, $v, $str);
      }

      return $str;
    }

    return str_replace ($var, $val, $str);
  }

  function str_preview ($s, $l)
  {
    if ($l < strlen ($s))
    {
      return substr ($s, 0, $l) . '...';
    }

    return $s;
  }

  function dir_name ($p)
  {
    return rtrim2 (dirname ($p), '\\./ ');
  }

  function delete_dir ($p)
  {
    if (false !== $h = @opendir ($p))
    {
      while (false !== $f = readdir ($h))
      {
        if (!($f == '.'))
        {
          if (!($f == '..'))
          {
            if (is_dir ($p . '/' . $f))
            {
              delete_dir ($p . '/' . $f);
              continue;
            }
            else
            {
              unlink ($p . '/' . $f);
              continue;
            }

            continue;
          }

          continue;
        }
      }

      closedir ($h);
      return rmdir ($p);
    }

    return false;
  }

  function is_image ($f)
  {
    $fp = @fopen ($f, 'rb');
    if (!($fp))
    {
      return false;
    }

    $hd = fread ($fp, 8);
    fclose ($fp);
    return (substr ($hd, 0, 2) == '��' OR $hd == '�PNG

');
  }

  function is_zip ($f)
  {
    $fp = @fopen ($f, 'rb');
    if (!($fp))
    {
      return false;
    }

    $hd = fread ($fp, 4);
    fclose ($fp);
    return $hd == 'PK';
  }

  function check_path ($b, $p)
  {
    if (strstr ($p, '../'))
    {
      return false;
    }

    $b = realpath ($b);
    $p = realpath ($p);
    if (strlen ($p) < strlen ($b))
    {
      return false;
    }

    return $b == substr ($p, 0, strlen ($b));
  }

  function ascii_str ($e)
  {
    $r = '';
    for ($i = 0; $i < strlen ($e); ++$i)
    {
      $r .= '&#' . ord ($e[$i]) . ';';
    }

    return $r;
  }

  function get_byte_value ($v)
  {
    $v = trim ($v);
    $l = strtolower ($v[strlen ($v) - 1]);
    switch ($l)
    {
      case 'g':
      {
        $v *= 1024;
      }

      case 'm':
      {
        $v *= 1024;
      }

      case 'k':
      {
        $v *= 1024;
      }
    }

    return $v;
  }

  function entities ($s, $q = ENT_QUOTES)
  {
    return htmlentities ($s, $q, 'UTF-8');
  }

  function get_date ($f, $t)
  {
    $s = time () - $t;
    if ($s < 60)
    {
      return 'a few seconds ago';
    }

    $m = $s / 60;
    if ($m < 60)
    {
      return floor ($m) . ' minutes ago';
    }

    $h = $m / 60;
    if ($h < 24)
    {
      return floor ($h) . ' hours ago';
    }

    $d = $h / 24;
    if ($d < 2)
    {
      return 'Yesterday, ' . date ('h:iA', $t);
    }

    if ($d <= 7)
    {
      return floor ($d) . ' days ago';
    }

    return date ($f, $t);
  }

  function php2js_array ($a, $assoc = 1)
  {
    reset ($a);
    $o = array ();
    while (list ($k, $v) = each ($a))
    {
      $o[] = ($assoc ? (is_numeric ($k) ? $k : '\'' . $k . '\'') . ':' : '') . (is_array ($v) ? '{' . php2js_array ($v) . '}' : (is_numeric ($v) ? $v : '\'' . addslashes ($v) . '\''));
    }

    return implode (',', $o);
  }

  function encodeurlraw ($s)
  {
    return rawurlencode (str_replace ('/', '%2F', $s));
  }

  function current_page ()
  {
    return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  }

  function previous_page ($url = '')
  {
    $refer = (isset ($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
    $current = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    if (!((!($refer == $current) AND !($refer == ''))))
    {
      return $url;
    }

    return $refer;
  }

  function json_encode ($var)
  {
    $search = array ('
', '
', '	', '\'');
    $replace = array ('\\r', '\\n', '\\t', '\\\'');
    $type = strtolower (gettype ($var));
    switch ($type)
    {
      case 'boolean':
      {
        if ($var)
        {
          return 'true';
        }

        return 'false';
      }

      case 'integer':
      {
        return (int)$var;
      }

      case 'double':
      {
      }

      case 'float':
      {
        return (double)$var;
      }

      case 'string':
      {
        if (is_numeric ($var))
        {
          return $var;
        }

        return '\'' . str_replace ($search, $replace, $var) . '\'';
      }

      case 'null':
      {
        return '\'\'';
      }

      case 'array':
      {
        if (array_keys ($var) === range (0, count ($var) - 1))
        {
          $var = array_map ('json_encode', $var);
          return '[' . implode (',', $var) . ']';
        }

        reset ($var);
        $vars = array ();
        while (list ($k, $v) = each ($var))
        {
          $vars[] = json_encode ($k) . ':' . json_encode ($v);
        }

        return '{' . implode (',', $vars) . '}';
      }
    }
  }

  function send_email ($to, $subject, $message, $headers = '', $params = '')
  {
    if (!(@mail ($to, $subject, $message, $headers, $params)))
    {
      echo 'Internal error: mail() failed. Please make sure that PHP is configured to send emails. Without this feature, users will not be able to activate accounts or reset passwords.';
    }

  }

  function parse_bb ($str, $enable_img = true, $slice = 40)
  {
    $search = array ('#\\[B\\](.+?)\\[\\/B\\]#mis', '#\\[I\\](.+?)\\[\\/I\\]#mis', '#\\[U\\](.+?)\\[\\/U\\]#mis', '#\\[COLOR\\=(.+?)\\](.+?)\\[\\/COLOR\\]#mis', '#(^|\\s+)((http|https|ftp|news)://)([^\\s\\n\\r\\t\\<\\>\\*\\[\\]\\"\']{3,})#mise', '#\\[IMG\\](((http|https|ftp|news)://)([^\\s\\n\\r\\t\\<\\>\\*\\[\\]\\"\']{3,}))\\[\\/IMG\\]#mise');
    $replace = array ('<strong>$1</strong>', '<span style="font-style:italic;">$1</span>', '<span style="text-decoration:underline;">$1</span>', '<span style="color:$1;">$2</span>', '\'$1<a href="$2$4">\'.str_slice(\'$0\',1000).\'</a>\'', ($enable_img ? '\'<img src="$1" alt="image" />\'' : '\'<a href="\'.entities(\'$1\',ENT_QUOTES).\'">\'.str_slice(\'$1\',' . $slice . ').\'</a>\''));
    $count = 0;
    return preg_replace ($search, $replace, $str);
  }

  function word_wrap ($str, $len, $break = '<wbr />')
  {
    $str = preg_replace ('#([^\\s	
\\<\\>]{' . $len . '})#ms', '$1' . $break, $str);
    $str = preg_replace ('#href=".+?"#mise', 'stripslashes(str_replace(\'' . $break . '\',\'\',\'$0\'))', $str);
    return $str;
  }

  function parse_message ($str, $wordwrap = 40, $parse_bb = true, $allow_img_tag = false)
  {
    $str = entities ($str, ENT_QUOTES);
    $str = str_replace ('	', '    ', $str);
    $str = str_replace ('  ', ' &nbsp;', $str);
    $str = nl2br ($str);
    if ($parse_bb)
    {
      $str = parse_bb ($str, $allow_img_tag, $wordwrap);
    }

    $str = word_wrap ($str, $wordwrap);
    return $str;
  }

  function record_log ($file, $str)
  {
    global $UPL;
    if (false !== $fp = @fopen ($file, 'at'))
    {
      @flock ($fp, LOCK_EX);
      @fwrite ($fp, @sprintf ('[%s User:%s IP:%s] %s
', @date ('m/d/y h:iA'), $UPL['USER']['username'], $_SERVER['REMOTE_ADDR'], $str));
      @fclose ($fp);
      return null;
    }

    exit ('record_log(): Unable to create/open files in the log directory. Check its permission');
  }

  function upload_log ($str)
  {
    record_log (LOGS_DIR . date ('Y_M_d') . '.log', $str);
  }

  function public_upload_log ($str)
  {
    record_log (LOGS_DIR . 'public_' . date ('Y_M_d') . '.log', 'Public upload: ' . $str);
  }

  function get_user_file ($userid, $file_id)
  {
    global $mysqlDB;
    $file = array ();
    $mysqlDB->query ('SELECT * FROM uploader_userfiles WHERE userid=' . $userid . ' AND file_id=' . $file_id . ' LIMIT 1');
    if ($mysqlDB->getRowCount ())
    {
      $file = $mysqlDB->getAssoc ();
      $mysqlDB->free ();
    }

    return $file;
  }

  function get_user_info ($userid)
  {
    global $mysqlDB;
    $info = array ();
    $mysqlDB->query ('SELECT * FROM uploader_users WHERE userid=' . $userid . ' LIMIT 1');
    if ($mysqlDB->getRowCount ())
    {
      $info = $mysqlDB->getAssoc ();
      $mysqlDB->free ();
    }

    return $info;
  }

  function processfile (&$file, $admin_mode = false)
  {
    global $UPL;
    $uploader_view = $UPL['SETTINGS']['uploader_view'];
    $file['size_string'] = get_size ($file['file_size'], 'B', 0);
    $file['date_string'] = get_date ('M d, Y', $file['file_date']);
    $file['url'] = $file['direct_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('files/%u_%s/%s', $file['file_id'], $file['file_key'], encodeurlraw ($file['file_name'])) : sprintf ('getfile.php?file_id=%d&amp;file_key=%s', $file['file_id'], $file['file_key']));
    if ($uploader_view)
    {
      if ($file['file_isimage'])
      {
        $file['url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('view/%u_%s', $file['file_id'], $file['file_key']) : sprintf ('view.php?file_id=%d&file_key=%s', $file['file_id'], $file['file_key']));
      }
    }

    $file['fullsize_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('view/full/%u_%s', $file['file_id'], $file['file_key']) : sprintf ('view.php?action=full&file_id=%d&file_key=%s', $file['file_id'], $file['file_key']));
    $file['download_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('files/download/%u_%s/%s', $file['file_id'], $file['file_key'], encodeurlraw ($file['file_name'])) : sprintf ('getfile.php?action=download&amp;file_id=%d&amp;file_key=%s', $file['file_id'], $file['file_key']));
    $file['square_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('thumbs/square/%d_%s/%s', $file['file_id'], $file['file_key'], encodeurlraw ($file['file_name'])) : sprintf ('getfile.php?action=thumb&amp;size=square&amp;file_id=%d&amp;file_key=%s', $file['file_id'], $file['file_key']));
    $file['small_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('thumbs/small/%d_%s/%s', $file['file_id'], $file['file_key'], encodeurlraw ($file['file_name'])) : sprintf ('getfile.php?action=thumb&amp;size=small&amp;file_id=%d&amp;file_key=%s', $file['file_id'], $file['file_key']));
    $file['large_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('thumbs/large/%d_%s/%s', $file['file_id'], $file['file_key'], encodeurlraw ($file['file_name'])) : sprintf ('getfile.php?action=thumb&amp;size=large&amp;file_id=%d&amp;file_key=%s', $file['file_id'], $file['file_key']));
    if ($admin_mode)
    {
      $file['edit_url'] = UPLOADER_URL . 'admin.php?action=edit_file&amp;userid=' . $file['userid'] . '&amp;file_id=' . $file['file_id'];
    }

  }

  function processfiles (&$files, $admin_mode = false)
  {
    $count = count ($files);
    for ($i = 0; $i < $count; ++$i)
    {
      processfile (&$files[$i], $admin_mode);
    }

  }

  function processfolder (&$folder, $public_mode = true, $admin_mode = false)
  {
    $folder_id = $folder['folder_id'];
    $folder_key = $folder['folder_key'];
    $folder['url'] = UPLOADER_URL . (MOD_REWRITE ? 'myfiles' : 'myfiles.php') . '?folder_id=' . $folder_id;
    $folder['edit_url'] = UPLOADER_URL . (MOD_REWRITE ? 'folders' : 'folders.php') . '?action=edit&amp;folder_id=' . $folder_id;
    $folder['delete_url'] = UPLOADER_URL . (MOD_REWRITE ? 'folders' : 'folders.php') . '?action=delete&amp;folder_id=' . $folder_id;
    $folder['tog_perm_url'] = UPLOADER_URL . (MOD_REWRITE ? 'folders' : 'folders.php') . '?action=togperm&amp;folder_id=' . $folder_id;
    $folder['slideshow_url'] = UPLOADER_URL . (MOD_REWRITE ? 'slideshow/' . $folder_id . ($folder_key != '' ? '_' . $folder_key : '') : 'slideshow.php?folder_id=' . $folder_id . '&amp;folder_key=' . $folder_key);
    if (!(isset ($folder['permission'])))
    {
      $folder['permission'] = get_folder_access_permission ($folder);
    }

    if ($public_mode)
    {
      $folder['browse_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('browse/%s/%u%s', encodeurlraw ($folder['username']), $folder_id, ($folder['folder_key'] == '' ? '' : '_' . $folder['folder_key'])) : 'browse.php?action=browse&amp;userid=' . $folder['userid'] . '&amp;folder_id=' . $folder_id . '&amp;folder_key=' . $folder['folder_key']);
    }

    if ($admin_mode)
    {
      $folder['url'] = UPLOADER_URL . 'admin.php?action=user_files&userid=' . $folder['userid'] . '&amp;folder_id=' . $folder_id;
      $folder['edit_url'] = UPLOADER_URL . 'admin.php?action=edit_folder&amp;userid=' . $folder['userid'] . '&amp;folder_id=' . $folder_id;
      $folder['delete_url'] = UPLOADER_URL . 'admin.php?action=delete_folder&amp;userid=' . $folder['userid'] . '&amp;folder_id=' . $folder_id;
    }

  }

  function processfolders (&$folders, $public_mode = false, $admin_mode = false)
  {
    $count = count ($folders);
    for ($i = 0; $i < $count; ++$i)
    {
      processfolder (&$folders[$i], $public_mode, $admin_mode);
    }

  }

  function processuser (&$userinfo, $admin_mode = false)
  {
    if (!(count ($userinfo)))
    {
      echo 'Error: processUser(), $userinfo is not valid.';
      return false;
    }

    $userid = $userinfo['userid'];
    $userinfo['browse_url'] = UPLOADER_URL . (MOD_REWRITE ? 'browse/' . $userinfo['username'] : 'browse.php?action=browse&amp;userid=' . $userinfo['userid']);
    $userinfo['info_url'] = UPLOADER_URL . (MOD_REWRITE ? 'info/' . $userinfo['username'] : 'browse.php?action=info&amp;userid=' . $userinfo['userid']);
    $userinfo['message_url'] = UPLOADER_URL . (MOD_REWRITE ? 'pm/' . $userinfo['username'] : 'usercp.php?action=sendpm&userid=' . $userinfo['userid']);
    $userinfo['add_contact_url'] = UPLOADER_URL . (MOD_REWRITE ? 'contacts/add/' . $userinfo['username'] : 'contacts.php?action=add&amp;userid=' . $userinfo['userid']);
    $userinfo['edit_contact_url'] = UPLOADER_URL . (MOD_REWRITE ? 'contacts/edit/' . $userinfo['username'] : 'contacts.php?action=edit&amp;userid=' . $userinfo['userid']);
    $userinfo['del_contact_url'] = UPLOADER_URL . (MOD_REWRITE ? 'contacts/delete/' . $userinfo['username'] : 'contacts.php?action=delete&amp;userid=' . $userinfo['userid']);
    if ($admin_mode)
    {
      $userinfo['email_url'] = UPLOADER_URL . 'admin.php?action=emailer&amp;to=' . rawurlencode ($userinfo['email']);
      $userinfo['edit_url'] = UPLOADER_URL . 'admin.php?action=edit_user&amp;userid=' . $userid;
      $userinfo['info_url'] = UPLOADER_URL . 'admin.php?action=user_info&amp;userid=' . $userid;
      $userinfo['prune_url'] = UPLOADER_URL . 'admin.php?action=prune_non_images&amp;userid=' . $userid;
      $userinfo['manage_files_url'] = UPLOADER_URL . 'admin.php?action=user_files&amp;userid=' . $userid;
      $userinfo['files_url'] = $userinfo['manage_files_url'];
      $userinfo['bandwidth_reset_url'] = UPLOADER_URL . 'admin.php?action=quick_edit_user_info&amp;task=bandwidth_counter&amp;userid=' . $userid;
    }

  }

  function processpublicfile (&$file, $admin_mode = false)
  {
    global $UPL;
    $uploader_view = $UPL['PUBLIC_SETTINGS']['uploader_view'];
    $file['url'] = $file['direct_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('pfiles/%u/%s', $file['file_id'], encodeurlraw ($file['file_name'])) : sprintf ('pfile.php?file_id=%d', $file['file_id']));
    if ($uploader_view)
    {
      if ($file['file_isimage'])
      {
        $file['url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('public/view/%u', $file['file_id']) : sprintf ('pview.php?file_id=%u', $file['file_id']));
      }
    }

    $file['fullsize_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('public/view/full/%u', $file['file_id']) : sprintf ('pview.php?action=full&amp;file_id=%u', $file['file_id']));
    $file['download_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('pfiles/download/%u/%s', $file['file_id'], encodeurlraw ($file['file_name'])) : sprintf ('pfile.php?action=download&amp;file_id=%d', $file['file_id']));
    $file['square_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('pthumbs/square/%d/%s', $file['file_id'], encodeurlraw ($file['file_name'])) : sprintf ('pfile.php?action=thumb&amp;size=square&amp;file_id=%d', $file['file_id']));
    $file['small_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('pthumbs/small/%d/%s', $file['file_id'], encodeurlraw ($file['file_name'])) : sprintf ('pfile.php?action=thumb&amp;size=small&amp;file_id=%d', $file['file_id']));
    $file['large_thumb_url'] = UPLOADER_URL . (MOD_REWRITE ? sprintf ('pthumbs/large/%d/%s', $file['file_id'], encodeurlraw ($file['file_name'])) : sprintf ('pfile.php?action=thumb&amp;size=large&amp;file_id=%d', $file['file_id']));
  }

  function processpublicset (&$set, $admin_mode = false)
  {
    $set['view_url'] = UPLOADER_URL . (MOD_REWRITE ? 'public/viewset/' . $set['upload_id'] : 'public.php?action=viewset&amp;upload_id=' . $set['upload_id']);
    $set['slideshow_url'] = UPLOADER_URL . (MOD_REWRITE ? 'public/slideshow/' . $set['upload_id'] : 'public.php?action=slideshow&amp;upload_id=' . $set['upload_id']);
    $set['manage_url'] = UPLOADER_URL . (MOD_REWRITE ? 'public/manage/' . $set['upload_key'] . '_' . $set['upload_id'] : 'public.php?action=manange&amp;upload_id=' . $set['upload_id'] . '&amp;upload_key=' . $set['upload_id']);
    if ($admin_mode)
    {
      $set['edit_url'] = 'admin.php?action=edit_public_set&amp;upload_id=' . $set['upload_id'];
      $set['delete_url'] = 'admin.php?action=delete_public_set&amp;upload_id=' . $set['upload_id'];
    }

  }

  function update_public_folder_status ($userid)
  {
    global $mysqlDB;
    if (!($mysqlDB->query ('SELECT COUNT(f.folder_id) AS public_folders_count FROM uploader_userfolders AS f WHERE userid=' . $userid . ' AND folder_ispublic=' . FOLDER_PUBLIC)))
    {
      exit ($mysqlDB->error (310, __FILE__));
    }

    $result = $mysqlDB->getAssoc ();
    $has_public = ($result['public_folders_count'] ? 1 : 0);
    if (!($mysqlDB->query ('UPDATE uploader_users SET fl_has_public=' . $has_public . ' WHERE userid=' . $userid)))
    {
      exit ($mysqlDB->error (313, __FILE__));
    }

  }

  function delete_file ($path)
  {
    if (is_file ($path))
    {
      @unlink ($path);
    }

    if (is_file ($path . '_small'))
    {
      @unlink ($path . '_small');
    }

    if (is_file ($path . '_large'))
    {
      @unlink ($path . '_large');
    }

    if (is_file ($path . '_square'))
    {
      @unlink ($path . '_square');
    }

  }

  function delete_public_file ($path)
  {
    if (is_file ($path))
    {
      @unlink ($path);
    }

    if (is_file ($path . '_small'))
    {
      @unlink ($path . '_small');
    }

    if (is_file ($path . '_large'))
    {
      @unlink ($path . '_large');
    }

    if (is_file ($path . '_square'))
    {
      @unlink ($path . '_square');
    }

  }

  function int2folderperm ($int)
  {
    $int & FOLDER_FRIEND_ACCESS;
    $int & FOLDER_FAMILY_ACCESS;
    return array ('friend' => (int), 'family' => (int));
  }

  function folderperm2int ($perm)
  {
    $int = 0;
    if ($perm['friend'])
    {
      $int |= FOLDER_FRIEND_ACCESS;
    }
    else
    {
      $int &= ~FOLDER_FRIEND_ACCESS;
    }

    if ($perm['family'])
    {
      $int |= FOLDER_FAMILY_ACCESS;
      return $int;
    }

    $int &= ~FOLDER_FAMILY_ACCESS;
    return $int;
  }

  function get_folder_access_permission ($folder)
  {
    if (isset ($folder['folder_permission']))
    {
      isset ($folder['folder_ispublic']);
    }

    $permission = int2folderperm ($folder['folder_permission']);
    switch ((int)$folder['folder_ispublic'])
    {
      case FOLDER_PUBLIC:
      {
        $permission['access'] = 'public';
        break;
      }

      case FOLDER_HIDDEN:
      {
        $permission['access'] = 'hidden';
        break;
      }
    }

    FOLDER_PRIVATE;
    $permission['access'] = 'private';
    $permission['public'] = $folder['folder_ispublic'] == FOLDER_PUBLIC;
    $permission['hidden'] = $folder['folder_ispublic'] == FOLDER_HIDDEN;
    $permission['private'] = $folder['folder_ispublic'] == FOLDER_PRIVATE;
    return $permission;
  }

  function compute_contact_relationship ($userid, $target_userid)
  {
    global $mysqlDB;
    $relationship = array ('is_contact' => 0, 'is_friend' => 0, 'is_family' => 0);
    $mysqlDB->query ('SELECT contact_type FROM uploader_usercontacts WHERE userid=' . $userid . ' AND contact_userid=' . $target_userid);
    if ($mysqlDB->getRowCount ())
    {
      $result = $mysqlDB->getAssoc ();
      $mysqlDB->free ();
      $contact_type = $result['contact_type'];
      $relationship['is_contact'] = 1;
      $contact_type & CONTACT_FRIEND;
      $relationship['is_friend'] = ;
      $contact_type & CONTACT_FAMILY;
      $relationship['is_family'] = ;
    }

    return $relationship;
  }

  function isbanned ($ip = '')
  {
    global $mysqlDB;
    $ip = ($ip == '' ? (isset ($_SERVER['X_FORWARDED_FOR']) ? $_SERVER['X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']) : $ip);
    $result = $mysqlDB->query2 ('SELECT * FROM uploader_banned WHERE ban_ip=' . sprintf ('%u', ip2long ($ip)) . ' LIMIT 1');
    if ($result->rowCount ())
    {
      $ban = $result->fetchAssoc ();
      return array ('public' => $ban['ban_public'], 'uploader' => $ban['ban_uploader']);
    }

    return false;
 // }
//
  // if (!((defined ('LICENSE_KEY') AND !(encode_key (get_server_key ()) != LICENSE_KEY))))
  //{
   // exit ('Invalid license key!');
  //}

?>