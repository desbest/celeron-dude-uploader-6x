<?



  function process_zip_file ($file)
  {
    set_time_limit (900);
    $zip = new Zip ();
    if (!($zip->open ($file)))
    {
      return false;
    }

    while (false !== $zip_entry = $zip->getFile ())
    {
      if (is_array ($zip_entry))
      {
        $tmp_name = tempnam (TMP_DIR, 'zip');
        $buf = $zip_entry['content'];
        $fp = @fopen ($tmp_name, 'ab');
        if (!($fp))
        {
          exit ('Could not create temporary file, upload.php on line 16');
        }

        @fwrite ($fp, $buf);
        @fclose ($fp);
        $_FILES[] = array ('name' => basename ($zip_entry['name']), 'size' => $zip_entry['size'], 'tmp_name' => $tmp_name, 'type' => 'none', 'error' => 0, 'extracted_from_zip' => 1);
        continue;
      }
    }

    $zip->close ();
    set_time_limit (0);
  }

  function validate_public_uploaded_file (&$file, &$errors, &$uploaded)
  {
    global $lang_upload;
    global $UPL;
    if (is_file ($file['tmp_name']))
    {
      @chmod ($file['tmp_name'], 511);
    }

    if ($file['error'] == 1)
    {
      $errors[] = parse ($lang_upload['upl_php_exceed'], array ('{filename}' => $file['name'], '{max_file_size}' => ini_get ('upload_max_filesize')));
      return false;
    }

    if (!($file['error'] == 4))
    {
      if ($file['size'] == 0)
      {
        if ($file['tmp_name'] == '')
        {
        }
      }
    }

    return false;
  }

  require_once 'includes/zip.class.php';
?>