<?



  function file_type ($file)
  {
    $ext = strtolower (trim (@strrchr ($file, '.'), '.'));
    $mime = array ('avi' => 'AVI Movie', 'bmp' => 'Bitmap Image', 'css' => 'Cascading Stylesheet', 'js' => 'Javascript File', 'doc' => 'Microsoft Word Document', 'exe' => 'Windows Executable File', 'gif' => 'GIF Image', 'htm' => 'HTML File', 'html' => 'HTML File', 'jpg' => 'JPEG Image', 'jpeg' => 'JPEG Image', 'mov' => 'Quicktime Movie', 'mpeg' => 'MPEG Movie', 'mpg' => 'MPEG Movie', 'mp3' => 'MP3 Audio', 'pdf' => 'Adobe Acrobat', 'php' => 'PHP Script', 'png' => 'PNG Image', 'ogg' => 'Ogg Vorbis Audio', 'ogm' => 'Ogg Vorbis Movie', 'qt' => 'Quicktime Movie', 'rar' => 'RAR Archive', 'swf' => 'Macromedia Shockwave/Flash', 'txt' => 'Text', 'torrent' => 'Bittorrent File', 'xml' => 'XML', 'xsl' => 'XSL', 'xls' => 'Microsoft Excel Sheet', 'zip' => 'ZIP Archive');
    if (isset ($mime[$ext]))
    {
      return $mime[$ext];
    }

    return 'Unknown';
  }

?>