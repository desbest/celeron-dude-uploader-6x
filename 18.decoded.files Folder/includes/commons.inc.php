<?



  function timer ($st = 0, $d = 8)
  {
    list ($m, $s) = explode (' ', microtime ());
    $t = round ((double)$s + (double)$m - $st, $d);
    if ($t < 1)
    {
      return $t * 1000 . ' ms';
    }

    return $t . ' s';
  }

  define ('DEBUG_ON', is_file ('debug'));
  require_once 'includes/license.inc.php';
  $UPL = array ();
  $UPL['RUNTIME'] = timer ();
  @error_reporting ((DEBUG_ON ? E_ALL : E_NONE));
  @ignore_user_abort (true);
  @set_magic_quotes_runtime (0);
  @ob_start ('ob_gzhandler');
  @set_time_limit (0);
  if (!(DEBUG_ON))
  {
    if (is_file ('install.php'))
    {
      exit ('Please run install.php');
    }

    if (is_file ('update.php'))
    {
      exit ('Site update in progress, please try again in a few hours.');
    }
  }

  require_once 'includes/configs.inc.php';
  require_once 'includes/constants.inc.php';
  require_once 'includes/functions_base.inc.php';
  require_once 'includes/functions_types.inc.php';
  require_once 'includes/functions_templates.inc.php';
  require_once 'includes/template.class.php';
  if (!(isset ($UPL['SETTINGS'])))
  {
    require 'includes/upl_settings.inc.php';
  }

  require_once 'includes/pub_settings.inc.php';
  require_once 'includes/mysql.class.php';
  require_once 'includes/messages.inc.php';
  if (get_magic_quotes_gpc ())
  {
    $_GET = strip_gpc ($_GET);
    $_POST = strip_gpc ($_POST);
    $_COOKIE = strip_gpc ($_COOKIE);
  }

  $demo = 0;
  $PUB = &$UPL['PUBLIC_SETTINGS'];
  define ('TPL_DIR', 'templates/' . $UPL['CONFIGS']['TEMPLATE_DIR'] . '/');
  define ('CURRENT_PAGE', basename ($_SERVER['SCRIPT_NAME']));
  define ('MOD_REWRITE', $UPL['CONFIGS']['MOD_REWRITE']);
  define ('UPLOADER_URL', $UPL['SETTINGS']['uploader_url']);
  if (!(isset ($_SERVER['HTTP_REFERER'])))
  {
    $_SERVER['HTTP_REFERER'] = '';
  }

  if (isset ($_GET['version']))
  {
    echo 'Uploader ' . UPLOADER_VERSION . '<br />Copyright Tuan Do (www.celerondude.com)';
    exit ();
  }

  $tpl_uploader = new Template (TPL_DIR . 'tpl_uploader.php');
  $tpl_message = new Template (TPL_DIR . 'tpl_message.php');
  extract ($UPL['MYSQL'], EXTR_OVERWRITE);
  $mysqlDB = new mysqlDB ($host, $username, $password, $database, (defined ('NO_PERSISTENT') ? 0 : $persistent));
  $action = gpc ('action', 'GP');
  if (is_array ($action))
  {
    $action = key ($action);
  }

  $task = gpc ('task', 'GP');
  if (is_array ($task))
  {
    $task = key ($task);
  }

  $UPL['USER'] = array ();
  $USER = &$UPL['USER'];
  $UPL['USER']['logged_in'] = 0;
  $UPL['USER']['userid'] = 0;
  $UPL['USER']['username'] = 'Guest';
  $UPL['USER']['level'] = LEVEL_NORMAL;
  $c_username = gpc ('uploader_username', 'C', 0);
  $c_password = gpc ('uploader_password', 'C', 0);
  $c_userid = gpc ('uploader_userid', 'C', 0);
  $c_session = gpc ('uploader_session', 'C', 0);
  if ($c_password !== 0)
  {
    if ($c_userid !== 0)
    {
      $c_userid = abs ((int)$c_userid);
      $mysqlDB->query (sprintf ('SELECT users.*, COUNT(messages.messageid) AS messages_count, COUNT(messages.messageid)-SUM(messages.is_read) AS unread_messages FROM uploader_users AS users LEFT JOIN uploader_messages AS messages USING(userid) WHERE users.userid=%d AND users.password=\'%s\' GROUP BY users.userid ;', $c_userid, $mysqlDB->escape ($c_password)));
      if ($mysqlDB->getRowCount ())
      {
        $UPL['USER'] = $mysqlDB->getAssoc ();
        $UPL['USER']['logged_in'] = 1;
        $mysqlDB->free ();
        if ($c_session === 0)
        {
          $mysqlDB->query (sprintf ('UPDATE uploader_users SET last_login_ip=\'%s\', last_login_time=%d WHERE userid=%d;', $_SERVER['REMOTE_ADDR'], time (), $c_userid));
          setcookie ('uploader_session', 'uploader_session', 0, '/', $UPL['CONFIGS']['COOKIE_DOMAIN'], 0);
        }
      }
    }
  }

  if ($UPL['SETTINGS']['m'])
  {
    if (!(defined ('ADMIN')))
    {
      $tpl_uploader->setr ('UPL', $UPL);
      $tpl_message->set ('message', $UPL['SETTINGS']['m_msg']);
      $tpl_uploader->set ('content', $tpl_message, 1);
      exit ();
    }
  }

  if (!(defined ('NO_AUTH_CHECK')))
  {
    $err = 'none';
    if (!($UPL['USER']['logged_in']))
    {
      $err = parse ($lang_commons['not_logged_in'], array ('{login_url}' => UPLOADER_URL . (MOD_REWRITE ? 'login?' : 'account.php?action=login&amp;') . 'return_url=' . rawurlencode (current_page ()), '{register_url}' => UPLOADER_URL . (MOD_REWRITE ? 'register' : 'account.php?action=register')));
    }
    else
    {
      if ($UPL['USER']['level'] != LEVEL_ADMIN)
      {
        if ($UPL['USER']['level'] != LEVEL_MODERATOR)
        {
          if ($UPL['USER']['is_suspended'])
          {
            $err = $lang_commons['account_suspended'];
          }
          else
          {
            if (!($UPL['USER']['is_activated']))
            {
              $err = $lang_commons['account_not_activated'];
            }
            else
            {
              if (!($UPL['USER']['is_approved']))
              {
                $err = $lang_commons['account_not_approved'];
              }
            }
          }
        }
      }
    }

    if ($err != 'none')
    {
      if (isset ($_GET['ajax']))
      {
        echo json_encode (array ('result' => 'failed', 'message' => 'You have logged out. Please login to perform this action.'));
        exit ();
      }

      $tpl_message->set ('message', $err);
      $tpl_uploader->setr ('content', $tpl_message, 1);
      exit ();
    }
  }

  $tpl_uploader->setr ('UPL', $UPL);
/*  if (!((defined ('LICENSE_KEY') AND !(encode_key (get_server_key ()) != LICENSE_KEY))))
  {
    exit ('Invalid license key!');
  } 
*/

?>