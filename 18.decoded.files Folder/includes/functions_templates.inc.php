<?


  function is_browse_enabled ()
  {
    global $UPL;
    if ($UPL['SETTINGS']['browsing'] == 'none')
    {
      return false;
    }

    if ($UPL['SETTINGS']['browsing'] == 'reg')
    {
      if (!($UPL['USER']['logged_in']))
      {
        return false;
      }
    }

    return true;
  }

?>