<?



  define ('UPLOADER_VERSION', '6.3.0');
  define ('SECURITY_ERROR', 'Illegal action!');
  define ('ERROR', 'Fatal error encountered!');
  define ('PHP_EXT', '.php');
  define ('TMP_DIR', 'temp/');
  define ('DATA_DIR', 'data/');
  define ('LOGS_DIR', 'data/logs/');
  define ('USERDATA_DIR', 'data/users/');
  define ('UPLSETTINGS_DIR', 'data/settings/');
  define ('PUBLIC_DATA_DIR', 'data/public/');
  define ('UPLOADER_SETTINGS', 'data/settings/upl_settings.php');
  define ('USER_SETTINGS', 'data/settings/usr_settings.php');
  define ('PUBLIC_SETTINGS', 'data/settings/pub_settings.php');
  define ('ANNOUNCEMENT_FILE', 'data/settings/announcements.php');
  define ('EMAIL_TEMPLATES', 'data/settings/email_templates.php');
  define ('LEVEL_YEARLY', 7);
  define ('LEVEL_QUARTERLY', 6);
  define ('LEVEL_MONTHLY', 5);
  define ('LEVEL_DONATOR', 4);
  define ('LEVEL_SUBSCRIBER', 3);
  define ('LEVEL_ADMIN', 2);
  define ('LEVEL_MODERATOR', 1);
  define ('LEVEL_NORMAL', 0);
  define ('MAX_USERNAME_LEN', 64);
  define ('MIN_USERNAME_LEN', 4);
  define ('FOLDER_PRIVATE', 0);
  define ('FOLDER_PUBLIC', 1);
  define ('FOLDER_HIDDEN', 2);
  define ('FOLDER_FRIEND_ACCESS', 1);
  define ('FOLDER_FAMILY_ACCESS', 2);
  define ('CONTACT_FRIEND', 1);
  define ('CONTACT_FAMILY', 2);
  define ('COMMENT_FILE', 0);
  define ('COMMENT_ANNOUNCEMENT', 1);
?>